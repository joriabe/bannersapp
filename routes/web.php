<?php

//use Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//forzar el uso de https
if (env('APP_ENV') === 'production') {
    \URL::forceScheme('https'); 
}

Route::get('/', function () {
    return view('welcome');
});

Route::get('/suscripcion', function () {
    return view('suscripcion');
});



//Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
//Route::post('/register', 'Auth\RegisterController@register');

Route::get('/home', 'HomeController@index')->name('home');

//controlador de prueba, para recibir los datos de factura, cambiar por la correcta
Route::post('/posd','CompaniasController@test');

//Rutas de creaacion de usuarios
Auth::routes();


//*********************************************/
//Rutas que exigen la autenticacion del usuario
//********************************************/

//**********CIA*****************************/

//rutas cias
//Route::resource('cias', 'CiasController');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('cias','CiasController@store')->name('cias.store')->middleware('permission:cias.create');

	Route::get('cias','CiasController@index')->name('cias.index')->middleware('permission:cias.index');

	Route::get('cias/create','CiasController@create')->name('cias.create')->middleware('permission:cias.create');

	Route::put('cias/{id}','CiasController@update')->name('cias.update')->middleware('permission:cias.edit');

	Route::get('cias/{id}/edit','CiasController@edit')->name('cias.edit')->middleware('permission:cias.edit');


	Route::delete('cias/{id}','CiasController@destroy')->name('cias.destroy')->middleware('permission:cias.delete');

	//Route::get('cias/{id}/edit','CiasController@edit')->name('cias.edit')->middleware('permission:cias.edit');
});

//**********AGENCIA*****************************/

//rutas agencias
//Route::resource('agencias', 'AgenciasController');
//Route::get('getagencias/{id}','AgenciasController@getagencias');

Route::middleware('auth')->group(function(){
	Route::post('agencias','AgenciasController@store')->name('agencias.store')->middleware('permission:agencias.create');

	Route::get('agencias/create','AgenciasController@create')->name('agencias.create')->middleware('permission:agencias.create');

	Route::put('agencias/{id}','AgenciasController@update')->name('agencias.update')->middleware('permission:agencias.edit');

	Route::get('agencias/{id}/edit','AgenciasController@edit')->name('agencias.edit')->middleware('permission:agencias.edit');

	Route::delete('agencias/{id}','AgenciasController@destroy')->name('agencias.destroy')->middleware('permission:agencias.delete');

	Route::get('getagencias/{id}','AgenciasController@getagencias');

	//Route::get('agencias/{id}/edit','AgenciasController@edit')->name('agencias.edit')->middleware('permission:agencias.edit');
});


//**********CLIENTES*****************************/

/*ruta resource para el ClienteController
Route::resource("clientes", 'ClienteController');
Route::get('/clientes/delete/{id}','ClienteController@delete');*/
Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('clientes','ClienteController@store')->name('clientes.store')->middleware('permission:clientes.create');

	Route::get('clientes','ClienteController@index')->name('clientes.index')->middleware('permission:clientes.index');

	Route::get('clientes/create','ClienteController@create')->name('clientes.create')->middleware('permission:clientes.create');

	Route::put('clientes/{id}','ClienteController@update')->name('clientes.update')->middleware('permission:clientes.edit');

	Route::get('clientes/delete/{id}','ClienteController@delete')->name('clientes.delete')->middleware('permission:clientes.delete');

	//Route::delete('clientes/{id}','ClienteController@destroy')->name('clientes.destroy')->middleware('permission:clientes.delete');

	Route::get('clientes/{id}/edit','ClienteController@edit')->name('clientes.edit')->middleware('permission:clientes.edit');
});

//**********PRUDCTOS*****************************/

/*ruta resource para el ProductoController
Route::resource("productos", 'ProductoController');
Route::get('/productos/delete/{id}','ProductoController@delete');*/
Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('productos','ProductoController@store')->name('productos.store')->middleware('permission:productos.create');

	Route::get('productos','ProductoController@index')->name('productos.index')->middleware('permission:productos.index');

	Route::get('productos/create','ProductoController@create')->name('productos.create')->middleware('permission:productos.create');

	Route::put('productos/{id}','ProductoController@update')->name('productos.update')->middleware('permission:productos.edit');

	Route::get('productos/delete/{id}','ProductoController@delete')->name('productos.delete')->middleware('permission:productos.delete');

	//Route::delete('productos/{id}','ProductoController@destroy')->name('productos.destroy')->middleware('permission:productos.delete');

	Route::get('productos/{id}/edit','ProductoController@edit')->name('productos.edit')->middleware('permission:productos.edit');
});

//**********CATEGORIAS*****************************/

//ruta resource para el CategoriaController
//Route::resource("categorias", 'CategoriaController');
//Route::get('/categorias/delete/{id}','CategoriaController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('categorias','CategoriaController@store')->name('categorias.store')->middleware('permission:categorias.create');

	Route::get('categorias','CategoriaController@index')->name('categorias.index')->middleware('permission:categorias.index');

	Route::get('categorias/create','CategoriaController@create')->name('categorias.create')->middleware('permission:categorias.create');

	Route::put('categorias/{id}','CategoriaController@update')->name('categorias.update')->middleware('permission:categorias.edit');

	Route::get('categorias/delete/{id}','CategoriaController@delete')->name('categorias.delete')->middleware('permission:categorias.delete');

	//Route::delete('categorias/{id}','CategoriaController@destroy')->name('categorias.destroy')->middleware('permission:categorias.delete');

	Route::get('categorias/{id}/edit','CategoriaController@edit')->name('categorias.edit')->middleware('permission:categorias.edit');
});

//**********IMPUESTOS*****************************/

//ruta resource para el ImpuestoController
//Route::resource("impuestos", 'ImpuestoController');
//Route::get('/impuestos/delete/{id}','ImpuestoController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('impuestos','ImpuestoController@store')->name('impuestos.store')->middleware('permission:impuestos.create');

	Route::get('impuestos','ImpuestoController@index')->name('impuestos.index')->middleware('permission:impuestos.index');

	Route::get('impuestos/create','ImpuestoController@create')->name('impuestos.create')->middleware('permission:impuestos.create');

	Route::put('impuestos/{id}','ImpuestoController@update')->name('impuestos.update')->middleware('permission:impuestos.edit');

	Route::get('impuestos/delete/{id}','ImpuestoController@delete')->name('impuestos.delete')->middleware('permission:impuestos.delete');

	//Route::delete('impuestos/{id}','ImpuestoController@destroy')->name('impuestos.destroy')->middleware('permission:impuestos.delete');

	Route::get('impuestos/{id}/edit','ImpuestoController@edit')->name('impuestos.edit')->middleware('permission:impuestos.edit');
});

//**********UNIDADES*****************************/

//ruta resource para el UnidadController
//Route::resource("unidades", 'UnidadController');
//Route::get('/unidades/delete/{id}','UnidadController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('unidades','UnidadController@store')->name('unidades.store')->middleware('permission:unidades.create');

	Route::get('unidades','UnidadController@index')->name('unidades.index')->middleware('permission:unidades.index');

	Route::get('unidades/create','UnidadController@create')->name('unidades.create')->middleware('permission:unidades.create');

	Route::put('unidades/{id}','UnidadController@update')->name('unidades.update')->middleware('permission:unidades.edit');

	Route::get('unidades/delete/{id}','UnidadController@delete')->name('unidades.delete')->middleware('permission:unidades.delete');

	//Route::delete('unidades/{id}','UnidadController@destroy')->name('unidades.destroy')->middleware('permission:unidades.delete');

	Route::get('unidades/{id}/edit','UnidadController@edit')->name('unidades.edit')->middleware('permission:unidades.edit');
});

//**********TIPO PAGO*****************************/

//ruta resource para el TipoPagoController
//Route::resource("tipopagos", 'TipoPagoController');
//Route::get('/tipopagos/delete/{id}','TipoPagoController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('tipopagos','TipoPagoController@store')->name('tipopagos.store')->middleware('permission:tipopagos.create');

	Route::get('tipopagos','TipoPagoController@index')->name('tipopagos.index')->middleware('permission:tipopagos.index');

	Route::get('tipopagos/create','TipoPagoController@create')->name('tipopagos.create')->middleware('permission:tipopagos.create');

	Route::put('tipopagos/{id}','TipoPagoController@update')->name('tipopagos.update')->middleware('permission:tipopagos.edit');

	Route::get('tipopagos/delete/{id}','TipoPagoController@delete')->name('tipopagos.delete')->middleware('permission:tipopagos.delete');

	//Route::delete('tipopagos/{id}','TipoPagoController@destroy')->name('tipopagos.destroy')->middleware('permission:tipopagos.delete');

	Route::get('tipopagos/{id}/edit','TipoPagoController@edit')->name('tipopagos.edit')->middleware('permission:tipopagos.edit');
});

//**********DATOS FACTURAS*****************************/

//ruta resource para el DatoFacturaController
//Route::resource("datofacturas", 'DatoFacturaController');
//Route::post('/datofacturas/crearoactualizar','DatoFacturaController@CrearoActualizar');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('datofacturas','DatoFacturaController@store')->name('datofacturas.store')->middleware('permission:datofacturas.create');

	Route::get('datofacturas','DatoFacturaController@index')->name('datofacturas.index')->middleware('permission:datofacturas.index');

	Route::post('/datofacturas/crearoactualizar','DatoFacturaController@CrearoActualizar')->name('datofacturas.CrearoActualizar')->middleware('permission:datofacturas.create');

	Route::get('datofacturas/create','DatoFacturaController@create')->name('datofacturas.create')->middleware('permission:datofacturas.create');

	Route::put('datofacturas/{id}','DatoFacturaController@update')->name('datofacturas.update')->middleware('permission:datofacturas.edit');

	Route::get('datofacturas/delete/{id}','DatoFacturaController@delete')->name('datofacturas.delete')->middleware('permission:datofacturas.delete');

	//Route::delete('datofacturas/{id}','DatoFacturaController@destroy')->name('datofacturas.destroy')->middleware('permission:datofacturas.delete');

	Route::get('datofacturas/{id}/edit','DatoFacturaController@edit')->name('datofacturas.edit')->middleware('permission:datofacturas.edit');

});

//**********LOG*****************************/

//ruta resource para el DatoFacturaController
//Route::resource("logs", 'LogController')->only(['index']);

Route::middleware(['auth', 'suscrito'])->group(function(){

	Route::get('logs','LogController@index')->name('logs.index')->middleware('permission:logs.index');

});

//**********FLUJO*****************************/

Route::middleware(['auth', 'suscrito'])->group(function(){

	Route::get('flujos','FlujoController@index')->name('flujos.index')->middleware('permission:flujos.index');
	Route::post('flujos','FlujoController@store')->name('flujos.store')->middleware('permission:flujos.create');

	Route::get('flujos/create','FlujoController@create')->name('flujos.create')->middleware('permission:flujos.create');
});

//**********CAJA*****************************/

//ruta resource para el CajaController
//Route::resource("cajas", 'CajaController');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('cajas','CajaController@store')->name('cajas.store')->middleware('permission:cajas.create');

	Route::get('cajas','CajaController@index')->name('cajas.index')->middleware('permission:cajas.index');

	Route::get('cajas/create','CajaController@create')->name('cajas.create')->middleware(['permission:cajas.create','cajaAbierta','autorizacionFacturacion','sinDescuentoDescuentoVencido']);

	Route::put('cajas/{id}','CajaController@update')->name('cajas.update')->middleware('permission:cajas.edit');

	Route::get('cajas/delete/{id}','CajaController@delete')->name('cajas.delete')->middleware('permission:cajas.delete');

	//Route::delete('cajas/{id}','CajaController@destroy')->name('cajas.destroy')->middleware('permission:cajas.delete');

	Route::get('cajas/{id}/edit','CajaController@edit')->name('cajas.edit')->middleware('permission:cajas.edit');
});

//**********USERS*****************************/

//ruta resource para el UserController
//Route::resource("users", 'UserController');
//Route::get('/users/delete/{id}','UserController@delete');
//Route::get('/users/activar/{id}','UserController@activar');


Route::middleware('auth')->group(function(){

	Route::get('users','UserController@index')->name('users.index')->middleware('permission:users.index');

	Route::put('users/{id}','UserController@update')->name('users.update')->middleware('permission:users.edit');

	Route::get('users/delete/{id}','UserController@delete')->name('users.delete')->middleware('permission:users.delete');

	Route::get('users/activar/{id}','UserController@activar')->name('users.activar')->middleware('permission:users.delete');

	//Route::delete('users/{id}','UserController@destroy')->name('users.destroy')->middleware('permission:users.delete');

	Route::get('users/{id}/edit','UserController@edit')->name('users.edit')->middleware('permission:users.edit');
	
	// Registration Routes...
	Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('permission:users.create');
	Route::post('register', 'Auth\RegisterController@register')->middleware('permission:users.create');
});

//**********ROLES*****************************/

//ruta resource para el RoleController
//Route::resource("roles", 'RoleController');

Route::middleware('auth')->group(function(){
	Route::post('roles','RoleController@store')->name('roles.store')->middleware('permission:roles.create');

	Route::get('roles','RoleController@index')->name('roles.index')->middleware('permission:roles.index');

	Route::get('roles/create','RoleController@create')->name('roles.create')->middleware('permission:roles.create');

	Route::put('roles/{id}','RoleController@update')->name('roles.update')->middleware('permission:roles.edit');

	Route::get('roles/{id}/edit','RoleController@edit')->name('roles.edit')->middleware('permission:roles.edit');
});

//**********FACTURA*****************************/

//ruta resource para el FacturaController
//Route::resource("facturas", 'FacturaController');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('facturas','FacturaController@store')->name('facturas.store')->middleware(['permission:facturas.create','autorizacionFacturacion']);

	Route::get('facturas','FacturaController@index')->name('facturas.index')->middleware('permission:facturas.index');

	Route::get('facturas/create','FacturaController@create')->name('facturas.create')->middleware('permission:facturas.create');

	Route::get('facturas/{id}','FacturaController@show')->name('facturas.show')->middleware('permission:facturas.show');

	Route::put('facturas/{id}','FacturaController@update')->name('facturas.update')->middleware('permission:facturas.edit');

	Route::get('facturas/delete/{id}','FacturaController@delete')->name('facturas.delete')->middleware('permission:facturas.delete');

	Route::get('facturas/{id}/edit','FacturaController@edit')->name('facturas.edit')->middleware('permission:facturas.edit');
});

//**********POS*****************************/

//Route::resource('pos','Pos\PosProductosController');

Route::middleware(['auth', 'suscrito'])->group(function(){

	Route::get('pos','Pos\PosProductosController@index')->name('pos.index')->middleware(['permission:facturas.create','caja', 'autorizacionFacturacion','sinDescuentoDescuentoVencido']);

	Route::get('pos/{id}','Pos\PosProductosController@show')->name('pos.show')->middleware('permission:facturas.create');

	Route::get('pos/{id}/edit','Pos\PosProductosController@edit')->name('pos.edit')->middleware('permission:facturas.create');
});


//**********POS OTRAS RUTAS*****************************/
Route::middleware(['auth', 'suscrito'])->group(function(){

//Vista y obtencion de los productos y servicios
Route::resource('pos_fac','Pos\PosFacturaController')->only(['show']);
//Obtencion de la lista de clientes
Route::get('/posd/clientes','Pos\DigitadorController@getClientes');
//Obtencion de la lista de tipos de pagos
Route::get('/posd/tipopago','Pos\DigitadorController@getTipoPago');
//Obtencion de los productos d ela cotizacion
Route::get('/posd/cotizacion/{id}','Pos\DigitadorController@getCotizacion');
});


//**********PROVEEDORES*****************************/

//Route::resource("proveedores", 'ProveedorController');
//Route::get('/proveedores/delete/{id}','ProveedorController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('proveedores','ProveedorController@store')->name('proveedores.store')->middleware('permission:proveedores.create');

	Route::get('proveedores','ProveedorController@index')->name('proveedores.index')->middleware('permission:proveedores.index');

	Route::get('proveedores/create','ProveedorController@create')->name('proveedores.create')->middleware('permission:proveedores.create');

	Route::get('proveedores/{id}','ProveedorController@show')->name('proveedores.show')->middleware('permission:proveedores.show');

	Route::put('proveedores/{id}','ProveedorController@update')->name('proveedores.update')->middleware('permission:proveedores.edit');

	Route::get('proveedores/delete/{id}','ProveedorController@delete')->name('proveedores.delete')->middleware('permission:proveedores.delete');

	Route::get('proveedores/{id}/edit','ProveedorController@edit')->name('proveedores.edit')->middleware('permission:proveedores.edit');
});


//**********DECLARACION DE COMPRAS*****************************/

//Route::resource("declaracioncompras", 'DeclaracionCompraController');
//Route::get('/declaracioncompras/delete/{id}','DeclaracionCompraController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('declaracioncompras','DeclaracionCompraController@store')->name('declaracioncompras.store')->middleware('permission:declaracioncompras.create');

	Route::get('declaracioncompras','DeclaracionCompraController@index')->name('declaracioncompras.index')->middleware('permission:declaracioncompras.index');

	Route::get('declaracioncompras/create','DeclaracionCompraController@create')->name('declaracioncompras.create')->middleware('permission:declaracioncompras.create');

	Route::get('declaracioncompras/{id}','DeclaracionCompraController@show')->name('declaracioncompras.show')->middleware('permission:declaracioncompras.show');

	Route::put('declaracioncompras/{id}','DeclaracionCompraController@update')->name('declaracioncompras.update')->middleware('permission:declaracioncompras.edit');

	Route::get('declaracioncompras/delete/{id}','DeclaracionCompraController@delete')->name('declaracioncompras.delete')->middleware('permission:declaracioncompras.delete');

	Route::get('declaracioncompras/{id}/edit','DeclaracionCompraController@edit')->name('declaracioncompras.edit')->middleware('permission:declaracioncompras.edit');
});

//**********CXC*****************************/

//Route::resource("cxcs", 'CxCController');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('cxcs','CxCController@store')->name('cxcs.store')->middleware('permission:cxcs.create');

	Route::get('cxcs','CxCController@index')->name('cxcs.index')->middleware('permission:cxcs.index');

	Route::get('cxcs/create','CxCController@create')->name('cxcs.create')->middleware('permission:cxcs.create');

	Route::get('cxcs/{id}','CxCController@show')->name('cxcs.show')->middleware('permission:cxcs.show');

	Route::put('cxcs/{id}','CxCController@update')->name('cxcs.update')->middleware('permission:cxcs.edit');

	Route::get('cxcs/{id}/edit','CxCController@edit')->name('cxcs.edit')->middleware('permission:cxcs.edit');
});


//**********DESCUENTO*****************************/

//Route::resource("descuentos", 'DescuentoController');
//Route::get('/descuentos/delete/{id}','DescuentoController@delete');
//Route::get('descuentos/activar/{id}','DescuentoController@activar');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('descuentos','DescuentoController@store')->name('descuentos.store')->middleware('permission:descuentos.create');

	Route::get('descuentos','DescuentoController@index')->name('descuentos.index')->middleware('permission:descuentos.index');

	Route::get('descuentos/create','DescuentoController@create')->name('descuentos.create')->middleware('permission:descuentos.create');

	Route::put('descuentos/{id}','DescuentoController@update')->name('descuentos.update')->middleware('permission:descuentos.edit');

	Route::get('descuentos/{id}/edit','DescuentoController@edit')->name('descuentos.edit')->middleware('permission:descuentos.edit');

	Route::get('/descuentos/delete/{id}','DescuentoController@delete')->name('descuentos.delete')->middleware('permission:descuentos.delete');
	Route::get('descuentos/activar/{id}','DescuentoController@activar')->name('descuentos.activar')->middleware('permission:descuentos.delete');
	Route::get('descuento/amasiva','DescuentoController@getAmasiva')->name('descuento.amasiva')->middleware('permission:descuentos.edit');

	Route::post('descuentos/amasiva','DescuentoController@postAmasiva');
    Route::get('descuento/amasiva/{producto}','DescuentoController@getProductos');

});


route::get('/opciones',function () {
    return view('layouts.opciones');
});

//********** REPORTES *****************************/
//Route::get('/reportesisv','ReportesController@getReporteView');
//Route::post('/dcomprasexcel','ReportesController@DeclaComprasExport');
//Route::post('/dventasexcel','ReportesController@DeclaVentasExport');
//Route::post('/facturasexcel','ReportesController@FacturasExport');
//Route::post('/flujosexcel','ReportesController@FlujosExport');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::get('/reportesisv','ReportesController@getReporteView')->middleware('permission:rep.declaraciones');

	Route::post('/dcomprasexcel','ReportesController@DeclaComprasExport')->middleware('permission:rep.declaraciones');

	Route::post('/dventasexcel','ReportesController@DeclaVentasExport')->middleware('permission:rep.declaraciones');

	Route::post('/facturasexcel','ReportesController@FacturasExport')->middleware('permission:rep.facturasexcel');

	Route::post('/flujosexcel','ReportesController@FlujosExport')->middleware('permission:rep.flujosexcel');

});

/************SEGUNDA PARTE**************************************/

//**********DECLARACION DE COMPRAS*****************************/

//Route::resource("cotizaciones", 'CotizacionController');
//Route::get('/cotizaciones/delete/{id}','CotizacionController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('cotizaciones','CotizacionController@store')->name('cotizaciones.store');

	Route::get('cotizaciones','CotizacionController@index')->name('cotizaciones.index')->middleware(['permission:cotizaciones.index','deleteCotizacionesVencidas']);

	Route::get('cotizaciones/delete/{id}','CotizacionController@delete')->name('cotizaciones.delete')->middleware('permission:cotizaciones.delete');

	Route::get('cotizaciones/{id}','CotizacionController@show')->name('cotizaciones.show')->middleware('permission:cotizaciones.show');
});

//**********DECLARACION DE OTRAS COMPRAS*****************************/

//Route::resource("declaracioncompras", 'DeclaracionCompraController');
//Route::get('/declaracioncompras/delete/{id}','DeclaracionCompraController@delete');

Route::middleware(['auth', 'suscrito'])->group(function(){
	Route::post('declaracionotrascompras','OtraCompraController@store')->name('declaracionotrascompras.store')->middleware('permission:declaracionotrascompras.create');

	Route::get('declaracionotrascompras','OtraCompraController@index')->name('declaracionotrascompras.index')->middleware('permission:declaracionotrascompras.index');

	Route::get('declaracionotrascompras/create','OtraCompraController@create')->name('declaracionotrascompras.create')->middleware('permission:declaracionotrascompras.create');

	Route::get('declaracionotrascompras/{id}','OtraCompraController@show')->name('declaracionotrascompras.show')->middleware('permission:declaracionotrascompras.show');

	Route::put('declaracionotrascompras/{id}','OtraCompraController@update')->name('declaracionotrascompras.update')->middleware('permission:declaracionotrascompras.edit');

	Route::get('declaracionotrascompras/delete/{id}','OtraCompraController@delete')->name('declaracionotrascompras.delete')->middleware('permission:declaracionotrascompras.delete');

	Route::get('declaracionotrascompras/{id}/edit','OtraCompraController@edit')->name('declaracionotrascompras.edit')->middleware('permission:declaracionotrascompras.edit');
});

/**********************TERCERA PARTE**********************************/

//Route::resource("cxps", 'CxPController');
Route::middleware(['auth', 'suscrito'])->group(function(){

	Route::get('cxps','CxPController@index')->name('cxps.index')->middleware('permission:cxps.index');

	Route::get('cxps/{id}','CxPController@show')->name('cxps.show')->middleware('permission:cxps.show');

	Route::put('cxps/{id}','CxPController@update')->name('cxps.update')->middleware('permission:cxps.edit');

	Route::get('cxps/{id}/edit','CxPController@edit')->name('cxps.edit')->middleware('permission:cxps.edit');
});

//rutas para el proceso de pago
Route::middleware(['auth'])->group(function(){

	Route::get('/subscribe/paypal', 'PaypalController@paypalRedirect')->name('paypal.redirect');
	Route::get('/subscribe/paypal/return', 'PaypalController@paypalReturn')->name('paypal.return');
	Route::get('/subscribe/paypal/cancel/{id}', 'PaypalController@cancelSuscription')->name('paypal.cancel')->middleware('permission:suscripcion.edit');

	//ruta para obtener los codigos de los planes
	Route::post('create_paypal_plan', 'PaypalController@create_plan')->middleware('permission:access.superadmin');

	//ruta para show subscripcion
	Route::get('/suscripcion','SuscripcionController@showSupscripcion')->name('suscripcion.show')->middleware('permission:suscripcion.edit');

	//ruta para el index de las suscripciones
	Route::get('suscripcion/index','SuscripcionController@index')->name('suscripcion.index')->middleware('permission:access.superadmin');

	Route::get('suscripcion/showfull','SuscripcionController@showFull')->name('suscripcion.showFull')->middleware('permission:access.superadmin');

	Route::get('/suscripcion/obtener', function () {
	    return view('suscripcion');
	});

	Route::get('/plan/create', function () {
	    return view('suscripcion.createplan');
	})->middleware('permission:access.superadmin');
});

//rutas para los wbhook
Route::post('paypal/webhook', 'PaypalController@webhook');

//rutas para envio de email
Route::post('/mailinfo','MailingController@mailinginfo')->name('mailing.info');
Route::post('/mailcomercial','MailingController@mailingcomercial')->name('mailing.comercial');







