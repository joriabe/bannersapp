@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	

<div class="row">
	<div class="card border-primary col-md-8 offset-md-2 text-center " style="width: 18rem;">
	  <div class="card-body">
	    <h5 class="card-title">Declaracion Mensual de Compras</h5>
	    <h6 class="card-subtitle mb-2 text-muted">Formato Excel</h6>
	    <p class="card-text">Seleccione el mes del reporte, para decargar el archivo de excel correspondiente.</p>
	   
		    <form action="{{ url('dcomprasexcel') }}"  method="POST" name="form1" id="form1">
				{{ csrf_field() }}
				
					<div class="form-group col-md-6 offset-md-3">
					    <label for="input_mes">Mes a Reportar: </label>
					    <input class="form-control" type="month" name="input_mes" id="input_mes">
					 </div>

					<button type="submit" class="btn btn-primary">
						<span data-feather="download"></span>
						Descargar
					</button>
				
			</form>
		
	  </div>
	</div>
</div>
<br>
<div class="row">
	<div class="card border-success col-md-8 offset-md-2 text-center " style="width: 18rem;">
	  <div class="card-body">
	    <h5 class="card-title">Declaracion Mensual de Ventas</h5>
	    <h6 class="card-subtitle mb-2 text-muted">Formato Excel</h6>
	    <p class="card-text">Seleccione el mes del reporte, para decargar el archivo de excel correspondiente.</p>
	   
		    <form action="{{ url('dventasexcel') }}"  method="POST" name="form1" id="form1">
				{{ csrf_field() }}
				
					<div class="form-group col-md-6 offset-md-3">
					    <label for="input_mes">Mes a Reportar: </label>
					    <input class="form-control" type="month" name="input_mes" id="input_mes">
					 </div>

					<button type="submit" class="btn btn-success">
						<span data-feather="download"></span>
						Descargar
					</button>
				
			</form>
		
	  </div>
	</div>
</div>
@endsection