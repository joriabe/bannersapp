<div id="f_cont" style="overflow-y: scroll;" class="row container-fluid w-100 h-100">
  <div id="facturas" class="w-100 justify-content-end">
  	
  </div> 
  <div id="total_factura" class="w-100 justify-content-end">
  	<table class="table table-sm" >
  		<tr class="table-info">
  			<th>Sub-Total:</th>
  			<td class="text-rigth"><span id="v_total"></td>
        <th>Descuentos:</th>
        <td class="text-rigth"><span id="v_desto"></td>
  		</tr>
  		<tr class="table-primary">
  			<th>Impuestos:</th>
  			<td class="text-rigth"><span id="v_impuestos"></td>
        <th>Total:</th>
        <td class="text-rigth"><b><span id="v_gtotal"></b></span></td>
  		</tr>
  	</table>
  </div>
  <!-- lo usa lector_cbarras para tenes acceso a la url de productos-->
  <p id="prod-url" hidden>{{url('pos/')}}</p>
  <!-- lo usa campo cotizaciones para tenes acceso a los productos-->
  <p id="coti-url" hidden>{{url('/posd/cotizacion')}}</p>
</div>
	

