@include('pos.navbar_productos')
<div class="tab-content" id="myTabContent">
  <div class="row">
    <div class="col-md-7">
      <input class="form-control form-control-sm" type="text" id="cod_barra" placeholder="Codigo de Producto" onkeyup="lector_cbarras(event);">
    </div>
    <div class="col-md-5">
       <input class="form-control form-control-sm" type="text" id="cod_coti" placeholder="Numero de Cotizacion" onkeyup="lector_coti(event);">
    </div>    
  </div>  
 
	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  		<div class="row">
  		@foreach($categorias as $categoria)
  			<div class="col-md-4">
  				<div class="b-categorias">
  					<a  class="btn btn-outline-success btn-lg btn-block" href="#" role="button" onclick="cargarProductos( {{$categoria->id}}, '{{ $categoria->categoria }}', '{{url('pos/')}}' );">{{ $categoria->categoria }}</a>	
  				</div>  				
  			</div>
  		@endforeach
  		</div>
	</div> 
</div>

@section('scrips')
<script src="{{url('js/pos/pos_productos.js')}}" language="javascript" type="text/javascript"></script>
<script src="{{url('js/pos/pos_factura.js')}}" language="javascript" type="text/javascript"></script>
<script src="{{url('js/pos/pos_cotizacion.js')}}" language="javascript" type="text/javascript"></script>
<script type="text/javascript" src="{{url('js/pos/epos-2.9.0.js')}}" language="javascript"></script>
<script type="text/javascript" src="{{url('js/pos/pos_epson_print.js')}}" language="javascript"></script>
@endsection