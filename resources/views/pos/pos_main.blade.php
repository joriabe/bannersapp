
<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>POS</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/pos_main.css')}}" rel="stylesheet">
    <!-- Custom styles for Selects with searchs option -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="{{ url('/home') }}">POS</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="col-md-6">
          @if(null!==session('caja'))
            <a class="navbar-brand" href="{{ route('cajas.edit', ['id' => session('caja')]) }}">Cerrar Caja</a>
          @else
            <a class="navbar-brand" href="{{ route('cajas.create') }}">Abrir Caja</a>
          @endif
            
            <a class="navbar-brand" href="" data-toggle="modal" data-target="#flujoModal">Nuevo Flujo</a>
    
        </div>
        
        <div class="col-md-6">
          <ul class="navbar-nav mr-auto justify-content-end">
            <li class="nav-item active">
              <a class="navbar-brand" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>              
            </li>
          </ul>
        </div>

      </div>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </nav>

    <main role="main" class="row no-gutters container-fluid h-100" >      
        <div style="margin-right: -10px;" class="h-100 col-md-5">
          <div style="margin-bottom: -10px" class="no-gutters container-fluid col-md-12 dfactura">
            @include('pos.factura')
          </div>
          <div style="margin-top: -10px" class="no-gutters container-fluid col-md-12 teclado">
            @include('pos.digitador')
          </div>       
        </div>
        <div style="margin-left: -10px;" class="h-100 col-md-7">
          @include('pos.productos')
        </div>      
      
    </main>

    @include('flujos.modal')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{url('js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/popper.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <!-- Custom JS for Selects with searchs option -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    @yield('scrips')
  </body>
</html>
