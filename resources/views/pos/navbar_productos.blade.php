<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="nav nav-tabs mr-auto" id="myTab" role="tablist">
      <li class="nav-item" id="tab-cat">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Categorias</a>
      </li>
  
    </ul>
    <form class="form-inline my-2 my-lg-0" onsubmit="buscarProducto('{{url('pos/')}}'); return false;" >
      <input class="form-control form-control-sm mr-sm-2" type="text" placeholder="Buscar" id="i_buscar">
    </form>
  </div>
</nav>
