<meta name="csrf-token" content="{{ csrf_token() }}" />
<br>
<!-- INICIO DEL TECLADO -->
<div class="btn-group col-md-10 offset-md-1" style="margin-top: -6px;" role="group" aria-label="Basic example">
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(7)">7</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(8)">8</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(9)">9</button>
  <button style="width: 35%" type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#clienteModal" title="Añadir Nuevo Cliente">+ Cliente</button>
</div>
<!-- Boton agregar cliente -->
<div class="btn-group col-md-10 offset-md-1" style="margin-top: 2px;" role="group" aria-label="Basic example">
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(4)">4</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(5)">5</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(6)">6</button>
  <a style="width: 35%" class="btn btn-success btn-lg" href="{{ route('facturas.index') }}" role="button" target="_blanck" title="Iniciar Nueva Factura">Lista Facturas</a>
</div>
<!-- Boton agregar producto -->
<div class="btn-group col-md-10 offset-md-1" style="margin-top: 2px;" role="group" aria-label="Basic example">
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(1)">1</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(2)">2</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(3)">3</button>
  <button style="width: 35%" type="button" class="btn btn-warning btn-lg" onclick="location.reload();" title="Borrar Datos y Refrescar Pantalla">- Limpiar</button>
</div>
<!-- aqui guardamos la url de preparar factura  -->
<p id="factura-url" hidden>{{ url('posd/') }}</p>
<div class="btn-group col-md-10 offset-md-1" style="margin-top: 2px;" role="group" aria-label="Basic example">
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num(0)">0</button>
  <button style="width: 20%" type="button" class="btn btn-secondary btn-lg" onclick="teclado_num('.')">.</button>
  <button style="width: 55%" type="button" class="btn btn-primary btn-lg" onclick="cargarmodal('{{ url('posd/') }}',1);" title="Iniciar Proceso de Facturacion">Preparar Factura</button>
</div>
<!-- FIN DEL TECLADO -->

<!-- Modal Factura-->
<div class="modal fade" id="envioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Envio de Factura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
      	<h4 class="text-center">Total de Factura: <span id="total_modal" class="text-primary"></span></h4>
      	<br>
        <input type="text" id="tfactura" hidden value="">
      	<div class="row">	
      	<div class="col-md-6">
      		<!-- aqui se agregaran los clientes -->
      		<div class="form-group">
	      		<label for="clientes">Cliente:</label>
		      	<select class="js-example-basic-single" id="clientes_select" name="clientes_select" style="width: 75%">

				    </select>
            <!-- aqui se agregaran los clientes desde el modal -->
            <button style="width: 30px" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#clienteModal" title="Añadir Nuevo Cliente">+</button>
    			</div>
          
          
    		</div>
		  <div class="col-md-6">
			<!-- aqui verificamos si es contado o credito-->
			<span>Tipo Factura:</span>
			<div class="custom-control custom-radio">
			  <input class="custom-control-input" type="radio" name="credito_contado" id="contado" value="1" checked onclick="credito_contado('{{ url('posd/') }}')">
			  <label class="custom-control-label" for="contado">
			    Contado
			  </label>
			</div>
			<div class="custom-control custom-radio">
			  <input class="custom-control-input" type="radio" name="credito_contado" id="credito" value="2" onclick="credito_contado('{{ url('posd/') }}')">
			  <label class="custom-control-label" for="credito">
			    Credito
			  </label>
			</div>
      <div class="custom-control custom-radio">
        <input class="custom-control-input" type="radio" name="credito_contado" id="cotizacion" value="3" onclick="credito_contado('{{ url('posd/') }}')">
        <label class="custom-control-label" for="cotizacion">
          Cotizacion
        </label>
      </div>
		</div>
		</div>
    <br>
		<div id="c_o_c" class="">
			<!-- aqui se agregaran los tipos de pago, o la fecha --> 
			
		</div>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="pruebaepson()">Prueba Epson</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Atras</button>
        <button id="btn_enviar" class="btn btn-secondary" onclick="enviarfactura('{{ url('/facturas') }}');">Enviar Factura</button>
        <button id="btn_coti" class="btn btn-secondary" onclick="enviarcotizacion('{{ url('/cotizaciones') }}');" disabled>Enviar Cotizacion</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal CAMBIO DE EFECTIVO AL CLIENTE -->
<div class="modal fade" id="cambioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambio del Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h3 id="cambiofactura" class="text-primary"></h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
        <a id="btn_verfactura" class="btn btn-primary" href="" role="button">Ver Factura</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal AÑADIR CLIENTE -->
<div class="modal fade" id="clienteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Añadir Nuevo Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Valida la informacion para mostrar msj en caso de error -->
        @include('alerts.request')
        
        {!! Form::open(['id'=>'clienteform', 'class'=>'form-horizontal','action'=>'ClienteController@store', 'method'=>'POST']) !!}
          
          <input type="hidden" name="form_pos" id="form_pos" value="1">
          <!--añade los objetos que se repiten del form -->
          @include('clientes.cliente')

          <div align="center">
            <button type="submit" class="btn btn-success">
              <span data-feather="plus"></span>
              Añadir Cliente
            </button>
          </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>