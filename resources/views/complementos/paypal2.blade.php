@php
    //obtiene una lista de los planes
    $planes = App\Plan::planesLista();	
@endphp

<div class="row">
	<div class="card border-success col-md-8 offset-md-2  " style="width: 18rem;">
		  <div class="card-body">
		    <h5 class="card-title" align="center">Pago de Suscripción</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Seleccione el plan de suscripción que que más se adapte a su negocio</h6>

			{{ Form::open(['id'=>'suscripcionform', 'class'=>'form-horizontal','action'=>'PaypalController@paypalRedirect', 'method'=>'GET']) }}

				<div class="col-5">
					<div class="form-group">
						{!! Form::label('Plan:') !!}
						{!! Form::select('id_plan',$planes,null,['class'=>'form-control', 'id'=>'id_plan','required' => 'required']) !!}
					</div>
				</div>

				<div align="center">
					<button type="submit" class="btn btn-success">
						<span data-feather="plus"></span>
						Suscribirse
					</button>
				</div>

			{{ Form::close() }}
		</div>
	</div>
</div>