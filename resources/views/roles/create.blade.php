@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Crear Rol</h1>

{!! Form::open(['id'=>'productoform', 'class'=>'form-horizontal','action'=>'RoleController@store', 'method'=>'POST']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('roles.role')

	<div align="center">
		<button type="submit" class="btn btn-success">
			<span data-feather="plus"></span>
			Añadir Rol
		</button>
	</div>

{!! Form::close() !!}

@endsection