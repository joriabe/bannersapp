@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Roles</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">ROL</th>
				<th class="text-center">DESCRIPCIÓN</th>
				<th class="text-center">OPER</th>						
			</tr>
			</thead>
	
			@foreach($roles as $role)
			<tr>
				<td class="text-center">{{ $role->id }}</td>
				<td class="text-center">{{ $role->name }}</td>
				<td class="text-center">{{ $role->description }}</td>

				@can('roles.edit')			
				<td class="text-center">
			
					<a class="btn btn-info btn-sm" href="{{ action('RoleController@edit', ['id' => $role->id]) }}" role="button" title="Editar Rol"><span data-feather="edit"></span></a>

			    </td>
			    @endcan

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$roles->render()!!}


		<div align="center">
			@can('roles.create')
			<a class="btn btn-success" href="{{ url('/roles/create') }}" role="button"><span data-feather="plus"></span> Nuevo Rol</a>
			@endcan     
		</div>
	</div>

@endsection

