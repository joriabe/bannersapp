{{ csrf_field() }}

<div class="row">
	<div class="col-6">
		<div class="form-group">
			{!! Form::label('Nombre:') !!}				
			{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'', 'id'=>'name','required' => 'required']) !!}		
		</div>
		<div class="form-group">
			{!! Form::label('URL Amigable:') !!}				
			{!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'', 'id'=>'slug','required' => 'required']) !!}		
		</div>
		<div class="form-group">
			{!! Form::label('Descripción:') !!}				
			{!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'', 'id'=>'description','required' => 'required']) !!}		
		</div>

		<h3>Permisos Especiales</h3>
		<div class="form-group">
			<label>{!! Form::radio('special','all-access') !!} Acceso Total</label>
			<label>{!! Form::radio('special','no-access') !!} Ningún Acceso</label>
		</div>

		<h3>Lista de Permisos</h3>
		<div class="form-group">
			<ul class="list-unstyled">
				@foreach($permisos as $permiso)
					<li>
						<label>
							{!! Form::checkbox('permissions[]', $permiso->id,null) !!}
							{!! $permiso->name !!}
							<em>({!! $permiso->description ?: 'Sin descripción' !!})</em>
						</label>
					</li>
				@endforeach
			</ul>
		</div>

	</div>
</div>
