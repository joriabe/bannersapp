@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Rol</h1>

{!! Form::model($roles,['route'=>['roles.update', $roles->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('roles.role')

	<div align="center">
		<button type="submit" class="btn btn-success">
			<span data-feather="plus"></span>
			Guardar
		</button>
	</div>
    
{!! Form::close() !!}

@stop()