<table>
	<thead>
		<tr>
			<th>Nº Factura</th>
			<th>Fecha</th>
			<th>Cliente</th>
			<th>Sub-Total</th>
			<th>Total</th>
			<th>Usuario</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		@foreach($facturas as $factura)

		<tr>
			<td>{{ $factura->codfactura }}</td>
			<td>{{ $factura->fecha }}</td>
			<td>{{ $factura->nombrecliente }}</td>
			<td>{{ $factura->totalantesimpuesto }}</td>
			<td>{{ $factura->totaldespuesimpuesto }}</td>
			<td>{{ $factura->name }}</td>
			<td>{{ $factura->estado2 }}</td>
		</tr>
		@endforeach
		
	</tbody>
</table>