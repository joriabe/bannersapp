<?php 
$num = 0;
$suma_ex = 0;
$suma_15 = 0;
$suma_18 = 0;
$suma_i15 = 0;
$suma_i18 = 0;
?>
<table>
    <thead>
    <tr>
        <th rowspan="2">#</th>
        <th rowspan="2">RTN</th>
        <th rowspan="2">Razón Social</th>
        <th rowspan="2">Fecha</th>
        <th rowspan="2">CAI</th>
        <th rowspan="2">NUMERO DE DOCUMENTO FISCAL</th>
        <th colspan="3">SUBTOTAL DE COMPRAS</th>
        <th colspan="2">CREDITO FISCAL ISV</th>
    </tr>
    <tr>        
        <th>Importe Exento</th>
        <th>Importe Gravado 15%</th>
        <th>Importe Gravado 18%</th>
        <th>Impuesto al 15%</th>
        <th>Impuesto al 18%</th>
    </tr>
    </thead>
    <tbody>
    @foreach($compras as $compra)

    <?php 
    $num++;
    $suma_ex += $compra->subtotal_exento;
    $suma_15 += $compra->subtotal_15 ;
    $suma_18 += $compra->subtotal_18;
    $suma_i15 += $compra->subtotal_15 * (15/100);
    $suma_i18 += $compra->subtotal_18 * (18/100);
    ?>
        <tr>
            <td>{{ $num }}</td>
            <td>{{ $compra->rtn }}</td>
            <td>{{ $compra->razon_social }}</td>
            <td>{{ $compra->fecha }}</td>
            <td>{{ $compra->cai }}</td>
            <td>{{ $compra->num_docto }}</td>
            <td>{{ $compra->subtotal_exento }}</td>
            <td>{{ $compra->subtotal_15 }}</td>
            <td>{{ $compra->subtotal_18 }}</td>
            <td>{{ $compra->subtotal_15 * (15/100) }}</td>
            <td>{{ $compra->subtotal_18 * (18/100) }}</td>
        </tr>
    @endforeach

    <tr>
        <td colspan="6">TOTALES --></td>
        <td>{{ $suma_ex }}</td>
        <td>{{ $suma_15 }}</td>
        <td>{{ $suma_18 }}</td>
        <td>{{ $suma_i15 }}</td>
        <td>{{ $suma_i18 }}</td>


    </tr>
    </tbody>
</table>