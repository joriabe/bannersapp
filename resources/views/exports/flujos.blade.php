<table>
	<thead>
		<tr>
		<th>Fecha</th>
		<th>Valor</th>
		<th>Descripcion</th>
		<th>Tipo Movimiento</th>
		<th>Tipo Pago</th>
		<th>Usuario</th>
		<th>Agencia</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($flujos as $flujo)
		<tr>
			<td>{{ $flujo->fecha }}</td>
			<td>{{ $flujo->valor }}</td>
			<td>{{ $flujo->referencia }}</td>
			<td>{{ $flujo->descripcion }}</td>
			<td>{{ $flujo->tipopago }}</td>
			<td>{{ $flujo->name }}</td>
			<td>{{ $flujo->agencia }}</td>
		</tr>

		@endforeach
		
	</tbody>
</table>

