<?php 
$num = 0;
$suma_ex = 0;
$suma_15 = 0;
$suma_18 = 0;
$suma_i15 = 0;
$suma_i18 = 0;
?>
<table>
    <thead>
    <tr>
        <th rowspan="2">#</th>
        <th rowspan="2">Numero Documento Fiscal</th>
        <th rowspan="2">Fecha</th>
        <th colspan="3">SUBTOTAL DE VENTAS</th>
        <th colspan="2">DEBITO FISCAL ISV</th>
    </tr>
    <tr>
        <th>Importe Exento</th>
        <th>Importe Gravado 15%</th>
        <th>Importe Gravado 18%</th>
        <th>Impuesto al 15%</th>
        <th>Impuesto al 18%</th>
    </tr>
    </thead>
    <tbody>


    @foreach($ventas as $venta)
    <?php 
    $num++;
    $suma_ex += $venta["importe_exento"];
    $suma_15 += $venta["importe_15"];
    $suma_18 += $venta["importe_18"];
    $suma_i15 += $venta["impuesto_15"];
    $suma_i18 += $venta["impuesto_18"];
    ?>
        <tr>            
            <td>{{ $num }}</td>
            <td>{{ $venta["codfactura"] }}</td>
            <td>{{ $venta["fecha"] }}</td>            
            <td>{{ $venta["importe_exento"] }}</td>
            <td>{{ $venta["importe_15"] }}</td>
            <td>{{ $venta["importe_18"] }}</td>
            <td>{{ $venta["impuesto_15"] }}</td>
            <td>{{ $venta["impuesto_18"] }}</td>           

        </tr>            
        
    @endforeach
    <tr>
        <td colspan="3">TOTALES --></td>
        <td>{{ $suma_ex }}</td>
        <td>{{ $suma_15 }}</td>
        <td>{{ $suma_18 }}</td>
        <td>{{ $suma_i15 }}</td>
        <td>{{ $suma_i18 }}</td>


    </tr>
    </tbody>
</table>