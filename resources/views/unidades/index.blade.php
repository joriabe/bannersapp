@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Unidades</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="450">UNIDAD</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($unidades as $unidad)
			<tr>
				<td class="text-center">{{ $unidad->acronimo }}</td>
				<td class="text-center">{{ $unidad->unidad }}</td>
				<td class="text-center">

					@can('unidades.edit')
					<a class="btn btn-info btn-sm" href="{{ action('UnidadController@edit', ['id' => $unidad->id]) }}" role="button" title="Editar unidad"><span data-feather="edit"></span></a>
					@endcan
					
					@can('unidades.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar unidad" onclick="borrar({{ $unidad->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$unidades->render()!!}

		<div align="center">
			@can('unidades.create')
			<a class="btn btn-success" href="{{ url('/unidades/create') }}" role="button"><span data-feather="plus"></span> Nueva Unidad</a>
			@endcan     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrar(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar la Unidad "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/unidades/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection