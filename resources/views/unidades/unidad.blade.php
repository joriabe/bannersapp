{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="form-group col-3">
			{!! Form::label('Acrónimo:') !!}				
			{!! Form::text('acronimo',null,['class'=>'form-control','placeholder'=>'', 'id'=>'acronimo','required' => 'required', 'maxlength'=>"4", 'size'=>"4", "onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}		
		</div>
		<div class="form-group col-12">
			{!! Form::label('Unidad:') !!}	
			{!! Form::text('unidad',null,['class'=>'form-control','placeholder'=>'', 'id'=>'unidad','required' => 'required']) !!}				
		</div>
	</div>


</div>