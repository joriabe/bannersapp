@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-8 offset-md-2" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Unidades</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Presentación o Unidad de medida del producto/servicio</h6>

			{!! Form::open(['id'=>'unidadform', 'class'=>'form-horizontal','action'=>'UnidadController@store', 'method'=>'POST']) !!}
				<input type="hidden" name="form_pos" id="form_pos" value="0">
				<!--añade los objetos que se repiten del form -->
				@include('unidades.unidad')

				<div align="center">
					<button type="submit" class="btn btn-primary">
						<span data-feather="plus"></span>
						Añadir
					</button>

					<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
				</div>

			{!! Form::close() !!}
		</div>
	</div>
</div>


@endsection