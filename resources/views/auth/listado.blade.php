@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Usuarios</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">ROL</th>
				<th class="text-center" width="150">USER</th>
				<th class="text-center" width="150">AGENCIA</th>
				<th class="text-center" width="100">EMAIL</th>						
			</tr>
			</thead>
	
			@foreach($users as $user)
			<tr>
				<td class="text-center">{{ $user->role }}</td>
				<td class="text-center">{{ $user->user }}</td>
				<td class="text-center">{{ $user->descripcion }}</td>
				<td class="text-center">{{ $user->email }}</td>			    				
			</tr>
			@endforeach			
		</table>

		<div align="center">
			@can('users.delete')
			<a class="btn btn-success" href="{{ route('register') }}" role="button"><span data-feather="user-plus"></span> Nuevo Usuario</a>  
			@endcan       
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarCliente(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Cliente "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/clientes/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection