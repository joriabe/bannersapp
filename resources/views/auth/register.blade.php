@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf


                        <div class="form-group row">
                            <label for="agencia_id" class="col-md-4 col-form-label text-md-right">Agencia:</label> 
                            <div class="col-md-6">       
                            

                            <select class="form-control"{{ $errors->has('agencia_id') ? ' is-invalid' : '' }} name="agencia_id" id="agencia_id" required>
                                <option id="" value="">Selecciona una Agencia</option>
                                @foreach ($cias as $cia)
                                    <option value="{{ $cia->id }}" disabled>{{ $cia->nombrecomercial }}</option>

                                    @foreach($cia->agencias as $agencia)
                                    <option value="{{ $agencia->id }}"> - {{ $agencia->descripcion }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                                @if ($errors->has('agencia_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('agencia_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>   

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rol_id" class="col-md-4 col-form-label text-md-right">Rol:</label> 
                            <div class="col-md-6">       
                            

                            <select class="form-control"{{ $errors->has('rol_id') ? ' is-invalid' : '' }} name="rol_id" id="rol_id" required>
                                <option id="" value="">Seleccione un Rol</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                                @if ($errors->has('rol_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rol_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>   

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
