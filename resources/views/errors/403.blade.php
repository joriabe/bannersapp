@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                    <div class="title m-b-md">
                        403 - Acción no Autorizada
                    </div>
                    <div class="links">
                        <a href="{{ route('home') }}">
                            Ir a Home
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
