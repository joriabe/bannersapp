@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-10 offset-md-1" style="width: 18rem;">
	 	<div class="card-body">
		    <h5 class="card-title"  align="center">Nueva Factura de Compra</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Facturas emitidas por proveedores</h6>

			{!! Form::open(['id'=>'declaracioncompraform', 'class'=>'form-horizontal','action'=>'DeclaracionCompraController@store', 'method'=>'POST']) !!}

			<!--añade los objetos que se repiten del form -->
			@include('declaracioncompras.declaracioncompra')

			<div align="center">
				<button type="submit" class="btn btn-primary">
					<span data-feather="plus"></span>
					Añadir
				</button>

				<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
			</div>
			    
			{!! Form::close() !!}

		</div>	
	</div>
</div>

@include('declaracioncompras.modal')

@endsection

