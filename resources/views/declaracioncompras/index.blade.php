@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Factuas de Compras</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">FECHA</th>
				<th class="text-center" width="150">FACTURA</th>
				<th class="text-center" width="150">PROVEEDOR</th>
				<th class="text-center" width="50">SUBTOTAL</th>
				<th class="text-center" width="50">IMPUESTO</th>
				<th class="text-center" width="50">TOTAL</th>	
				<th class="text-center" width="50">OPER</th>					
			</tr>
			</thead>
	
			@foreach($declaracioncompras as $declaracioncompra)
			<tr>
				<td class="text-center">{{ $declaracioncompra->fecha }}</td>
				<td class="text-center">{{ $declaracioncompra->num_docto }}</td>
				<td class="text-center">{{$declaracioncompra->name}}</td>
				<td class="text-center">{{number_format(
										$declaracioncompra->subtotal_exento +
										$declaracioncompra->subtotal_15 +
										$declaracioncompra->subtotal_18
										,2)}}
				</td>
				<td class="text-center">{{number_format(
										($declaracioncompra->subtotal_15 * 0.15) + 
										($declaracioncompra->subtotal_18 * 0.18)
										,2)}}
				</td>
				<td class="text-center">{{number_format(
										$declaracioncompra->subtotal_exento      +
										($declaracioncompra->subtotal_15 * 1.15) +
										($declaracioncompra->subtotal_18 * 1.18) 
										,2)}}
				</td>
				<td class="text-center">

					@can('declaracioncompras.edit')
					<a class="btn btn-info btn-sm" href="{{ action('DeclaracionCompraController@edit', ['id' => $declaracioncompra->id]) }}" role="button" title="Editar Factura"><span data-feather="edit"></span></a>
					@endcan
					@can('declaracioncompras.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar Factura" onclick="borrarFacura({{ $declaracioncompra->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$declaracioncompras->render()!!}

		<div align="center">
			@can('declaracioncompras.create')
			<a class="btn btn-success" href="{{ url('/declaracioncompras/create') }}" role="button"><span data-feather="pluss"></span> Nueva Factura Compra</a>
			@endcan     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarFacura(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Factura "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/declaracioncompras/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection