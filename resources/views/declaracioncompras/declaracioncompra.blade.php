{{ csrf_field() }}
<div class="row">
	<div class="col-12">
		<div class="row">
			<div class="col-3">
				<div class="form-group">
					{{ Form::label('Fecha Factura:') }}	

					@isset($declaracioncompras)
					{{ Form::date('fecha', $declaracioncompras->fecha,['class'=>'form-control', 'id'=>'fecha','required' => 'required']) }}
					@else
					{{ Form::date('fecha', null,['class'=>'form-control', 'id'=>'fecha','required' => 'required']) }}
					@endisset			
				</div>
			</div>
		</div>
		<div class="row align-items-center">
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Proveedor:') }}	
					{{ Form::select('proveedor_id',$proveedores,null,['class'=>'js-example-basic-single form-control', 'id'=>'proveedor_id','required' => 'required']) }}			
				</div>				
			</div>
			<button style="width: 30px; height: 30px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#proveedorModal" title="Añadir Nuevo Proveedor">+</button>
		</div>

		<input type="hidden" name="tipo" id="tipo" value=$tipo>


		
		<div class="row">

			<div class="col-4">
					<div class="form-group">
						{{ Form::label('Tipo de Documento:') }}

						@isset($declaracioncompras)				
						{{ Form::select('tipo_docto',['0'=>'Factura Fiscal','1'=>'Recibos de Servicios Públicos','2'=>'Documentos del Sistema Financiero y Seguros','3'=>' Boletos de Transportes Aéreos de Pasajeros','4'=>' Otros Autorizados por el SAR'],$declaracioncompras->tipo_docto,['class'=>'form-control', 'id'=>'tipo_docto','required' => 'required','onclick'=>'mostrardiv(3,this)']) }}
							@php
								$num = $declaracioncompras->num_docto;	
							@endphp
						@else
						{{ Form::select('tipo_docto',['0'=>'Factura Fiscal','1'=>'Recibos de Servicios Públicos','2'=>'Documentos del Sistema Financiero y Seguros','3'=>' Boletos de Transportes Aéreos de Pasajeros','4'=>' Otros Autorizados por el SAR'],null,['class'=>'form-control', 'id'=>'tipo_docto','required' => 'required','onclick'=>'mostrardiv(3,this)']) }}
							@php
								$num = null;	
							@endphp

						@endisset	
					</div>
				</div>

	
				<div class="col-4"  id="div_ff">
					<div class="form-group">
						{{ Form::label('Número de factura:') }}				
						{{ Form::text('num_docto1',$num,['class'=>'form-control','placeholder'=>'Código numerico de 19 caracteres', 'title' => 'Ej. 000-000-00-00000000', 'id'=>'num_docto1','pattern'=>'^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{8}$']) }}	
					</div>
				</div>

				

				<div class="col-4"  id="div_fnf" style="display: none;">
					<div class="form-group">
						{{ Form::label('Número de factura:') }}				
						{{ Form::text('num_docto2',$num,['class'=>'form-control','placeholder'=>'Código de Factura', 'id'=>'num_docto2']) }}	
					</div>
				</div>

		</div>

		<div class="row">
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Valor Exento:') }}
					<div class="input-group">	
						<div class="input-group-prepend">
					       	<div class="input-group-text">L.</div>
						</div>			
						
						{{ Form::number('subtotal_exento',null,['class'=>'form-control','placeholder'=>'', 'id'=>'subtotal_exento',"step"=>"any", 'title' => 'Subtotal de la factura que se encuentra exento de impuesto.']) }}
								
					</div>
				</div>
			</div>
					
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Subtotal Grabable 15%:') }}
					<div class="input-group">	
						<div class="input-group-prepend">
					       	<div class="input-group-text">L.</div>
						</div>			
						{{ Form::number('subtotal_15',null,['class'=>'form-control','placeholder'=>'', 'id'=>'subtotal_15',"step"=>"any", 'title' => 'Subtotal de la factura que esta sujeto al 15% impuesto.']) }}				
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Subtotal Grabable 18%:') }}
					<div class="input-group">
						<div class="input-group-prepend">
					       	<div class="input-group-text">L.</div>
						</div>			
						{{ Form::number('subtotal_18',null,['class'=>'form-control','placeholder'=>'', 'id'=>'subtotal_18',"step"=>"any", 'title' => 'Subtotal de la factura que esta sujeto al 15% impuesto.']) }}		
					</div>
				</div>
			</div>
		</div>
		@isset($declaracioncompras)
		@else
	 		<div class="row">
				<div class="col-4">
					<div class="form-group">
							{{ Form::label('Se creara una Cuenta por Pagar?') }}
							<div class="input-group input-group-sm">	
								<label>{{ Form::radio('cxp','no',true,['onclick'=>'mostrardiv(1,this)']) }} No </label>
								<label>{{ Form::radio('cxp','si',false,['onclick'=>'mostrardiv(1,this)']) }} Si</label>
							</div>	
					</div>
				</div>		
				<div id="div_cxp" class="col-4" style="display: none;">	
					<div class="form-group">
						{{ Form::label('Fecha de Vencimiento:') }}
						{{ Form::date('fecha_vencimiento', null,['class'=>'form-control', 'id'=>'fecha_vencimiento']) }}			
					</div>
				</div>
		   	</div>
		
			
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						{{ Form::label('Se afectara el flujo?') }}
						<div class="input-group input-group-sm">	
							
							<label title="El ingreso de esta factura, NO afectara los flujos de efectivo.">
								{{ Form::radio('flujo','no',true, ['onclick'=>'mostrardiv(2,this)']) }} No </label>
							
							<label title="El ingreso de esta factura, SI afectara los flujos de efectivo.">
								{{ Form::radio('flujo','si',false,['onclick'=>'mostrardiv(2,this)']) }} Si</label>
						</div>	
					</div>
				</div>
			</div>
			<div id="div_flj" class="row" style="display: none;">
				<div class="col-12">
					<div class="table-responsive">		
						<table class="table table-striped table-bordered table-sm w-100">		
							<thead class="thead-dark">
								<tr>
									<th class="text-center" width="50">TIPO DE PAGO</th>
									<th class="text-center" width="50">VALOR</th>
									<th class="text-center" width="150">COMPROBANTE PAGO</th>			
								</tr>
							</thead>
			
							@foreach($tipo_pagos as $tipo_pago)
							<tr>
								<td class="text-center" >
									<p class="h6">{{ $tipo_pago->tipopago }} </p>
								</td>
								<td class="text-center">
									<div class="input-group">
										<div class="input-group-prepend">
									       	<div class="input-group-text">L.</div>
									    </div>		
										{!! Form::number('valor_pagado[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'valor_pagado',"step"=>"any"]) !!}				
									</div>		
								</td>
								<td class="text-center">
									{{ Form::text('comprobantepago[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'comprobantepago']) }}	
									{{ Form::hidden('tipo_pago_id[]', $tipo_pago->id ,['class'=>'form-control','placeholder'=>'', 'id'=>'comprobantepago']) }}	
								</td>				
							</tr>
							@endforeach			
						</table>
					</div>
				</div>
			</div>
		@endisset
	</div>
</div>

@section('scrips')

	<script type="text/javascript">
		function mostrardiv(div,thi){
			//cambiamos el style
			//style="display: inline-block;"
			if (div == 1) {
				if (thi.value == "no") {
					document.getElementById("div_cxp").style.display = "none";
				}
				if (thi.value == "si") {
					document.getElementById("div_cxp").style.display = "block";
				}
			}
			if (div == 2) {
				if (thi.value == "no") {
					document.getElementById("div_flj").style.display = "none";
				}
				if (thi.value == "si") {
					document.getElementById("div_flj").style.display = "block";
				}
			}
			if (div == 3) {
				if (thi.value == "0") {
					document.getElementById("div_ff").style.display = "block";
					document.getElementById("div_fnf").style.display = "none";
				}
				else
				{
					document.getElementById("div_ff").style.display = "none";
					document.getElementById("div_fnf").style.display = "block";
				}
			}									
		}
	</script>

@endsection
