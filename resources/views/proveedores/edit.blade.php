@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Proveedor</h1>

{!! Form::model($proveedores,['route'=>['proveedores.update', $proveedores->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('proveedores.proveedor')

	<div align="center">
		<button type="submit" class="btn btn-primary">
			<span data-feather="plus"></span>
			Guardar
		</button>
		<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
	</div>
    
{!! Form::close() !!}

@stop()