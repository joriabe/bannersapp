@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Proveedores</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>
	

	

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="200">NOMBRE</th>
				<th class="text-center" width="100">RTN</th>
				<th class="text-center" width="50">TELEFONO</th>
				<th class="text-center" width="100">EMAIL</th>
				<th class="text-center" width="70">OPER</th>						
			</tr>
			</thead>
	
			@foreach($listados as $proveedor)
			<tr>
				<td class="text-center">{{ $proveedor->codinterno }}</td>
				<td class="text-center">{{ $proveedor->name }}</td>
				<td class="text-center">{{ $proveedor->rtn }}</td>
				<td class="text-center">{{ $proveedor->telefono }}</td>
				<td class="text-center">{{ $proveedor->correo }}</td>
				<td class="text-center">
					
					@can('proveedores.show')
					<a class="btn btn-info btn-sm" href="{{ action('ProveedorController@show', ['id' => $proveedor->id]) }}" role="button" title="Ver Proveedor"><span data-feather="search"></span></a>
					@endcan

					@can('proveedores.edit')
					<a class="btn btn-primary btn-sm" href="{{ action('ProveedorController@edit', ['id' => $proveedor->id]) }}" role="button" title="Editar Proveedor"><span data-feather="edit"></span></a>
					@endcan
					
					@can('proveedores.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar Proveedor" onclick="borrarProveedor({{ $proveedor->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}

		@can('proveedores.create')
		<div align="center">
			<a class="btn btn-success" href="{{ url('/proveedores/create') }}" role="button"><span data-feather="user"></span> Nuevo Proveedor</a>     
		</div>
		@endcan
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarProveedor(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Proveedor "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/proveedores/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection