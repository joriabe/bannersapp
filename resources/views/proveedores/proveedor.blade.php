</br>
<h6 class="card-subtitle mb-2 text-muted" align="center">Datos Generales</h6>

<div class="row">
	
	<div class="col-3">
		<div class="form-group">
			{!! Form::label('ID:') !!}				
			{!! Form::text('codinterno',null,['class'=>'form-control','placeholder'=>'', 'id'=>'codinterno','maxlength'=>'16','required' => 'required', "onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}		
		</div>
	</div>
	
	<div class="col-3">
		<div class="form-group">
			{!! Form::label('RTN:') !!}	
			{!! Form::number('rtn',null,['class'=>'form-control','placeholder'=>'Cód. numérico 14 dígitos', 'pattern'=>"[0-9]{14}", 'title' => 'Ej. 00000000000000','id'=>'rtn','required' => 'required',"onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}			
		</div>
	</div>
	<div class="col-6">
		<div class="form-group">
			{!! Form::label('CAI:') !!}	
			{!! Form::text('cai',null,['class'=>'form-control','placeholder'=>'Código alfanumerico 37 caracteres', 'title' => 'Ej. Z000Z0-00Z00Z-0000Z0-ZZ0ZZ0-Z0Z0Z0-Z0', 'id'=>'cai','pattern'=>'^[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{2}$', 'required' => 'required',"onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}				
		</div>	
	</div>
</div>
<div class="row">
	<div class="col-3">
		<div class="form-group">
			{!! Form::label('Nombre Proveedor:') !!}	
			{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'', 'id'=>'name','required' => 'required']) !!}		
		</div>
	</div>
	<div class="col-3">
		<div class="form-group">
			{!! Form::label('Razón Social:') !!}	
			{!! Form::text('razon_social',null,['class'=>'form-control','placeholder'=>'', 'id'=>'razon_social','required' => 'required']) !!}		
		</div>
	</div>
</div>

</br>
<div class="row">

	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Datos de Contacto</h6>
		<div class="row">
			<div class="col-4">
				<div class="form-group">
					{!! Form::label('Depto. o Cargo') !!}	
					{!! Form::text('cargo_contacto',null,['class'=>'form-control','placeholder'=>'', 'id'=>'cargo_contacto']) !!}			
				</div>
			</div>
			<div class="col-8">
				<div class="form-group">
					{!! Form::label('Nombre de Contacto:') !!}	
					{!! Form::text('name_contacto',null,['class'=>'form-control','placeholder'=>'', 'id'=>'name_contacto']) !!}			
				</div>
			</div>
		</div>
		
		<div class="form-group">
			{!! Form::label('Ciudad:') !!}	
			{!! Form::text('ciudad',null,['class'=>'form-control','placeholder'=>'', 'id'=>'ciudad']) !!}			
		</div>
		
		<div class="form-group">
			{!! Form::label('Direcció:') !!}	
			{!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'direccion']) !!}			
		</div>

		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Teléfono:') !!}	
					{!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'', 'id'=>'telefono']) !!}			
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Email:') !!}	
					{!! Form::email('correo',null,['class'=>'form-control','placeholder'=>'', 'id'=>'correo']) !!}			
				</div>		
			</div>
		</div>
	</div>

	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Otros Datos</h6>
		<div class="row">
			<div class="col-4">
				<div class="form-group">
					{!! Form::label('Días de Credito:') !!}
					<div class="input-group">		
						{!! Form::number('dias_credito',null,['class'=>'form-control','placeholder'=>'', 'id'=>'dias_credito']) !!}
							<div class="input-group-append">
							   	<div class="input-group-text">Días</div>
							</div>	
					</div>		
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12">
				<div class="form-group">
					{!! Form::label('Condiciones de Pago:') !!}	
					{!! Form::textarea('condiciones_pago',null,['class'=>'form-control','placeholder'=>'interese moratorios, cuentas para depósito etc.', 'id'=>'condiciones_pago',  'rows'=>"4"]) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="form-group">
					{!! Form::label('Nota:') !!}	
					{!! Form::text('nota',null,['class'=>'form-control','placeholder'=>'Otra información relevante.', 'id'=>'nota']) !!}			
				</div>
			</div>	
		</div>
	</div>
</div>	
