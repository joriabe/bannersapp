@extends('layouts.dashboard')

@section('content')

<div class="card">
	<h5 class="card-header">{{ $proveedores->razon_social }}&nbsp({{ $proveedores->name }})</h5>
	<div class="card-body">
    <!--<h5 class="card-title">{{ $proveedores->razon_social }}</h5>-->
   	    <div class="row">
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Código:</p></b>
					<p class="card-text">{{ $proveedores->codinterno }} </p>
				</div>
			</div>

			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">RTN:</p></b>
					<p class="card-text">{{ $proveedores->rtn }} </p>
				</div>
			</div>
			
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">CAI:</p></b>
					<p class="card-text">{{ $proveedores->cai }} </p>
				</div>
			</div>
		</div>
		
		<br/>
    	<b><p class="card-text"><span data-feather="at"></span>  Datos de Contacto </p></b>
		<hr>
		
		<div class="row">
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Nombre del Contacto:</p></b>
					<p class="card-text">{{ $proveedores->cargo_contacto }}&nbsp{{ $proveedores->name_contacto }} </p>
				</div>
			</div>

			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Telefono:</p></b>
					<p class="card-text">{{ $proveedores->telefono }} </p>
				</div>
			</div>
			
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Correo:</p></b>
					<p class="card-text">{{ $proveedores->correo }} </p>
				</div>
			</div>
		</div>
	
		<br/><b><p class="card-text"> Dirección </p></b><hr>

		<div class="row">
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Ciudad:</p></b>
					<p class="card-text">{{ $proveedores->ciudad }}</p>
				</div>
			</div>

			<div class="col-8">
				<div class="form-group">
					<b><p class="card-text">Dirección:</p></b>
					<p class="card-text">{{ $proveedores->direccion }} </p>
				</div>
			</div>
		</div>

	<br/><b><p class="card-text">Condiciones de Pago </p></b><hr>

	<div class="row">
		<div class="col-4">
			<div class="form-group">
				<b><p class="card-text">Dias de Credito:</p></b>
				<p class="card-text">{{ $proveedores->dias_credito }} &nbsp días. </p>
			</div>
		</div>

		<div class="col-4">
			<div class="form-group">
				<b><p class="card-text">Condiciones de Pago:</p></b>
				<p class="card-text">{{ $proveedores->condiciones_pago }} </p>
			</div>
		</div>

		<div class="col-4">
			<div class="form-group">
				<b><p class="card-text">Nota:</p></b>
				<p class="card-text">{{ $proveedores->nota }} </p>
			</div>
		</div>

	</div>


    
	<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
</div>

    
{!! Form::close() !!}


@endsection