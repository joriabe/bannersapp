{{ csrf_field() }}

</br>	

<div class="row">
	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Datos Generales</h6>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('ID:') !!}				
					{!! Form::text('codinterno',null,['class'=>'form-control','placeholder'=>'', 'id'=>'codinterno','required' => 'required', "onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}		
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('Descripción:') !!}	
			{!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'descripcion','required' => 'required']) !!}				
		</div>

		<div class="row align-items-center">
			<div class="col-5">
				<div class="form-group">
					{!! Form::label('Proveedor:') !!}
					{!! Form::select('proveedor_id',$proveedores,null,['class'=>'js-example-basic-single form-control', 'id'=>'proveedor_id','required' => 'required']) !!}
				</div>
			</div>
			<button style="width: 30px; height: 30px; margin-top: 5px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#proveedorModal" title="Añadir Nuevo Proveedor">+</button>

			<div class="col-5">
				<div class="form-group">
					{!! Form::label('Unidad:') !!}
					{!! Form::select('unidad_id',$unidades,null,['class'=>'form-control', 'id'=>'unidad_id','required' => 'required']) !!}
				</div>
			</div>
			<button style="width: 30px; height: 30px; margin-top: 5px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#unidadModal" title="Añadir Nueva Unidad">+</button>

		</div>

		<div class="row align-items-center">

			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Precio Antes de Impuesto:') !!}
					<div class="input-group">
						<div class="input-group-prepend">
					    	<div class="input-group-text">L.</div>
						</div>			
					{!! Form::number('precio',null,['class'=>'form-control','placeholder'=>'', 'id'=>'precio','required' => 'required',"step"=>"any"]) !!}
				
					</div>		
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Costo:') !!}
					<div class="input-group">
						<div class="input-group-prepend">
					    	<div class="input-group-text">L.</div>
						</div>			
					{!! Form::number('costo',null,['class'=>'form-control','placeholder'=>'', 'id'=>'costo',"step"=>"any"]) !!}
				
					</div>		
				</div>
			</div>
		</div>
	</div>

	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Datos de Categorización</h6>

		<div class="row align-items-center">
			<div class="col-6">
				@isset($productos)
					<div class="form-group">
						{!! Form::label('Tipo:') !!}	
						{!! Form::select('prodoctooservicio', ['0' => 'Producto', '1' => 'Servicio'],null,['class'=>'form-control', 'id'=>'prodoctooservicio','required' => 'required','disabled', 'selected']) !!}			
					</div>
	
				@else
					<div class="form-group">
						{!! Form::label('Tipo:') !!}	
						{!! Form::select('prodoctooservicio', ['0' => 'Producto', '1' => 'Servicio'],null,['class'=>'form-control', 'id'=>'prodoctooservicio','required' => 'required']) !!}			
					</div>
				@endisset
			</div>
			<div class="col-5">
				<div class="form-group">
					{!! Form::label('Categoría:') !!}	
					{!! Form::select('categoria_id',$categorias,null,['class'=>'js-example-basic-single form-control', 'id'=>'categoria_id','required' => 'required']) !!}			
				</div>				
			</div>
			<button style="width: 30px; height: 30px; margin-top: 5px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#categoriaModal" title="Añadir Nueva Categoria">+</button>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Impuesto:') !!}
					<div class="input-group">	
						{!! Form::select('tipoimpositivos_id',$impuestos,null,['class'=>'form-control', 'id'=>'tipoimpositivos_id','required' => 'required']) !!}
						<div class="input-group-append">
						   	<div class="input-group-text">%</div>
						</div>	
					</div>			
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Descuento:') !!}
					{!! Form::select('descuento_id',$descuentos,null,['class'=>'form-control', 'id'=>'descuento_id','required' => 'required']) !!}		
				</div>
			</div>
		</div>
	</div>
</div>

</br>	

<div class="row">
	<div class="col-12">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Existencias y Alertas</h6>

		<div class="table-responsive">		
			<table class="table table-striped table-bordered table-sm">		
				<thead class="thead-dark">
					<tr>
						<th class="text-center">BODEGA</th>
						<th class="text-center" width="200">CANTIDAD</th>
						<th class="text-center" width="200">EXISTENCIA MINIMA</th>
						<th class="text-center">ALERTAR EXISTENCIA MINIMA</th>
						<th class="text-center">FACTURAR SIN EXISTENCIA</th>
					</tr>
				</thead>

				@isset($inventarios)

					@foreach($inventarios as $inventario)
						<tr>
							<td class="text-center" >
								{{ Form::hidden('bodega_id[]', $inventario->bodega_id ,['class'=>'form-control','placeholder'=>'', 'id'=>'bodega_id']) }}	
								
								<p class="h6">{{ $inventario->bodega }} </p>
							</td>
							<td class="text-center">
								<div class="input-group">			
									{!! Form::number('cantidad[]',$inventario->cantidad,['class'=>'form-control','placeholder'=>'', 'id'=>'cantidad',"step"=>"any", "readonly"=>"readonly"]) !!}
									<div class="input-group-append">
									   	<div class="input-group-text">ud.</div>
									</div>	
								</div>	
							</td>
							<td class="text-center">
								<div class="input-group">			
									{!! Form::number('cantidad_minima[]',$inventario->cantidad_minima,['class'=>'form-control','placeholder'=>'', 'id'=>'cantidad_minima',"step"=>"any"]) !!}
									<div class="input-group-append">
									   	<div class="input-group-text">ud.</div>
									</div>	
								</div>
							</td>
							@php
							 	$si = true;
							 	$no = false;

							 	$si2 = true;
							 	$no2 = false;

							 	if($inventario->alerta_existencia == 0)
							 	{
							 		$si = false;
							 		$no = true;
							 	}

							 	if($inventario->facturar_sin_existencia == 0)
							 	{
							 		$si2 = false;
							 		$no2 = true;
							 	}
							@endphp
							<td class="text-center">
								<div class="form-group">
									<label>{{ Form::radio('alerta_existencia[]','1',$si) }} Si</label>
									<label>{{ Form::radio('alerta_existencia[]','0',$no) }} No</label>
								</div>	
							</td>

							<td class="text-center">
								<div class="form-group">
									<label>{{ Form::radio('facturar_sin_existencia[]','1',$si2) }} Si</label>
									<label>{{ Form::radio('facturar_sin_existencia[]','0',$no2) }} No</label>
								</div>	
							</td>				
						</tr>
					@endforeach	

				@else

					@foreach($bodegas as $bodega)
						<tr>
							<td class="text-center" >
								{{ Form::hidden('bodega_id[]', $bodega->id ,['class'=>'form-control','placeholder'=>'', 'id'=>'bodega_id']) }}	
								
								<p class="h6">{{ $bodega->bodega }} </p>
							</td>
							<td class="text-center">
								<div class="input-group">			
									{!! Form::number('cantidad[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'cantidad',"step"=>"any"]) !!}
									<div class="input-group-append">
									   	<div class="input-group-text">ud.</div>
									</div>	
								</div>	
							</td>
							<td class="text-center">
								<div class="input-group">			
									{!! Form::number('cantidad_minima[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'cantidad_minima',"step"=>"any"]) !!}
									<div class="input-group-append">
									   	<div class="input-group-text">ud.</div>
									</div>	
								</div>
							</td>
							<td class="text-center">
								<div class="form-group">
									<label>{{ Form::radio('alerta_existencia[]','1',false) }} Si</label>
									<label>{{ Form::radio('alerta_existencia[]','0',true) }} No</label>
								</div>	
							</td>

							<td class="text-center">
								<div class="form-group">
									<label>{{ Form::radio('facturar_sin_existencia[]','1',true) }} Si</label>
									<label>{{ Form::radio('facturar_sin_existencia[]','0',false) }} No</label>
								</div>	
							</td>				
						</tr>
					@endforeach			

				@endisset
				
			</table>
		</div>
	</div>
</div>