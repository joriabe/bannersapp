@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-12" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Producto/Servicio</h5>

			{!! Form::model($productos,['route'=>['productos.update', $productos->id], 'method'=>'PUT']) !!}

				<!--añade los objetos que se repiten del form -->
				@include('productos.producto')

				<div align="center">
					<button type="submit" class="btn btn-primary">
						<span data-feather="plus"></span>
						Guardar
					</button>

					<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
				</div>
			    
			{!! Form::close() !!}

		</div>
	</div>
</div>

@include('productos.modal')

@stop()