@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')


	<div class="row">
		<div class="col-md-8">
			<h1>Productos y Servicios</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>



	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="200">DESCRIPCIÓN</th>
				<th class="text-center" width="50">PRECIO</th>
				<th class="text-center" width="100">CATEGORÍA</th>
				<th class="text-center" width="50">IMPUESTO (%)</th>
				<th class="text-center" width="50">P/S</th>	
				<th class="text-center" width="50">OPER</th>					
			</tr>
			</thead>
	
			@foreach($listados as $producto)
			<tr>
				<td class="text-center">{{ $producto->codinterno }} </td>
				<td class="text-center">{{ $producto->descripcion }}</td>
				<td class="text-center">{{number_format($producto->precio,2)}}</td>
				<td class="text-center">{{ $producto->categoria }}</td>
				<td class="text-center">{{ number_format($producto->impuesto,2)}}</td>
				<td class="text-center">
					@if(($producto->prodoctooservicio)==='0')
						Producto
					@else
						Servicio
					@endif
				</td>
				<td class="text-center">
					@can('productos.edit')
					<a class="btn btn-info btn-sm" href="{{ action('ProductoController@edit', ['id' => $producto->id]) }}" role="button" title="Editar Producto/Servicio"><span data-feather="edit"></span></a>
					@endcan
					@can('productos.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar Producto/Servicio" onclick="borrarCliente({{ $producto->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}

		<div align="center">
			@can('productos.create')
			<a class="btn btn-success" href="{{ url('/productos/create') }}" role="button"><span data-feather="plus"></span> Nuevo Producto/Servicio</a>
			@endcan     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarCliente(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Producto/Servicio "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/productos/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection