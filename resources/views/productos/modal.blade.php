<!-- Modal AÑADIR categoria -->
<div class="modal fade" id="categoriaModal" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="categoriaModalLabel">Añadir Nueva Categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(['id'=>'categoriaform', 'class'=>'form-horizontal','action'=>'CategoriaController@store', 'method'=>'POST']) }}
          <input type="hidden" name="form_pos" id="form_pos" value="1">
          <!--añade los objetos que se repiten del form -->
		  @include('categorias.categoria')

          <div align="center">
            <button type="submit" class="btn btn-success">
              <span data-feather="plus"></span>
              Añadir Categoria
            </button>
          </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal AÑADIR UNIDAD -->
<div class="modal fade" id="unidadModal" tabindex="-1" role="dialog" aria-labelledby="unidadModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="unidadModalLabel">Añadir Nueva Unidad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'unidadform', 'class'=>'form-horizontal','action'=>'UnidadController@store', 'method'=>'POST']) !!}
          <input type="hidden" name="form_pos" id="form_pos" value="1">
          <!--añade los objetos que se repiten del form -->
		  @include('unidades.unidad')

          <div align="center">
            <button type="submit" class="btn btn-success">
              <span data-feather="plus"></span>
              Añadir Unidad
            </button>
          </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal AÑADIR PROVEEDOR -->
<div class="modal fade" id="proveedorModal" tabindex="-1" role="dialog" aria-labelledby="proveedorModalLavel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="proveedorModalLavel">Añadir Nuevo Proveedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'proveedorform', 'class'=>'form-horizontal','action'=>'ProveedorController@store', 'method'=>'POST']) !!}
          <input type="hidden" name="form_pos" id="form_pos" value="1">
          <!--añade los objetos que se repiten del form -->
      @include('proveedores.proveedor')

          <div align="center">
            <button type="submit" class="btn btn-success">
              <span data-feather="plus"></span>
              Añadir Proveedor
            </button>
          </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>

@section('scrips')

<script type="text/javascript">
	// this is the id of the form
	$("#categoriaform").submit(function(e) {

		e.preventDefault(); // avoid to execute the actual submit of the form.

	    var form = $(this);
	    var url = form.attr('action');
	    
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: form.serialize(), // serializes the form's elements.
	           dataType: 'json', //convierte la respuesta a javascrip
	           success: function(data)
	           {
	               alert(data.msg); // show response from the php script.
	               $('#categoriaModal').modal('hide');

	               //cargar_clientes(clietes_url);
	    		   html_cliente = '<option selected value="'+data.id+'">'+data.nombre+'</option>';
	    		   document.getElementById("categoria_id").innerHTML += html_cliente;
	           }
	         });	    
	    
	});	

	// this is the id of the form
	$("#unidadform").submit(function(e) {

		e.preventDefault(); // avoid to execute the actual submit of the form.

	    var form = $(this);
	    var url = form.attr('action');
	    
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: form.serialize(), // serializes the form's elements.
	           dataType: 'json', //convierte la respuesta a javascrip
	           success: function(data)
	           {
	               alert(data.msg); // show response from the php script.
	               $('#unidadModal').modal('hide');

	               //cargar_clientes(clietes_url);
	    		   html_cliente = '<option selected value="'+data.id+'">'+data.nombre+'</option>';
	    		   document.getElementById("unidad_id").innerHTML += html_cliente;
	           }
	         });	    
	    
	});
</script>



@endsection