{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="form-group">
			@isset($cajas)
			@else
			{{ Form::label('Apertura:') }}				
			<div class="input-group  input-group-sm">	
				<div class="input-group-prepend">
			       	<div class="input-group-text">L.</div>
			    </div>		
				{{ Form::number('apertura',null,['class'=>'form-control','placeholder'=>'', 'id'=>'apertura','required' => 'required',"step"=>"any"]) }}
			</div>
			@endisset		
		</div>
		<div class="form-group">
			@isset($cajas)
			{{ Form::label('Cierre:') }}
			<div class="input-group input-group-sm">
				<div class="input-group-prepend">
			       	<div class="input-group-text">L.</div>
			    </div>			
				{{ Form::number('cierre',null,['class'=>'form-control','placeholder'=>'', 'id'=>'cierre','required' => 'required',"step"=>"any"]) }}
			</div>
			@endisset		
		</div>
		<div class="form-group input-group-sm">
			{{ Form::label('Nota:') }}				
			{{ Form::textarea('nota',null,['class'=>'form-control','placeholder'=>'', 'id'=>'nota','required' => 'required', 'rows'=>"4"]) }}		
		</div>

		@isset($cajas)
		
			{{ Form::label('Movimientos del Día:') }}
			<div class="table-responsive">		
				<table class="table table-striped table-bordered table-sm">

				<thead class="thead-dark">
					<tr>
						<th class="text-center">Descripción</th>
						<th class="text-center">Valor L.</th>			
					</tr>
				</thead>
				@php
					$total = 0;	
				@endphp
				@foreach($flujos as $flujo)					
					<tr>
						<td class="text-center">{{ $flujo['tipo_pago'] }}</td>
						<td class="text-right">{{  number_format($flujo['total'],2) }}</td>			
					</tr>
					@php
						$total += $flujo['total'];
					@endphp		
				@endforeach

				<thead class="thead-dark">
					<tr>
						<th class="text-center">Total</th>
						<th class="text-right">{{  number_format($total,2) }}</th>			
					</tr>
				</thead>


				</table>
			</div>
		@endisset

	</div>
</div>