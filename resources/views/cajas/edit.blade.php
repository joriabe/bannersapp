@extends('layouts.dashboard')


@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-danger col-md-8 offset-md-2  " style="width: 18rem;">
		  <div class="card-body">
		    <h5 class="card-title" align="center">Cerrar Caja</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Debe de realizar un arqueo de caja</h6>

			{{ Form::model($cajas,['route'=>['cajas.update', $cajas->id], 'method'=>'PUT']) }}

			<!--añade los objetos que se repiten del form -->
			@include('cajas.caja')

			<div align="center">
				<button type="submit" class="btn btn-danger">
					<span data-feather="minus"></span>
					Cerrar
				</button>
			</div>
	    
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop()