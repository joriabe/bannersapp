@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Cajas</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center">FECHA</th>
				<th class="text-center">AGENCIA</th>
				<th class="text-center">USUARIO</th>
				<th class="text-center">APERTURA</th>
				<th class="text-center">CIERRE</th>
				<th class="text-center">NOTA</th>	
				<th class="text-center">OPER</th>					
			</tr>
			</thead>
	
			@foreach($cajas as $caja)
			<tr>
				<td class="text-center">{{ $caja->created_at }}</td>
				<td class="text-center">{{ $caja->agencia_name }}</td>
				<td class="text-center">{{ $caja->user_name }}</td>
				<td class="text-center">{{number_format($caja->apertura,2)}}</td>
				<td class="text-center">{{number_format($caja->cierre,2)}}</td>
				<td class="text-center">{{ $caja->nota }}</td>
				<td class="text-center">

					@can('rep.flujosexcel')
						<a class="btn btn-info btn-sm" href="{{ action('CajaController@edit', ['id' => $caja->id]) }}" role="button" title="Editar Caja"><span data-feather="edit"></span></a>
					@else
						@if($caja->id ==  Session::get('caja'))
							<a class="btn btn-info btn-sm" href="{{ action('CajaController@edit', ['id' => $caja->id]) }}" role="button" title="Editar Caja"><span data-feather="edit"></span></a>
						@endif
					@endcan    
			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$cajas->render()!!}
	
	</div>

@endsection


