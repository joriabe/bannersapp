@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-success col-md-8 offset-md-2  " style="width: 18rem;">
		  <div class="card-body">
		    <h5 class="card-title" align="center">Apertura de Caja</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Debe de ingresar el monto de apertura de caja</h6>

			{{ Form::open(['id'=>'cajaform', 'class'=>'form-horizontal','action'=>'CajaController@store', 'method'=>'POST']) }}

				<!--añade los objetos que se repiten del form -->
				@include('cajas.caja')

				<div align="center">
					<button type="submit" class="btn btn-success">
						<span data-feather="plus"></span>
						Abrir Caja
					</button>
				</div>

			{{ Form::close() }}
		</div>
	</div>
</div>

@endsection