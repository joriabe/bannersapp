@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Clientes</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="200">NOMBRE</th>
				<th class="text-center" width="100">RTN</th>
				<th class="text-center" width="50">TELEFONO</th>
				<th class="text-center" width="100">EMAIL</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($listados as $cliente)
			<tr>
				<td class="text-center">{{ $cliente->codinterno }}</td>
				<td class="text-center">{{ $cliente->nombrecliente }}</td>
				<td class="text-center">{{ $cliente->rtn }}</td>
				<td class="text-center">{{ $cliente->telefono }}</td>
				<td class="text-center">{{ $cliente->correo }}</td>
				<td class="text-center">
					
					@can('clientes.edit')
					<a class="btn btn-info btn-sm" href="{{ action('ClienteController@edit', ['id' => $cliente->id]) }}" role="button" title="Editar Cliente"><span data-feather="edit"></span></a>
					@endcan
					
					@can('clientes.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar Cliente" onclick="borrarCliente({{ $cliente->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

			    	<!-- Es la misma funsion que el boton de  arriba (softdelete)
                	{{ Form::button('Eli',['class'=>'btn btn-primary','onclick' => "borrarCliente($cliente->id);"]) }}
					-->

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}
		@can('clientes.create')
		<div align="center">
			<a class="btn btn-success" href="{{ url('/clientes/create') }}" role="button"><span data-feather="user"></span> Nuevo Cliente</a>     
		</div>
		@endcan
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarCliente(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Cliente "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/clientes/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection