@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Cliente</h1>

{!! Form::model($clientes,['route'=>['clientes.update', $clientes->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('clientes.cliente')

	<div align="center">
		<button type="submit" class="btn btn-primary">
			<span data-feather="plus"></span>
			Guardar
		</button>

		<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
	</div>
    
{!! Form::close() !!}

@stop()