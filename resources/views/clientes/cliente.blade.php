<br/>
<div class="row">
	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Datos Generales</h6>
		<div class="form-group">
			{{ Form::label('ID:') }}				
			{{ Form::text('codinterno',null,['class'=>'form-control','placeholder'=>'', 'id'=>'codinterno','required' => 'required', "onKeyUp"=>"this.value=this.value.toUpperCase();"]) }}		
		</div>
		<div class="form-group">
			{{ Form::label('RTN:') }}	
			{{ Form::number('rtn',null,['class'=>'form-control','placeholder'=>'Cod. numerico de 14 dígitos', 'pattern'=>'[0-9]{14}', 'id'=>'rtn','required' => 'required', ]) }}				
		</div>
		<div class="form-group">
			{{ Form::label('Nombre Cliente:') }}	
			{{ Form::text('nombrecliente',null,['class'=>'form-control','placeholder'=>'', 'id'=>'nombrecliente','required' => 'required']) }}		
		</div>

		<div class="form-group">
			{{ Form::label('Razón Social:') }}	
			{{ Form::text('razonsocialcliente',null,['class'=>'form-control','placeholder'=>'', 'id'=>'razonsocialcliente','required' => 'required']) }}		
		</div>
	</div>

	<div class="col-6">
		<h6 class="card-subtitle mb-2 text-muted" align="center">Datos de Contacto</h6>

		<div class="form-group">
			{{ Form::label('Direcció:') }}	
			{{ Form::text('direccion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'direccion','required' => 'required']) }}			
		</div>

		<div class="form-group">
			{{ Form::label('Teléfono:') }}	
			{{ Form::text('telefono',null,['class'=>'form-control','placeholder'=>'', 'id'=>'telefono']) }}			
		</div>

		<div class="form-group">
			{{ Form::label('Email:') }}	
			{{ Form::email('correo',null,['class'=>'form-control','placeholder'=>'', 'id'=>'correo']) }}			
		</div>		
	</div>
</div>