@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-10 offset-md-1" style="width: 18rem;">
	 	<div class="card-body">
		    <h5 class="card-title"  align="center">Nuevo Cliente</h5>

			{{  Form::open(['id'=>'clienteform', 'class'=>'form-horizontal','action'=>'ClienteController@store', 'method'=>'POST']) }}

			<input type="hidden" name="form_pos" id="form_pos" value="0">
			<!--añade los objetos que se repiten del form -->
			@include('clientes.cliente')

			<div align="center">
				<button type="submit" class="btn btn-primary">
					<span data-feather="plus"></span>
					Añadir
				</button>

				<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
			</div>
			
			{{ Form::close() }}
		</div>	
	</div>
</div>

@endsection