@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Datos de Facturas</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">AGENCIA</th>
				<th class="text-center" width="100">CAI</th>
				<th class="text-center" width="50">FECHA LIMITE</th>
				<th class="text-center" width="10">RANGO INFERIOR</th>
				<th class="text-center" width="100">RANGO SUPERIOR</th>
				<th class="text-center" width="50">CORRELATIVO</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($datofacturas as $datofactura)
			<tr>
				<td class="text-center">{{ $datofactura->codigointerno }}</td>
				<td class="text-center">{{ $datofactura->kai }}</td>
				<td class="text-center">{{ $datofactura->fechalimite }}</td>
				<td class="text-center">{{ $datofactura->rangoinferior }}</td>
				<td class="text-center">{{ $datofactura->ragosuperior }}</td>
				<td class="text-center">{{ $datofactura->correlativof }}</td>
				<td class="text-center">
					
					@can('datofacturas.edit')
					<a class="btn btn-info btn-sm" href="{{ action('DatoFacturaController@edit', ['id' => $datofactura->id]) }}" role="button" title="Editar Datos de facturación."><span data-feather="edit"></span></a>
					@endcan

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$datofacturas->render()!!}

		<div align="center">
			@can('datofacturas.create')
			<a class="btn btn-success" href="{{ url('/datofacturas/create') }}" role="button"><span data-feather="user"></span> Nuevo Dato de Facturación</a>     
			@endcan
		</div>
	</div>

@endsection

