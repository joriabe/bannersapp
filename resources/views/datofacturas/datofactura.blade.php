{{ csrf_field() }}

{{ Form::hidden('id',null,['id'=>'id']) }}

</br>
<div class="row">
	<div class="col-6">
		<div class="form-group">
			{{ Form::label('Agencia:') }}	
			{{ Form::select('agencia_id',$agencias,null,['class'=>'js-example-basic-single form-control', 'id'=>'agencia_id','required' => 'required']) }}			
		</div>

		<div class="form-group">
			{{ Form::label('CAI:') }}	
			{{ Form::text('kai',null,['class'=>'form-control','placeholder'=>'Código alfanumerico de 37 caracteres', 'title' => 'Ej. Z000Z0-00Z00Z-0000Z0-ZZ0ZZ0-Z0Z0Z0-Z0', 'id'=>'kai','pattern'=>'^[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{2}$', 'required' => 'required',"onKeyUp"=>"this.value=this.value.toUpperCase();"]) }}				
		</div>

		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{{ Form::label('Rango de Factura Inferior:') }}				
					{{ Form::text('rangoinferior',null,['class'=>'form-control','placeholder'=>'Código numerico de 19 caracteres', 'title' => 'Ej. 000-000-00-00000000', 'id'=>'rangoinferior','pattern'=>'^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{8}$','required' => 'required']) }}	
				</div>
			</div>

			<div class="col-6">
				<div class="form-group">
					{{ Form::label('Rango de Factura Superior:') }}				
					{{ Form::text('ragosuperior',null,['class'=>'form-control','placeholder'=>'Código numerico de 19 caracteres', 'title' => 'Ej. 000-000-00-00000000', 'id'=>'ragosuperior','pattern'=>'^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{8}$','required' => 'required']) }}
				</div>
			</div>
		</div>

	</div>

	<div class="col-6">

		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{{ Form::label('Fecha Limite de Emisión:') }}	

					@isset($datofacturas)
					{{ Form::date('fechalimite', $datofacturas->fechalimite,['class'=>'form-control', 'id'=>'fechalimite','required' => 'required']) }}
					@else
					{{ Form::date('fechalimite', null,['class'=>'form-control', 'id'=>'fechalimite','required' => 'required']) }}
					@endisset			
				</div>
			</div>

			<div class="col-6">				
				<div class="form-group">
					@isset($datofacturas)
					@else
						{{ Form::label('Inicio del Correlativo:') }}	
						{{   Form::number('correlativof',null,['class'=>'form-control','placeholder'=>'9999', 'id'=>'correlativof', 'title' => 'Inicio de la secuencia para la numeración de la facturas.']) }}
					@endisset
					<small id="correlativofHelp" class="form-text text-muted">Numero actual de la facturacion.</small>		
				</div>				
			</div>
		</div>


		<div class="form-group">
			{{ Form::label('Frase:') }}	
			{{ Form::text('frase',null,['class'=>'form-control','placeholder'=>'', 'id'=>'frase']) }}			
		</div>

		<div class="form-group">
			{{ Form::label('Logo para Factura:') }}

			@isset($datofacturas)
			{{ Form::file('logo', ['class'=>'form-control','type'=>'file', 'id'=>'logo']) }}
			@else
			{{ Form::file('logo', ['class'=>'form-control','type'=>'file', 'id'=>'logo','required' => 'required']) }}
			@endisset			
		</div>		
	</div>
</div>