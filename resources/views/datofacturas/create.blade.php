@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-10 offset-md-1" style="width: 18rem;">
	 	<div class="card-body">
		    <h5 class="card-title"  align="center">Datos para Facturación</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Información necesaria para generar las facturas</h6>


			{!! Form::open(['id'=>'datofacturaform', 'class'=>'form-horizontal','action'=>'DatoFacturaController@CrearoActualizar', 'method'=>'POST','enctype' => 'multipart/form-data']) !!}

				<!--añade los objetos que se repiten del form -->
				@include('datofacturas.datofactura')

				<div align="center">
					<button type="submit" class="btn btn-primary">
						<span data-feather="plus"></span>
						Añadir
					</button>

					<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
				</div>

			{!! Form::close() !!}

		</div>	
	</div>
</div>

@endsection