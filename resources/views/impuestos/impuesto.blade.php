{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="form-group">
			{{ Form::label('Descripción:') }}	
			{{ Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'descripcion','required' => 'required']) }}				
		</div>
		<div class="form-group">
			{{ Form::label('Impuesto:') }}
			<div class="input-group">	
			{{ Form::number('impuesto',null,['class'=>'form-control','placeholder'=>'', 'id'=>'impuesto','required' => 'required',"step"=>"any"]) }}
				<div class="input-group-append">
				   	<div class="input-group-text">%</div>
				</div>
			</div>
		</div>
	</div>
</div>