@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Impuestos</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">IMPUESTO</th>
				<th class="text-center" width="450">DESCRIPCIÓN</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($impuestos as $impuesto)
			<tr>
				<td class="text-center">{{ $impuesto->impuesto }}</td>
				<td class="text-center">{{ $impuesto->descripcion }}</td>
				<td class="text-center">
					@can('impuestos.edit')
					<a class="btn btn-info btn-sm" href="{{ action('ImpuestoController@edit', ['id' => $impuesto->id]) }}" role="button" title="Editar impuesto"><span data-feather="edit"></span></a>
					@endcan
					@can('impuestos.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar impuesto" onclick="borrar({{ $impuesto->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$impuestos->render()!!}

		<div align="center">
			@can('impuestos.create')
			<a class="btn btn-success" href="{{ url('/impuestos/create') }}" role="button"><span data-feather="plus"></span> Nuevo Impuesto</a>
			@endcan     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrar(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Impuesto "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/impuestos/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection