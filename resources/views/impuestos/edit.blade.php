@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-8 offset-md-2" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Impuestos</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Impuesto aplicable a productos/servicios</h6>

			{!! Form::model($impuestos,['route'=>['impuestos.update', $impuestos->id], 'method'=>'PUT']) !!}

				<!--añade los objetos que se repiten del form -->
				@include('impuestos.impuesto')

				<div align="center">
					<button type="submit" class="btn btn-primary">
						<span data-feather="plus"></span>
						Guardar
					</button>

					<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
				</div>
			    
			{!! Form::close() !!}
		</div>
	</div>
</div>

@stop()