@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Facturas</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>
	

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="100">COD</th>
				<th class="text-center" width="50">FECHA</th>
				<th class="text-center" width="200">CLIENTE</th>
				<th class="text-center" width="100">SUB TOTAL</th>
				<th class="text-center" width="100">TOTAL</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($listados as $factura)
			<tr>
				<td class="text-left">{{ $factura->codfactura }}

					@if(!is_null($factura->deleted_at))
						<span class="badge badge-pill badge-danger">Anulada</span>
					@endif
				</td>
				<td class="text-center">{{ $factura->fecha }}</td>
				<td class="text-center">{{ $factura->nombrecliente }}</td>
				<td class="text-center">{{ number_format($factura->totalantesimpuesto,2) }}</td>
				<td class="text-center">{{ number_format($factura->totaldespuesimpuesto,2) }}</td>
				<td class="text-left">
					<!--
					@can('facturas.edit')
					<a class="btn btn-info btn-sm" href="{{ action('FacturaController@edit', ['id' => $factura->id]) }}" role="button" title="Editar Factura"><span data-feather="edit"></span></a>
					@endcan
					-->

					@can('facturas.delete')
					@if(is_null($factura->deleted_at))
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Anular Factura" onclick="borrarFactura({{ $factura->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endif
			    	@endcan

			    	<a class="btn btn-info btn-sm" href="{{ action('FacturaController@show', ['id' => $factura->id]) }}" role="button" title="Imprimir Factura" target="_blank"><span data-feather="file"></span></a>

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}


		<div align="center">
			<a class="btn btn-success" href="{{ url('/pos') }}" role="button"><span data-feather="plus"></span> Nueva Factura</a>

			@can('flujos.create')
			<a class="btn btn-info" href="{{ url('/flujos/create') }}" role="button"><span data-feather="edit"></span>Corregir Flujo</a>
			@endcan
			
			@can('rep.facturasexcel')
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#facturas_excel">
			<span data-feather="download"></span> Descargar Excel
			</button>
			@endcan        
		</div>
	</div>


<!-- Modal -->
<div class="modal fade" id="facturas_excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Descargar Excel Facturas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('facturasexcel') }}"  method="POST" name="form1" id="form1">
		{{ csrf_field() }}
      <div class="modal-body">
        <div class="form-group col-md-6">
		    <label for="input_fechai">Fecha Incial:</label>
		    <input class="form-control" type="date" name="input_fechai" id="input_fechai">
		 </div>
		 <div class="form-group col-md-6">
		    <label for="input_fechaf">Fecha Final:</label>
		    <input class="form-control" type="date" name="input_fechaf" id="input_fechaf">
		 </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">
			<span data-feather="download"></span>
			Descargar
		</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrarFactura(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Anular la Factura "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/facturas/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection