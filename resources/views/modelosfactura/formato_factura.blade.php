<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>Factura-{{ $factura->codfactura }}</title>
<style>
div {
  text-align: center;
}
p {
  font-size: 11pt;
  margin-bottom: -10px;
}

td, th {
  font-size: 10pt;
}
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
body  {
  font-family: "Courier New", Courier, monospace; 
}
</style>
</head>
<body style="width: 280px;" onload="window.print()">
<?php 
  $fecha_factura = date("d-m-Y", strtotime($factura->fecha));
  $fecha_limite = date("d-m-Y", strtotime($datosf->fechalimite)); 
  $items = 0; 
?>

<div>
<p><b>{{ $cia->nombrecomercial }}</b></p>   
<p>{{ $cia->razonsocial }}</p>    
<p>{{ $cia->direccion }}</p>    
<p>Correo Electronico: {{ $cia->correo }}</p>   
<p><b>R.T.N.: {{ $cia->rtn }}</b></p>    
<p>Tel: {{ $cia->telefono }}</p>
</div>
<br>

<table>
  <tr>
    <td>C.A.I.: {{ $datosf->kai }}</td>
  </tr>
</table>

<table style="text-align: left; width: 100%;" cellpadding="1" cellspacing="0">
  <tr>
    <td>FACTURA: {{ $factura->codfactura }}</td>    
  </tr>
  <tr>
    <td>FECHA: {{ $fecha_factura }}</td>   
  </tr>  
  <tr>
    <td>CLIENTE: {{ $cliente->razonsocialcliente }}</td>
  </tr>
  <tr>
    <td>RTN: {{ $cliente->rtn }}</td>
  </tr>
</table>


<p style="text-align: center;"><b><small>DETALLE DE COMPRA</small></b></p>
<hr>
<table style="text-align: left; width: 100%;" cellpadding="1" cellspacing="0">
  <tr>
    <th style="width: 10%; text-align: left;">UDS</th>
    <th style="width: 45%; text-align: left;">DESCRIPCION</th>
    <th style="width: 20%; text-align: center;">PxU</th>
    <th style="width: 25%; text-align: right;">IMPORTE</th>
  </tr>
  @foreach($detalles as $detalle)
  <tr>
    <td style="text-align: left;">{{ $detalle->cantidad }}</td>
    <td style="text-align: left;">{{ $detalle->descripcion }}</td>
    <td style="text-align: center;">{{ $detalle->precio }}</td>
    <td style="text-align: right;">{{ number_format($detalle->subtotal, 2, '.', ',') }}</td>
  </tr>
  <?php
    $items++; 
  ?>
  @endforeach
</table>
<hr>
<p style="margin-top: -5px; margin-bottom: 0px;"><small>Items en factura: {{ $items }}</small></p>
<table style="text-align: left; width: 80%; margin-right: 0px; margin-left: auto;" cellpadding="1" cellspacing="0">
  @foreach($pies as $pie)
  <tr>
    <td>Subtotal</td>
    <td>{{ $pie->descripcion }}</td>
    <td style="text-align: right;">{{ number_format($pie->totalimpuesto, 2, '.', ',') }}</td>
  </tr>
  @endforeach
  @foreach($pies as $pie)
    @if($pie->calculoimpuesto > 0)
    <tr>
      <td></td>
      <td>{{ $pie->descripcion }}</td>
      <td style="text-align: right;">{{ number_format($pie->calculoimpuesto, 2, '.', ',') }}</td>
    </tr>
    @endif  
  @endforeach
  @if($factura->descuento > 0)
  <tr>
    <td></td>
    <td>Descuentos</td>
    <td style="text-align: right;">{{ number_format($factura->descuento, 2, '.', ',') }}</td>
  </tr>
  @endif
</table>
<p style="text-align: center;"><b><small>DETALLE DE PAGO</small></b></p>
<hr>
<table style="text-align: left; width: 80%; margin-right: 0px; margin-left: auto;" cellpadding="1" cellspacing="0">
  <tr>
    <td><b>TOTAL A PAGAR</b></td>
    <td style="text-align: right;"><b>{{ number_format($factura->totaldespuesimpuesto, 2, '.', ',') }}</b></td>
  </tr>
  <tr>
    <td>EFECTIVO</td>
    <td style="text-align: right;">{{ number_format($factura->pagado, 2, '.', ',') }}</td>
  </tr>
  <tr>
    <td>CAMBIO</td>
    <td style="text-align: right;">{{ number_format($factura->cambio, 2, '.', ',') }}</td>
  </tr>
</table>
<br>
<div>
<p>** {{ $datosf->frase }} **</p>
</div>
<br>
<p><b>Rango Autorizado:</b> {{ $datosf->rangoinferior }}</p>
<p><b>a la</b> {{ $datosf->ragosuperior }}</p>
<p><b>Fecha Limite:</b> {{ $fecha_limite }} </p>

</body>
</html>
