@include('modelosfactura.conversor')
<!DOCTYPE html>
<html>
<head>
	<title>Cotizacion-{{ $cotizacion->cod_interno }}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<style type="text/css">
		p {
			margin-bottom: -3px;
		}
		hr {
			width: 100%;
			color: gray;
			height: 1px;
			background-color:gray;
			margin-top: 10px;
		}
	</style>
</head>
<body style="width: 815px;" onload="window.print()">
<?php 
  $fecha_factura = date("d-m-Y h:i:s a", strtotime($cotizacion->fecha));
  $letras = convertir($cotizacion->totaldespuesimpuesto);
?>

<div class="row">
	<div class="col-md-4 text-center">
		<img src="{{ $logo }}" class="img-fluid" alt="Logo Agencia">
		<h6>{{ $cia->web }}</h6>
		<h6>{{ $cia->razonsocial }}</h6>
	</div>
	<div class="col-md-4 text-center">
		<h5>{{ $cia->nombrecomercial }}</h5>
		<h6>{{ $cia->correo }}</h6>
		<h6>{{ $cia->direccion }}</h6>
		<h6>Tel: {{ $cia->telefono }}</h6>			
	</div>
	<div class="col-md-4 text-center">
		<div class="card">
		  <div class="card-body">
		    <h6>COTIZACION: {{ $cotizacion->cod_interno }}</h6>
			<h6>R.T.N.: {{ $cia->rtn }}</h6>
		  </div>
		</div>				
	</div>	
</div>
<br>
<div class="row">
	<div class="col-md-12">
		<div class="card-group">
			<div class="card">
			  <div class="card-body">			  	
		  		<p><b>CLIENTE:</b> {{ $cliente->nombrecliente }}</p>
		  		<p><b>RTN:</b> {{ $cliente->rtn }}</p>		  		
		  		<p><b>DIRECCION:</b> {{ $cliente->direccion }}</p>			  	
			  </div>
			</div>
			<div class="card">
			  <div class="card-body">			  	
		  		<p><b>FECHA:</b> {{ $fecha_factura }}</p>
			  	<p><b>VENDEDOR:</b> {{ $cotizacion->name }}</p>
			  	@isset($cotizacion->fechavencimiento)
				    <p><b>F. VENCIMIENTO:</b> {{ date("d-m-Y", strtotime($cotizacion->fechavencimiento)) }}</p>
				@endisset			  			  	
			  </div>
			</div>			  
		</div>
	</div>	
</div>
<br>
<div class="row">
	<div class="col-md-12">
			<table class="table table-sm table-bordered" style="width: 100%;" cellpadding="1" cellspacing="0">
			  <tr>
			  	<th style="width: 5%; text-align: center;">No.</th>
			  	<th style="width: 5%; text-align: left;">CODIGO</th>			    
			    <th style="width: 40%; text-align: left;">DESCRIPCION</th>
			    <th style="width: 10%; text-align: center;">UNIDAD</th>
			    <th style="width: 20%; text-align: center;">PRECIO</th>
			    <th style="width: 20%; text-align: right;">IMPORTE</th>
			  </tr>
			  @foreach($detalles as $detalle)
			  <tr>
			  	<td style="text-align: center;">{{ $loop->iteration }}</td>
			  	<td style="text-align: left;">{{ $detalle->p_id }}</td>			    
			    <td style="text-align: left;">{{ $detalle->descripcion }}</td>
			    <td style="text-align: center;">{{ $detalle->cantidad }}</td>
			    <td style="text-align: center;">{{ number_format($detalle->precio, 2, '.', ',') }}</td>
			    <td style="text-align: right;">{{ number_format($detalle->subtotal, 2, '.', ',') }}</td>
			  </tr>
			 @endforeach
			 <tr>
			 	<td class="align-middle text-justify" colspan="3">
			 		<h6>IMPORTE EN LETRAS: {{ $letras }}</h6>
			 	</td>
			 	<td colspan="2">
			 		@foreach($pies as $pie)
						<p style="text-align: right;">Subtotal Gravado {{ $pie->descripcion }}:</p>				  
					@endforeach
					@foreach($pies as $pie)
						<p style="text-align: right;">{{ $pie->descripcion }}:</p>						
					@endforeach
					@if($cotizacion->descuento > 0)
						<p style="text-align: right;">Descuentos:</p>
					@endif
					<h6 style="margin-top: 2px" class="text-right">TOTAL A PAGAR</h6>										
			 	</td>
			 	<td>
			 		@foreach($pies as $pie)
			 		<p style="text-align: right;">{{ number_format($pie->totalimpuesto, 2, '.', ',') }}</p>
			 		@endforeach
			 		@foreach($pies as $pie)
			 		<p style="text-align: right;">{{ number_format($pie->calculoimpuesto, 2, '.', ',') }}</p>
			 		@endforeach
			 		@if($cotizacion->descuento > 0)
						<p style="text-align: right;">{{ number_format($cotizacion->descuento, 2, '.', ',') }}</p>
					@endif
					<h6 style="margin-top: 2px" class="text-right">{{ number_format($cotizacion->totaldespuesimpuesto, 2, '.', ',') }}</h6>				 		
			 	</td>
			 </tr>
			</table>			
		</div>	
</div>
<div class="row">
	<div class="col-md-6 text-center">
		<br><br>
		<p>________________________</p>
		<p>FIRMA CLIENTE</p>
	</div>
	<div class="col-md-6 text-center">
		<br><br>
		<p>________________________</p>
		<p>FIRMA VENDEDOR</p>
	</div>
	<div class="col-md-4">
		
	</div>	
</div>
<div class="row">
	<hr>
	<div class="col-md-12">
		<p class="text-justify">NOTA: LOS PRECIOS PUEDEN VARIAR DE ACUERDO A LA DISPONIBILIDAD DE MATERIALES.</p>
		<br>
		<p class="text-center">{{ $cotizacion->observaciones }}</p>
	</div>
</div>



</body>
</html>

