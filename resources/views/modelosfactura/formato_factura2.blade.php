<!DOCTYPE html>
<html>
<head>
	<title>Factura-{{ $factura->codfactura }}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<style type="text/css">
		p {
			margin-bottom: -3px;
		}
		hr {
			width: 100%;
			color: gray;
			height: 1px;
			background-color:gray;
			margin-top: -3px;
		}
	</style>
</head>
<body style="width: 815px;" onload="window.print()">
<?php 
  $fecha_factura = date("d-m-Y h:i:s a", strtotime($factura->fecha));
  $fecha_limite = date("d-m-Y", strtotime($datosf->fechalimite)); 
  $items = 0; 
?>
	<div class="row">
		<div class="col-md-12">
		<table class="w-100">
			<tr>
				<td>
					<img style="width: 300px;" src="{{ $datosf->logo }}" alt="Logo Compañia">
				</td>
				<td class="text-right">
					<h5>{{ $cia->nombrecomercial }}</h5>
					<p>{{ $cia->razonsocial }}</p>
					<p>{{ $cia->direccion }}</p>
					<p>Correo Electronico: {{ $cia->correo }}</p>
					<p>R.T.N.: {{ $cia->rtn }}</p>
					<p>Tel: {{ $cia->telefono }}</p>
					<p>C.A.I.: {{ $datosf->kai }}</p>
				</td>
			</tr>
		</table>
		</div>
	</div>
	<br><br>
	<div class="row text-left">
		<div class="col-md-12">
			<p><b>FACTURA:</b> {{ $factura->codfactura }}</p>
			<p><b>FECHA:</b> {{ $fecha_factura }}</p>
			<p><b>CLIENTE:</b> {{ $cliente->nombrecliente }}</p>
			<p><b>RTN:</b> {{ $cliente->rtn }}</p>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12 text-center">
			<h6>DETALLE DE COMPRA</h6>
		</div>
		<div class="col-md-12">
			<table class="table table-sm" style="width: 100%;" cellpadding="1" cellspacing="0">
			  <tr>
			    <th style="width: 10%; text-align: left;">UDS</th>
			    <th style="width: 45%; text-align: left;">DESCRIPCION</th>
			    <th style="width: 20%; text-align: center;">PxU</th>
			    <th style="width: 25%; text-align: right;">IMPORTE</th>
			  </tr>
			  @foreach($detalles as $detalle)
			  <tr>
			    <td style="text-align: left;">{{ $detalle->cantidad }}</td>
			    <td style="text-align: left;">{{ $detalle->descripcion }}</td>
			    <td style="text-align: center;">{{ $detalle->precio }}</td>
			    <td style="text-align: right;">{{ number_format($detalle->subtotal, 2, '.', ',') }}</td>
			  </tr>
			  <?php
			    $items++; 
			  ?>
			  @endforeach
			</table>
		</div>
	</div>
	<hr>
	<p style="margin-top: -5px; margin-bottom: 0px;">Items en factura: {{ $items }}</p>
	<div class="row">
		<div class="col-md-6">				
		</div>
		<div class="col-md-6">
			<table style="text-align: left; width: 100%; margin-right: 0px; margin-left: auto;" cellpadding="1" cellspacing="0">
				@foreach($pies as $pie)
				  <tr>
				    <td>Subtotal</td>
				    <td>{{ $pie->descripcion }}</td>
				    <td style="text-align: right;">{{ number_format($pie->totalimpuesto, 2, '.', ',') }}</td>
				  </tr>
				  @endforeach
				  @foreach($pies as $pie)
				    @if($pie->calculoimpuesto > 0)
				    <tr>
				      <td></td>
				      <td>{{ $pie->descripcion }}</td>
				      <td style="text-align: right;">{{ number_format($pie->calculoimpuesto, 2, '.', ',') }}</td>
				    </tr>
				    @endif  
				  @endforeach
				  @if($factura->descuento > 0)
				  <tr>
				    <td></td>
				    <td>Descuentos</td>
				    <td style="text-align: right;">{{ number_format($factura->descuento, 2, '.', ',') }}</td>
				  </tr>
				@endif
			</table>
			
		</div>
		
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			
		</div>
		<div class="col-md-6">
			<h6 style="text-align: center;">DETALLE DE PAGO</h6>
			<hr>
			<table style="text-align: left; width: 100%; margin-right: 0px; margin-left: auto;" cellpadding="1" cellspacing="0">
			  <tr>
			    <td><b>TOTAL A PAGAR</b></td>
			    <td style="text-align: right;"><b>{{ number_format($factura->totaldespuesimpuesto, 2, '.', ',') }}</b></td>
			  </tr>
			  <tr>
			    <td>EFECTIVO</td>
			    <td style="text-align: right;">{{ number_format($factura->pagado, 2, '.', ',') }}</td>
			  </tr>
			  <tr>
			    <td>CAMBIO</td>
			    <td style="text-align: right;">{{ number_format($factura->cambio, 2, '.', ',') }}</td>
			  </tr>
			</table>
		</div>		
	</div>
	<br><br>
	<div class="row text-center">
		<div class="col-md-12">
			<p>** {{ $datosf->frase }} **</p>
			<br>
			<p><b>Rango Autorizado:</b> {{ $datosf->rangoinferior }} <b>a la</b> {{ $datosf->ragosuperior }}</p>
			<p><b>Fecha Limite:</b> {{ $fecha_limite }} </p>
		</div>
		
	</div>

</body>
</html>