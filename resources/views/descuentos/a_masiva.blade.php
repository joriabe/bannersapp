@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Asinacion Masiva de Descuentos</h1>
<br>
<div class="row container-fluid">

	<div class="col-md-6">
		<h6>Busque y Seleccione los Productos a Asignar el Descuento:</h6>
		<h6><small class="text-muted">Busqueda por categoria, nombre o codigo del producto.</small></h6>	
		<form class="form-inline my-2 my-lg-0" name="form1" id="form1" enctype="multipart/form-data" onsubmit="buscarProducto('{{url('descuento/amasiva/')}}', event);">
	      	<input class="form-control mr-sm-2" id="i_buscar" type="text" placeholder="Buscar" aria-describedby="buscarhelp">
	      	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
	    </form>
	    <button style="float: right;" class="btn btn-outline-info my-2 my-sm-0" title="Seleccionar Todos" onclick="cambio_todos();"><span data-feather="arrow-right"></span></button>
	    <small id="buscarhelp" 	class="form-text text-muted">Para mostrar todos los prodcutos, escribir "all" o "todos".</small>
	    <br>
	    <div id="busca_productos">
	      		
	    </div>

	</div>

	<div class="col-md-6">
		<h6>Seleccione el Descuento a Aplicar a los Productos:</h6>
		<h6><small class="text-muted">El descuento se aplicara a todos los producto marcados.</small></h6>
		<form action="{{ url('descuentos/amasiva') }}" method="POST" name="form2" id="form2" enctype="multipart/form-data">
		{{ csrf_field() }}
		
		<div class="row">
			<div class="col-8">
				{{  Form::select('descuento_id',$descuentos,null,['class'=>'js-example-basic-single form-control', 'id'=>'descuento_id','required' => 'required']) }}
				<small class="form-text text-muted">Se muestran los precios sin % ISV y sin % Descuento.</small>
			</div>


			<div class="col-4">
				<button type="submit" class="btn btn-success">
					<span data-feather="plus"></span>
					Asignar
				</button>
			</div>
		</div>

		<br>
		<div style="margin-top: 10px" id="productos">
	      		
	    </div>

		</form>
	</div>

</div>


@endsection

@section('scrips')

	<script type="text/javascript">

	function buscarProducto(url, e){

		e.preventDefault()

		if (document.getElementById("i_buscar").value.trim() == "") { return; }

		var producto_buscar = url + '/' + document.getElementById("i_buscar").value.trim();
		
		//alert(producto_buscar);

		//AJAX
		$.get(producto_buscar, function (data){
			

			if (data.length == 0){
				alert("No se Encontraron Coincidencias");

			}else{
				//creamos el contenido
				var html_productos = '<ul id="lista_p" class="list-group list-group-flush">';
				for (var i=0; i<data.length; ++i){
					html_productos += '<li href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" id="'+data[i].p_id+'" title="'+data[i].descripcion+'" onclick="cambio(this);">'+data[i].descripcion+'<span class="badge badge-primary badge-pill">'+data[i].precio+' L.</span></li>';

				}
				html_productos += '</ul>';
				
				document.getElementById("busca_productos").innerHTML = html_productos;
								
			}
		});
		
	}

	function cambio(thi){

		var precio = thi.children[0].innerHTML;

		html_cbox = '<div class="form-check"><input class="form-check-input" type="checkbox" name="productos[]" value="'+thi.id+'" checked><label class="form-check-label" for="defaultCheck1">'+thi.title+' -- '+precio+'</label></div>'

		document.getElementById("productos").innerHTML += html_cbox;

		thi.remove();

	}
	function cambio_todos(){

		li = document.getElementById("lista_p").children;

		var i;
		var html_cbox = "";

		for (i = 0; i < li.length; i++) {

			var precio = li[i].children[0].innerHTML;

	        html_cbox = '<div class="form-check"><input class="form-check-input" type="checkbox" name="productos[]" value="'+li[i].id+'" checked><label class="form-check-label" for="defaultCheck1">'+li[i].title+' -- '+precio+'</label></div>';

	        document.getElementById("productos").innerHTML += html_cbox;
	    }

	    document.getElementById("busca_productos").innerHTML = "";

	}

	</script>




@endsection
