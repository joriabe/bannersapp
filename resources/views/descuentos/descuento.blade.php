{{ csrf_field() }}
</br>
<div class="row">
	<div class="col-12">
		<div class="row">
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Fecha Inicio:') }}	

					@isset($descuentos)
					{{ Form::date('fecha_inicio', $descuentos->fecha_inicio,['class'=>'form-control', 'id'=>'fecha_inicio','required' => 'required']) }}
					@else
					{{ Form::date('fecha_inicio', null,['class'=>'form-control', 'id'=>'fecha_inicio','required' => 'required']) }}
					@endisset			
				</div>
			</div>
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('Fecha Fin:') }}	

					@isset($descuentos)
					{{ Form::date('fecha_fin', $descuentos->fecha_fin,['class'=>'form-control', 'id'=>'fecha_fin','required' => 'required']) }}
					@else
					{{ Form::date('fecha_fin', null,['class'=>'form-control', 'id'=>'fecha_fin','required' => 'required']) }}
					@endisset			
				</div>
			</div>

			<div class="col-4">	
				<div class="form-group">
					{{ Form::label('Descueto:') }}
					<div class="input-group">	
					{{ Form::number('descuento',null,['class'=>'form-control','placeholder'=>'', 'id'=>'descuento','required' => 'required',"step"=>"any"]) }}	
						<div class="input-group-append">
					       	<div class="input-group-text">%</div>
						</div>		
					</div>
				</div>
			</div>
		</div>


		<div class="form-group">
			{{ Form::label('Descripcion:') }}	
			{{ Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'descripcion','required' => 'required', 'maxlength'=>'40']) }}		
		</div>
	</div>

	<div class="col-6">
	</div>
</div>