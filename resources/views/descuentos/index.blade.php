@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Descuentos</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">FECHA IN</th>
				<th class="text-center" width="50">FECHA FN</th>
				<th class="text-center" width="350">DESCRIPCIÓN</th>
				<th class="text-center" width="50">DESCUENTO (%)</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($descuentos as $descuento)
			<tr>
				<td class="text-center">{{ $descuento->fecha_inicio }}</td>
				<td class="text-center">{{ $descuento->fecha_fin }}</td>
				<td class="text-center">{{ $descuento->descripcion }}</td>
				<td class="text-center">{{ $descuento->descuento }}</td>
				<td class="text-center">

					@can('descuentos.edit')
					<a class="btn btn-info btn-sm" href="{{ action('DescuentoController@edit', ['id' => $descuento->id]) }}" role="button" title="Editar Descuento"><span data-feather="edit"></span></a>
					@endcan
					
					@can('descuentos.delete')
					@if(null===$descuento->deleted_at)
						<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Desactivar Descuento" onclick="desactivarUser({{ $descuento->id }});"><span data-feather="x"></span>
				    	</button>
				    @else
				    	<button style="float: right;" type="button" class="btn btn-success btn-sm" title="Reactiva Descuento" onclick="activarUser({{ $descuento->id }});"><span data-feather="check"></span>
				    	</button>
				    @endif
				    @endcan

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$descuentos->render()!!}
	</div>
	<div align="center">
		
			@can('descuentos.create')
			<a class="btn btn-success" href="{{ url('/descuentos/create') }}" role="button"><span data-feather="plus"></span> Nuevo Descuento</a>
			@endcan     


			@can('descuentos.edit')
			<a class="btn btn-primary" href="{{ url('/descuento/amasiva') }}" role="button"><span data-feather="plus"></span> Asginación Masiva</a>
			@endcan     
	
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function desactivarUser(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Desactivar el Descuento: "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/descuentos/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

	<script type="text/javascript">
		function activarUser(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Activar el Descuento: "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/descuentos/activar') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection