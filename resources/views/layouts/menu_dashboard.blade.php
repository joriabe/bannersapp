<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="#">
          <span data-feather="home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file"></span>
          Orders
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/productos') }}">
          <span data-feather="shopping-cart"></span>
          Productos
        </a>
      </li>

      @can('clientes.index')

      <li class="nav-item">
        <a class="nav-link" href="{{ url('/clientes') }}">
          <span data-feather="users"></span>
          Clientes
        </a>
      </li>

      @endcan

      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="bar-chart-2"></span>
          Reports
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/usuarios') }}">
          <span data-feather="user-check"></span>
          Usuarios
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdown1" aria-expanded="false" data-toggle="collapse">
          <span data-feather="layers"></span>
          Compañias
        </a>
        <ul id="navbarDropdown1" class="collapse list-unstyled">
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/cias') }}">
              <span data-feather="list"></span> 
              Listado Compañias
            </a>
          </li> 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cias.create') }}">
              <span data-feather="briefcase"></span> 
              Nueva Compañia
            </a>
          </li>                                  
        </ul>
       
      </li>

        

    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Saved reports</span>
      <a class="d-flex align-items-center text-muted" href="#">
        <span data-feather="plus-circle"></span>
      </a>
    </h6>
    <ul class="nav flex-column mb-2">
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Current month
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Last quarter
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Social engagement
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Year-end sale
        </a>
      </li>
    </ul>
  </div>
</nav>