@extends('layouts.dashboard')

@section('content')


	<div class="card">
		<h5 class="card-header" align="center">Opciones del Sistema</h5>
		
		<div class="card-body">
		   
			<div class="row">
				<div class="col-4">
					
					<div class="card">
	  					<h6 class="card-header"><span data-feather="list"></span> Productos/Servicios</h6>
	  					<div class="card-body">
		
		    				<ul>
		    					@can('productos.index')
					  			<li style="margin-left: 10px" class="nav-item"> 
					  				<a class="nav-link" href="{{ route('productos.index') }}"">
             						Listado de Productos</a>
        						</li>
        						@endcan

        						@can('categorias.index')
					  			<li style="margin-left: 10px" class="nav-item"> 
					  				<a class="nav-link" href="{{ route('categorias.index') }}"">
             						Listado de Categorías</a>
        						</li>
        						@endcan

        						@can('descuentos.index')
					  			<li style="margin-left: 10px" class="nav-item"> 
					  				<a class="nav-link" href="{{ route('descuentos.index') }}"">
             						Listado de Descuentos</a>
        						</li>
        						@endcan

        						@can('unidades.index')
					  			<li style="margin-left: 10px" class="nav-item"> 
					  				<a class="nav-link" href="{{ route('unidades.index') }}"">
             						Listado de Unidades</a>
        						</li>
        						@endcan
							</ul>  
	 				 	</div>
					</div>

				</div>

			</div>
		</div>
	</div>



@endsection