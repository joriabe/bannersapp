    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="#">
          <span data-feather="home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownFactura-User" aria-expanded="false" data-toggle="collapse">
          <span data-feather="dollar-sign"></span>
          Facturación
        </a>
        <ul id="navbarDropdownFactura-User" class="collapse list-unstyled">
          @can('facturas.create') 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('pos.index') }}">
              <span data-feather="plus"></span> 
              Nueva Factura
            </a>
          </li>
          @endcan

          @can('facturas.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('facturas.index') }}">
              <span data-feather="list"></span> 
              Listado Facturas
            </a>
          </li>
          @endcan

          @can('cajas.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cajas.index') }}">
              <span data-feather="inbox"></span> 
              Cajas Abiertas
            </a>
          </li>
          @endcan
          @can('datofacturas.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('datofacturas.index') }}">
              <span data-feather="list"></span> 
              Listado Datos Fact.
            </a>
          </li>
          @endcan                                        
        </ul>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownProducto-User" aria-expanded="false" data-toggle="collapse">
          <span data-feather="box"></span>
          Productos
        </a>
        <ul id="navbarDropdownProducto-User" class="collapse list-unstyled">
          
          @can('productos.create') 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('productos.create') }}">
              <span data-feather="plus"></span> 
              Nuevo Productos
            </a>
          </li>
          @endcan

          @can('productos.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('productos.index') }}">
              <span data-feather="list"></span> 
              Listado Productos
            </a>
          </li>
          @endcan

          @can('unidades.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('unidades.index') }}">
              <span data-feather="list"></span> 
              Listado Unidades
            </a>
          </li>
          @endcan  

          @can('categorias.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('categorias.index') }}">
              <span data-feather="list"></span> 
              Listado Categorías
            </a>
          </li>
          @endcan                                
        </ul>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownCliente-Cliente" aria-expanded="false" data-toggle="collapse">
          <span data-feather="user"></span>
          Clientes
        </a>
        <ul id="navbarDropdownCliente-Cliente" class="collapse list-unstyled">
          @can('clientes.create') 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('clientes.create') }}">
              <span data-feather="plus"></span> 
              Nuevo Clientes
            </a>
          </li>
          @endcan    
          @can('clientes.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('clientes.index') }}">
              <span data-feather="list"></span> 
              Listado Clientes
            </a>
          </li>
          @endcan

          @can('cxcs.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cxcs.index') }}">
              <span data-feather="list"></span> 
              Listado CxC
            </a>
          </li>
          @endcan    

          @can('cotizaciones.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cotizaciones.index') }}">
              <span data-feather="list"></span> 
              Listado Cotinzaciones
            </a>
          </li>
          @endcan                             
        </ul>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownCliente-Proveedor" aria-expanded="false" data-toggle="collapse">
          <span data-feather="truck"></span>
          Proveedores
        </a>
        <ul id="navbarDropdownCliente-Proveedor" class="collapse list-unstyled">
          @can('proveedores.create') 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('proveedores.create') }}">
              <span data-feather="plus"></span> 
              Nuevo Proveedor
            </a>
          </li>
          @endcan    
          @can('proveedores.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('proveedores.index') }}">
              <span data-feather="list"></span> 
              Listado Proveedores
            </a>
          </li>
          @endcan

          @can('declaracioncompras.create')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('declaracioncompras.create') }}">
              <span data-feather="plus"></span> 
              Nueva Fact. Compra
            </a>
          </li>
          @endcan

          @can('declaracioncompras.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('declaracioncompras.index') }}">
              <span data-feather="list"></span> 
              Listado Compras
            </a>
          </li>
          @endcan    

          @can('cxps.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cxps.index') }}">
              <span data-feather="list"></span> 
              Listado CxP
            </a>
          </li>
          @endcan                         
        </ul>
      </li>      

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownOtro-descuento" aria-expanded="false" data-toggle="collapse">
          <span data-feather="percent"></span>
          Descuentos
        </a>
        <ul id="navbarDropdownOtro-descuento" class="collapse list-unstyled">
         
          @can('descuentos.create')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('descuentos.create') }}">
              <span data-feather="plus"></span> 
              Nuevo Descuento
            </a>
          </li>
          @endcan

          @can('descuentos.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('descuentos.index') }}">
              <span data-feather="list"></span> 
              Listado Descuento
            </a>
          </li>
          @endcan 

          @can('descuentos.edit')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/descuento/amasiva') }}">
              <span data-feather="copy"></span> 
              Asignación Masiva
            </a>
          </li>
          @endcan                        
        </ul>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownOtro-User" aria-expanded="false" data-toggle="collapse">
          <span data-feather="clipboard"></span>
          Otros
        </a>
        <ul id="navbarDropdownOtro-User" class="collapse list-unstyled">
         
          @can('impuestos.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('impuestos.index') }}">
              <span data-feather="list"></span> 
              Listado Impuestos
            </a>
          </li>
          @endcan

          @can('tipopagos.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('tipopagos.index') }}">
              <span data-feather="list"></span> 
              Listado Metodos Pago
            </a>
          </li>
          @endcan                        
        </ul>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownReporte-User" aria-expanded="false" data-toggle="collapse">
          <span data-feather="bar-chart-2"></span>
          Reportes
        </a>
        <ul id="navbarDropdownReporte-User" class="collapse list-unstyled">

          @can('rep.declaraciones')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/reportesisv') }}">
              <span data-feather="file"></span> 
              Declaraciones SAR
            </a>
          </li>
          @endcan

          @can('flujos.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('flujos.index') }}">
              <span data-feather="inbox"></span> 
              Flujos de Caja
            </a>
          </li>
          @endcan

          @can('logs.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('logs.index') }}">
              <span data-feather="user"></span> 
              Actividad Usuarios
            </a>
          </li>
          @endcan

        </ul>
      </li>

        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownReporte-opcion" aria-expanded="false" data-toggle="collapse">
          <span data-feather="settings"></span>
          Opciones
        </a>
        <ul id="navbarDropdownReporte-opcion" class="collapse list-unstyled">

          @can('suscripcion.edit')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/suscripcion') }}">
              <span data-feather="credit-card"></span> 
              Suscripción
            </a>
          </li>
          @endcan

        </ul>
      </li>

    </ul>