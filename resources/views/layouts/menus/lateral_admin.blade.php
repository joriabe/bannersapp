    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="#">
          <span data-feather="home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
        
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownCia" aria-expanded="false" data-toggle="collapse">
          <span data-feather="layers"></span>
          Compañias
        </a>
        <ul id="navbarDropdownCia" class="collapse list-unstyled">
          @can('cias.index')
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cias.index') }}"">
              <span data-feather="list"></span> 
              Listado Compañias
            </a>
          </li>
          @endcan
          @can('cias.create') 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('cias.create') }}"">
              <span data-feather="plus"></span> 
              Nueva Compañia
            </a>
          </li>
          @endcan()                                  
        </ul>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownUser" aria-expanded="false" data-toggle="collapse">
          <span data-feather="user"></span>
          Usuarios
        </a>
        <ul id="navbarDropdownUser" class="collapse list-unstyled">
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('users.index') }}"">
              <span data-feather="list"></span> 
              Listado Usuarios
            </a>
          </li> 
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/register') }}"">
              <span data-feather="plus"></span> 
              Nuevo Usuario
            </a>
          </li>
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('roles.index') }}"">
              <span data-feather="clipboard"></span> 
              Roles
            </a>
          </li>                                    
        </ul>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbarDropdownSuscripcion" aria-expanded="false" data-toggle="collapse">
          <span data-feather="dollar-sign"></span>
          Suscripciones
        </a>
        <ul id="navbarDropdownSuscripcion" class="collapse list-unstyled">
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ route('suscripcion.index') }}"">
              <span data-feather="list"></span> 
              Suscripciones
            </a>
          </li>
          <li style="margin-left: 10px" class="nav-item">
            <a class="nav-link" href="{{ url('/plan/create') }}"">
              <span data-feather="plus"></span> 
              Nuevo Plan
            </a>
          </li>                                  
        </ul>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="bar-chart-2"></span>
          Reports
        </a>
      </li>

    </ul>