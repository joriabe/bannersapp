@extends('layouts.dashboard')

@section('content')

<div class="card">
	<h5 class="card-header">Subscripción</h5>
	<div class="card-body">
    <!--<h5 class="card-title">{{ $subs->razon_social }}</h5>-->
   	    <div class="row">
			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Fecha de Inicio:</p></b>
					<p class="card-text">{{ date_format($subs->fecha_inicio, "d/m/Y  H:i") }} </p>
				</div>
			</div>

			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Siguiente pago:</p></b>
					<p class="card-text">{{ date_format($subs->fecha_renovacion, "d/m/Y  H:i") }} </p>
				</div>
			</div>
		</div>
		
		<br/>
    	<b><p class="card-text"><span data-feather="at"></span>  Datos del Plan </p></b>
		<hr>
		
		<div class="row">
			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Plan:</p></b>
					<p class="card-text">{{ $subs->descripcion_factura }}</p>
				</div>
			</div>

			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Valor:</p></b>
					<p class="card-text">L.{{ number_format($subs->valor,2) }} </p>
				</div>
			</div>			

			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Agencias:</p></b>
					<p class="card-text">{{ $subs->sucursales }}</p>
				</div>
			</div>

			<div class="col-3">
				<div class="form-group">
					<b><p class="card-text">Usuario:</p></b>
					<p class="card-text">{{ $subs->usuarios }} </p>
				</div>
			</div>
		</div>
    
    	<br/>

		<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>

	</div>
</div>
    
@endsection

