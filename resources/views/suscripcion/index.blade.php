@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Suscripciones</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="100">COD SUSCRI</th>
				<th class="text-center" width="100">CIA</th>
				<th class="text-center" width="200">PLAN</th>
				<th class="text-center" width="50">INICIO</th>
				<th class="text-center" width="50">RENOVACION</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($listados as $suscripcion)
			<tr>
				<td class="text-center">{{ $suscripcion->cod_agreement }}</td>
				<td class="text-center">{{ $suscripcion->nombrecomercial }}</td>
				<td class="text-center">{{ $suscripcion->descripcion_factura }}</td>
				<td class="text-center">{{ $suscripcion->fecha_inicio }}</td>
				<td class="text-center">{{ $suscripcion->fecha_renovacion }}</td>
				<td class="text-center">
					
					@can('suscripcion.showFull')
					<a class="btn btn-info btn-sm" href="{{ action('SuscripcionController@showFull', ['id' => $suscripcion->id]) }}" role="button" title="Ver y Editar Suscripción"><span data-feather="edit"></span></a>
					@endcan
			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}

	</div>

@endsection
