@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
@include('alerts.succes')

@php
	$cias = \App\Cia::pluck('nombrecomercial','id');	
@endphp

<div class="row">
	<div class="card border-dark col-12" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Creación de Plan</h5>


			{!! Form::open(['id'=>'planform', 'class'=>'form-horizontal','action'=>'PaypalController@create_plan', 'method'=>'POST']) !!}

				

					</br>	
						
					<h6 class="card-subtitle mb-2 text-muted" align="center">Datos Generales</h6>

					<div class="row align-items-center">
						
						<div class="col-4">
						
							<div class="form-group">
								{!! Form::label('Plan:') !!}				
								{!! Form::text('plan',null,['class'=>'form-control','placeholder'=>'nombre del plan', 'id'=>'plan','required' => 'required', "onKeyUp"=>"this.value=this.value.toUpperCase();"]) !!}		
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Descripción:') !!}	
								{!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción del plan', 'id'=>'descripcion','required' => 'required']) !!}				
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Descripción en Factura:') !!}	
								{!! Form::text('descripcion_factura',null,['class'=>'form-control','placeholder'=>'Descripción del plan visualizado en la factura', 'id'=>'descripcion_factura','required' => 'required']) !!}				
							</div>
						</div>
					</div>

					<div class="row align-items-center">

						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Precio:') !!}
								<div class="input-group">
									<div class="input-group-prepend">
								    	<div class="input-group-text">$.</div>
									</div>			
								{!! Form::number('valor',null,['class'=>'form-control','placeholder'=>'', 'id'=>'valor','required' => 'required',"step"=>"any"]) !!}
							
								</div>		
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Instalación:') !!}
								<div class="input-group">
									<div class="input-group-prepend">
								    	<div class="input-group-text">$.</div>
									</div>			
								{!! Form::number('instlacion',null,['class'=>'form-control','placeholder'=>'', 'id'=>'instlacion','required' => 'required',"step"=>"any"]) !!}
							
								</div>		
							</div>
						</div>

						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Plan especial para Cia:') !!}	
								{!! Form::select('cia_id',$cias,null,['class'=>'form-control', 'id'=>'cia_id','required' => 'required']) !!}				
							</div>
						</div>

					</div>

					<div class="row align-items-center">

						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Frecuencia:') !!}
								{!! Form::select('frecuencia',['Year' => 'Anual', 'Month' => 'Mensual'],null,['class'=>'form-control', 'id'=>'frecuencia','required' => 'required']) !!}
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Cobros por Frecuencia:') !!}
								{!! Form::select('intervalo',['1' => 'Una Vez', '2' => 'Dos Veces', '3' => 'Tres Veces', '4' => 'Cuatro Veces'],null,['class'=>'form-control', 'id'=>'intervalo','required' => 'required']) !!}
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Ciclo:') !!}
								{!! Form::select('ciclo',['0' => 'Perpetuo','1' => 'Una Vez', '2' => 'Dos Veces', '3' => 'Tres Veces', '4' => 'Cuatro Veces'],null,['class'=>'form-control', 'id'=>'ciclo','required' => 'required']) !!}
							</div>
						</div>

					</div>

					</br>	
					<h6 class="card-subtitle mb-2 text-muted" align="center">Beneficios del Plan</h6>
				

					<div class="row align-items-center">
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Cías:') !!}		
								{!! Form::number('cias',null,['class'=>'form-control','placeholder'=>'N. Cías', 'id'=>'cias','required' => 'required']) !!}
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Sucursales:') !!}		
								{!! Form::number('sucursales',null,['class'=>'form-control','placeholder'=>'N.Sucursales', 'id'=>'sucursales','required' => 'required']) !!}
							</div>
						</div>
						<div class="col-4">
							<div class="form-group">
								{!! Form::label('Usuarios:') !!}		
								{!! Form::number('usuarios',null,['class'=>'form-control','placeholder'=>'N. Usuarios', 'id'=>'usuarios','required' => 'required']) !!}
							</div>
						</div>
					</div>
		




			<div align="center">
				<button type="submit" class="btn btn-primary">
					<span data-feather="plus"></span>
					Crear Plan
				</button>

				<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
			</div>

			{!! Form::close() !!}

		</div>
	</div>
</div>

@include('productos.modal')

@endsection