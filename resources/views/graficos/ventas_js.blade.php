@section('scrips')

    <script type="text/javascript">
        var ctx = document.getElementById('ventas_graf').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: [
                @foreach ($labels as $label)
                "{{ $label }}", 
                @endforeach
                ],
                datasets: [
                @foreach ($datas as $data)
                {
                    label: "{{ $nombres[$loop->index] }}",
                    fill: false,
                    backgroundColor: colores({{ $loop->index }}),
                    borderColor: colores({{ $loop->index }}),
                    data: [
                    @foreach ($data as $dat)
                    "{{ number_format($dat, 2, '.', '') }}", 
                    @endforeach
                    ],
                },                
                @endforeach
                ]
            },

            // Configuration options go here
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Ventas Ultimos 30 Dias'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Dia'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Valor Facturado'
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0, // deshabilitar curvas
                    }
                }
            }
        });

        function colores(num) {

            var colors = ["rgb(54, 162, 235)", "rgb(255, 99, 132)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(153, 102, 255)","rgb(255, 160, 63)","rgb(201, 203, 207)"];

            return colors[num];

        }

    </script>

@endsection