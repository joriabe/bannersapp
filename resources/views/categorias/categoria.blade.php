{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="form-group col-3">
			{{ Form::label('Acrónimo:') }}				
			{{ Form::text('acronimo',null,['class'=>'form-control','placeholder'=>'', 'id'=>'acronimo','required' => 'required','maxlength'=>"10", 'size'=>"10", "onKeyUp"=>"this.value=this.value.toUpperCase();" ]) }}		
		</div>
		<div class="form-group col-12">
			{{ Form::label('Categoría:') }}	
			{{ Form::text('categoria',null,['class'=>'form-control','placeholder'=>'', 'id'=>'categoria','required' => 'required']) }}				
		</div>
	</div>
</div>