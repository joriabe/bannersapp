@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Categorías</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="450">CATEGORÍA</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($categorias as $categoria)
			<tr>
				<td class="text-center">{{ $categoria->acronimo }}</td>
				<td class="text-center">{{ $categoria->categoria }}</td>
				<td class="text-center">

					@can('categorias.edit')
					<a class="btn btn-info btn-sm" href="{{ action('CategoriaController@edit', ['id' => $categoria->id]) }}" role="button" title="Editar categoría"><span data-feather="edit"></span></a>
					@endcan

					@can('categorias.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar categoría" onclick="borrar({{ $categoria->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan()

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$categorias->render()!!}
		
		<div align="center">
			@can('categorias.create')
			<a class="btn btn-success" href="{{ url('/categorias/create') }}" role="button"><span data-feather="plus"></span> Nueva Categoría</a>
			@endcan()     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrar(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar la Categoría "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/categorias/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection