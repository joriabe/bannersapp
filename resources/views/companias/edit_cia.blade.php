@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Compañia</h1>

<form action="{{ route('cias.update', $cia->id) }}" method="POST" name="form1" id="form1" enctype="multipart/form-data">
	{{ csrf_field() }}
	@method('PUT')

		<div class="col-6">
			<p>Datos de la Compañia:</p>
			<div class="form-group">
				<label for="input_rtn">RTN:</label>
				<input class="form-control" type="number" id="input_rtn" name="input_rtn" placeholder="RTN de la Compañia (14 dígitos)" pattern="[0-9]{14}" title = "RTN debe estar compuesto de 14 dígitos" required value="{{ $cia->rtn }}">			
			</div>

			<div class="form-group">
				<label for="input_nombre">Nombre Comercial:</label>
				<input class="form-control" type="text" id="input_nombre" name="input_nombre" placeholder="Nombre Completo de la Compañia" required value="{{ $cia->nombrecomercial }}">			
			</div>

			<div class="form-group">
				<label for="input_razons">Razon Social:</label>
				<input class="form-control" type="text" id="input_razons" name="input_razons" placeholder="Razon Social" required value="{{ $cia->razonsocial }}">			
			</div>

			<div class="form-group">
				{{ Form::label('Logo Comercia:') }}
				{{ Form::file('input_logo', ['class'=>'form-control','type'=>'file', 'id'=>'input_logo','required' => 'required']) }}	
			</div>		
		</div>

	<button type="submit" class="btn btn-success">
		<span data-feather="plus"></span>
		Actualizar Compañia
	</button>

</form>

@endsection