@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Agencia</h1>

<form action="{{ route('agencias.update', $agencia->id) }}" method="POST" name="form1" id="form1">
	{{ csrf_field() }}
	@method('PUT')

	<p>Datos de la Agencia</p>
<div class="row justify-content-start">
	<div class="col-md-6">

		<div class="form-group">
			<label for="input_cinterno">Codigo Interno:</label>
			<input class="form-control" type="text" id="input_cinterno" name="input_cinterno" placeholder="" required value="{{ $agencia->codigointerno }}">			
		</div>

		<div class="form-group">
			<label for="input_nombrecomercial">Nombre Comercial:</label>
			<input class="form-control"  type="text" id="input_nombrecomercial" name="input_nombrecomercial" required value="{{ $agencia->nombrecomercial }}">			
		</div>

		<div class="form-group">
			<label for="input_descripcion">Descripción:</label>
			<textarea class="form-control" id="input_descripcion" name="input_descripcion" required >{{ $agencia->descripcion }}</textarea>			
		</div>

		<div class="form-group">
			<label for="input_direccion">Dirección:</label>
			<input class="form-control" type="text" id="input_direccion" name="input_direccion" placeholder="Dirrección " required value="{{ $agencia->direccion }}">			
		</div>

		<div class="form-group">
			<label for="input_telefono">Telefono:</label>
			<input class="form-control" type="text" id="input_telefono" name="input_telefono" placeholder="Telefono" required value="{{ $agencia->telefono }}">			
		</div>

		<div class="form-group">
			<label for="input_email">Correo Electronico:</label>
			<input class="form-control" type="email" id="input_email" name="input_email" placeholder="@ Email" value="{{ $agencia->correo }}">			
		</div>

		<div class="form-group">
			<label for="input_web">Correo Electronico:</label>
			<input class="form-control" type="text" id="input_web" name="input_web" placeholder="www.misitioweb.com" value="{{ $agencia->web }}">			
		</div>
	</div>
</div>

	<button type="submit" class="btn btn-success">
		<span data-feather="plus"></span>
		Actualizar Agencia
	</button>

</form>

@endsection