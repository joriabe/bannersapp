@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Crear nueva compañia</h1>

<form action="{{ action('CiasController@store') }}" method="POST" name="form1" id="form1" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<p>Datos de la Compañia:</p>
			<div class="form-group">
				<label for="input_rtn">RTN:</label>
				<input class="form-control" type="number" id="input_rtn" name="input_rtn" placeholder="RTN de la Compañia (14 dígitos)" pattern="[0-9]{14}" title = "RTN debe estar compuesto de 14 dígitos" required value="{{ old('input_rtn') }}">			
			</div>

			<div class="form-group">
				<label for="input_nombre">Nombre Comercial:</label>
				<input class="form-control" type="text" id="input_nombre" name="input_nombre" placeholder="Nombre Completo de la Compañia" required value="{{ old('input_nombre') }}">			
			</div>

			<div class="form-group">
				<label for="input_razons">Razon Social:</label>
				<input class="form-control" type="text" id="input_razons" name="input_razons" placeholder="Razon Social" required value="{{ old('input_razons') }}">			
			</div>

			<div class="form-group">
				<label for="input_logo">Logo Comercial:</label>
				<input class="form-control" type="file" id="input_logo" name="input_logo" >			
			</div>
		</div>

		<div class="col-6">
			<p>Datos de la Agencia</p>

			<div class="form-group">
				<label for="input_cinterno">Codigo Interno:</label>
				<input class="form-control" type="text" id="input_cinterno" name="input_cinterno" placeholder="" required value="{{ old('input_cinterno') }}">			
			</div>

			<div class="form-group">
				<label for="input_nombrecomercial">Nombre Comercial:</label>
				<input class="form-control"  type="text" id="input_nombrecomercial" name="input_nombrecomercial" required value="{{ old('agencia->nombrecomercial')  }}">			
			</div>

			<div class="form-group">
				<label for="input_descripcion">Descripción:</label>
				<textarea class="form-control" id="input_descripcion" name="input_descripcion" required >{{ old('input_descripcion') }}</textarea>			
			</div>

			<div class="form-group">
				<label for="input_direccion">Dirección:</label>
				<input class="form-control" type="text" id="input_direccion" name="input_direccion" placeholder="Dirrección " required value="{{ old('input_direccion') }}">			
			</div>

			<div class="form-group">
				<label for="input_telefono">Telefono:</label>
				<input class="form-control" type="text" id="input_telefono" name="input_telefono" placeholder="Telefono" required value="{{ old('input_telefono') }}">			
			</div>

			<div class="form-group">
				<label for="input_email">Correo Electronico:</label>
				<input class="form-control" type="email" id="input_email" name="input_email" placeholder="@ Email"  value="{{ old('input_email') }}">			
			</div>

			<div class="form-group">
				<label for="input_web">Pagina Web:</label>
				<input class="form-control" type="text" id="input_web" name="input_web" placeholder="www.misitioweb.com"  value="{{ old('input_web') }}">			
			</div>

			

	</div>

	<button type="submit" class="btn btn-success">
		<span data-feather="plus"></span>
		Añadir Compañia
	</button>

</form>

@endsection