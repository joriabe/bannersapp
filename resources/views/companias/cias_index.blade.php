@extends('layouts.dashboard')

@section('content')

<h1>Menu Compañias</h1>
<hr>

@foreach($cias as $cia)
<br>
	<div class="row justify-content-center">
		<h5><u>Datos de la Compañia:</u></h5>
		<div class="row">		
		<div class="col-md-2">
			<img style="width: 100%" src="{{ $cia->logo }}">
		</div>
		<div class="col-md-10">
			<div class="table-responsive">	
			<table class="table text-left table-borderless">
			  <tr>
			    <th width="150">Compañia ID:</th>
			    <td width="100">{{ $cia->id }}</td>		    
			    <th width="150">Nombre Comercial:</th>
			    <td width="250">{{ $cia->nombrecomercial }}</td>
			    @can('cias.delete')
			    <td width="10">
			       	<form method="POST" action="{{ route('cias.destroy', $cia->id ) }}" id="form1" name="form1" onsubmit="return confirm('¿Desea eliminar la compañia seleccionada?');">
			    		@method('DELETE')
                        @csrf                        
                        <button type="submit" class="btn btn-danger btn-sm" form="form1" title="Borrar Compañia">
			    		<span data-feather="delete"></span>			   
		    			</button>
                    </form>	
			    </td>
			    @endcan
			  </tr>
			  <tr>
			  	<th>RTN:</th>
			    <td>{{ $cia->rtn }}</td>			    
			    <th>Razon Social:</th>
			    <td>{{ $cia->razonsocial }}</td>
			  	@can('cias.edit')  
			    <td>
			    	<a class="btn btn-info btn-sm" href="{{ route('cias.edit', $cia->id ) }}" role="button" title="Editar Compañia"><span data-feather="edit"></span></a>
			    </td>
			  	@endcan
			  </tr> 			 
			</table>
			</div>
		</div>
		</div>	


		<h6>Agencias de la Compañia:</h6>
		<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="200">DESCRIPCION</th>
				<th class="text-center" width="100">DIRECCION</th>
				<th class="text-center" width="50">TELEFONO</th>
				<th class="text-center" width="100">CORREO</th>
				<th class="text-center" width="100">CODIGO INTERNO</th>
				<th class="text-center" width="50">OPER</th>
								
			</tr>
			</thead>
	
			@foreach($cia->agencias as $agencia)
			<tr>
				<td class="text-center">{{ $agencia->descripcion }}</td>
				<td class="text-center">{{ $agencia->direccion }}</td>
				<td class="text-center">{{ $agencia->telefono }}</td>
				<td class="text-center">{{ $agencia->correo }}</td>
				<td class="text-center">{{ $agencia->codigointerno }}</td>
				<td class="justify-content-center">
					<table>
						<tr>
							@can('agencias.delete')
							<td>
								<form method="POST" action="{{ route('agencias.destroy', $agencia->id ) }}" id="form2" name="form2" onsubmit="return confirm('¿Desea eliminar la agencia seleccionada?');">
						    		@method('DELETE')
			                        @csrf                        
			                        <button type="submit" class="btn btn-danger btn-sm" form="form2" title="Borrar Agencia">
						    		<span data-feather="delete"></span>			   
					    			</button>		    			
			                    </form>	
							</td>
							@endcan
							@can('agencias.edit')
							<td>
								<a class="btn btn-info btn-sm" href="{{ route('agencias.edit', $agencia->id ) }}" role="button" title="Editar Agencia"><span data-feather="edit"></span></a>
							</td>
							@endcan
						</tr>
					</table>                    
	        	</td>			
				
			</tr>
			@endforeach			
		</table>
		</div>

	</div>
	<div class="row justify-content-end">
		<a class="btn btn-primary" href="{{ action('AgenciasController@create', ['id' => $cia->id]) }}" role="button"><span data-feather="folder-plus"></span> Nueva Agencia</a>
	</div>	
	<hr style=" border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));">
@endforeach

@endsection

