@extends('layouts.dashboard')

@section('content')
<h1>Crear nueva Agencia</h1>

<form action="{{action('AgenciasController@store')}}" method="POST" name="form1" id="form1">
	{{ csrf_field() }}

	<p>Datos de la Agencia</p>
<div class="row justify-content-start">
	<div class="col-md-6">
		<input type="hidden" name="cia_id" value="{{ $cia_id }}">

		<div class="form-group">
			<label for="input_cinterno">Codigo Interno:</label>
			<input class="form-control" type="text" id="input_cinterno" name="input_cinterno" placeholder="" required value="{{ old('input_cinterno') }}">			
		</div>

		<div class="form-group">
			<label for="input_nombrecomercial">Nombre Comercial:</label>
			<input class="form-control" type="text" id="input_nombrecomercial" name="input_nombrecomercial" required value="{{ old('input_nombrecomercial') }}">			
		</div>

		<div class="form-group">
			<label for="input_descripcion">Descripción:</label>
			<textarea class="form-control" id="input_descripcion" name="input_descripcion" required >{{ old('input_descripcion') }}</textarea>			
		</div>

		<div class="form-group">
			<label for="input_direccion">Dirección:</label>
			<input class="form-control" type="text" id="input_direccion" name="input_direccion" placeholder="Dirrección " required value="{{ old('input_direccion') }}">			
		</div>

		<div class="form-group">
			<label for="input_telefono">Telefono:</label>
			<input class="form-control" type="text" id="input_telefono" name="input_telefono" placeholder="Telefono" required value="{{ old('input_telefono') }}">			
		</div>

		<div class="form-group">
			<label for="input_email">Correo Electronico:</label>
			<input class="form-control" type="email" id="input_email" name="input_email" placeholder="@ Email" value="{{ old('input_email') }}">			
		</div>

		<div class="form-group">
			<label for="input_web">Correo Electronico:</label>
			<input class="form-control" type="text" id="input_web" name="input_web" placeholder="www.misitioweb.com" value="{{ old('input_web') }}">			
		</div>

	</div>
</div>

	<button type="submit" class="btn btn-success">
		<span data-feather="plus"></span>
		Añadir Agencia
	</button>

</form>

@endsection