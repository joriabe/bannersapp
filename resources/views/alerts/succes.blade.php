@if(Session::has('message'))
	
	<div class="alert alert-success alert-dismissable">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  <strong>Correcto!</strong> 

	  {{Session::get('message')}}
	</div>
@endif
@if(Session::has('message_alert'))
	
	<div class="alert alert-warning alert-dismissable">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  <strong>Alerta!</strong> 

	  {{Session::get('message_alert')}}
	</div>
@endif

@if(Session::has('message_danger'))
	
	<div class="alert alert-danger alert-dismissable">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  <strong>Peligro!</strong> 

	  {{Session::get('message_danger')}}
	</div>
@endif