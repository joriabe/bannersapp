@if(count($errors) > 0)
	<div class="alert alert-danger alert-dismissable">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<strong>Error!</strong> 
		
		<ul>
			@foreach($errors->all() as $error)
				<Li>{!! $error !!} </Li>	
			@endforeach
		</ul>

	</div>
@endif