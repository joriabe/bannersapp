@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')
<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
@include('alerts.succes')

<div class="row">
	<div class="card border-dark col-12" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Registro Flujo Caja</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Registro de movimiento en los diferentes métodos de pago</h6>



			{{ Form::open(['id'=>'flujostoform', 'class'=>'form-horizontal','action'=>'FlujoController@store', 'method'=>'POST']) }}
	
			{{ csrf_field() }}
			
				    </br>
			<div class="row">
				<div class="col-6">
					<h6 class="card-subtitle mb-2 text-muted" align="center">Datos del Flujo</h6>
					<div class="form-group">
						{{ Form::label('Agencia:') }}	
						{{ Form::select('agencia_id',$agencias,null,['class'=>'js-example-basic-single form-control', 'id'=>'agencia_id','required' => 'required']) }}			
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							{{ Form::label('Tipo de Pago:') }}	
							{{ Form::select('tipopago_id',$tipo_pagos,null,['class'=>'js-example-basic-single form-control', 'id'=>'tipopago_id','required' => 'required']) }}			
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('Tipo de Movimiento:') }}	
							{{ Form::select('movimiento_id',['2'=>'Retiro','4'=>'Ajuste'],null,['class'=>'form-control', 'id'=>'movimiento_id','required' => 'required']) }}			
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-md-8">
							{{ Form::label('Valor:') }}
							<div class="input-group">		
								<div class="input-group-prepend">
							       	<div class="input-group-text">L.</div>
								</div>	
								{{ Form::number('valor',null,['class'=>'form-control','placeholder'=>'', 'id'=>'valor','required' => 'required',"step"=>"any"]) }}
							</div>	
						</div>
						<div class="form-group col-md-4">
							{{ Form::label('Tipo de Flujo:') }}
							<div class="form-group">
								<label>{{ Form::radio('signo','1',false) }} Positivo</label>
								<label>{{ Form::radio('signo','-1',true) }} Negativo</label>
							</div>	
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('Descripción:') }}	
						{{ Form::text('referencia',null,['class'=>'form-control','placeholder'=>'Breve descripción del flujo.', 'id'=>'referencia','required' => 'required']) }}		
					</div>
				</div>

				<div class="col-6">
					<h6 class="card-subtitle mb-2 text-muted" align="center">Resumen de los Flujos</h6>
					
					@php
						$agencia = "";
						$subtotal = 0;
						$total = 0;
					@endphp

					<div class="table-responsive">		
						<table class="table table-striped table-bordered table-sm">
							@foreach($flujos as $flujo)

							@if($agencia !=  $flujo->agencia_name )		
								<thead class="thead-dark">
									<tr>
										<th colspan="2"class="text-center">{{ $flujo->agencia_name }}</th>			
									</tr>
								</thead>

								@php
									$agencia2 =  $flujo->agencia_name
								@endphp
				
								@foreach($flujos as $flujo2)
									@if($agencia2 ==  $flujo2->agencia_name )	
										<tr>
											<td class="text-center">{{ $flujo2->tipopago }}</td>
											<td class="text-right">{{ number_format($flujo2->suma_valor,2) }}</td>			
										</tr>
										@php
											$subtotal = $subtotal + $flujo2->suma_valor	
										@endphp
									@endif
								@endforeach

								<tr>
									<td class="text-center" style="font-weight:bold">Subtotal</td>
									<td class="text-right" style="font-weight:bold">{{ number_format($subtotal,2) }}</td>

								</tr>							
							
								@php
									$total = $total + $subtotal;
									$subtotal = 0;	 
								@endphp	

							@endif
							
							@php
								$agencia =  $flujo->agencia_name
							@endphp

							@endforeach
						</table>
						<table class="table table-striped table-bordered table-sm">
							<thead class="thead-dark">
								<tr>
									<th class="text-center">Total</th>	
									<th class="text-right">{{ number_format($total,2) }}</th>			
								</tr>
							</thead>			
						</table>
					</div>

				</div>
			</div>


			<div align="center">
				<button type="submit" class="btn btn-primary">
					<span data-feather="plus"></span>
					Registrar
				</button>
				<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
			</div>

			{{ Form::close() }}
		</div>
	</div>
</div>

@endsection