@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Flujo de Caja</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>
	
	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" >FECHA</th>
				<th class="text-center" >AGENCIA</th>
				<th class="text-center" >DESCRIPCION</th>
				<th class="text-center" >VALOR</th>
				<th class="text-center" >TIPO MOV</th>
				<th class="text-center">TIPO PAGO</th>
				<th class="text-center" >USUARIO</th>						
			</tr>
			</thead>
	
			@foreach($listados as $flujo)
			<tr>
				<td class="text-center">{{ $flujo->fecha }}</td>
				<td class="text-center">{{ $flujo->agencia_name }}</td>
				<td class="text-left">{{ $flujo->referencia }}</td>
				<td class="text-right">	{{  number_format($flujo->valor,2) }}</td>
				<td class="text-center">{{ $flujo->movimiento }}</td>
				<td class="text-center">{{ $flujo->tipopago }}</td>
				<td class="text-center">{{ $flujo->user_name }}</td>			
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}

		<div align="center">
			@can('flujos.create')
			<a class="btn btn-success" href="{{ url('/flujos/create') }}" role="button"><span data-feather="plus"></span> Nuevo Flujo</a>
			@endcan
			@can('rep.flujosexcel')
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#flujos_excel">
			<span data-feather="download"></span> Descargar Excel
			</button>
			@endcan      
		</div>

	</div>


<!-- Modal -->
<div class="modal fade" id="flujos_excel" tabindex="-1" role="dialog" aria-labelledby="flujos_excelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="flujos_excelLabel">Descargar Excel Flujos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('flujosexcel') }}"  method="POST" name="form1" id="form1">
		{{ csrf_field() }}
      <div class="modal-body">
        <div class="form-group col-md-6">
		    <label for="input_fechai">Fecha Inicial:</label>
		    <input class="form-control" type="date" name="input_fechai" id="input_fechai">
		 </div>
		 <div class="form-group col-md-6">
		    <label for="input_fechaf">Fecha Final:</label>
		    <input class="form-control" type="date" name="input_fechaf" id="input_fechaf">
		 </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">
			<span data-feather="download"></span>
			Descargar
		</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection
