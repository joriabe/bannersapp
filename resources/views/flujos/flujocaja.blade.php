{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="row">
			<div class="form-group col-md-4">
				{{ Form::label('Valor:') }}				
				<div class="input-group input-group-sm">	
					<div class="input-group-prepend">
				       	<div class="input-group-text">L.</div>
				    </div>		
					{{ Form::number('valor',null,['class'=>'form-control','placeholder'=>'', 'id'=>'valor','required' => 'required',"step"=>"any"]) }}
				</div>
			</div>

			<div class="form-group col-md-4">
				{{ Form::label('Tipo de Flujo:') }}
				<div class="input-group input-group-sm">	
					<label>{{ Form::radio('signo','1',false) }} Positivo</label>
					<label>{{ Form::radio('signo','-1',true) }} Negativo</label>
				</div>	
			</div>

			<div class="form-group col-md-4">
				{{ Form::label('Tipo de Movimiento:') }}
				<div class="input-group input-group-sm">	
					{{ Form::select('movimiento_id',['2'=>'Retiro','4'=>'Ajuste'],null,['class'=>'form-control', 'id'=>'movimiento_id','required' => 'required']) }}	
				</div>
			</div>	
		</div>
	</div>
		<div class="col-12">
			<div class="row">
				<div class="form-group col-md-12">
					{{ Form::label('Descripción:') }}	
					<div class="input-group input-group-sm">				
						{{ Form::textarea('referencia',null,['class'=>'form-control','placeholder'=>'', 'id'=>'referencia','required' => 'required', 'rows'=>"2"]) }}		
					</div>
				</div>
			</div>
		</div>



	<input type="hidden" name="agencia_id" id="agencia_id" value= {{ Session::get('agencia_id') }}>
	<input type="hidden" name="tipopago_id" id="tipopago_id" value='1'>

	<input type="hidden" name="modal" id="modal" value='1'>

	<div class="form-group col-md-12">
	{{ Form::label('Movimientos del Día:') }}

		<div class="table-responsive">		
			<table class="table table-sm table-striped">

				<thead class="thead-dark">
					<tr>
						<th class="text-center">Descripción</th>
						<th class="text-center">Valor L.</th>			
					</tr>
				</thead>
				@php
					$total = 0;	
				@endphp
				@foreach($flujos as $flujo)					
					<tr>
						<td class="text-center">{{ $flujo['tipo_pago'] }}</td>
						<td class="text-right">{{  number_format($flujo['total'],2) }}</td>			
					</tr>
					@php
						$total += $flujo['total'];
					@endphp		
				@endforeach

				<thead class="thead-dark">
					<tr>
						<th class="text-center">Total</th>
						<th class="text-right">{{  number_format($total,2) }}</th>			
					</tr>
				</thead>

			</table>
		
		</div>
	
	</div>

</div>
