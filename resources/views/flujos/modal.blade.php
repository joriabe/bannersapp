<!-- Modal AÑADIR Flujo -->
<div class="modal fade" id="flujoModal" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="categoriaModalLabel">Añadir Nuevo Flujo de Caja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(['id'=>'flujoCajaForm', 'class'=>'form-horizontal','action'=>'FlujoController@store', 'method'=>'POST']) }}
          <input type="hidden" name="form_pos" id="form_pos" value="1">
          <!--añade los objetos que se repiten del form -->
		  @include('flujos.flujocaja')

          <div align="center">
            <button type="submit" class="btn btn-success">
              <span data-feather="plus"></span>
              Añadir
            </button>
          </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>


@section('scrips')

<script type="text/javascript">
  // this is the id of the form
  $("#flujoCajaForm").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

      var form = $(this);
      var url = form.attr('action');
      
      $.ajax({
             type: "POST",
             url: url,
             data: form.serialize(), // serializes the form's elements.
             dataType: 'json', //convierte la respuesta a javascrip
             success: function(data)
             {
                 alert(data.msg); // show response from the php script.
                 $('#categoriaModal').modal('hide');

                 //cargar_clientes(clietes_url);
             html_cliente = '<option selected value="'+data.id+'">'+data.nombre+'</option>';
             document.getElementById("categoria_id").innerHTML += html_cliente;
             }
           });      
      
  }); 

</script>



@endsection