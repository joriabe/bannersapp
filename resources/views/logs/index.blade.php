@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')	

	<div class="row">
		<div class="col-md-8">
			<h1>Registro de Actividad</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" >TIME</th>
				<th class="text-center" >CIA</th>
				<th class="text-center" >AGENCIA</th>
				<th class="text-center" >TABLA</th>
				<th class="text-center" >DESCRIPCION</th>
				<th class="text-center" >REF</th>
				<th class="text-center">USUARIO</th>						
			</tr>
			</thead>
	
			@foreach($listados as $log)
			<tr>
				<td class="text-center">{{ $log->created_at }}</td>
				<td class="text-center">{{ $log->cia_name }}</td>
				<td class="text-center">{{ $log->agencia_name }}</td>
				<td class="text-center">{{ $log->tabla }}</td>
				<td class="text-center">{{ $log->descripcion }}</td>
				<td class="text-center">{{ $log->referencia }}</td>
				<td class="text-center">{{ $log->user_name }}</td>			
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}
	</div>

@endsection
