<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Sistema de Gestion de Facturas</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{url('landing/css/bootstrap.min.css')}}" />

    <!-- Owl Carousel -->
    <link type="text/css" rel="stylesheet" href="{{url('landing/css/owl.carousel.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{url('landing/css/owl.theme.default.css')}}" />

    <!-- Magnific Popup -->
    <link type="text/css" rel="stylesheet" href="{{url('landing/css/magnific-popup.css')}}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{url('landing/css/font-awesome.min.css')}}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{url('landing/css/style.css')}}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Header -->
    <header id="home">
        <!-- Background Image -->
        <div class="bg-img" style="background-image: url('{{url('landing/./img/background1.jpg')}}');">
            <div class="overlay"></div>
        </div>
        <!-- /Background Image -->

        <!-- Nav -->
        <nav id="nav" class="navbar nav-transparent">
            <div class="container">

                <div class="navbar-header">
                    <!-- Logo -->
                    <div class="navbar-brand">
                        <a href="{{ url('/') }}">
                            <img class="logo" src="{{url('landing/img/logo.png')}}" alt="logo">
                            <img class="logo-alt" src="{{url('landing/img/logo-alt.png')}}" alt="logo">
                        </a>
                    </div>
                    <!-- /Logo -->

                    <!-- Collapse nav button -->
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                    <!-- /Collapse nav button -->
                </div>

                <!--  Main navigation  -->
                <ul class="main-nav nav navbar-nav navbar-right">
                    <li><a href="#home">Inicio</a></li>
                    <li><a href="#about">Acerca</a></li>
                    <li><a href="#service">Características</a></li>
                    <li><a href="#features">SGF-PYME</a></li>
                    <li><a href="#pricing">Precios</a></li>
                    <!-- <li><a href="#team">Team</a></li>
                    <li class="has-dropdown"><a href="#blog">Blog</a>
                        <ul class="dropdown">
                            <li><a href="blog-single.html">blog post</a></li>
                        </ul>
                    </li> -->
                    <li><a href="#contact">Contacto</a></li>
                    @if (Route::has('login'))                            
                            @auth
                                <li><a href="{{ url('/home') }}">Escritorio</a></li>
                            @else
                                <li><a href="{{ route('login') }}">Login</a></li>
                            @endauth                            
                    @endif


                </ul>
                <!-- /Main navigation -->

            </div>
        </nav>
        <!-- /Nav -->

        <!-- home wrapper -->
        <div class="home-wrapper">
            <div class="container">
                <div class="row">

                    <!-- home content -->
                    <div class="col-md-10 col-md-offset-1">
                        <div class="home-content">
                            <div class="col-md-6 col-md-offset-3">
                                <img class="img-responsive" src="{{url('landing/img/logo-alt.png')}}">
                            </div>
                            <div class="col-md-12">
                                <h5 class="white-text">El primer Sistema de Gestión de Facturas en línea diseñado para la Pequeña y Mediana Empresa Hondureña.
                                </h5>
                                <a class="white-btn" href="#about">¡Comencemos!</a>
                                <a class="main-btn" href="#service">Conoce mas</a>
                            </div>
                            

                            
                        </div>
                    </div>
                    <!-- /home content -->

                </div>
            </div>
        </div>
        <!-- /home wrapper -->

    </header>
    <!-- /Header -->

    <!-- About -->
    <div id="about" class="section md-padding">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- Section header -->
                <div class="section-header text-center">
                    <h2 class="title">La solución del futuro para el empresario de hoy</h2>
                </div>
                <!-- /Section header -->

                <!-- about -->
                <div class="col-md-4">
                    <div class="about">
                        <i class="fa fa-lock"></i>
                        <h3>Seguro</h3>
                        <p>Con accesos restringidos y perfiles de usuarios adaptables, SGF-PYME le brinda a tu empresa la seguridad continua que necesitas.</p>
                        <!-- <a href="#">Read more</a> -->
                    </div>
                </div>
                <!-- /about -->

                <!-- about -->
                <div class="col-md-4">
                    <div class="about">
                        <i class="fa fa-cogs"></i>
                        <h3>Completo</h3>
                        <p>SGF-PYME cuenta con funciones innovadoras y módulos intuitivos que te permitirán ver tu empresa de una manera nueva y eficiente.</p>
                        <!-- <a href="#">Read more</a> -->
                    </div>
                </div>
                <!-- /about -->

                <!-- about -->
                <div class="col-md-4">
                    <div class="about">
                        <i class="fa fa-magic"></i>
                        <h3>Personalizable</h3>
                        <p>SGF-PYME se ha diseñado con la flexibilidad necesaria para adoptar las características que hacen única a tu empresa, haciéndolos la pareja perfecta en un mercado competitivo.</p>
                        <!-- <a href="#">Read more</a> -->
                    </div>
                </div>
                <!-- /about -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /About -->

    <!-- Service -->
    <div id="service" class="section md-padding">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- Section header -->
                <div class="section-header text-center">
                    <h2 class="title">Que te ofrecemos</h2>
                </div>
                <!-- /Section header -->

                <!-- Row -->
                <div class="row">

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-print"></i>
                        <h3>Facturación</h3>
                        <p>Registro de facturas al crédito y al contado, creación de cotizaciones y gestión de los productos y servicios de la empresa.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-book"></i>
                        <h3>Cuentas por Cobrar y por Pagar</h3>
                        <p>Registro detallado de los pagos que han sido realizados, con la opción de enviar notificaciones ya sea por correo o por el mismo sistema de las cuentas próximas a vencer.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-line-chart"></i>
                        <h3>Declaración Jurada de Impuesto sobre Ventas</h3>
                        <p>Generación de los reportes de compras y de ventas, necesarios para la realización de la declaración mensual del Impuesto sobre Ventas ante el SAR.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- Row -->
                </div>

                <!-- Row -->
                <div class="row">

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-address-book-o"></i>
                        <h3>Gestión de clientes y proveedores</h3>
                        <p>Registro de clientes y proveedores de la empresa con el cual se podrá monitorear la productividad financiera que cada uno aporta.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-bank"></i>
                        <h3>Flujos de caja </h3>
                        <p>Registro de todos los movimientos que corresponden a cada una de las cajas de la empresa, los cuales facilitaran el cierre al finalizar la jornada.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- service -->
                <div class="col-md-4 col-sm-12">
                    <div class="service">
                        <i class="fa fa-pencil-square-o"></i>
                        <h3>Registro de actividades</h3>
                        <p>Cada gestión realizada en el sistema será registrada, ya sea creación, modificación, o eliminación de datos y el usuario que la realiza.</p>
                    </div>
                </div>
                <!-- /service -->

                <!-- Row -->
                </div>

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Service -->

    <!-- Why Choose Us -->
    <div id="features" class="section md-padding bg-grey">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row vertical-align">

                <!-- why choose us content -->
                <div class="col-md-6">
                    <div class="section-header">
                        <h2 class="title">Por que elegir SGF-PYME</h2>
                    </div>
                    <div class="feature"> 
                        <i class="fa fa-check"></i>
                        <p>Un sistema comprensivo - SGF Pyme incorpora todos los aspectos financieros de tu empresa en una plataforma dinámica e intuitiva.</p>
                    </div>
                    <div class="feature">
                        <i class="fa fa-check"></i>
                        <p>Seguridad - Con SGF Pyme, tendrás los registros financieros de tu empresa consolidados en un lugar seguro y privado.</p>
                    </div>
                    <div class="feature">                        
                        <i class="fa fa-check"></i>   
                        <p>Accesibilidad - Nuestros servidores potentes y confiables te garantizan la disponibilidad del sistema y la integridad de los datos los 365 días del año.</p>                                              
                    </div>
                    <div class="feature">
                        <i class="fa fa-check"></i>
                        <p>Apoyo técnico continuo - Siempre tendrás a nuestro equipo de trabajo a tu disposición para guiarte en el uso del sistema y atender tus necesidades con la mayor brevedad posible.</p>
                    </div>
                    <div class="feature">
                        <i class="fa fa-check"></i>
                        <p>Eres un miembro más del equipo - Nuestros desarrolladores trabajan en conjunto con los usuarios, asegurando que SGF Pyme evolucione y crezca a la par de tu empresa.</p>
                    </div>
                </div>
                <!-- /why choose us content -->

                <!-- About slider -->
                <div class="col-md-6">
                    <div id="about-slider" class="owl-carousel owl-theme">
                        <img class="img-responsive" src="{{url('landing/./img/about1.jpg')}}" alt="Un sistema comprensivo">
                        <img class="img-responsive" src="{{url('landing/./img/about2.jpg')}}" alt="Seguridad">
                        <img class="img-responsive" src="{{url('landing/./img/about3.jpg')}}" alt="Accesibilidad">
                        <img class="img-responsive" src="{{url('landing/./img/about4.png')}}" alt="Apoyo técnico continuo">
                        <img class="img-responsive" src="{{url('landing/./img/about5.jpg')}}" alt="Eres un miembro más del equipo">
                    </div>
                </div>
                <!-- /About slider -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Why Choose Us -->
    

    <!-- Pricing -->
    <div id="pricing" class="section md-padding">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- Section header -->
                <div style="margin-bottom: -10px" class="section-header text-center">
                    <h2 class="title">Nuestros Precios</h2>
                </div>
                <!-- /Section header -->

                <!-- pricing -->
                <div class="col-sm-4">
                    <div class="pricing">
                        <div class="price-head">
                            <span class="price-title">Plan Basico</span>
                            <div class="price">
                                <h3>$25<span class="duration">/ mes</span></h3>
                            </div>
                        </div>
                        <ul class="price-content">
                            <li>
                                <p>1 Compañía</p>
                            </li>
                            <li>
                                <p>1 Sucursal</p>
                            </li>
                            <li>
                                <p>2 Usuarios</p>
                            </li>
                            <li>
                                <p>Acceso a todas las funciones del sistema y futuras actualizaciones.</p>
                            </li>
                            <li>
                                <p>* Pago único de $60 por instalación y capacitación inicial.</p>
                            </li>
                        </ul>
                        <div class="price-btn">
                            <button class="outline-btn" data-toggle="modal" data-target="#myModal" onclick="modal_asunto(1);">¡Lo Quiero!</button>
                        </div>
                    </div>
                </div>
                <!-- /pricing -->

                <!-- pricing -->
                <div class="col-sm-4">
                    <div class="pricing">
                        <div class="price-head">
                            <span class="price-title">Plan Prime</span>
                            <div class="price">
                                <h3>$55<span class="duration">/ mes</span></h3>
                            </div>
                        </div>
                        <ul class="price-content">
                            <li>
                                <p>1 Compañía</p>
                            </li>
                            <li>
                                <p>2 Sucursales</p>
                            </li>
                            <li>
                                <p>5 Usuarios</p>
                            </li>
                            <li>
                                <p>Acceso a todas las funciones del sistema y futuras actualizaciones.</p>
                            </li>
                            <li>
                                <p>* Pago único de $100 por instalación y capacitación inicial.</p>
                            </li>
                        </ul>
                        <div class="price-btn">
                            <button class="outline-btn" data-toggle="modal" data-target="#myModal" onclick="modal_asunto(2);">¡Lo Quiero!</button>
                        </div>
                    </div>
                </div>
                <!-- /pricing -->

                <!-- pricing -->
                <div class="col-sm-4">
                    <div class="pricing">
                        <div class="price-head">
                            <span class="price-title">Plan Ultimate</span>
                            <div class="price">
                                <h3>$85<span class="duration">/ mes</span></h3>
                            </div>
                        </div>
                        <ul class="price-content">
                            <li>
                                <p>1 Compañía</p>
                            </li>
                            <li>
                                <p>3 Sucursales</p>
                            </li>
                            <li>
                                <p>7 Usuarios</p>
                            </li>
                            <li>
                                <p>Acceso a todas las funciones del sistema y futuras actualizaciones.</p>
                            </li>
                            <li>
                                <p>* Pago único de $150 por instalación y capacitación inicial.</p>
                            </li>
                        </ul>
                        <div class="price-btn">
                            <button class="outline-btn" data-toggle="modal" data-target="#myModal" onclick="modal_asunto(3);">¡Lo Quiero!</button>
                        </div>
                    </div>
                </div>
                <!-- /pricing -->

            </div>
            <!-- Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Pricing -->


    <!-- Testimonial -->
    <div id="testimonial" class="section md-padding">

        <!-- Background Image -->
        <div class="bg-img" style="background-image: url('{{url('landing/./img/background3.jpg')}}');">
            <div class="overlay"></div>
        </div>
        <!-- /Background Image -->

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- Testimonial slider -->
                <div class="col-md-10 col-md-offset-1">
                    <div id="testimonial-slider" class="owl-carousel owl-theme">

                        <!-- testimonial -->
                        <div class="testimonial">
                            <div class="testimonial-meta">
                                <img src="{{url('landing/./img/banners_logo.jpg')}}" alt="" class="img-rounded">
                                <h3 class="white-text">Banners de Honduras</h3>
                                <span>Diseño y Publicidad</span>
                            </div>
                            <p class="white-text">SGF-PYME es más que un sistema de facturación. Le permitió a la administración tener control sobre las distintas áreas del negocio en una sola aplicación y lo mejor de todo, le brindo a la gerencia una mayor movilidad, al poder consultar datos desde cualquier sitio.</p>
                        </div>
                        <!-- /testimonial -->

                        <!-- testimonial -->
                        <div class="testimonial">
                            <div class="testimonial-meta">
                                <img src="{{url('landing/./img/victorias_logo.png')}}" alt="">
                                <h3 class="white-text">Victoria's Salon</h3>
                                <span>Centro de Estetica</span>
                            </div>
                            <p class="white-text">Victoria's Salon tiene 15 años dedicados a exaltar la belleza femenina en la ciudad de La Ceiba, decidieron trabajar con SGF-PYME con el objetivo de tener información veraz, en el momento oportuno. Gracias a la implementación, sus operaciones administrativas cambiaron radicalmente.</p>
                        </div>
                        <!-- /testimonial -->

                    </div>
                </div>
                <!-- /Testimonial slider -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Testimonial -->

   

    <!-- Contact -->
    <div id="contact" class="section md-padding">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">
                @include('alerts.succes')
                <!-- Section-header -->
                <div class="section-header text-center">
                    <h2 class="title">Contactanos</h2>
                </div>
                <!-- /Section-header -->

                <!-- contact -->
                <div class="col-sm-4">
                    <div class="contact">
                        <i class="fa fa-phone"></i>
                        <h3>Teléfonos</h3>
                        <p>9787-9668</p>
                        <p>2449-0663</p>
                        <p>2442-5050</p>
                    </div>
                </div>
                <!-- /contact -->

                <!-- contact -->
                <div class="col-sm-4">
                    <div class="contact">
                        <i class="fa fa-envelope"></i>
                        <h3>Email</h3>
                        <p>info@sgfpyme.com</p>
                    </div>
                </div>
                <!-- /contact -->

                <!-- contact -->
                <div class="col-sm-4">
                    <div class="contact">
                        <i class="fa fa-map-marker"></i>
                        <h3>Dirección</h3>
                        <p>Res. Los Álamos, calle principal</p>
                        <p>San Pedro Sula, Cortez.</p>
                    </div>
                </div>
                <!-- /contact -->
                <div class="col-md-12 text-center">
                    <br>
                    <h4>Envíanos un mensaje a través del siguiente formulario y pronto estaremos en contacto.</h4>
                </div>
                <!-- contact form -->
                <div class="col-md-8 col-md-offset-2">

                    <form class="contact-form" action="{{ action('MailingController@mailinginfo') }}" method="POST" onsubmit="return validateForm();">
                        
                        <input type="text" class="input" placeholder="Nombre" name="nombre">
                        <input type="email" class="input" placeholder="Email" name="email">
                        <input type="text" class="input" placeholder="Asunto" name="asunto">                                        
                        <textarea class="input" placeholder="Mensaje" name="mensaje"></textarea>
                        {{ csrf_field() }}

                        <div class="form-inline text-left"> 
                            <div class="form-group">
                                <label class="form-label"><span id="sum1">{{ random_int(1, 20) }}</span> + <span id="sum2">{{ random_int(1, 20) }}</span> =</label>
                                <input class="form-control" placeholder="respuesta" type="text" name="res" id="res" pattern="[0-9]{1,2}">
                            </div>
                        </div>
                        <button class="main-btn" type="submit">Enviar Mensaje</button>
                    </form>
                </div>
                <!-- /contact form -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Contact -->


    <!-- Footer -->
    <footer id="footer" class="sm-padding bg-dark">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <div class="col-md-12">

                    <!-- footer logo -->
                    <div class="footer-logo">
                        <a href="{{ url('/') }}"><img src="{{url('landing/img/logo-alt.png')}}" alt="logo"></a>
                    </div>
                    <!-- /footer logo -->

                    <!-- footer follow 
                    <ul class="footer-follow">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    /footer follow -->

                    <!-- footer copyright -->
                    <div class="footer-copyright">
                        <p>Copyright © 2019. Todos los derechos reservados. Diseñado por <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                    </div>
                    <!-- /footer copyright -->

                </div>

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </footer>
    <!-- /Footer -->

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <!-- /Preloader -->

    <!-- jQuery Plugins -->
    <script type="text/javascript" src="{{url('landing/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('landing/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('landing/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{url('landing/js/jquery.magnific-popup.js')}}"></script>
    <script type="text/javascript" src="{{url('landing/js/main.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript"> 

        function validateForm()
        {
          let sum1 = document.getElementById('sum1').innerText;
          let sum2 = document.getElementById('sum2').innerText;
          let res = parseInt(document.getElementById('res').value);

          let suma = parseInt(sum1) + parseInt(sum2);
          
          if (suma == res) {
            return true;
          } else {
            Swal.fire({
              title: '¡Error!',
              text: 'Verifica que el resultado de la suma sea el correcto.',
              icon: 'error',
              confirmButtonText: 'Aceptar'
            })
            return false;
          }    

        }         
    </script>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Adquirir Plan</h4>
          </div>
          <div class="modal-body">
            <!-- contact form -->
            <form class="contact-form" action="{{ action('MailingController@mailingcomercial') }}" method="POST">
                
                <input type="text" class="input" placeholder="Nombre" name="modal_nombre" required>
                <input type="text" class="input" placeholder="Telefono" name="modal_telefono" required>
                <input type="email" class="input" placeholder="Email" name="modal_email" required>
                <input type="text" class="input" id="modal_asunto" name="modal_asunto" readonly>               
                {{ csrf_field() }} 
                <button class="main-btn" type="submit">!Adquirir¡</button>
            </form>            
            <!-- /contact form -->
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

</body>

</html>
