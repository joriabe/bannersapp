@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

    @include('graficos.ventas')

   	&nbsp
   	@if(($cxcs->count()) > 0)
    @include('tablas.cxcvencidas')
    @endif

    &nbsp
   	@if(($cxps->count()) > 0)
    @include('tablas.cxpvencidas')
    @endif


@endsection

@include('graficos.ventas_js')

