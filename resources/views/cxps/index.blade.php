@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Cuentas por Pagar</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">FECHA</th>
				<th class="text-center" width="50">FECHA VEN</th>
				<th class="text-center" width="150">FACTURA</th>
				<th class="text-center" width="200">PROVEEDOR</th>
				<th class="text-center" width="50">VALOR PAG</th>
				<th class="text-center" width="50">VALOR PEN</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
			
			@php
				$fecha = Carbon\Carbon::now();
				$status = "";
			@endphp

			@foreach($listados as $cxp)

			@php
				$vencimiento = new Carbon\Carbon($cxp->fecha_vencimiento);

				
				if($fecha >=  $vencimiento){
					$status = "vencida";
				}
				elseif (((new Carbon\Carbon($fecha))->addWeeks(2)) > $vencimiento )
				{
					$status = "por vencer";
				}
			
			@endphp

			<tr>
				<td class="text-center">{{ $cxp->fecha }}</td>
				<td class="text-center">{{ $cxp->fecha_vencimiento }}</td>
				<td class="text-left">{{ $cxp->num_docto }}
					@if($status == "vencida") 
						<span class="badge badge-pill badge-danger">Vencida</span>
					@elseif($status == "por vencer")
						<span class="badge badge-pill badge-warning">Por Vencer</span>
					@endif
				</td>
				<td class="text-center">{{ $cxp->proveedor_name }}</td>
				<td class="text-right">{{ number_format($cxp->valor_pagado,2) }}</td>
				<td class="text-right">{{ number_format($cxp->valor_pendiente,2) }}</td>
				<td class="text-right">
					@if($cxp->valor_pendiente > 0)
					@can('cxcs.edit')
					<a class="btn btn-success btn-sm" href="{{ action('CxPController@edit', ['id' => $cxp->id]) }}" role="button" title="Pagar CxP"><span data-feather="dollar-sign"></span></a>
					@endcan
					@endif

					@can('cxcs.show')
					<a class="btn btn-info btn-sm" href="{{ action('CxPController@show', ['id' => $cxp->id]) }}" role="button" title="Inspexionar CxP"><span data-feather="search"></span></a>
					@endcan
					

				</td>				
			</tr>
			@php
				$status = '';
			@endphp
			

			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}


@endsection

