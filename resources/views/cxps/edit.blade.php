@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')


{!! Form::model(null,['route'=>['cxps.update', $cxps->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('cxps.cxp')

    
{!! Form::close() !!}

@stop()