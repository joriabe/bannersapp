@extends('layouts.dashboard')

@section('content')

<div class="card">
	<h5 class="card-header">CxP  Factura: &nbsp {{ $compras->num_docto }}&nbsp({{ $compras->name }})</h5>
	<div class="card-body">
   	    
	<br/>
	<b><p class="card-text"><span data-feather="at"></span>  Datos de Generales </p></b>
	<hr>

	    <div class="row">
		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Valor Pendiente:</p></b>
				<p class="card-text">L.{{ number_format($cxps->valor_pendiente,2) }} </p>
			</div>
		</div>

		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Valor Pagado:</p></b>
				<p class="card-text">L.{{  number_format($cxps->valor_pagado,2) }} </p>
			</div>
		</div>
		
		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Fecha Compra:</p></b>
				<p class="card-text">{{ $cxps->fecha }} </p>
			</div>
		</div>

		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Fecha Vencimiento:</p></b>
				<p class="card-text">{{ $cxps->fecha_vencimiento }} </p>
			</div>
		</div>
	</div>
	
	<br/>
	<b><p class="card-text"><span data-feather="at"></span>  Datos de Contacto </p></b>
	<hr>
	
	<div class="row">
		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Telefono:</p></b>
				<p class="card-text">{{ $compras->telefono }} </p>
			</div>
		</div>
		
		<div class="col-3">
			<div class="form-group">
				<b><p class="card-text">Correo:</p></b>
				<p class="card-text">{{ $compras->correo }} </p>
			</div>
		</div>
	</div>

	<br/><b><p class="card-text">Pagos</p></b><hr>

	<div class="row">
		<div class="col-12">
		<div class="table-responsive">		
			<table class="table table-striped table-bordered table-sm">		
				<thead class="thead-dark">
				<tr>
					<th class="text-center" width="50">FECHA</th>
					<th class="text-center" width="50">VALOR</th>
					<th class="text-center" width="150">REFERENCIA</th>
					<th class="text-center" width="100">METODO PAGO</th>

				</tr>
				</thead>
		
				@foreach($pagos as $pago)
				<tr>
					<td class="text-center">{{ $pago->created_at }}</td>
					<td class="text-center">{{ number_format($pago->valor_pago,2) }}</td>
					<td class="text-center">{{ $pago->comprobantepago }}</td>
					<td class="text-center">{{ $pago->tipopago }}</td>		
				</tr>
				@endforeach			
			</table>
		</div>
		</div>
	</div>


    
    <br/><a href=" {{ URL::previous() }}" class="btn btn-success">Atras</a>
  </div>
</div>

    
{!! Form::close() !!}


@endsection