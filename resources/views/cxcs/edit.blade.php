@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')


{!! Form::model(null,['route'=>['cxcs.update', $cxcs->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('cxcs.cxc')

    
{!! Form::close() !!}

@stop()