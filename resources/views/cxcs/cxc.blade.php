{{ csrf_field() }}

<!-- 3 de los 4 elemenots novisibles -->
{{ Form::hidden('factura_id', $facturas->id ,['class'=>'form-control','placeholder'=>'', 'id'=>'factura_id']) }}

{{ Form::hidden('total_factura', $facturas->totaldespuesimpuesto ,['class'=>'form-control','placeholder'=>'', 'id'=>'total_factura']) }}

{{ Form::hidden('codfactura', $facturas->codfactura ,['class'=>'form-control','placeholder'=>'', 'id'=>'codfactura']) }}

{{ Form::hidden('pagado', $cxcs->valor_pagado ,['class'=>'form-control','placeholder'=>'', 'id'=>'pagado']) }}

<div class="card">
	<h5 class="card-header">Abonar a Cuenta por Cobrar</h5>
	<div class="card-body">

   	    <br/>
    	<b><p class="card-text"><span data-feather="at"></span>  Datos de Contacto </p></b>
		<hr>

   	    <div class="row">			
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Factura:</p></b>
					<p class="card-text">{{ $facturas->codfactura }} </p>
				</div>
			</div>

			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Valor Pendiente:</p></b>
					<p class="card-text">L.{{ number_format($cxcs->valor_pendiente,2) }} </p>
				</div>
			</div>
			
			<div class="col-4">
				<div class="form-group">
					<b><p class="card-text">Valor Pagado:</p></b>
					<p class="card-text">L.{{ number_format($cxcs->valor_pagado,2) }} </p>
				</div>
			</div>

		</div>
		
		<br/>
    	<b><p class="card-text"><span data-feather="at"></span>Abono</p></b>
		<hr>
		
		<div class="row">
			<div class="col-12">
				<div class="table-responsive">		
					<table class="table table-striped table-bordered table-sm">		
						<thead class="thead-dark">
							<tr>
								<th class="text-center" width="50">TIPO DE PAGO</th>
								<th class="text-center" width="50">VALOR</th>
								<th class="text-center" width="150">COMPROBANTE PAGO</th>			
							</tr>
						</thead>
				
						@foreach($tipo_pagos as $tipo_pago)
						<tr>
							<td class="text-center" >
								<p class="h6">{{ $tipo_pago->tipopago }} </p>
							</td>
							<td class="text-center">
								<div class="input-group">
									<div class="input-group-prepend">
								       	<div class="input-group-text">L.</div>
								    </div>		
									{!! Form::number('valor_pagado[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'valor_pagado',"step"=>"any"]) !!}				
								</div>		
							</td>
							<td class="text-center">
								{{ Form::text('comprobantepago[]',null,['class'=>'form-control','placeholder'=>'', 'id'=>'comprobantepago']) }}	
								{{ Form::hidden('tipo_pago_id[]', $tipo_pago->id ,['class'=>'form-control','placeholder'=>'', 'id'=>'comprobantepago']) }}	
							</td>				
						</tr>
						@endforeach			
					</table>
				</div>
			</div>
		</div>
	  	<div class="row">			
			<div class="col-6">
				<a href=" {{ URL::previous() }}" class="btn btn-success">Atras</a>

				&nbsp &nbsp

				{{ Form::button('Aplicar',['class'=>'btn btn-primary', 'type'=>"submit"]) }}
				<span data-feather="plus"></span>
			</div>

	  	</div>
	</div>
</div>
