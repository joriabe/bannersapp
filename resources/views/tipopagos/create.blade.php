@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<div class="row">
	<div class="card border-dark col-md-8 offset-md-2" style="width: 18rem;">
		<div class="card-body">
		    <h5 class="card-title"  align="center">Tipo de Pago</h5>
		    <h6 class="card-subtitle mb-2 text-muted" align="center">Formas de pago en el que el cliente puede comprar</h6>

			{!! Form::open(['id'=>'tipopagoform', 'class'=>'form-horizontal','action'=>'TipoPagoController@store', 'method'=>'POST']) !!}

				<!--añade los objetos que se repiten del form -->
				@include('tipopagos.tipopago')

				<div align="center">
					<button type="submit" class="btn btn-primary">
						<span data-feather="plus"></span>
						Añadir
					</button>

					<a href=" {{ URL::previous() }}" class="btn btn-success"><span data-feather="arrow-left"></span>Atras</a>
				</div>

			{!! Form::close() !!}

@endsection