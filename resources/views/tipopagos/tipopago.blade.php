{{ csrf_field() }}

<div class="row">
	<div class="col-12">
		<div class="form-group">
			{!! Form::label('Tipo de Pago:') !!}	
			{!! Form::text('tipopago',null,['class'=>'form-control','placeholder'=>'', 'id'=>'tipopago','required' => 'required']) !!}				
		</div>
	</div>
</div>