@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Tipo de Pagos</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">COD</th>
				<th class="text-center" width="450">TIPO DE PAGO</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
	
			@foreach($tipopagos as $tipopago)
			<tr>
				<td class="text-center">{{ $tipopago->id }}</td>
				<td class="text-center">{{ $tipopago->tipopago }}</td>
				<td class="text-center">

					@can('tipopagos.edit')
					<a class="btn btn-info btn-sm" href="{{ action('TipoPagoController@edit', ['id' => $tipopago->id]) }}" role="button" title="Editar tipo de pago"><span data-feather="edit"></span></a>
					@endcan
					
					@can('tipopagos.delete')
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Borrar tipo de pago" onclick="borrar({{ $tipopago->id }});"><span data-feather="delete"></span>
			    	</button>
			    	@endcan

				</td>				
			</tr>
			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$tipopagos->render()!!}

		<div align="center">
			@can('tipopagos.create')
			<a class="btn btn-success" href="{{ url('/tipopagos/create') }}" role="button"><span data-feather="plus"></span> Nuevo Tipo de Pago</a>
			@endcan     
		</div>
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function borrar(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Borrar el Tipo de Pago "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/tipopagos/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection