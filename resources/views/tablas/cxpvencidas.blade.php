@isset($cxps)
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h6 class="text-center">CxP Vencidas y a Vencer</h6>

            <div class="table-responsive">		
			<table class="table table-sm table-bordered  table-striped">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="50">Fecha</th>
				<th class="text-center" width="50">Fecha Ven</th>
				<th class="text-center" width="150">Factura</th>
				<th class="text-center" width="200">Proveedor</th>
				<th class="text-center" width="50">Valor Pag</th>
				<th class="text-center" width="50">Valor Pen</th>
				<th class="text-center" width="50">Oper</th>						
			</tr>
			</thead>
			
			@php
				$fecha = Carbon\Carbon::now();
				$status = "";
			@endphp

			@foreach($cxps as $cxp)

			@php
				$vencimiento = new Carbon\Carbon($cxp->fecha_vencimiento);

				
				if($fecha >=  $vencimiento){
					$status = "vencida";
				}
				elseif (((new Carbon\Carbon($fecha))->addWeeks(2)) > $vencimiento )
				{
					$status = "por vencer";
				}
			
			@endphp

			<tr>
				<td class="text-center">{{ $cxp->fecha }}</td>
				<td class="text-center">{{ $cxp->fecha_vencimiento }}</td>
				<td class="text-left">{{ $cxp->num_docto }}
					@if($status == "vencida") 
						<span class="badge badge-pill badge-danger">Vencida</span>
					@elseif($status == "por vencer")
						<span class="badge badge-pill badge-warning">Por Vencer</span>
					@endif
				</td>
				<td class="text-center">{{ $cxp->proveedor_name }}</td>
				<td class="text-right">{{ number_format($cxp->valor_pagado,2) }}</td>
				<td class="text-right">{{ number_format($cxp->valor_pendiente,2) }}</td>
				<td class="text-center">

					@if($cxp->valor_pendiente > 0)
					@can('cxps.edit')
					<a class="btn btn-success btn-sm" href="{{ action('CxPController@edit', ['id' => $cxp->id]) }}" role="button" title="Pagar CxP"><span data-feather="dollar-sign"></span></a>
					@endcan
					@endif

					@can('cxps.show')
					<a class="btn btn-info btn-sm" href="{{ action('CxPController@show', ['id' => $cxp->id]) }}" role="button" title="Inspexionar CxP"><span data-feather="search"></span></a>
					@endcan
					

				</td>				
			</tr>
			@php
				$status = '';
			@endphp
			

			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$cxps->render()!!}
        </div>
    </div>
</div>
@endisset