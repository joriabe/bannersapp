@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<h1>Users</h1>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center">CIA</th>
				<th class="text-center">AGENCIA</th>
				<th class="text-center">USER</th>
				<th class="text-center">ROLE</th>
				<th class="text-center">EMAIL</th>
				<th class="text-center">STATUS</th>
				<th class="text-center">OPER</th>						
			</tr>
			</thead>
	
			@foreach($users as $user)
			<tr>
				<td class="text-center">{{ $user->cia_name }}</td>
				<td class="text-center">{{ $user->agencia_name }}</td>
				<td class="text-center">{{ $user->name }}</td>
				<td class="text-center">{{ $user->rol_name }}</td>
				<td class="text-center">{{ $user->email }}</td>
				<td class="text-center">
					@if(null===$user->deleted_at)
				    	ACTIVO
				    @else
				    	INACTIVO
				    @endif
				</td>
				<td class="text-center">

					@can('users.edit')
					<a class="btn btn-info btn-sm" href="{{ action('UserController@edit', ['id' => $user->id]) }}" role="button" title="Editar Usuario"><span data-feather="edit"></span></a>
					@endcan
					
					@can('users.delete')
					@if(null===$user->deleted_at)
						<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Desactivar Usuario" onclick="desactivarUser({{ $user->id }});"><span data-feather="user-x"></span>
				    	</button>
				    @else
				    	<button style="float: right;" type="button" class="btn btn-success btn-sm" title="Reactiva Usuario" onclick="activarUser({{ $user->id }});"><span data-feather="user-check"></span>
				    	</button>
				    @endif
				    @endcan

			    </td>

				</td>				
			</tr>
			@endforeach			
		</table>
		
		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$users->render()!!}
		
		@can('users.create')
		<div align="center">
			<a class="btn btn-success" href="{{ url('/register') }}" role="button"><span data-feather="user"></span> Nuevo Usuario</a>     
		</div>
		@endcan
	</div>

@endsection



@section('scrips')

	<script type="text/javascript">
		function desactivarUser(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Desactivar el user "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/users/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

	<script type="text/javascript">
		function activarUser(id){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Activar el user "+id+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/users/activar') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection