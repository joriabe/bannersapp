@extends('layouts.dashboard')

@section('content')

<!-- Valida la informacion para mostrar msj en caso de error -->
@include('alerts.request')

<h1>Editar Usuarios</h1>

{!! Form::model($users,['route'=>['users.update', $users->id], 'method'=>'PUT']) !!}

	<!--añade los objetos que se repiten del form -->
	@include('users.user')

	<div align="center">
		<button type="submit" class="btn btn-success">
			<span data-feather="plus"></span>
			Guardar
		</button>
	</div>
    
{!! Form::close() !!}

@stop()