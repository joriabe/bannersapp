{{ csrf_field() }}

<div class="row">
	<div class="col-6">
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Cia:') !!}
					{!! Form::select('cia_id',$cias,null,['class'=>'form-control', 'id'=>'cia_id','required' => 'required']) !!}	
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					{!! Form::label('Agencia:') !!}
					{!! Form::select('agencia_id',$agencias,null,['class'=>'form-control', 'id'=>'agencia_id','required' => 'required']) !!}	
				</div>
			</div>	
		</div>
		<div class="form-group">
			{!! Form::label('Nombre:') !!}				
			{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'', 'id'=>'name','required' => 'required']) !!}		
		</div>

		<div class="form-group">
			{!! Form::label('Rol:') !!}
			{!! Form::select('role_id',$roles,null,['class'=>'form-control', 'id'=>'role_id','required' => 'required']) !!}
		</div>
	</div>
</div>

