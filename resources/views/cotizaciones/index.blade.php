@extends('layouts.dashboard')

@section('content')

	<!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
	@include('alerts.succes')

	<div class="row">
		<div class="col-md-8">
			<h1>Cotizaciones</h1>
		</div>
		<!-- Incluye el input para realizar la busqueda, necesita de las variables: etiqueta y ruta -->
		@include('complementos.busqueda_index')
	</div>

	<div class="table-responsive">		
		<table class="table table-striped table-bordered table-sm">		
			<thead class="thead-dark">
			<tr>
				<th class="text-center" width="25">ID</th>
				<th class="text-center" width="75">FECHA</th>
				<th class="text-center" width="75">FECHA VEN</th>
				<th class="text-center" width="125">AGENCIA</th>
				<th class="text-center" width="150">CLIENTE</th>
				<th class="text-center" width="75">VALOR</th>
				<th class="text-center" width="50">OPER</th>						
			</tr>
			</thead>
			@php
				$fecha = Carbon\Carbon::now();
				$status = "";
			@endphp

			@foreach($listados as $cotizacion)

			@php
				$vencimiento = new Carbon\Carbon($cotizacion->fechavencimiento);

				
				if($fecha >=  $vencimiento){
					$status = "vencida";
				}
				elseif (((new Carbon\Carbon($fecha))->addWeeks(2)) > $vencimiento )
				{
					$status = "por vencer";
				}
			
			@endphp

			<tr>
				<td class="text-left">{{ $cotizacion->cod_interno }}
					@if($status == "vencida") 
						<span class="badge badge-pill badge-danger">Vencida</span>
					@elseif($status == "por vencer")
						<span class="badge badge-pill badge-warning">Por Vencer</span>
					@endif
				</td>
				<td class="text-center">{{ date("d-m-Y", strtotime($cotizacion->fecha)) }}</td>
				<td class="text-center">{{ date("d-m-Y", strtotime($cotizacion->fechavencimiento)) }}</td>
				<td class="text-center">{{ $cotizacion->nombrecomercial }}
				<td class="text-center">{{ $cotizacion->nombrecliente }}</td>
				<td class="text-right">{{ number_format($cotizacion->totaldespuesimpuesto,2) }}</td>
				<td class="text-left">


					@can('cotizaciones.show')
					<a class="btn btn-info btn-sm" href="{{ action('CotizacionController@show', ['id' => $cotizacion->id]) }}" role="button" title="Ver Cotización" target="_blank"><span data-feather="search"></span></a>
					@endcan

					@can('cotizaciones.delete')
					@if(is_null($cotizacion->deleted_at))
					<button style="float: right;" type="button" class="btn btn-danger btn-sm" title="Eliminar Cotización" onclick="borrarFactura({{ $cotizacion->id }},{{ $cotizacion->cod_interno }});"><span data-feather="delete"></span>
			    	</button>
			    	@endif
			    	@endcan
					

				</td>				
			</tr>
			@php
				$status = '';
			@endphp
			

			@endforeach			
		</table>

		<!--Muestra la lista de las paginas para mostrar los demas elementos
		para esto se debio de paginar (paginate) en el controllador -->
		{!!$listados->appends($_GET)->links()!!}
	
@endsection

@section('scrips')

	<script type="text/javascript">
		function borrarFactura(id, cod){
			//Ingresamos un mensaje a mostrar
			var mensaje = confirm("¿Confirma que desea Eliminar la Cotización "+cod+" ?");
			//Detectamos si el usuario acepto el mensaje
			if (mensaje) {
				var a = document.createElement("a");
				a.href = "{{ url('/cotizaciones/delete') }}/"+id;
				a.click();
			}						
		}
	</script>

@endsection