<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agencia extends Model
{
  use SoftDeletes;

  protected $fillable = ['id','nombrecomercial', 'descripcion','direccion','telefono','correo','web','codigointerno','cia_id'];

  protected $dates = ['deleted_at'];

  //*********Consultas BD*******************//
  public static function agenciasLista()
  {
    return Agencia::whereNull('deleted_at')
    ->orderBy('agencias.cia_id', 'asc')
    ->pluck('nombrecomercial','id');
  }

  public static function agenciasCiaLista()
  {
    return Agencia::where('cia_id', '=', session('cia_id'))
          ->whereNull('deleted_at')
          ->orderBy('agencias.cia_id', 'asc')
          ->pluck('nombrecomercial','id');
    }


  //*********Relaciones*********************//
	
  public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
  public function dato_facturas()
  {
    return $this->hasOne(DatoFactura::class);
  }
  public function facturas()
  {
      return $this->hasMany(Factura::class);
  }
  public function flujos()
  {
      return $this->hasMany(Flujo::class);
  }
  public function usuarios()
  {
      return $this->hasMany(User::class);
  }
  public function cajas()
  {
      return $this->hasMany(Caja::class);
  }




}
