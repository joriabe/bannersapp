<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;

    protected $fillable = ['id','categoria','acronimo','cia_id'];

    //*********Consultas BD*******************//
    public static function categoriasLista()
    {
    	return Categoria::where('cia_id', '=', session('cia_id'))
    					->whereNull('deleted_at')
                        ->orderBy('categorias.categoria', 'asc')
        				->pluck('categoria','id');
    }

    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
	public function productoservicios()
  	{
   		return $this->hasMany(ProductoServicio::class, 'categoria_id');
	}
}
