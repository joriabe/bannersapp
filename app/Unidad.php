<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidad extends Model
{
    use SoftDeletes;

    protected $fillable = ['id','unidad','acronimo','cia_id'];

    //*********Consultas BD*******************//
    public static function unidadesLista()
    {
    	return Unidad::where('cia_id', '=', session('cia_id'))
    					->whereNull('deleted_at')
                        ->orderBy('unidads.unidad', 'asc')
        				->pluck('unidad','id');
    }

    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
	public function productoservicios()
  	{
   		return $this->hasMany(ProductoServicio::class, 'unidad_id');
	}
}
