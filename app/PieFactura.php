<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PieFactura extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'totalimpuesto','calculoimpuesto','factura_id','tipo_impositivo_id'];
    protected $dates = ['deleted_at'];

    public function facturas()
	{
		return $this->belongsTo(Factura::class);
	}
	public function tipo_impositivos()
  {
   		return $this->hasMany(TipoImpositivo::class);
	}
}
