<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use SoftDeletes;
	
	protected $fillable = ['id','tabla','descripcion','referencia','cia_id','agencia_id','user_id'];

	protected $dates = ['deleted_at'];

	//CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Log::where('logs.cia_id', '=', session('cia_id'))
            ->join('cias', 'logs.cia_id', '=', 'cias.id')
            ->join('agencias', 'logs.agencia_id', '=', 'agencias.id')
            ->join('users', 'logs.user_id', '=', 'users.id')
	        ->filtro($request)
            ->orderBy('logs.updated_at', 'desc')
            ->select('logs.*','cias.nombrecomercial as cia_name','agencias.descripcion as agencia_name','users.name as user_name')  
            ->paginate(100);
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return $query->whereDate('logs.created_at', 'like', '%'.$busqueda.'%')
                      ->orwhere('agencias.descripcion', 'like', '%'.$busqueda.'%')
                      ->orwhere('users.name', 'like', '%'.$busqueda.'%');
    }
}
