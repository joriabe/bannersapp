<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class CxP extends Model
{
	use SoftDeletes;	
    
    protected $table = 'cxps';
    
    protected $fillable = ['id','fecha','fecha_vencimiento','valor_pendiente','valor_pagado','agencia_id','compra_id','user_id'];

    protected $dates = ['deleted_at'];

	//CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return CxP::where('agencias.cia_id', '=', session('cia_id'))
            ->join('agencias', 'cxps.agencia_id', '=', 'agencias.id')
            ->join('declaracion_compras', 'cxps.compra_id', '=', 'declaracion_compras.id')
            ->join('proveedores', 'declaracion_compras.proveedor_id', '=', 'proveedores.id')
            ->filtro($request)
            ->orderBy('cxps.updated_at', 'desc')
            ->select('cxps.*','proveedores.name as proveedor_name','declaracion_compras.num_docto')        
            ->paginate(9); 
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
        $busqueda = trim($request->i_buscar);

        return $query->where('cxps.fecha', 'like', '%'.$busqueda.'%')
                      ->orwhere('cxps.fecha_vencimiento', 'like', '%'.$busqueda.'%')
                      ->orwhere('proveedores.name', 'like', '%'.$busqueda.'%');
    }

    //Debuelve un conjunto de cxp que estan vencidas o por vencer
    public static function cxpVencidasAvencer()
    {   
        $hoy = Carbon::now();
        $hoymas2semanas = Carbon::now()->addWeeks(2);

        //$vencimiento = new Carbon\Carbon($cotizacion->fechavencimiento);

          $cxps = CxP::where('agencias.cia_id', '=', session('cia_id'))
            ->join('agencias', 'cxps.agencia_id', '=', 'agencias.id')
            ->join('declaracion_compras', 'cxps.compra_id', '=', 'declaracion_compras.id')
            ->join('proveedores', 'declaracion_compras.proveedor_id', '=', 'proveedores.id')
            ->where('valor_pendiente','>',0)
            ->whereBetween('cxps.fecha_vencimiento', array($hoy, $hoymas2semanas))
            ->orderBy('cxps.fecha_vencimiento', 'desc')
            ->select('cxps.*','proveedores.name as proveedor_name','declaracion_compras.num_docto')        
            ->paginate(4);


            return $cxps;
    }
}
