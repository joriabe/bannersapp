<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPago extends Model
{
  use SoftDeletes;
  
  public $incrementing = false;

  protected $fillable = ['id', 'tipopago','cia_id'];
  protected $dates = ['deleted_at'];

  //*********Consultas BD*******************//
  public static function tipopagoLista()
  {
    return TipoPago::where('cia_id', '=', session('cia_id'))
              ->whereNull('deleted_at')
              ->orWhere('id','=',1)
              ->orderBy('tipo_pagos.tipopago', 'asc')
              ->pluck('tipopago','id');
  }

  public static function tipopagos()
  {
    return TipoPago::where('cia_id', '=', session('cia_id'))
              ->whereNull('deleted_at')
              ->orWhere('id','=',1)
              ->orderBy('tipo_pagos.tipopago', 'asc')
              ->get();
  }

  //*********Relaciones*********************//
  public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
  public function flujos()
  {
      return $this->hasMany(Flujo::class);
  }

}
