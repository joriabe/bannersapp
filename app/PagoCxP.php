<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PagoCxP extends Model
{
	protected $table = 'pago_cxps';

    use SoftDeletes;

    protected $fillable = ['id', 'total_factura','valor_pago','comprobantepago','factura_id','tipopago_id'];
    protected $dates = ['deleted_at'];
}
