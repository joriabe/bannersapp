<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caja extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['id', 'apertura','cierre','nota','agencia_id','user_id'];
    protected $dates = ['deleted_at'];

    public function agencias()
	{
		return $this->belongsTo(Agencia::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
