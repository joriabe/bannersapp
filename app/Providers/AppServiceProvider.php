<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('validacionRangos', function ($attribute, $value, $parameters, $validator)
        {
            
            //elimina los diguitos, que es la numeracion fija del documento
            $rango_superior = (int) substr($parameters[0], 12, 19);  //dejando solo el codigo variable o correlativo 00000000
            $rango_inferior = (int) substr($value, 12, 19);
            
            //dd($rango_superior, $rango_inferior);

            return ($rango_inferior < $rango_superior);
        });

        Validator::extend('validacionMontos', function ($attribute, $value, $parameters, $validator)
        {
            $total_pagar = 0;

            foreach ($value as $apagar) {
                $total_pagar += $apagar;
            }

            return (($total_pagar+$parameters[0]) <= $parameters[1] and $total_pagar > 0);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
