<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoMovimiento extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'descripcion'];
    protected $dates = ['deleted_at'];
    
    public function flujos()
  	{
      return $this->hasMany(Flujo::class);
  	}
}
