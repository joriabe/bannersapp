<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cia extends Model
{
  use SoftDeletes;

  //public $incrementing = false;

  protected $fillable = ['id', 'rtn','nombrecomercial','razonsocial','logo'];

  protected $dates = ['deleted_at'];

  
  public function agencias()
  {
      return $this->hasMany(Agencia::class);
  }
	public function clientes()
  {
   		return $this->hasMany(Cliente::class);
	}
	public function categorias()
  {
   		return $this->hasMany(Categoria::class);
	}
  public function tipo_impositvos()
  {
      return $this->hasMany(TipoImpositivo::class);
  }
  public function producto_servicios()
  {
      return $this->hasMany(ProductoServicio::class);
  }
  public function tipo_pagos()
  {
      return $this->hasMany(TipoPago::class);
  }
  public function proveedores()
  {
      return $this->hasMany(Proveedor::class);
  }
  public function declaracion_compras()
  {
      return $this->hasMany(DeclaracionCompra::class);
  }
}
