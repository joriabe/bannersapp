<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
	use SoftDeletes;

    protected $table = 'planes';

	protected $fillable = ['id_plan','plan','descripcion','descripcion_factura','valor','frecuencia','intervalo','ciclo','cias','sucursales','usuarios','instlacion','cia_id'];

    protected $dates = ['deleted_at'];

      //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //debuelve un listado de los planes para los dropdown
    public static function planesLista()
    {
    	return Plan::where('cia_id', '=', 1)
    					->orwhere('cia_id', '=', session('cia_id'))
    					->whereNull('deleted_at')
                        ->orderBy('id', 'asc')
        				->pluck('descripcion_factura','id_plan');
    }
    
    //debuelve un listado de los planes para los dropdown
    public static function planesListaAll()
    {
        return Plan::whereNull('deleted_at')
                        ->orderBy('id', 'asc')
                        ->pluck('descripcion_factura','id_plan');
    }
}
