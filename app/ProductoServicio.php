<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

class ProductoServicio extends Model
{
    use SoftDeletes;

  protected $table = 'producto_servicios';
	protected $fillable = ['id','codinterno','descripcion','precio','costo','prodoctooservicio','categoria_id','descuento_id','tipoimpositivos_id','proveedor_id','unidad_id','cia_id'];
	
	protected $dates = ['deleted_at'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return ProductoServicio::where('producto_servicios.cia_id', '=', session('cia_id'))
			      ->join('categorias', 'producto_servicios.categoria_id', '=', 'categorias.id')
            ->join('tipo_impositivos', 'producto_servicios.tipoimpositivos_id', '=', 'tipo_impositivos.id')
            ->filtro($request)
       		  ->orderBy('producto_servicios.updated_at', 'desc')
            ->select('producto_servicios.*','categorias.categoria','tipo_impositivos.impuesto')
            ->paginate(9);              
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return  $query->where('producto_servicios.descripcion', 'like', '%'.$busqueda.'%')
                    ->orwhere('codinterno', 'like', '%'.$busqueda.'%')
                    ->orwhere('categoria', 'like', '%'.$busqueda.'%');
    }

	public function categorias()
	{
		return $this->belongsTo(Categoria::class, 'categoria_id');
	}
	public function unidades()
	{
		return $this->belongsTo(Unidad::class, 'unidad_id');
	}
	public function tipo_impositivo()
	{
		return $this->belongsTo(TipoImpositivo::class);
	}
	public function detalle_facturas()
  	{
      	return $this->hasMany(DetalleFactura::class);
  	}
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
	public function descuentos()
	{
		return $this->belongsTo(Descuento::class);
	}

}
