<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;
	
	protected $fillable = ['id','codinterno','rtn','nombrecliente','razonsocialcliente','direccion','telefono','correo','cia_id'];

	protected $dates = ['deleted_at'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Cliente::where('cia_id','=',session('cia_id'))
            ->orderBy('clientes.updated_at', 'desc')
	        ->filtro($request)
	        ->paginate(9);
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return $query->where('nombrecliente', 'like', '%'.$busqueda.'%')
                      ->orwhere('codinterno', 'like', '%'.$busqueda.'%')
                      ->orwhere('rtn', 'like', '%'.$busqueda.'%');
    }

	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
	public function facturas()
  	{
      return $this->hasMany(Factura::class);
  	}



}
