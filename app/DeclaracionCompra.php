<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeclaracionCompra extends Model
{
    use SoftDeletes;

	//protected $fillable = ['fecha','numfact1','numfact2','numfact3','numfact4','subtotal_exento','subtotal_15','subtotal_18','proveedor_id','cia_id'];

	protected $fillable = ['id','fecha','tipo_docto','num_docto','subtotal_exento','subtotal_15','subtotal_18','proveedor_id','cia_id'];
	
	protected $dates = ['deleted_at'];

	//*********Consultas BD*******************//


    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}

	public function proveedores()
	{
		return $this->belongsTo(Proveedor::class);
	}
}
