<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleCotizacion extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['id', 'descripcion','cantidad','precio','subtotal','cotizacion_id','producto_servicio_id'];
    
    protected $dates = ['deleted_at'];

    public function cotizaciones()
	{
		return $this->belongsTo(Cotizacion::class);
	}
	public function producto_servicios()
	{
		return $this->belongsTo(ProductoServicio::class);
	}
}
