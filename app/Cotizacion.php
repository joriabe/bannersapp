<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

class Cotizacion extends Model
{
use SoftDeletes;
    
    protected $fillable = ['id','cod_interno', 'fecha','fechavencimiento','totalantesimpuesto','totaldespuesimpuesto','descuento','observaciones','agencia_id','cliente_id','usuario_id'];
    protected $dates = ['fecha','deleted_at'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Cotizacion::where('agencias.cia_id', '=', session('cia_id'))
        ->join('clientes', 'cotizacions.cliente_id', '=', 'clientes.id')
        ->join('agencias', 'cotizacions.agencia_id', '=', 'agencias.id')
        ->orderBy('cotizacions.cod_interno', 'desc')
        ->filtro($request)
        ->select('cotizacions.*', 'nombrecliente','nombrecomercial')  
        ->paginate(9);
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return $query->where('cotizacions.fechavencimiento', 'like', '%'.$busqueda.'%')
     		 		 ->orwhere('cotizacions.fecha', 'like', '%'.$busqueda.'%')
                     ->orwhere('nombrecliente', 'like', '%'.$busqueda.'%');
    }

  	public function agencias()
	{
		return $this->belongsTo(Agencia::class);
	}
	public function clientes()
	{
		return $this->belongsTo(Cliente::class);
	}
	public function users()
	{
		return $this->belongsTo(User::class);
	}
	public function detalle_cotizaciones()
  	{
      return $this->hasMany(DetalleCotizacion::class);
  	}
  	public function pie_Cotizacion()
	{
		return $this->hasMany(PieCotizacion::class);
	}
}
