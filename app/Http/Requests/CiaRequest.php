<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //cia
            'input_rtn'          => 'digits:14',
            'input_logo'         => 'image|dimensions:max_width=900,max_height=900',
        ];
    }
}
