<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatoFacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        return [
            'rtn'          => 'digits:14',
            'logo'         => 'image|dimensions:max_width=900,max_height=900',
            'kai'          => 'alpha_dash|size:37',
            'ragosuperior' => 'size:19',
            'rangoinferior'=> 'size:19|validacionRangos:'.request('ragosuperior'),
        ];
    }
}
