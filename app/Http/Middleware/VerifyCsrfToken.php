<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'paypal/webhook',//Debido a que la petición viene fuera de la sesión y no contiene el CSRF token. Añadimos la ruta a las excepciones donde la validación no es necesaria. 
    ];
}
