<?php

namespace App\Http\Middleware;

use Closure;

use Session;
use Redirect;
use Carbon\Carbon;

use \App\Supscripcion as Subscripcion;

class Suspscripcion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        //determina la fecha de renovacion
        switch (session('suscripcion')) {
            case 'sin suscripcion':
                $mensage = "No tiene una suscripción activa, favor seleccione uno de los siguientes planes de suscripción.";
                break;
            case 'activa':
                return $next($request);
                break;
            case 'cancelado':
                $mensage = "Cancelo su suscripción. Si desea renovar su suscripción, favor seleccione alguno de los siguientes planes.";
                break;
            default:
                # code...
                break;
        }

        return redirect('/suscripcion/obtener')->with('message_alert', $mensage); 
       
    
    }
}
