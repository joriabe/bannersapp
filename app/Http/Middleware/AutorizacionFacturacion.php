<?php

namespace App\Http\Middleware;

use Closure;

use Session;

use \App\DatoFactura;
use App\Http\Controllers\DatoFacturaController as DatoFacturaC;

class AutorizacionFacturacion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $datosfactura = DatoFactura::where('agencia_id', Session('agencia_id'))->first();

        $mensage = "";
        $hoy = new \DateTime();
        $numerolimite = (int) DatoFacturaC::codVariable($datosfactura->ragosuperior);

        $a = 0;
        $b = 0;

        if($datosfactura->fechalimite > $hoy)
        {
            $a = 1;
        }
        else
        {
            $mensage = $mensage." Se ha sobrepasado la fecha limite de emisión.";
        }

        if($datosfactura->correlativof < $numerolimite)
        {
            $b = 1;
        }
        else
        {
            $mensage = $mensage." Se ha alcanzado el rango de numeración superior de facturación.";
        }

        if (($a + $b) == 2) 
        {
            return $next($request);
        }
        else
        {
            return redirect('/home')->with('message_alert', $mensage); 
        }
        
    }
}
