<?php

namespace App\Http\Middleware;

use Closure;

use \App\Cotizacion;
use Session;

class deleteCotizacionesVencidas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hoy = new \DateTime();

        $cotizaciones = Cotizacion::where('fechavencimiento', '<=', $hoy )
                                ->where('agencia_id', Session('agencia_id'))
                                ->get();

        if (isset($cotizaciones) ) 
        {
            foreach ($cotizaciones as $cotizacion) {
                $cotizacion->delete();
            }
            
        }

        return $next($request);
    }
}
