<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

use \App\ProductoServicio as Producto;
use \App\Descuento;

use Session;

class sinDescuentoDescuentoVencido
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hoy = new \DateTime();

        //obtiene la lista de descuentos vencidos
        $DescuentosVencidos = Descuento::where('fecha_fin', '<=', $hoy )
                                ->where('cia_id', Session('cia_id'))
                                ->get();

        //Determina si exiten descuentos vencidos
        if (isset($DescuentosVencidos)) 
        {
            //Recorre la lista de los descuentos vendiso
            foreach ($DescuentosVencidos as $vencidos) {

                //obtiene la lista de los productos con descuentos vencidos
                $productos = Producto::where('descuento_id', $vencidos->id)->get();
                //elimina el desciento
                $vencidos->delete();

                //determina si existen productos con el descuento vencido
                if (isset($productos)) 
                {
                    //recorre la lista de los productos con descuento vencido
                    foreach ($productos as $producto) {

                        //le asigna el descuento por defecto (Sin descuento)
                        $producto->descuento_id = 1;
                        $producto->save();
                    }//fin for
                }//fin if
            }//fin for  
        }//fin if

        return $next($request);
    }
}
