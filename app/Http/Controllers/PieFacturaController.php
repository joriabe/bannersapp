<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\PieFactura;


class PieFacturaController extends Controller
{
	
    public static function createPieFactura($pie, $factura_id)
    {
        //guarda el registro del pie
        $pies = PieFactura::create([
            'totalimpuesto' => round($pie['totalimpuesto'],2),
            'calculoimpuesto' => round($pie['calculoimpuesto'],2),
            'tipo_impositivo_id' => $pie['tipo_impositivo_id'],
            'factura_id' => $factura_id,
        ]);

        return;

    }

    
 
    public static function createPieFacturaLote($pies, $factura_id)
    {

        //recorre el lote de los pies de factura
        foreach ($pies as $pie) 
        {
            PieFacturaController::createPieFactura($pie, $factura_id);
        }

        return;
    }

    public static function findPieFactura($factura_id)
    {
        $pies = PieFactura::where('factura_id','=', $factura_id);

        $pagos[]=array();

        foreach ($pies as $pie) 
        {
                $pagos = array_add($pagos, "$tipo_impositivo->tipoimpositivos_id", [ 
                    'tipopago_id' => $pie->totalimpuesto + $pie->calculoimpuesto,
                    'valor_pagado' => $calculoimpuesto,
                    'comprobantepago' => $tipo_impositivo->tipoimpositivos_id,
                ]);
        }
    }
}
