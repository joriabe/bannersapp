<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\Cotizacion;
use \App\DetalleCotizacion;

class DetalleCotizacionController extends Controller
{
    public static function createCotizacionDetalle($cotizacion, $cotizacion_id)
    {
        //guarda el registro del cotizacion de la factura
        $cotizaciones = DetalleCotizacion::create([
            'descripcion' => $cotizacion['descripcion'],
            'cantidad' => $cotizacion['cantidad'],
            'precio' => round($cotizacion['precio'],2),
            'subtotal' => round($cotizacion['subtotal'],2),
            'cotizacion_id' => $cotizacion_id,
            'producto_servicio_id' => $cotizacion['producto_servicio_id'],
        ]);

        return;

    }

    
 
    public static function createDetalleCotizacionLote($cotizaciones, $cotizacion_id)
    {

        //recorre el lote de los cotizaciones de la factura
        foreach ($cotizaciones as $cotizacion) 
        {
            DetalleCotizacionController::createCotizacionDetalle($cotizacion, $cotizacion_id);
        }

        return;

    }
}
