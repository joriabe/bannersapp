<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\DeclaracionCompraRequest;

use \App\DeclaracionCompra;
use \App\Proveedor;
use \App\TipoPago;

use Illuminate\Support\Facades\DB;

use \App\CxP as CxPmodel;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\CxPController as CxP;
use App\Http\Controllers\FlujoController as Flujo;
use App\Http\Controllers\CxCController as CxC;


use Session;
use Redirect;

class DeclaracionCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $declaracioncompras = DB::table('declaracion_compras')
        ->join('proveedores', 'declaracion_compras.proveedor_id', '=', 'proveedores.id')
        ->where('declaracion_compras.cia_id', '=', session('cia_id'))
        ->whereNull('declaracion_compras.deleted_at')
        ->select('declaracion_compras.*', 'proveedores.name')
        ->orderBy('declaracion_compras.updated_at', 'desc')
        ->paginate(9);
        return view('declaracioncompras.index', compact('declaracioncompras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedor::proveedoresLista();

        $tipo_pagos = TipoPago::where('cia_id', '=', session('cia_id'))
        ->whereNull('deleted_at')
        ->orWhere('id','=',1)
        ->orderBy('tipo_pagos.tipopago', 'asc')
        ->get();

        return view('declaracioncompras.create', compact(['proveedores','tipo_pagos']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeclaracionCompraRequest $request)
    {
        if ($request['tipo_docto'] == 0)
        {
            $num_docto = $request['num_docto1'];
        }
        else
        {
            $num_docto =  $request['num_docto2'];
        }

        $declaracioncompras = DeclaracionCompra::create([
            'fecha' => $request['fecha'],
            'tipo_docto' => $request['tipo_docto'],
            'num_docto' => $num_docto,
            'subtotal_exento' => $request['subtotal_exento'],
            'subtotal_15' => $request['subtotal_15'],
            'subtotal_18' => $request['subtotal_18'],
            'proveedor_id' => $request['proveedor_id'],
            'cia_id' => session('cia_id'),
        ]);

        //guarda un registro de lo realizado 
        Log::register('declaracion_compras','Ingreso factura: '.$num_docto, $declaracioncompras->id);

        if($request['cxp'] == 'si')
        {
            $valor_pendiente = $request['subtotal_exento'] + ($request['subtotal_15'] * 1.15) + ($request['subtotal_18'] *1.18);

            //Crea la cuenta por pagar $factura_id, $fecha_vencimiento, $valor_pendiente
            CxP::createCxP($declaracioncompras->id, $request['fecha_vencimiento'], $valor_pendiente);
        }


        if($request['flujo'] == 'si')
        {
            //contrulle el array de pago a partir de las tres columnas del array
            $pago = CxC::crearArrayPago($request->tipo_pago_id, $request->valor_pagado, $request->comprobantepago);

            //realiza el registro del flujo de caja createFlujoLote($pagos, $referencia, $movimiento_id, $signo)
            //para los tipos de pagos que se realizaron
            Flujo::createFlujoLote($pago,'Pago de fact: '.$num_docto, 8, -1);
        }

        return redirect('/declaracioncompras')->with('message', 'Factura '.$num_docto.' registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $declaracioncompras = DeclaracionCompra::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('declaracioncompras.show', compact('declaracioncompras'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $declaracioncompras = DeclaracionCompra::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();


        $proveedores = Proveedor::proveedoresLista();

        return view('declaracioncompras.edit', compact('declaracioncompras','proveedores','fecha_vencimiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeclaracionCompraRequest $request, $id)
    {
        $declaracioncompras = DeclaracionCompra::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $declaracioncompras->fill($request ->all());
        $declaracioncompras->save();

        //guarda un registro de lo realizado 
        Log::register('declaracion_compras','Actualización Factura: '.$request['num_docto'], $declaracioncompras->id);

        Session::flash('message','Factura '.$request['num_docto'].' actualizado correctamente');
        return Redirect::to('/declaracioncompras');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $declaracioncompras = DeclaracionCompra::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        //busca y anula la cxp
        $cxp = CxPmodel::where('compra_id','=',$id)->first();
        if (!is_null($cxp))
        {
            $cxp->delete(); 
        }

        $declaracioncompras->delete();

        //guarda un registro de lo realizado 
        Log::register('declaracion_compras','Eliminación Factura: '.$declaracioncompras->num_docto, $declaracioncompras->id);

        Session::flash('message','Factura '.$declaracioncompras->num_docto.' eliminado correctamente');
        return Redirect::to('/declaracioncompras');
    }
}
