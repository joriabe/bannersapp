<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\TipoPago;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class TipoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipopagos = TipoPago::where('cia_id','=',session('cia_id'))
        ->orderBy('tipo_pagos.updated_at', 'desc')
        ->paginate(9);
        return view('tipopagos.index', compact('tipopagos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipopagos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipopagos = TipoPago::create([
            'tipopago' => $request['tipopago'],
            'cia_id' => session('cia_id'),
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_pagos','Creación Tipo Pago: '.$tipopagos->tipopago, $tipopagos->tipopago);

        return redirect('/tipopagos')->with('message', 'Tipo de Pago registrado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipopagos = TipoPago::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('tipopagos.edit', compact('tipopagos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipopagos = TipoPago::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $tipopagos->fill($request ->all());
        $tipopagos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_pagos','Actualización Tipo Pago: '.$tipopagos->tipopago, $tipopagos->id);

        Session::flash('message','Tipo de Pago '.$request['tipopago'].'   actualizado correctamente');
        return Redirect::to('/tipopagos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
        $tipopagos = TipoPago::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        $tipopagos->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_pagos','Eliminación Tipo Pago: '.$tipopagos->tipopago, $tipopagos->id);

        Session::flash('message','Tipo de Pago '.$id.' eliminado correctamente');
        return Redirect::to('/tipopagos');
    }
}
