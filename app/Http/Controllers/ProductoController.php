<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\ProductoServicio;
use \App\TipoImpositivo;
use \App\Categoria;
use \App\Unidad;
use \App\Descuento;
use \App\Proveedor;
use \App\Bodega;
use \App\ProductoInventario;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\InventarioController as Inventario;

use Session;
use Redirect;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //obtiene la consulta sobre el index y sus respectivos filtros
        $listados = ProductoServicio::indexConsulta($request);

        //variables necesarias para el form de la busqueda
        $ruta = "productos.index";
        $etiqueta = "Busqueda por codigo, descripcion o categoria.";
              
        return view('productos.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //obtiene una lista de los impuestos
        $impuestos = TipoImpositivo::impuestosLista();
        //obtiene una lista de las categorias
        $categorias = Categoria::categoriasLista();
        //obtiene una lista de las unidades
        $unidades = Unidad::unidadesLista();
        //obtiene una lista de los descuentos
        $descuentos = Descuento::descuentosLista();
        //obtiene una lista de los proveedores
        $proveedores = Proveedor::proveedoresListaProducto();
        //obtiene una lista de las bodegas
        $bodegas = Bodega::bodegas();

        return view('productos.create', compact('impuestos','categorias','unidades','descuentos','proveedores','bodegas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //abre una nueva transaccion
        DB::beginTransaction();
        
        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {

            //crea el registro para el producto
            $productos = new ProductoServicio([
                'codinterno' => $request['codinterno'],
                'descripcion' => $request['descripcion'],
                'precio' => $request['precio'],
                'costo' => $request['costo'],
                'prodoctooservicio' => $request['prodoctooservicio'],
                'descuento_id' => $request['descuento_id'],
                'categoria_id' => $request['categoria_id'],
                'tipoimpositivos_id' => $request['tipoimpositivos_id'],
                'proveedor_id' => $request['proveedor_id'],
                'unidad_id' => $request['unidad_id'],
                'cia_id' => session('cia_id'),
            ]);
            $productos->save();

            //si se esta registrando un producto
            if ($request['prodoctooservicio'] == 0) {
                //crea el array para guardar la informacion de prodcto_inventarios
                //crearArrayInventario($bodega_id,$cantidad, $cantidad_minima, $alerta_existencia, $facturar_sin_existencia, $prducto_id)
                $inventario = Inventario::crearArrayInventario($request->bodega_id, $request->cantidad, $request->cantidad_minima, $request->alerta_existencia, $request->facturar_sin_existencia,  $productos->id);

                //realizar el registro del inventario
                Inventario::createInventarioLote($inventario);
            }

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia
            Log::register('producto_servicios','Creación Producto: '.$productos->descripcion, $productos->id);

            //realiza los cambio en la base de datos
            DB::commit();

            return redirect('/productos')->with('message', 'Producto/Servicio registrado correctamente'); 

        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //respuesta al js de cuanto es el cambio
            return response()->json([
                'Ocurrio un Error:' => $error
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //obtiene una lista de los impuestos
        $impuestos = TipoImpositivo::impuestosLista();
        //obtiene una lista de las categorias
        $categorias = Categoria::categoriasLista();
        //obtiene una lista de las unidades
        $unidades = Unidad::unidadesLista();
        //obtiene una lista de los descuentos
        $descuentos = Descuento::descuentosLista();
        //obtiene una lista de los proveedores
        $proveedores = Proveedor::proveedoresListaProducto();

        //obtiene una lista de las bodegas
        $bodegas = Bodega::bodegas();

        $productos = ProductoServicio::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $inventarios = ProductoInventario::inventarioProducto($id);

        //dd($inventario);

        return view('productos.edit', compact('productos','impuestos','categorias','unidades','descuentos','proveedores','bodegas','inventarios'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productos = ProductoServicio::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $productos->fill($request ->all());
        $productos->save();

        //si se esta registrando un producto
        if ($request['prodoctooservicio'] == 0) {
            //crea el array para guardar la informacion de prodcto_inventarios
            //crearArrayInventario($bodega_id,$cantidad, $cantidad_minima, $alerta_existencia, $facturar_sin_existencia, $prducto_id)
            $inventario = Inventario::crearArrayInventario($request->bodega_id, $request->cantidad, $request->cantidad_minima, $request->alerta_existencia, $request->facturar_sin_existencia,  $productos->id);

            //realizar el registro del inventario
            Inventario::updateInventarioLote($inventario);
        }


        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('producto_servicios','Actualización Producto: '.$productos->descripcion, $productos->id);

        Session::flash('message','Producto/Servicio '.$request['descripcion'].'   actualizado correctamente');
        return Redirect::to('/productos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
                //abre una nueva transaccion
        DB::beginTransaction();
        
        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {

        $productos = ProductoServicio::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $productos->delete();

        //si se esta registrando un producto
        if ($productos->prodoctooservicio == 0) {

            //realizar el registro del inventario
            Inventario::deleteInventarioLote($productos->id);
        }


        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('producto_servicios','Eliminación Producto: '.$productos->descripcion, $productos->id);

        

            //realiza los cambio en la base de datos
            DB::commit();
            
            Session::flash('message','Producto/Servicio '.$id.' eliminado correctamente');
            return Redirect::to('/productos');

        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //respuesta al js de cuanto es el cambio
            return response()->json([
                'Ocurrio un Error:' => $error
            ]);
        }
    }

    public static function asignarDescuento($descuento_id, $id)
    {
        $productos = ProductoServicio::find($id);

        $productos->descuento_id = $descuento_id;
        $productos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('producto_servicios','Actualización Producto: '.$productos->descripcion.' (descuento)', $productos->id);

        return;
    }

    public static function removerDescuentos($descuento_id)
    {
        $productos = ProductoServicio::where('descuento_id','=', $descuento_id)
        ->where('cia_id','=',session('cia_id'))
        ->get();

        foreach ($productos as $producto) {
            ProductoController::asignarDescuento(1, $producto->id); // 1 = Sin Descuento
        }

        return;
    }
}
