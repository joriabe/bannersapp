<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\DescuentoRequest;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\ProductoController as Producto;

use \App\Descuento;

use Session;
use Redirect;

class DescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descuentos = Descuento::where('cia_id','=',session('cia_id'))
        ->withTrashed()
        ->orderBy('descuentos.updated_at', 'desc')
        ->paginate(9);
        return view('descuentos.index', compact('descuentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('descuentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DescuentoRequest $request)
    {
        $descuentos = new Descuento([
            'fecha_inicio' => $request['fecha_inicio'],
            'fecha_fin' => $request['fecha_fin'],
            'descripcion' => $request['descripcion'],
            'descuento' => $request['descuento'],
            'cia_id' => session('cia_id'),
        ]);

        $descuentos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('descuentos','Creación Descuento : '.$descuentos->descripcion, $descuentos->id);

        return redirect('/descuentos')->with('message', 'Descuento: '.$descuentos->descripcion.'  registrado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$descuentos = Descuento::find($id);

        //return view('descuentos.show', compact('descuentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $descuentos = Descuento::withTrashed()
        ->where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('descuentos.edit', compact('descuentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DescuentoRequest $request, $id)
    {
        $descuentos = Descuento::withTrashed()
        ->where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $descuentos->fill($request ->all());
        $descuentos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('descuentos','Actualización Descuento : '.$descuentos->descripcion, $descuentos->id);

        Session::flash('message','Descuento '.$request['descripcion'].', actualizado correctamente');
        return Redirect::to('/descuentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $descuentos = Descuento::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        Producto::removerDescuentos($id);
        
        $descuentos->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('descuentos','Eliminación descuento : '.$descuentos->descripcion, $descuentos->id);

        Session::flash('message','Descuento: '.$descuentos->descripcion.', eliminado correctamente');
        return Redirect::to('/descuentos');
    }

    public function activar($id)
    {
        $descuentos = Descuento::withTrashed()
        ->where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        $descuentos->restore();

        $descuentos = Descuento::find($id);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('descuentos','Habilitar Descuento: '.$descuentos->descripcion, $descuentos->id);

        Session::flash('message','Descuento: '.$descuentos->descripcion.', reactivado');
        return Redirect::to('/descuentos');
    }

    public function getAmasiva()
    {   
        //obtiene una lista de los descuentos
        $descuentos = Descuento::descuentosLista();

        return view('descuentos.a_masiva', compact('descuentos'));
    }

    public function getProductos($producto)
    {   
        
        //aqui buscamos el producto enviado desde el buscador
        if ($producto == "all" || $producto == "todos" ) {

            $productos = \DB::table('producto_servicios')
            ->where('producto_servicios.cia_id','=',session('cia_id'))
            ->select('producto_servicios.id as p_id', 'producto_servicios.descripcion', 'producto_servicios.precio')
            ->orderBy('descripcion', 'asc')
            ->get();

        } else {

           $productos = \DB::table('producto_servicios')
            ->join('categorias', 'producto_servicios.categoria_id', '=', 'categorias.id')
            ->where('producto_servicios.cia_id','=',session('cia_id'))
            ->where(function ($query) use ($producto) {
                $query->where('descripcion', 'like' , '%'.$producto.'%')
                      ->orWhere('codinterno', 'like' , '%'.$producto.'%')
                      ->orWhere('categoria', 'like' , '%'.$producto.'%');
            })            
            ->select('producto_servicios.id as p_id', 'producto_servicios.descripcion', 'producto_servicios.precio')
            ->orderBy('descripcion', 'asc')
            ->get();
        }

        return $productos;
    }

    public function postAmasiva(Request $request)
    {
        $contador = 0;

        foreach ($request->productos as $producto) {
            Producto::asignarDescuento($request->descuento_id, $producto);
            $contador ++;
        }

        return redirect('/descuentos')->with('message', 'Descuento aplicado a: '.$contador.'  productos/servicios.'); 
        
    }


}
