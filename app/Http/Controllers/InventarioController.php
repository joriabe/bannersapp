<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use \App\ProductoInventario;
use \App\Bodega;

use App\Http\Controllers\LogController as Log;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Contruye el array para el inventario de la tabala producto_inventarios a partir de sus 6 columnas basicas
    public static function crearArrayInventario($bodega_id,$cantidad, $cantidad_minima, $alerta_existencia, $facturar_sin_existencia, $producto_id)
    {
        //declara un array vacio para el inventario
        //sera utilizado para llenar la tabla producto_invetarios
        $inventario = array();

        //contador de las interacciones dentro del array
        $contador = 0;

        //recorre los arrayas
        foreach ($bodega_id as $id) {

            //une los arrays segun el formato de la tabla pago_facturas
            $inventario = array_add($inventario,"$contador",[
                    'bodega_id'              =>  $bodega_id[$contador],
                    'cantidad'               =>  $cantidad[$contador],
                    'cantidad_minima'        =>  $cantidad_minima[$contador],
                    'alerta_existencia'      =>  $alerta_existencia[$contador],
                    'facturar_sin_existencia'=>  $facturar_sin_existencia[$contador],
                    'producto_id'            =>  $producto_id
                ]);
            
            //amuenta el contador
            $contador ++;
            
        }
        return $inventario;
    }

    //
    public static function createInventario($inventario)
    {
        //guarda el registro del inventario
        $inventarios = ProductoInventario::create([
            'cantidad' => $inventario['cantidad'],
            'cantidad_minima' => $inventario['cantidad_minima'],
            'alerta_existencia' => $inventario['alerta_existencia'],
            'facturar_sin_existencia' => $inventario['facturar_sin_existencia'],
            'producto_id' => $inventario['producto_id'],
            'bodega_id' => $inventario['bodega_id'],
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('producto_inventarios','Registro inventario: '.$inventarios->id, $inventarios->id);

        return;
    }

    //Descompone las matrices para poder ser registradas 
    public static function createInventarioLote($inventarios)
    {
        //recorre el lote de los inventarios de factura
        foreach ($inventarios as $inventario) 
        {
            InventarioController::createInventario($inventario);

        }
        return;
    }

    //
    public static function updateInventario($inventario)
    {
        //busca los registros de inventarios correspondientes para poder actualizar
        $inventarios = ProductoInventario::where('bodega_id',$inventario['bodega_id'])
        ->where('producto_id',$inventario['producto_id'])
        ->firstOrFail();


        $inventarios->fill($inventario);
        $inventarios->save();

        Log::register('producto_inventarios','Actualizar inventario: '.$inventarios->id, $inventarios->id);

        return;
    }

    //Descompone las matrices para poder ser actualuzados 
    public static function updateInventarioLote($inventarios)
    {
        //recorre el lote de los inventarios
        foreach ($inventarios as $inventario) 
        {
            InventarioController::updateInventario($inventario);

        }
        return;
    }

    //Descompone las matrices para poder ser actualuzados 
    public static function deleteInventarioLote($id_producto)
    {
        //obtiene todos los invetarios donde este almacenado el producto.
        $inventarios = ProductoInventario::where('producto_id',$id_producto)
        ->get();

        //recorre el lote de los inventarios
        foreach ($inventarios as $inventario) 
        {
            $inventario->delete();

            Log::register('producto_inventarios','Eliminar inventario: '.$inventario->id, $inventario->id);

        }
        return;
    }


    public static function updateProductoExistencia($detalle)
    {
        //obtiene todos los invetarios donde este almacenado el producto.
        $inventarios = ProductoInventario::where('producto_id',$id_producto)
        ->were('bodega_id', $bodega_id)
        ->get();


        //guarda el registro del detalle de la factura
        $detalles = DetalleFactura::create([
            'descripcion' => $detalle['descripcion'],
            'cantidad' => $detalle['cantidad'],
            'precio' => round($detalle['precio'],2),
            'subtotal' => round($detalle['subtotal'],2),
            'factura_id' => $factura_id,
            'producto_servicio_id' => $detalle['producto_servicio_id'],
        ]);

        return;

    }

    
 
    public static function updateProductosExistencias($productos_id)
    {
        $bodega_id = Bodega::bodegaPrincipal();

        //recorre el lote de los detalles de la factura
        foreach ($productos_id as $prudcto_id) 
        {
            //obtiene el iventario del producto dado de la bodega principal
            $inventarios = ProductoInventario::where('producto_id', $prudcto_id)
            ->were('bodega_id', $bodega_id)
            ->first();

            ProductoInventario::updateProductoExistencia($detalle);
        }

        return;

    }
}
