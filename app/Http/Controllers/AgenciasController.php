<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\LogController as Log;

use App\Agencia as Agencia;

class AgenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('companias.new_agencia' , ['cia_id'=> $request->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //creamos la agencia
        $agencia = new Agencia; 
        $agencia->nombrecomercial = $request->input_nombrecomercial;
        $agencia->descripcion = $request->input_descripcion;
        $agencia->direccion = $request->input_direccion;
        $agencia->telefono = $request->input_telefono;
        $agencia->correo = $request->input_email;
        $agencia->web = $request->input_web;
        $agencia->codigointerno = $request->input_cinterno;
        $agencia->cia_id = intval($request->cia_id);
        $agencia->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('agencias','Creació agencia: '.$agencia->descripcion,$agencia->id);

        return redirect('cias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agencia = Agencia::find($id);

        return view('companias.edit_agencia' , ['agencia'=> $agencia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agencia = Agencia::find($id);
        
        $agencia->nombrecomercial = $request->input_nombrecomercial;
        $agencia->descripcion = $request->input_descripcion;
        $agencia->direccion = $request->input_direccion;
        $agencia->telefono = $request->input_telefono;
        $agencia->correo = $request->input_email;
        $agencia->web = $request->input_web;
        $agencia->codigointerno = $request->input_cinterno;
        $agencia->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('agencias','Edición agencia: '.$agencia->descripcion, $agencia->id);

        return redirect('cias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agencia = Agencia::find($id);
        $agencia->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('agencias','Eliminación agencia: '.$agencia->descripcion, $agencia->id);

        return redirect('cias');
    }

    public function getagencias(Request $request, $id)
    {
        if($request->ajax()){
            $agencias = Agencia::agencias($id);
            return response()->json($agencias);
        }
    }
}
