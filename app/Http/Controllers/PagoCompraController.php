<?php

namespace App\Http\Controllers;

use App\PagoCompra;
use Illuminate\Http\Request;

use App\Http\Controllers\LogController as Log;

use \App\PagoCxP;

class PagoCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PagoCompra  $pagoCompra
     * @return \Illuminate\Http\Response
     */
    public function show(PagoCompra $pagoCompra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PagoCompra  $pagoCompra
     * @return \Illuminate\Http\Response
     */
    public function edit(PagoCompra $pagoCompra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PagoCompra  $pagoCompra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PagoCompra $pagoCompra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PagoCompra  $pagoCompra
     * @return \Illuminate\Http\Response
     */
    public function destroy(PagoCompra $pagoCompra)
    {
        //
    }

    public static function createPagoCompra($pago, $total_factura, $factura_id)
    {
        //guarda el registro del pago
        $pagos = PagoCxP::create([
            'total_factura' => round(($total_factura),2),
            'valor_pago' => round($pago['valor_pagado'],2),
            'comprobantepago' => $pago['comprobantepago'],
            'factura_id' => $factura_id,
            'tipopago_id' => $pago['tipopago_id'],
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('pago_cxps','Pago CxP: '.$factura_id, $pagos->id);

        return;
    }
 
    public static function createPagoCompraLote($pagos, $total_factura, $factura_id)
    {
        //recorre el lote de los pagos de factura
        foreach ($pagos as $pago) 
        {
            if ($pago['valor_pagado'] != 0 or $pago['valor_pagado'] != -0) {
                PagoCompraController::createPagoCompra($pago, $total_factura, $factura_id);
            }
            
        }
        return;
    } 
}
