<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\Factura;
use \App\DetalleFactura;


class DetalleFacturaController extends Controller
{
	/**
     * Realiza el recorrido y los calculos correspondiente
     * para la el detaye de la factura
     * @param  array  $detalles
     * @return array  [$pie_factura[],$totalantesimpuestos, $totaldespuesimpuestos]
     */
    public static function calculoDetalle($detalles)
    {   
        //declara un array vacio para el pie de la factura
        //sera utilizado para llenar la tabla pie_factura
        $pie_factura[]=array();

        //obtendra el subtotal antes de impuestos
        $totalantesimpuesto = 0;

        //obtendra el valor total de los impuestos
        $impuestosacumulativos = 0;

        //obtiene el valor total del descuento
        $totaldescuentos = 0;

        //realiza un recorrido por todo el detalle de la factura
        foreach ($detalles as $detalle)
        {
            //obtiene el valor del tipo impositivo
            $tipo_impositivo =  DB::table('producto_servicios')
                                ->join('tipo_impositivos', 'tipoimpositivos_id', '=', 'tipo_impositivos.id')
                                ->join('descuentos','descuento_id', '=','descuentos.id')
                               ->where('producto_servicios.id','=',$detalle['producto_servicio_id'])
                               ->select('tipoimpositivos_id','impuesto', 'descuento')
                               ->first();

            //calcula el total del valor sin impuesto
            $subtotal = $detalle['cantidad'] * $detalle['precio'];

            //caclula el valor del descuento
            $descuentos = $subtotal*($tipo_impositivo->descuento/100);

            //calcula el valor del impuesto
            $calculoimpuesto = ($subtotal-$descuentos )*($tipo_impositivo->impuesto/100);

            //obtiene el valor acumulativo del subtotal antes de impuestos
            $totalantesimpuesto = $totalantesimpuesto + $subtotal;

            //obtiene el valor acumulativo de los impuestos
            $impuestosacumulativos = $impuestosacumulativos + $calculoimpuesto;

            //obtiene el valor acumulativo del descuento
            $totaldescuentos += $descuentos;

            //determina si exite una llave (tipo_impositivo) en el array de pie_factura
            //el cual va haciendo la suma de los tipos de impuesto
            $flag = array_has($pie_factura, "$tipo_impositivo->tipoimpositivos_id");
            if( $flag == false)
            {
                //si no exite la llave, crea los datos para ese impuesto
                $pie_factura = array_add($pie_factura, "$tipo_impositivo->tipoimpositivos_id", [ 
                    'totalimpuesto' => $subtotal - $descuentos,
                    'calculoimpuesto' => $calculoimpuesto,
                    'tipo_impositivo_id' => $tipo_impositivo->tipoimpositivos_id,
                ]);
            }
            else
            {
                //si ya exite la clave, se le suman el total de impustos, y el calculo del impuesto

                //suma del caculo de los impuestos por tipo impositvo
                array_set($pie_factura, "$tipo_impositivo->tipoimpositivos_id".'.calculoimpuesto',
                        (data_get($pie_factura, 
                             ("$tipo_impositivo->tipoimpositivos_id").'.calculoimpuesto') + $calculoimpuesto));

                //suma del caculo del total antes de impuestos por tipo impositvo
                array_set($pie_factura, "$tipo_impositivo->tipoimpositivos_id".'.totalimpuesto',
                        ( data_get($pie_factura, 
                            "$tipo_impositivo->tipoimpositivos_id".'.totalimpuesto') + $subtotal - $descuentos));

                //nota, array_set establece un valor de un array
                //data_get obtiene el valor de un array
                //($array, 'nivel1.nivel2')
            }//fin de if
        }//fin de for

        //limpia el array pie_factura para quitar la llave 0
        array_forget($pie_factura, "0");

        //forma el array con la informacion procesada
        $array = [
            'pie_factura' => $pie_factura,
            'totalantesimpuesto' => $totalantesimpuesto,
            'totaldespuesimpuesto' => $totalantesimpuesto + $impuestosacumulativos - $totaldescuentos,
            'descuento' => $totaldescuentos, 
        ];

        return $array;
    }

    public static function calculoDetalleSI($detalles)
    {   
        //declara un array vacio para el pie de la factura
        //sera utilizado para llenar la tabla pie_factura
        $pie_factura[]=array();

        //obtendra el subtotal antes de impuestos
        $totalantesimpuesto = 0;

        //obtendra el valor total de los impuestos
        $impuestosacumulativos = 0;

        //obtiene el valor total del descuento
        $totaldescuentos = 0;

        //realiza un recorrido por todo el detalle de la factura
        foreach ($detalles as $detalle)
        {
            //obtiene el valor del tipo impositivo
            $tipo_impositivo =  DB::table('producto_servicios')
                                ->join('tipo_impositivos', 'tipoimpositivos_id', '=', 'tipo_impositivos.id')
                                ->join('descuentos','descuento_id', '=','descuentos.id')
                               ->where('producto_servicios.id','=',$detalle['producto_servicio_id'])
                               ->select('tipoimpositivos_id','impuesto', 'descuento')
                               ->first();

            //calcula el total del valor sin impuesto
            $subtotal = $detalle['cantidad'] * $detalle['precio'];

            //caclula el valor del descuento
            $descuentos = $subtotal*($tipo_impositivo->descuento/100);

            //calcula el valor del impuesto (sin)
            $calculoimpuesto = 0;

            //obtiene el valor acumulativo del subtotal antes de impuestos
            $totalantesimpuesto = $totalantesimpuesto + $subtotal;

            //obtiene el valor acumulativo de los impuestos
            $impuestosacumulativos = $impuestosacumulativos + $calculoimpuesto;

            //obtiene el valor acumulativo del descuento
            $totaldescuentos += $descuentos;

            //determina si exite una llave (2) en el array de pie_factura
            //el cual va haciendo la suma de los tipos de impuesto
            $flag = array_has($pie_factura, "2");
            if( $flag == false)
            {
                //si no exite la llave, crea los datos para ese impuesto
                $pie_factura = array_add($pie_factura, "2", [ 
                    'totalimpuesto' => $subtotal,
                    'calculoimpuesto' => $calculoimpuesto,
                    'tipo_impositivo_id' => 2,
                ]);
            }
            else
            {
                //si ya exite la clave, se le suman el total de impustos, y el calculo del impuesto

                //suma del caculo de los impuestos por tipo impositvo
                array_set($pie_factura, "2".'.calculoimpuesto',
                        (data_get($pie_factura, 
                             ("2").'.calculoimpuesto') + $calculoimpuesto));

                //suma del caculo del total antes de impuestos por tipo impositvo
                array_set($pie_factura, "2".'.totalimpuesto',
                        ( data_get($pie_factura, 
                            "2".'.totalimpuesto') + $subtotal));

                //nota, array_set establece un valor de un array
                //data_get obtiene el valor de un array
                //($array, 'nivel1.nivel2')
            }//fin de if
        }//fin de for

        //limpia el array pie_factura para quitar la llave 0
        array_forget($pie_factura, "0");

        //forma el array con la informacion procesada
        $array = [
            'pie_factura' => $pie_factura,
            'totalantesimpuesto' => $totalantesimpuesto,
            'totaldespuesimpuesto' => $totalantesimpuesto + $impuestosacumulativos - $totaldescuentos,
            'descuento' => $totaldescuentos, 
        ];

        return $array;
    }

    public static function createDetalle($detalle, $factura_id)
    {
        //guarda el registro del detalle de la factura
        $detalles = DetalleFactura::create([
            'descripcion' => $detalle['descripcion'],
            'cantidad' => $detalle['cantidad'],
            'precio' => round($detalle['precio'],2),
            'subtotal' => round($detalle['subtotal'],2),
            'factura_id' => $factura_id,
            'producto_servicio_id' => $detalle['producto_servicio_id'],
        ]);

        return;

    }

    
 
    public static function createDetalleLote($detalles, $factura_id)
    {

        //recorre el lote de los detalles de la factura
        foreach ($detalles as $detalle) 
        {
            DetalleFacturaController::createDetalle($detalle, $factura_id);
        }

        return;

    }
}
