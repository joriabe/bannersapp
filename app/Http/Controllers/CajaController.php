<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\FlujoController as Flujo;

use \App\Caja;

use Session;
use Redirect;

class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cajas = DB::table('cajas')
        ->join('agencias', 'cajas.agencia_id', '=', 'agencias.id')
        ->join('users', 'cajas.user_id', '=', 'users.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereNull('cajas.deleted_at')
        ->orderBy('cajas.updated_at', 'desc')
        ->select('cajas.*','agencias.descripcion as agencia_name','users.name as user_name')        
        ->paginate(9);
        
        return view('cajas.index', compact('cajas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cajas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $cajas = new Caja([
            'apertura' => $request['apertura'],
            'cierre' => '0',
            'nota' => $request['nota'],
            'agencia_id' => session('agencia_id'),
            'user_id' => Auth::id(),
        ]);
        $cajas->save();

        //almacena la caja que el usuario abrio para luego ser cerrada
        session(['caja' => $cajas->id]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('cajas','Apertura caja: '.$cajas->nota, $cajas->id);

        return redirect('/pos')->with('message', 'Caja iniciada con: '.$request['apertura']); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if (Auth::user()->can('rep.flujosexcel')) 
        {
            $cajas = Caja::where('cajas.id','=',$id) //inerjoin cia
            ->join('agencias', 'cajas.agencia_id', '=', 'agencias.id')
            ->where('agencias.cia_id','=',session('cia_id'))
            ->where('cajas.id','=',$id)
            ->select('cajas.*')
            ->firstOrFail();
        }
        else
        {
            $cajas = Caja::where('cajas.id','=',$id) //inerjoin cia
            ->join('agencias', 'cajas.agencia_id', '=', 'agencias.id')
            ->where('agencias.cia_id','=',session('cia_id'))
            ->where('cajas.id','=',$id)
            ->where('cajas.user_id','=', Auth::id())
            ->select('cajas.*')
            ->firstOrFail();
        }


        $flujos = Flujo::resumenFlujoCaja($cajas->apertura, $id);

        return view('cajas.edit', compact('cajas','flujos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        session()->forget('caja');
        
        $cajas = Caja::where('id','=',$id)
        ->where('agencia_id','=',session('agencia_id'))
        ->firstOrFail();

        $cajas->fill($request ->all());
        $cajas->save();

        $cajas->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('cajas','Cierre caja: '.$cajas->nota, $cajas->id);

        Session::flash('message','Caja cerrada exitosamente');
        return Redirect::to('/pos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
