<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use \App\Cotizacion;


use App\Http\Controllers\DetalleFacturaController as DetalleFactura;
use App\Http\Controllers\DetalleCotizacionController as DetalleCotizacion;
use App\Http\Controllers\PieCotizacionController as Pie;

use App\Http\Controllers\DatoFacturaController as DatoFactura;
use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Cotizacion::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "cotizaciones.index";
      $etiqueta = "Busqueda por ID, fecha, fecha v,  cliente.";
      
      return view('cotizaciones.index', compact('listados','ruta','etiqueta'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //$array = $pru;
        $array = json_decode($request->getContent(), true);

        //asigna el detalle de la factura a un nuevo array
        $detalle_facturas = $array[4];

        //realiza los calculos del detalle y el pie de la factura
        $calculo_detalle =  DetalleFactura::calculoDetalle($detalle_facturas); // return array  [$pie_factura[],$totalantesimpuestos, $totaldespuesimpuestos, $descuento]


        //abre una nueva transaccion
        DB::beginTransaction();

        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {

            //contrulle el codigo de la factura
            $cod_cotizacion = DatoFactura::codCotizacion(); 

            //guarda el encabezado de la factura
            $cotizaciones = Cotizacion::create([
                'cod_interno' =>  $cod_cotizacion,
                'fecha' => new \DateTime(),
                'fechavencimiento' =>  $array[3],
                'totalantesimpuesto' =>  round($calculo_detalle['totalantesimpuesto'], 2),
                'totaldespuesimpuesto' => round($calculo_detalle['totaldespuesimpuesto'], 2),
                'descuento' => round($calculo_detalle['descuento'], 2),
                'observaciones' =>  $array[2],
                'agencia_id' => session('agencia_id'),
                'cliente_id' => $array[1],
                'usuario_id' => Auth::id(),
            ]);            

            //realiza el registro del detalle de la cotizacion //$detalle:cotizaciones, $cotizaciones_id
            DetalleCotizacion::createDetalleCotizacionLote($detalle_facturas, $cotizaciones->id);

            //realiza el registro del calculo de los impuestos de la factura //$detalle:facturas, $facturas_id
            Pie::createPieCotizacionLote($calculo_detalle['pie_factura'], $cotizaciones->id);

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia
            Log::register('cotizacions','Creación Cotización: '.$cotizaciones->id,$cotizaciones->id);

            //realiza los cambio en la base de datos
            DB::commit();

        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //respuesta al js de cuanto es el cambio
            return response()->json([
                'msg' => $error,
                'msg2' => 1
            ]);
        }
        
         //respuesta al js de cuanto es el cambio
        return response()->json([
            'msg' => 'Cotizacion ingresada con exito',
            'msg2' => $cotizaciones->id
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //optenemos la cotizacion
        $cotizacion = DB::table('cotizacions')
        ->join('agencias', 'cotizacions.agencia_id', '=', 'agencias.id')
        ->join('users', 'cotizacions.usuario_id', '=', 'users.id')
        ->where([
            ['cotizacions.id', '=', $id],
            ['agencias.cia_id', '=', session('cia_id')],
        ])
        ->select('cotizacions.*','name')
        ->first();

        //si no se encuentra la factura en base a $id y cia_id
        if ( !isset($cotizacion) ) {
          abort(404);
        }

        $datos_cia = \DB::table('cias')
            ->join('agencias', 'cias.id', '=', 'agencias.cia_id')
            ->select('agencias.nombrecomercial', 'razonsocial', 'direccion', 'correo', 'rtn', 'telefono', 'web')
            ->where('agencias.id', '=' , $cotizacion->agencia_id)            
            ->first();

        $cliente = \DB::table('clientes')
            ->select('rtn','nombrecliente','direccion')
            ->where('id', '=' , $cotizacion->cliente_id)
            ->first();

        $detalle_cotizacion = \DB::table('detalle_cotizacions')
            ->join('producto_servicios', 'producto_servicios.id', '=', 'detalle_cotizacions.producto_servicio_id')
            ->select('cantidad','detalle_cotizacions.descripcion', 'detalle_cotizacions.precio', 'subtotal', 'codinterno as p_id')
            ->where('cotizacion_id', '=' , $id)
            ->get();

        $pie_cotizacion = \DB::table('pie_cotizacions')
            ->join('tipo_impositivos', 'pie_cotizacions.tipo_impositivo_id', '=', 'tipo_impositivos.id')
            ->select('descripcion', 'totalimpuesto', 'calculoimpuesto')
            ->where('cotizacion_id', '=' , $id)
            ->get();

        $logo = DB::table('dato_facturas') 
            ->where('agencia_id', '=' , $cotizacion->agencia_id)
            ->whereNull('deleted_at') 
            ->value('logo');

        return view('modelosfactura.modelocotizacion_1',['cotizacion' => $cotizacion, 'cia' => $datos_cia, 'cliente' => $cliente, 'detalles' => $detalle_cotizacion, 'pies' => $pie_cotizacion, 'logo' => $logo ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        //Busca y anula la factura
        $cotizaciones = Cotizacion::find($id);

        $cotizaciones->delete();

        //guarda un registro de lo realizado 
        Log::register('cotizacions','Anulación de Cotización: '.$cotizaciones->cod_interno, $cotizaciones->id);

        Session::flash('message','Cotización '.$cotizaciones->cod_interno.' anulada correctamente');
        return Redirect::to('/cotizaciones');
    }
        
}
