<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\DatoFacturaRequest;

use Carbon\Carbon;

use \App\DatoFactura;
use \App\Agencia;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class DatoFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datofacturas =  DB::table('dato_facturas')
        ->join('agencias', 'dato_facturas.agencia_id', '=', 'agencias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereNull('dato_facturas.deleted_at')
        ->whereNull('agencias.deleted_at')
        ->orderBy('dato_facturas.updated_at', 'desc')
        ->select('dato_facturas.*','agencias.codigointerno')        
        ->paginate(9);

        return view('datofacturas.index', compact('datofacturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencias = Agencia::agenciasCiaLista();
        return view('datofacturas.create', compact('agencias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DatoFacturaRequest $request)
    {

        if($request->hasfile('logo')){
            //obtenemos el campo file definido en el formulario
            $file = $request->file('logo');
            //determinamos la estencion del archivo
            $extension = $file->clientExtension();
            //contruimos el nombre del archivo
            $nombre = session('cia_id')."-LogoFactura.".$extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put($nombre,  \File::get($file));
            //url de la imagen
            $url = asset('storage/' . $nombre); 
        }


        $datofacturas = new DatoFactura([
            'correlativof' => $request['correlativof'],
            'correlativo' => 1,
            'contador_cotizacion' => 1,
            'kai' => $request['kai'],
            'ragosuperior' => $request['ragosuperior'],
            'rangoinferior' => $request['rangoinferior'],
            'fechalimite' => $request['fechalimite'],
            'frase' => $request['frase'],
            'logo' => $url,
            'agencia_id' => $request['agencia_id'],
        ]);

        $datofacturas->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('dato_facturas','Creación de Datos: '.$datofacturas->rangoinferior.' al '.$datofacturas->rangoinferior, $datofacturas->id);

        return redirect('/datofacturas')->with('message', 'Datos de Facturación registrados correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agencias = Agencia::agenciasCiaLista();

        $datofacturas = DatoFactura::where('dato_facturas.id','=',$id)
        ->join('agencias', 'dato_facturas.agencia_id', '=', 'agencias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->select('dato_facturas.*')
        ->firstOrFail();

        return view('datofacturas.edit', compact('datofacturas', 'agencias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DatoFacturaRequest $request, $id)
    {

        $anteriores = DatoFactura::where('dato_facturas.id','=',$id)
        ->join('agencias', 'dato_facturas.agencia_id', '=', 'agencias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->select('dato_facturas.*')
        ->firstOrFail();

        //opciones del logo
        //obtenemos el campo file definido en el formulario
        if($request->hasfile('logo')){
            //obtenemos el campo file definido en el formulario
            $file = $request->file('logo');
            //determinamos la estencion del archivo
            $extension = $file->clientExtension();
            //contruimos el nombre del archivo
            $nombre = session('cia_id')."-LogoFactura.".$extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put($nombre,  \File::get($file));
            //url de la imagen
            $url = asset('storage/' . $nombre); 
        }
        else 
        {
            $url =  $anteriores['logo'];
        }

        //obtiene el correlativo
        //$correlativof = $this->codVariable($request['rangoinferior']);

        //Crea un nuevo registro con los datos de actualizacion
        $datofacturas = new DatoFactura([   
            'correlativof' => $anteriores['correlativof'],
            'correlativo' => $anteriores['correlativo'], //asigna el correlativo ya guardado
            'contador_cotizacion' => $anteriores['contador_cotizacion'], //asigna el correlativo ya guardado
            'kai' => $request['kai'],
            'ragosuperior' => $request['ragosuperior'],
            'rangoinferior' => $request['rangoinferior'],
            'fechalimite' => $request['fechalimite'],
            'frase' => $request['frase'],
            'logo' => $url,
            'agencia_id' => $request['agencia_id'],
        ]);

        $datofacturas->save();
        
        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('dato_facturas','Actualización de Datos: '.$datofacturas->rangoinferior.' al '.$datofacturas->rangoinferior, $datofacturas->id);

        //Da de baja los datos anteriores
        $anteriores->delete();

        Session::flash('message','Datos de Factura actualizados correctamente');
        return Redirect::to('/datofacturas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Determina si existe datos de factura para un id de agencia determinado
    public function DatoFacturaExiste($id)
    {   
        $agencias = DatoFactura::where('agencia_id','=',$id)->first();

        if(is_null($agencias))
        {
            return false;
        }else{
            return true;
        }        
    }

    //Determina si se realizara una actualizacion de los datos existentes de facturacion
    //o en su defecto se crearan
    public function CrearoActualizar(DatoFacturaRequest $request)
    {
  
        if($this->DatoFacturaExiste($request['agencia_id'])){
            //actualizar
            $this->update($request, $request['id']);

        }else{
            //crear
            $this->store($request);

            //return redirect()->action('DatoFacturaController@store', ['request'=>$request]);
        }

        return redirect('/datofacturas');

    }

    //Contruye el codigo de la factura
    public static function codFactura($f)
    {
        //obtiene los datos de la factura
        $datofacturas = DatoFactura::where('agencia_id','=',session('agencia_id'))->first();

        if($f == 1)
        {
            //obtiene el codigo fijo 000-000-00-
            $codfijo = DatoFacturaController::codFijo($datofacturas['ragosuperior']);

            //obtiene el cogigo variable 00000000
            $codvariable = str_pad($datofacturas['correlativof'], 8, "0", STR_PAD_LEFT);

            //concatena las dos partes
            $cod = $codfijo.$codvariable;

            //aumenta en uno el correlativo

            $datofacturas->correlativof = ((int) ($datofacturas['correlativof']) + 1);
            $datofacturas->save();

            return($cod);
        }
        else
        {
            $cod = $datofacturas['correlativo'];

            //aumenta en uno el correlativo
            $correlativo = (int) $datofacturas['correlativo'] + 1;
            $datofacturas->fill(["correlativo" => $correlativo]);
            $datofacturas->save();

            return ($cod);
        }   
    }

    //Obtiene el codigo fijo del codigo de factura
    public static function codFijo($ragosuperior)
    {   
        //composicion normal 000-000-00-00000000
        //elimina los guiones "-" para solo obtener el codigo
        $codsinguiones = str_replace("-", "", $ragosuperior); //obtendra 16 caracteres
        
        //elimina los diguitos, que es la numeracion correlativa del documento
        $codfijo =  substr($codsinguiones, 0, 8);  //dejando solo el codigo fijo

        //la siguiente secuencia de instrucciones, coloca los guiones para el codigo fijo
        $codfijo = 
                (substr($codfijo, 0, 3)."-".
                 substr($codfijo, 3, 3)."-".
                 substr($codfijo, 6, 8)."-");

        return $codfijo; //000-000-00-

    }

    //Obtiene el codigo Variable del codigo de factura
    public static function codVariable($ragoinferior)
    {   
        //composicion normal 000-000-00-00000000
        //elimina los guiones "-" para solo obtener el codigo
        $codsinguiones = str_replace("-", "", $ragoinferior); //obtendra 16 caracteres
        
        //elimina los diguitos, que es la numeracion fija del documento
        $codvariable =  substr($codsinguiones, 9, 16);  //dejando solo el codigo variable o correlativo

        return $codvariable; //00000000

    }

    //Contruye el codigo de la factura
    public static function codCotizacion()
    {
        //obtiene los datos de la factura
        $datofacturas = DatoFactura::where('agencia_id','=',session('agencia_id'))->first();


        $cod = session('agencia_id').'-'.$datofacturas['contador_cotizacion'];

        //aumenta en uno el contador_cotizacion
        $contador_cotizacion = (int) $datofacturas['contador_cotizacion'] + 1;
        $datofacturas->fill(["contador_cotizacion" => $contador_cotizacion]);
        $datofacturas->save();

        return ($cod);
    
    }

}
