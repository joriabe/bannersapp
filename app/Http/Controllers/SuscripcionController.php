<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Supscripcion;
use \App\Plan;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\PaypalController as Paypal;

use Session;
use Redirect;
use Carbon\Carbon;

class SuscripcionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Supscripcion::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "suscripcion.index";
      $etiqueta = "Busqueda por cia, plan, fecha inicio, fecha renovación.";
      
      return view('suscripcion.index', compact('listados','ruta','etiqueta'));
    }

	public static function createSupscripcion($id_plan, $cod_agreement)
    {
    	//obtiene los datos del plan
    	$plan = Plan::where('id_plan', $id_plan)->first();

    	$fecha_inicio = Carbon::now();

    	//determina la fecha de renovacion
    	switch ($plan->frecuencia) {
    		case 'Month':
    			$fecha_renovacion = Carbon::now()->addMonth();
    			break;
    		case 'Year':
    			$fecha_renovacion = Carbon::now()->addYear();
    			break;
    		default:
    			# code...
    			break;
    	}

        //guarda el registro de la supscripcion
        $supscripciones = Supscripcion::create([
            'cia_id' => session('cia_id'),
            'plan_id' => $plan->id,
            'cod_agreement' => $cod_agreement,
            'fecha_renovacion' => $fecha_renovacion,
            'fecha_inicio' => $fecha_inicio,
            'fecha_cancelacion' => null,
        ]);

        session(['suscripcion' => 'activa']); 

        //guarda un registro de lo realizado 
        Log::register('supscripcions','Creación Suscripción: '.$cod_agreement, $supscripciones->id);

        return;

    }

    public static function cancelSupscripcion($cod_agreement)
    {
    	//obtiene la subscripcion
    	$subs = Supscripcion::where('cod_agreement', $cod_agreement)->first();

    	$subs->fecha_cancelacion = Carbon::now();

    	$subs->save();

        //guarda un registro de lo realizado 
        Log::register('supscripcions','Cancelación Subscripción: '.$cod_agreement, $subs->id);

        return;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showSupscripcion()
    {

        //obtiene la subscripcion
    	$subs = Supscripcion::where('supscripcions.cia_id','=',session('cia_id'))
    			->join('planes', 'planes.id','=','supscripcions.plan_id')
        		->firstOrFail();

        if(is_null($subs))
        {
            return redirect('/suscripcion/obtener')->with('message_alert', 'No tiene una suscripción activa, por favor selecione alguna de las siguientes opciones.'); 
        }
        else
        {
            return view('suscripcion.show', compact('subs'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showFull()
    {

        //obtiene la subscripcion
        $subs = Supscripcion::select('supscripcions.*','planes.*')  
                ->join('planes', 'planes.id','=','supscripcions.plan_id')
                ->firstOrFail();

        $planes = Plan::planesListaAll();

        return view('suscripcion.showFull', compact('subs','planes'));        
    }

    public static function estadoSuscripcion()
    {
    	$estado = "";

        //obtiene la subscripcion
    	$subs = Supscripcion::where('supscripcions.cia_id','=',session('cia_id'))
    			->join('planes', 'planes.id','=','supscripcions.plan_id')
        		->first();


            if(is_null($subs))
            {

                $estado = "sin suscripcion";
            }
            else
            {
                if($subs->fecha_renovacion <= Carbon::now())
                {
                	if(is_null($subs->fecha_cancelacion))
                	{

        		        $agreement = new Paypal;

						$estado = $agreement->stateSuscription($subs->cod_agreement);

						if($estado =="Active")
						{
							//renovar suscripcion
	                		SuscripcionController::renovarSupscripcion();
	                		$estado = "activa";
	                	}
	                	else
	                	{
	                		$estado = "cancelado";
	                	}

               		}
               		else
               		{
               			$estado = "cancelado";	
               		}
                }
                else
                {
                	$estado = "activa";
                }
            
            }

        //asigna las variables de session
        session(['suscripcion' => $estado]); 
        return $estado;

    }

    public static function renovarSupscripcion()
    {
    	//obtiene la subscripcion
    	$subs = Supscripcion::where('supscripcions.cia_id','=',session('cia_id'))
        		->firstOrFail();

        //elimina el antiguo registro de la suscripcion
        $eli = Supscripcion::find($subs->id);
        $eli->delete();

        $fecha_renovacion = Carbon::parse($subs->fecha_renovacion);

    	//determina la fecha de renovacion
    	switch ($subs->frecuencia) {
    		case 'Month':
    			$fecha_renovacion->addMonth();
    			break;
    		case 'Year':
    			$fecha_renovacion->addYear();
    			break;
    		default:
    			# code...
    			break;
    	}

        //guarda el registro de la supscripcion
        $supscripciones = Supscripcion::create([
            'cia_id' => session('cia_id'),
            'plan_id' => $subs->plan_id,
            'cod_agreement' => $subs->cod_agreement,
            'fecha_renovacion' => $fecha_renovacion,
            'fecha_inicio' => $subs->fecha_inicio,
            'fecha_cancelacion' => null,
        ]);



        //guarda un registro de lo realizado 
        Log::register('supscripcions','Renovación Subscripción: '.$supscripciones->cod_agreement, $supscripciones->id);

        return;
    }


}
