<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\EstadoFactura;

class EstadoFacturaController extends Controller
{
    /**
     * Almacena el estado de la factura.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function createEstadoFactura($f,$factura_id, $estado)
    {

        //guarda el estado de la factura
        $estados = EstadoFactura::create([
            'estado1' => $f,
            'estado2' => $estado,
            'factura_id' => $factura_id,
        ]);

        return;

    }

        /**
     * Almacena el estado de la factura.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateEstadoFactura($factura_id, $estado)
    {
        //acturaliza la info de la cuenta por pagar
        $estados = EstadoFactura::where('factura_id','=',$factura_id)->first();

        $estados->estado2 = $estado;

        $estados->save();

        return;

    }
}
