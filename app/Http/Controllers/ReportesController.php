<?php

namespace App\Http\Controllers;
use App\Exports\DeclaracionComprasExport;
use App\Exports\DeclaracionVentasExport;
use App\Exports\FacturasExport;
use App\Exports\FlujosExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class ReportesController extends Controller
{
    public function getReporteView()
    {
    	return view('reportes.form_declaracion');
    }

    public function DeclaComprasExport(Request $request) 
    {	
    	//obtenemos el mes del reporte
        $mes = intval(substr($request->input_mes,5,2));
        //creamos el nombre
        $mest = date("F Y", strtotime($request->input_mes));
        $nombre = "D. Compras " . $mest . ".xlsx";
 		
 		//iniciamos la descarga
 		return (new DeclaracionComprasExport)->forMonth($mes)->download($nombre);
    }

    public function DeclaVentasExport(Request $request) 
    {	
    	//obtenemos el mes del reporte
        $mes = intval(substr($request->input_mes,5,2));
        //creamos el nombre
        $mest = date("F Y", strtotime($request->input_mes));
        $nombre = "D. Ventas " . $mest . ".xlsx";
 		
 		//iniciamos la descarga
 		return (new DeclaracionVentasExport)->forMonth($mes)->download($nombre);
    }
    public function FacturasExport(Request $request) 
    {   
        //obtenemos el mes del reporte
        $dias = $request;
        //creamos el nombre
        $diai = date("d-m-Y", strtotime($request->input_fechai));
        $diaf = date("d-m-Y", strtotime($request->input_fechaf));

        $nombre = "facturas del ". $diai ." al " . $diaf . ".xlsx"; 

        
        //iniciamos la descarga
        return (new FacturasExport)->forDate($dias)->download($nombre);
    }
    public function FlujosExport(Request $request) 
    {   
        //obtenemos el mes del reporte
        $dias = $request;
        //creamos el nombre
        $diai = date("d-m-Y", strtotime($request->input_fechai));
        $diaf = date("d-m-Y", strtotime($request->input_fechaf));

        $nombre = "Flujos de caja del ". $diai ." al " . $diaf . ".xlsx"; 
        
        //iniciamos la descarga
        return (new FlujosExport)->forDate($dias)->download($nombre);
    }
}
