<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Caffeinated\Shinobi\Models\Role;

use \App\User;
use \App\Cia;
use \App\Agencia;
use \App\RoleUser;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
        ->join('agencias', 'users.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.*','roles.name as rol_name','cias.razonsocial as cia_name','agencias.nombrecomercial as agencia_name')        
        ->paginate(9);

        return view('users.index', compact('users'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::get()->pluck('name','id');
        $cias = Cia::get()->pluck('nombrecomercial','id');
        $agencias = Agencia::agenciasLista();
        //$users = User::find($id);
        
        $users = DB::table('users')
        ->join('agencias', 'users.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.*','roles.id as role_id','cias.id as cia_id')        
        ->where('users.id','=',$id)
        ->first();

        return view('users.edit', compact('users','roles','cias','agencias'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //busca el usuario a actualizar
        $users = User::find($id);

        //actualizar el usuario
        $users->update($request->all());

        //actualizar el rol
        $users->roles()->sync($request->get('role_id'));

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('users','Actualización User: '.$users->name, $users->id);

        Session::flash('message','Usuario Actualizado correctamente');
        return Redirect::to('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $users = User::find($id);
        $users->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('users','Deshabilitar User: '.$users->name, $users->id);

        Session::flash('message','Usuario '.$id.' Desactivado');
        return Redirect::to('/users');
    }

    public function activar($id)
    {
        $users = User::onlyTrashed($id);
        $users->restore();

        $users = User::find($id);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('users','Habilitar User: '.$users->name, $users->id);

        Session::flash('message','Usuario '.$id.' Reactivado');
        return Redirect::to('/users');
    }
}
