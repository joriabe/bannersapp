<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use \App\Factura;
use \App\EstadoFactura;
use \App\CxC as CxCmodel;

use App\Http\Controllers\DetalleFacturaController as Detalle;
use App\Http\Controllers\PagoFacturaController as Pago;
use App\Http\Controllers\EstadoFacturaController as Estado;
use App\Http\Controllers\CxCController as CxC;
use App\Http\Controllers\FlujoController as Flujo;
use App\Http\Controllers\DatoFacturaController as DatoFactura;
use App\Http\Controllers\PieFacturaController as Pie;
use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\InventarioController as Inventario;

use Session;
use Redirect;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Factura::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "facturas.index";
      $etiqueta = "Busqueda por codigo, fecha o cliente.";
      
      return view('facturas.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      /*$pru = [1, 2, 1, 
        [['tipopago_id' => 1, 'valor_pagado' => 500.333333, 'comprobantepago'=>'comp 1']],
        [['producto_servicio_id' => 4,'descripcion'=>'pro1','cantidad' => 2, 'precio' => 25, 'subtotal' => 50],
         ['producto_servicio_id' => 5,'descripcion'=>'pro2','cantidad' => 1, 'precio' => 100, 'subtotal' => 100],
         ['producto_servicio_id' => 5,'descripcion'=>'pro3','cantidad' => 1, 'precio' => 100, 'subtotal' => 100],
         ['producto_servicio_id' => 4,'descripcion'=>'pro4','cantidad' => 1, 'precio' => 25, 'subtotal' => 25]  ]];*/
      
      /*$pru = [2, 2, 1, new \DateTime(),
        [['producto_servicio_id' => 4,'descripcion'=>'pro1','cantidad' => 2, 'precio' => 25, 'subtotal' => 50],
         ['producto_servicio_id' => 5,'descripcion'=>'pro2','cantidad' => 1, 'precio' => 100, 'subtotal' => 100],
         ['producto_servicio_id' => 5,'descripcion'=>'pro3','cantidad' => 1, 'precio' => 100, 'subtotal' => 100],
         ['producto_servicio_id' => 4,'descripcion'=>'pro4','cantidad' => 1, 'precio' => 25, 'subtotal' => 25]  ]];*/
        
        $array = json_decode($request->getContent(), true);
        //$array = $pru;

        //asigna el detalle de la factura a un nuevo array
        $detalle_facturas = $array[4];

        //determina si es f o no f
        if($array[2] == 1)
        {
          //realiza los calculos del detalle y el pie de la factura
          $calculo_detalle =  Detalle::calculoDetalle($detalle_facturas); // return array  [$pie_factura[],$totalantesimpuestos, $totaldespuesimpuestos, $descuento]
        }
        else
        {
          //realiza los calculos del detalle y el pie de la factura
          $calculo_detalle =  Detalle::calculoDetalleSI($detalle_facturas); // return array  [$pie_factura[],$totalantesimpuestos, $totaldespuesimpuestos, $descuento]
        }

        //determina si es pago en contado '1' o a credito '2'
        if($array[0] == 1)
        {
            //asigna el detalle del pago a un nuevo array
            $pago_facturas = $array[3];
            //estado de la factura - tabla estadofacturas    
            $estado = 'Pagado';

            //retorna el valor pagado
            $pagado = Pago::calculoPago($pago_facturas);// $TotalPagado

            //obtiene el cambio para el cliente
            $cambio = ($pagado - $calculo_detalle['totaldespuesimpuesto']);
        }
        else
        {
            //asigna la fecha de vencimiento
            $fecha_vencimiento = $array[3];
            //estado de la factura - tabla estadofacturas
            $estado = 'Pendiente';

            //Devido a que la factura es al credito, el pago es 0
            $pagado = 0;

            //Lo mismo sucede con el cambio al cliente
            $cambio = 0; 
        }

        
        //abre una nueva transaccion
        DB::beginTransaction();
        
        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {
            //contrulle el codigo de la factura
            $codfactura = DatoFactura::codFactura($array[2]); 

            //guarda el encabezado de la factura
            $facturas = Factura::create([
                'fecha' => new \DateTime(),
                'codfactura' => $codfactura,
                'totalantesimpuesto' =>  round($calculo_detalle['totalantesimpuesto'], 2),
                'totaldespuesimpuesto' => round($calculo_detalle['totaldespuesimpuesto'], 2),
                'descuento' => round($calculo_detalle['descuento'], 2),
                'pagado' =>  round($pagado, 2),
                'cambio' =>  round($cambio, 2),
                'agencia_id' => session('agencia_id'),
                'cliente_id' => $array[1],
                'usuario_id' => Auth::id(),
            ]);

            //realiza el registro del detalle de la factura //$detalle:facturas, $facturas_id
            Detalle::createDetalleLote($detalle_facturas, $facturas->id);

            //realiza la actualizacion de la existencia de los productos
            //$detalle:facturas
            Inventario::updateProductosExistencias($detalle_facturas['producto_servicio_id']);

            //realiza el registro del calculo de los impuestos de la factura //$detalle:facturas, $facturas_id
            Pie::createPieFacturaLote($calculo_detalle['pie_factura'], $facturas->id);

            //realiza el guardado del estado de la factura
            Estado::createEstadoFactura($array[2],$facturas->id, $estado);

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia
            Log::register('facturas','Creación factura: '.$facturas->codfactura,$facturas->id);

            //determina si es pago en contado '1' o a credito '2'
            if($array[0] == 1)
            {
                //realiza el registro del flujo de caja //$pagos, $referencia, $movimiento_id, $signo
                //para los tipos de pagos que se realizaron
                Flujo::createFlujoLote($pago_facturas, 'Valor pagado por la fact.: '.$facturas->codfactura, 1 , 1);

                //si existe cambio, se registra la salida del efectivo
                if($cambio > 0)
                {
                    //realiza el registro del retiro del cambio ($valor, $referencia, $tipopago_id, $movimiento_id, $signo)
                    Flujo::createFlujo($cambio, 'Cambio por la fact.: '.$facturas->codfactura, 1, 5, -1);
                    //1=efectivo, 5=cabio, -1=salida                    
                }

                //realiza el registro del pago de la factura
                Pago::createPagoFacturaLote($pago_facturas, $calculo_detalle['totaldespuesimpuesto'], $facturas->id);
            }
            else
            {
                //realiza el guardado de la cuenta por cobrar
                CxC::createCxC($facturas->id, $array[3], $calculo_detalle['totaldespuesimpuesto']);
            }  
            //realiza los cambio en la base de datos
            DB::commit();

        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //respuesta al js de cuanto es el cambio
            return response()->json([
                'msg' => $error,
                'msg2' => 1
            ]);
        }
        
         //respuesta al js de cuanto es el cambio
        return response()->json([
            'msg' => 'El Cambio es de: ' . number_format($cambio, 2, '.', ',') . ' L.',
            'msg2' => $facturas->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        //optenemos la factura
        $factura = DB::table('facturas')
        ->join('agencias', 'facturas.agencia_id', '=', 'agencias.id')
        ->join('users', 'facturas.usuario_id', '=', 'users.id')
        ->join('estado_facturas', 'estado_facturas.factura_id', '=', 'facturas.id')
        ->where([
            ['facturas.id', '=', $id],
            ['agencias.cia_id', '=', session('cia_id')],
        ])
        ->select('facturas.*','name','estado2','estado1')
        ->first();

        //si no se encuentra la factura en base a $id y cia_id
        if ( !isset($factura) ) {
          abort(404);
        }



        $datos_cia = \DB::table('cias')
            ->join('agencias', 'cias.id', '=', 'agencias.cia_id')
            ->select('agencias.nombrecomercial', 'razonsocial', 'direccion', 'correo', 'rtn', 'telefono', 'web')
            ->where('agencias.id', '=' , $factura->agencia_id)            
            ->first();

        $datos_factura = \DB::table('dato_facturas')
            ->select('kai','logo', 'frase', 'rangoinferior', 'ragosuperior', 'fechalimite')
            ->where('agencia_id', '=' , $factura->agencia_id)
            ->whereNull('deleted_at') 
            ->first();

        $cliente = \DB::table('clientes')
            ->select('rtn','nombrecliente','direccion')
            ->where('id', '=' , $factura->cliente_id)
            ->first();

        $detalle_factura = \DB::table('detalle_facturas')
            ->join('producto_servicios', 'producto_servicios.id', '=', 'detalle_facturas.producto_servicio_id')
            ->select('cantidad','detalle_facturas.descripcion', 'detalle_facturas.precio', 'subtotal', 'codinterno as p_id')
            ->where('factura_id', '=' , $id)
            ->get();

        $pie_factura = \DB::table('pie_facturas')
            ->join('tipo_impositivos', 'pie_facturas.tipo_impositivo_id', '=', 'tipo_impositivos.id')
            ->select('descripcion', 'totalimpuesto', 'calculoimpuesto')
            ->where('factura_id', '=' , $id)
            ->get();

        $fechav = DB::table('cxcs') 
            ->where('factura_id', '=' , $id)
            ->value('fecha_vencimiento');

        return view('modelosfactura.modelofactura_1',['factura' => $factura, 'cia' => $datos_cia, 'datosf' => $datos_factura, 'cliente' => $cliente, 'detalles' => $detalle_factura, 'pies' => $pie_factura, 'fechav' => $fechav ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        //Busca y anula la factura
        $facturas = Factura::find($id);        

        //busca y anula la cxc
        $cxc = CxCmodel::where('factura_id','=',$id)->first();
        if (!is_null($cxc))
        {
          $cxc->delete();
        }
        else
        {
          //obtiene los pagos realizados a esa factura
          $pago_facturas = Pago::findPagoFactura($id);

          //realiza el registro del flujo de caja //$pagos, $referencia, $movimiento_id, $signo
          //para los tipos de pagos que se realizaron
          Flujo::createFlujoLote($pago_facturas, 'Anulación fact.: '.$facturas->codfactura, 7 , -1);
        }

        //realiza el guardado del estado de la factura
        Estado::updateEstadoFactura($facturas->id, 'Anulada');

        //guarda un registro de lo realizado 
        Log::register('facturas','Anulación de Factura: '.$facturas->codfactura, $facturas->id);

        Session::flash('message','Factura '.$facturas->codfactura.' anulada correctamente');

        $facturas->delete();//coloca como nula la factura despues de ser utilizada    

        return Redirect::to('/facturas');
    }
    
}
