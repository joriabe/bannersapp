<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Unidad;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class UnidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unidades = Unidad::where('cia_id','=',session('cia_id'))
        ->orderBy('unidads.updated_at', 'desc')
        ->paginate(9);
        return view('unidades.index', compact('unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unidades = new Unidad([
            'unidad' => $request['unidad'],
            'acronimo' => $request['acronimo'],
            'cia_id' => session('cia_id'),
        ]);

        $unidades->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('unidades','Creación Unidad: '.$unidades->acronimo, $unidades->id);

         //Retornamos la Respuesta Segun sea el caso.
        if ($request['form_pos'] == 1) {
            //Si se Llamo desde un modal
            $respuesta = ['msg' => 'Categoria Registrada Correctamente', 'id' => $unidades->id, 'nombre' => $unidades->unidad];
            print json_encode($respuesta);
        } 
        if ($request['form_pos'] == 0) {
            //Si se Llamo desde el dashboard
            return redirect('/unidades')->with('message', 'Unidad registrada correctamente'); 
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unidades = Unidad::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('unidades.edit', compact('unidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unidades = Unidad::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $unidades->fill($request ->all());
        $unidades->save();


        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('unidades','Actualización Unidad: '.$unidades->acronimo, $unidades->id);

        Session::flash('message','Unidad '.$request['acronimo'].'   actualizado correctamente');
        return Redirect::to('/unidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
        $unidades = Unidad::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        $unidades->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('unidades','Eliminación Unidad: '.$unidades->acronimo, $unidades->id);

        Session::flash('message','Unidad '.$id.' eliminado correctamente');
        return Redirect::to('/unidades');
    }
}
