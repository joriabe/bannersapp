<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Used to process plans
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Common\PayPalModel;



// use to process billing agreements
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use App\Http\Controllers\SuscripcionController as Suspcrioncion;
use \App\Plan as PlanAlmacenado;
use session;

use \App\Plan as PlanModel;

 
class PaypalController extends Controller
{
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;
    private $setReturnUrl;
    private $setCancelUrl;
    
    // Create a new instance with our paypal credentials
    public function __construct()
    {
        // Detect if we are running in live mode or sandbox
        if(config('paypal.settings.mode') == 'live'){
            $this->client_id = config('paypal.live_client_id');
            $this->secret = config('paypal.live_secret');
        } else {
            $this->client_id = config('paypal.sandbox_client_id');
            $this->secret = config('paypal.sandbox_secret');
        }

        $this->setReturnUrl = config('paypal.setReturnUrl');
        $this->setCancelUrl = config('paypal.setCancelUrl');

        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));
    }

    public function create_plan(Request $request){

        // Detect if we are running in live mode or sandbox
        if(config('paypal.settings.mode') == 'live'){
            $client_id = config('paypal.live_client_id');
            $secret = config('paypal.live_secret');
        } else {
            $client_id = config('paypal.sandbox_client_id');
            $secret = config('paypal.sandbox_secret');
        }

        $setReturnUrl = config('paypal.setReturnUrl');
        $setCancelUrl = config('paypal.setCancelUrl');

        // Set the Paypal API Context/Credentials
        $apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $apiContext->setConfig(config('paypal.settings'));




    	 // Create a new billing plan
        $plan = new Plan();
        $plan->setName( $request->plan)
          ->setDescription( $request->descripcion)
          ->setType('infinite'); //infinitie o fixed

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName( $request->descripcion_factura)
          ->setType('REGULAR') //regular o trial
          ->setFrequency( $request->frecuencia)
          ->setFrequencyInterval( $request->intervalo) //1
          ->setCycles( $request->ciclo) //0
          ->setAmount(new Currency(array('value' => ( $request->valor), 'currency' => 'USD')));

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl($setReturnUrl)
          ->setCancelUrl($setCancelUrl)
          ->setAutoBillAmount('yes')
          ->setInitialFailAmountAction('CONTINUE')
          ->setMaxFailAttempts('0');

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        //create the plan
        try {
            $createdPlan = $plan->create($this->apiContext);

            try {
                $patch = new Patch();
                $value = new PayPalModel('{"state":"ACTIVE"}');
                $patch->setOp('replace')
                  ->setPath('/')
                  ->setValue($value);
                $patchRequest = new PatchRequest();
                $patchRequest->addPatch($patch);
                $createdPlan->update($patchRequest, $this->apiContext);
                $plan = Plan::get($createdPlan->getId(), $this->apiContext);

                // Output plan id
                //echo 'Plan ID:' . $plan->getId();

                //guradar los datos del plan en la base de datos
                $planes = PlanModel::create([
                    'id_plan' => $plan->getId(),
                    'plan' => $request->plan,
                    'descripcion' => $request->descripcion,
                    'descripcion_factura' => $request->descripcion_factura,
                    'valor' => round($request->valor,2),
                    'frecuencia' => $request->frecuencia,
                    'intervalo' => $request->intervalo,
                    'ciclo' => $request->ciclo,
                    'cias' => $request->cias,
                    'sucursales' => $request->sucursales,
                    'usuarios' => $request->usuarios,
                    'instlacion' => $request->instlacion,
                    'cia_id' => $request->cia_id,
                ]);

            return redirect('/plan/create')->with('message', 'Plan '.$plan->getId().' registrado correctamente');


            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                die($ex);
            }
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }
    }

 	public function paypalRedirect(Request $request){
        
        /*ID de Planes SandBox
		basico
		P-41F87283W376955087J5PLAY
		P-1NL90613MD265453R7J6KCAA

		prime
		P-8HR217264U09312457J75YAQ
		P-4MS333731C449971A7KBFRNA

		ultimate
		P-0TF9438331233873E7KBWIFY
		P-0EN90231T94694404BFIWSLA
		*/

        //asigna el id del plan
        //asigna las variables de session
        session(['id_plan' => $request->id_plan]);

        //obtiene los datos del plan
        $planes = PlanAlmacenado::where('id_plan', $request->id_plan)->first();

        // Create nuevo acuerdo
        $agreement = new Agreement();
        $agreement->setName('Acuerdo '.$planes->descripcion_factura)
          ->setDescription($planes->descripcion_factura)
          ->setStartDate(\Carbon\Carbon::now()->addMinutes(5)->toIso8601String());

        // Se le asigna el plan que se del acuerdo
        $plan = new Plan();
        $plan->setId($request->id_plan);
        $agreement->setPlan($plan);

        // Agregar un pagador
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        try {
          // Crea el acuerdo
          $agreement = $agreement->create($this->apiContext);

          // Obtien la direccion de succes y lo redirije
          $approvalUrl = $agreement->getApprovalLink();

          return redirect($approvalUrl);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
          echo $ex->getCode();
          echo $ex->getData();
          die($ex);
        } catch (Exception $ex) {
          die($ex);
        }

    }

    public function paypalReturn(Request $request){

        $token = $request->token;
        $agreement = new \PayPal\Api\Agreement();

        try {
            // Execute agreement
            $result = $agreement->execute($token, $this->apiContext);
            /*$user = Auth::user();
            $user->role = 'subscriber';
            $user->paypal = 1;*/
            if(isset($result->id)){
                
                //Realice un registro del plan adquirido por el cliente    
                 Suspcrioncion::createSupscripcion(session('id_plan'), $result->id);

                //Elimina la variable session que ya no se utilizara 
                 session()->forget('id_plan');
            }

            //Se suscribio exitosamente
            return redirect('/home')->with('message', 'Suscripción realizada exitosamente!!');

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return redirect('/home')->with('message_danger', 'La Suscripción no pudo ser efectuada!!');
        }
    }



    public function cancelSuscription($cod_agreement){

        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Deleting the agreement");

        $agreement = new \PayPal\Api\Agreement();
        $agreement->setId($cod_agreement);

        try {
            $agreement->cancel($agreementStateDescriptor, $this->apiContext);
                
            //realiza la cancelacion de la subscripcion en la bd
            Suspcrioncion::cancelSupscripcion($cod_agreement);
     
            //Se suscribio exitosamente
            return redirect('/home')->with('message', 'Suscripción cancela exitosamente!!');

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return redirect('/home')->with('message_danger', 'La Suscripción no pudo ser cancelada!!');
        }
    }


    public function stateSuscription($cod_agreement){

        $agreement = new \PayPal\Api\Agreement();

        $result ="";

        try {
            $result = $agreement->get($cod_agreement,$this->apiContext);
                
            return($result->state);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return redirect('/home')->with('message_danger', 'No se pudo acceder a la Suscripción!!');
        }
    }

    public function webhook(Request $request)
    {

        $array = json_decode($request->getContent(), true);

        dd($array('event_type'));
 
  
        switch ($array('event_type'))
        {
            case "BILLING.SUBSCRIPTION.CANCELLED":
                
                break;

                
            case "BILLING.SUBSCRIPTION.CREATED":
                
                break;

            case "BILLING.SUBSCRIPTION.RE-ACTIVATED":
                
                break;
          
            case "BILLING.SUBSCRIPTION.SUSPENDED":
                
                break;
                  
            case "BILLING.SUBSCRIPTION.UPDATED":
                
                break;
                  
            case "CUSTOMER.PAYOUT.FAILED":
                
                break;
    
            case "PAYMENT.CAPTURE.COMPLETED":
                // Handle payment completed
                break;
            case "PAYMENT.CAPTURE.DENIED":
                // Handle payment denied
                break;
                // Handle other webhooks
            default:
                break;
        }
        return response()->json('',200);
    
    }

}
