<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\CxC;
use \App\CxP;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //calculamos el mes anterior
        $fecha = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) );

        $agencias = DB::table('agencias')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereNull('deleted_at')
        ->select('id','nombrecomercial')
        ->get();
        //por cada agencia
        foreach ($agencias as $agencia) {
            $nombres[] = $agencia->nombrecomercial;
            //recorremos los ultimos 30 dias
            for ($i=30; $i >= 0; $i--) {
                //restamos los dias a la fecha actual    
                $fecha = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-".$i." day" ) );

                //obtenemos la suma de esa fecha
                $facturas = DB::table('facturas')
                ->where('agencia_id', '=', $agencia->id)
                ->whereDate('fecha','=', $fecha )
                ->whereNull('facturas.deleted_at')
                ->sum('totaldespuesimpuesto');

                ${"data" . $agencia->id}[] = $facturas;
            }
        }
        //asignamos las agencias
        foreach ($agencias as $agencia){
            $datas[] = ${"data" . $agencia->id};
        }
        //asignamos los labels
        for ($i=30; $i >= 0; $i--) {
            $fecha = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-".$i." day" ) );

            $labels[] = date("d/m", strtotime($fecha));
        }


        //obtiene las cxc vencidas y las que estan por vencer (que le resten al menos dos semanas)
        $cxcVencidasAvencer = CxC::cxcVencidasAvencer();

        //obtiene las cxc vencidas y las que estan por vencer (que le resten al menos dos semanas)
        $cxpVencidasAvencer = CxP::cxpVencidasAvencer();


        return view('home', ['labels'=> $labels , 'datas' => $datas, 'nombres' => $nombres, 'cxcs' =>  $cxcVencidasAvencer, 'cxps' =>  $cxpVencidasAvencer]);
    }
    public function ventasDiariasMes()
    {

        //calculamos el mes anterior
        $fecha = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) );

        $facturas = DB::table('facturas')
        ->join('agencias', 'facturas.agencia_id', '=', 'agencias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereDate('fecha','>=', $fecha )
        ->whereNull('facturas.deleted_at')
        ->select('nombrecomercial', (DB::raw('DATE(fecha) as fechad')), (DB::raw('SUM(totaldespuesimpuesto) as total')))
        ->groupBy('nombrecomercial', 'fechad')
        ->orderBy('fechad', 'asc')
        ->get();

        dd($facturas);

    }
    
}
