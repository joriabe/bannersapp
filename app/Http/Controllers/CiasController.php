<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\CiaRequest;

use App\Cia as Cia;
use App\Agencia as Agencia;
use App\TipoPago;
use App\Cliente;
use App\Descuento;

use App\Http\Controllers\LogController as Log;

class CiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //obtenermos las cias con sus respectivas agencias
        $agencias = Cia::with('agencias')
        ->orderBy('cias.updated_at', 'desc')
        ->get();

        return view('companias.cias_index',['cias' => $agencias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companias.new_cia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function store(CiaRequest $request)
    {
        //abre una nueva transaccion
        DB::beginTransaction();
        
        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {
            //creamos la cia
            $cia = new Cia;
            $cia->rtn = $request->input_rtn;
            $cia->nombrecomercial = $request->input_nombre;
            $cia->razonsocial = $request->input_razons;
            //opciones del logo
            //obtenemos el campo file definido en el formulario
            $file = $request->file('input_logo'); 
            if (isset($file)) {    
               //obtenemos el nombre del archivo.
               $ext = $file->getClientOriginalExtension(); 
               $nombre =  $request->input_rtn.'_cialogo.'.$ext;
               //indicamos que queremos guardar un nuevo archivo en el disco local
               \Storage::disk('public')->put($nombre,  \File::get($file));
               //url de la imagen
               $url = asset('storage/' . $nombre);
                //opciones del logo
                $cia->logo = $url;
            }else {
                $cia->logo = asset('storage/sinlogo.png');
            }
            $cia->save();

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia    
            Log::register('cias','Creación Cía: '.$cia->nombrecomercial, $cia->id);

            //tomamos el id de la nueva cia
            $insertedid = $cia->id;

            //creamos la agencia
            $agencia = new Agencia;
            $agencia->nombrecomercial = $request->input_nombrecomercial;
            $agencia->descripcion = $request->input_descripcion;
            $agencia->direccion = $request->input_direccion;
            $agencia->telefono = $request->input_telefono;
            $agencia->correo = $request->input_email;
            $agencia->web = $request->input_web;
            $agencia->codigointerno = $request->input_cinterno;
            $agencia->cia_id = intval($insertedid);
            $agencia->save();

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia    
            Log::register('agencias','Creación Agencia: '.$agencia->descripcion, $agencia->id);


            //realiza los cambio en la base de datos
            DB::commit();
            
        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //respuesta al js de cuanto es el cambio
            return response()->json([
                'Ocurrio un Error:' => $error
            ]);
        }
        
        return redirect('cias');


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cia = Cia::find($id);

        return view('companias.edit_cia',['cia' => $cia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CiaRequest $request, $id)
    {
        $cia = Cia::find($id);

        $cia->rtn = $request->input_rtn;
        $cia->nombrecomercial = $request->input_nombre;
        $cia->razonsocial = $request->input_razons;
        //opciones del logo
        //obtenemos el campo file definido en el formulario
        $file = $request->file('input_logo');
        //comprobamos si hay logo nuevo
        if (isset($file)) {
            //obtenemos el nombre del archivo
           $ext = $file->getClientOriginalExtension();
           $nombre = $request->input_rtn.'_cialogo.'.$ext;     
           //indicamos que queremos guardar un nuevo archivo en el disco local
           \Storage::disk('public')->put($nombre ,  \File::get($file));
           //url de la imagen
           $url = asset('storage/' . $nombre);
            //opciones del logo
            $cia->logo = $url;               
        }                      
           
        $cia->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia    
        Log::register('cias','Edición Cía: '.$cia->nombrecomercial, $cia->id);

        return redirect('cias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cia = Cia::find($id);
        $cia->delete();

        //borramos las agencias de la cia borrada
        $deletedRows = Agencia::where('cia_id', $id)->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia    
        Log::register('cias','Eliminación Cía: '.$cia->nombrecomercial, $cia->id);


        return redirect('cias');
    }
   
    
}

