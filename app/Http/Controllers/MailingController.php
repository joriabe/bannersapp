<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailingInfo;
use App\Notifications\MailingComercial;
use App\User;

class MailingController extends Controller
{
    public function mailinginfo(Request $request)
    {
    	//creamos array con los datos del formulario
    	$info[] = $request->asunto;
		$info[] = $request->nombre;
		$info[] = $request->email;
		$info[] = $request->mensaje;

		//seleccionamos el usuario al que le enviaremos el mensaje
        $user = User::find(13);
        //enviamos el correo al usuario seleccionado
    	$user->notify(new MailingInfo($info));

    	return redirect('/#contact')->with('message', 'Hemos Recibido su Mensaje, Pronto Estaremos en Contacto');
    }
    public function mailingcomercial(Request $request)
    {
    	//creamos array con los datos del formulario
    	$comercial[] = $request->modal_nombre;
		$comercial[] = $request->modal_telefono;
		$comercial[] = $request->modal_email;
		$comercial[] = $request->modal_asunto;

		//seleccionamos el usuario al que le enviaremos el mensaje
        $user = User::find(14);
        //enviamos el correo al usuario seleccionado
    	$user->notify(new MailingComercial($comercial));

    	return redirect('/#contact')->with('message', 'Gracias por Confiar en SGF-PYME, te Llamaremos los antes posible.');
    }
}
