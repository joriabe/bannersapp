<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cia as Cia;
use App\Agencia as Agencia;

class CompaniasController extends Controller
{
    public function getCias()
    {
    	//obtenermos las cias con sus respectivas agencias
    	$agencias = Cia::with('agencias')->get();

    	return view('companias.cias_index',['cias' => $agencias]);
    }
    public function getNewCia()
    {
    	return view('companias.new_cia');
    }
    public function postNewCia(Request $request)
    {

    	//creamos la cia
    	$cia = new Cia;
    	$cia->rtn = $request->input_rtn;
    	$cia->nombrecomercial = $request->input_nombre;
    	$cia->razonsocial = $request->input_razons;
    	//No guarda el lgo
    	$cia->logo = "";
    	$cia->save();

    	//tomamos el id de la nueva cia
    	$insertedid = $cia->id;

    	//creamos la agencia
    	$agencia = new Agencia;
    	$agencia->descripcion = $request->input_descripcion;
    	$agencia->direccion = $request->input_direccion;
    	$agencia->telefono = $request->input_telefono;
    	$agencia->correo = $request->input_email;
    	$agencia->codigointerno = $request->input_cinterno;
    	$agencia->cia_id = intval($insertedid);
    	$agencia->save();

    	return redirect('cias');
    }
    public function getNewAgencia($id)
    {
        return view('companias.new_agencia' , ['cia_id'=> $id]);
    }
    public function postNewAgencia(Request $request)
    {

        //creamos la agencia
        $agencia = new Agencia;
        $agencia->descripcion = $request->input_descripcion;
        $agencia->direccion = $request->input_direccion;
        $agencia->telefono = $request->input_telefono;
        $agencia->correo = $request->input_email;
        $agencia->codigointerno = $request->input_cinterno;
        $agencia->cia_id = intval($request->cia_id);
        $agencia->save();

        return redirect('cias');
        
    }
    public function getCiaDte($id){
        $cia = Cia::find($id);
        $cia->delete();

        return redirect('cias');
    }
    public function getAgenciaDte($id){
        $agencia = Agencia::find($id);
        $agencia->delete();

        return redirect('cias');
    }
    //funcion de prueba
    public function test(Request $request){
        //decodificamos el array JSON --> PHP
        $array = json_decode($request->getContent(), true);
        //obtenemos credito o contado
        $c_o_c = $array[0];
        $totalpag = 0;
        //verificamos si es contado 
        if ($c_o_c == 1) {
            //obtenemos el resto de campos
            $id_cliente = $array[1];
            $f_fiscal = $array[2];
            $pago_facturas = $array[3];
            $detalle_factura = $array[4];
            
            //reccorrido de ejemplo para obtener el valor pagado total
            
            foreach ($pago_facturas as $pago) {
                $totalpag = $totalpag + floatval($pago['valor_pagado']);
            }
        }
        //verificamos si es credito
        if ($c_o_c == 2) {
            //obtenemos el resto de los campos
            $id_cliente = $array[1];
            $f_fiscal = $array[2];
            $fecha_vencimiento = $array[3];
            $detalle_factura = $array[4];
        }
        $totalpro = 0;
        //ejemplos de recorrido para obtener la suma total de los productos
        foreach ($detalle_factura as $detalle) {
            $totalpro = $totalpro + floatval($detalle['subtotal']);
        }
        //ejemple para saber cuanto es el cambio
        $cambio = $totalpag - $totalpro; 

        //respuesta al js de cuanto es el cambio
        return response()->json([
            'msg' => 'El Cambio es de: ' . $cambio,
            'msg2' => ' Lps.'
        ]);

    }
}
