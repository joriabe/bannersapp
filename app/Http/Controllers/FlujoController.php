<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

use \App\Flujo;
use \App\TipoPago;
use \App\Agencia;
use \App\Caja;

class FlujoController extends Controller
{
    public function index (Request $request)
    {                 
        //obtiene la consulta sobre el index y sus respectivos filtros
        $listados = Flujo::indexConsulta($request);

        //variables necesarias para el form de la busqueda
        $ruta = "flujos.index";
        $etiqueta = "Busqueda por fecha, agencia, descripció, valor o usuario.";

        return view('flujos.index', compact('listados','ruta','etiqueta'));
    }


    public static function createFlujo($valor, $referencia, $tipopago_id, $movimiento_id, $signo)
    {
        //guarda el registro del flujo
        $flujos = Flujo::create([
            'fecha' => new \DateTime(),
            'valor' =>round($valor * $signo,2),
            'referencia' => $referencia,
            'tipopago_id' => $tipopago_id,
            'usuario_id' => Auth::id(),
            'movimiento_id' => $movimiento_id,
            'agencia_id' => session('agencia_id'),
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('flujos','Creación : '.$flujos->id, $flujos->id);

        return;

    }
 
    public static function createFlujoLote($pagos, $referencia, $movimiento_id, $signo)
    {

    	//recorre el lote de los flujos
    	foreach ($pagos as $pago) 
    	{
            if ((double)$pago['valor_pagado'] != 0 or (double)$pago['valor_pagado'] != -0) {
    		FlujoController::createFlujo(
                $pago['valor_pagado'],$referencia,$pago['tipopago_id'], $movimiento_id, $signo);
            }
    	}

        return;

    }

    public function create()
    {
        //obtiene una lista de los impuestos
        $tipo_pagos = TipoPago::tipopagoLista();
        //obtiene una lista de las categorias
        $flujos = $this->resumenFlujo();
        //obtiene una lista de las categorias
        $agencias = Agencia::where('agencias.cia_id','=',session('cia_id'))
        ->whereNull('agencias.deleted_at')
        ->pluck('descripcion', 'id');

        return view('flujos.create', compact('tipo_pagos','flujos','agencias'));
    }

    public function store(Request $request)
    {

        //guarda el registro del flujo
        $flujos = Flujo::create([
            'fecha' => new \DateTime(),
            'valor' =>round($request['valor'] * $request['signo'],2),
            'referencia' => $request['referencia'],
            'tipopago_id' => $request['tipopago_id'],
            'usuario_id' => Auth::id(),
            'movimiento_id' => $request['movimiento_id'],
            'agencia_id' => $request['agencia_id'],
        ]);

        //determina si es llamado desde un modal
        if($request['modal'] == 1)
        {
            return Redirect::to('/pos');
        }
        else
        {
            Session::flash('message','Regitro de L.'.$flujos['valor'].' registrado correctamente');
             return Redirect::to('/flujos/create');
        }



    }

    public static function resumenFlujo()
    {
        $flujos = DB::table('flujos')
        ->join('tipo_pagos', 'flujos.tipopago_id', '=', 'tipo_pagos.id')
        ->join('agencias', 'flujos.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereNull('agencias.deleted_at')
        ->whereNull('cias.deleted_at')
        ->select('agencias.descripcion as agencia_name', 'tipo_pagos.tipopago', (DB::raw('SUM(flujos.valor) as suma_valor')))
        ->groupBy('agencia_name', 'tipopago')
        ->get();

     
    
        return $flujos;

    }


    public static function resumenFlujoCaja($apertura)
    {
        //obtiene los flujos para la caja del usuario
        $flujos = Flujo::resumenCajaFlujo(session('caja'));

        $final[]=array();
        $total = 0;

        //Agrega con cuanto se aperturo la caja
        $final = array_add($final, "apertura", [ 
                'tipo_pago' => "Apertura Caja",
                'total'     => $apertura,
            ]);

        //recorre el flujo consultado
        foreach ($flujos as $flujo)
        {

            //determina si el flujo es en efectivo
            if ($flujo->tipo_pago_id == 1)
            {
                //determina si el flujo es debido a una venta (positivo o negativo)
                if( $flujo->movimiento_id == 1 or
                    $flujo->movimiento_id == 5 or
                    $flujo->movimiento_id == 7 or
                    $flujo->movimiento_id == 3 or
                    $flujo->movimiento_id == 7 )
                {

                    //determina si existe la llave para venta (efectivo)
                    if (( array_has($final, "venta")) == false) 
                    {
                        //si no existe, se crea la llave
                        $final = array_add($final, "venta", [ 
                                'tipo_pago' => "Ventas Efectivo",
                                'total' => $flujo->suma_valor]);

                    }
                    else
                    {
                        //si ya exite la clave, se le suman los valores

                         //suma del caculo de los impuestos por tipo impositvo
                        array_set($final, 'venta.total',
                                 (data_get($final, 
                                    'venta.total') + $flujo->suma_valor));
                    }//fin if
                }
                else//otro tipo de movimiento
                {
                    //ingreso de efectivo
                    if ($flujo->suma_valor > 0) 
                    {
                        //determina si existe la llave para gastos (efectivo)
                        if (( array_has($final, "entradas")) == false) 
                        {
                            //si no existe, se crea la llave
                            $final = array_add($final, "entradas", [ 
                                    'tipo_pago' => "Entradas Efectivo",
                                    'total' => $flujo->suma_valor]);

                        }
                        else
                        {
                            //si ya exite la clave, se le suman los valores

                             //suma del caculo de los impuestos por tipo impositvo
                            array_set($final, 'entradas.total',
                                     (data_get($final, 
                                        'entradas.total') + (double)$flujo->suma_valor));
                        }//fin if
                    }
                    else //salidas de efectivo
                    {
                        //determina si existe la llave para gastos (efectivo)
                        if (( array_has($final, "gastos")) == false) 
                        {
                            //si no existe, se crea la llave
                            $final = array_add($final, "gastos", [ 
                                    'tipo_pago' => "Salidas Efectivo",
                                    'total' => $flujo->suma_valor]);

                        }
                        else
                        {
                            //si ya exite la clave, se le suman los valores

                             //suma del caculo de los impuestos por tipo impositvo
                            array_set($final, 'gastos.total',
                                     (data_get($final, 
                                        'gastos.total') + (double)$flujo->suma_valor));
                        }//fin if
                    }//fin if
                    


                }//fin if

            }//fin if

           
        }

        //limpia el array pie_factura para quitar la llave 0
        array_forget($final, "0");
    
        return $final;

    }

}
