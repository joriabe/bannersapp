<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\CxCRequest;

use \App\CxC;
use \App\PagoFactura;
use \App\Factura;
use \App\TipoPago;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\PagoFacturaController as Pago;
use App\Http\Controllers\EstadoFacturaController as Estado;
use App\Http\Controllers\FlujoController as Flujo;


class CxCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = CxC::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "cxcs.index";
      $etiqueta = "Busqueda por fecha, fecha v. o cliente.";
      
      return view('cxcs.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('cxcs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cxcs = CxC::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $facturas = Factura::where('facturas.id','=',$cxcs->factura_id)
        ->join('clientes', 'facturas.cliente_id', '=', 'clientes.id')
        ->select('facturas.*','clientes.*')  
        ->first();


        $pagos = DB::table('pago_facturas')
        ->where('pago_facturas.factura_id', '=', $cxcs->factura_id)
        ->join('tipo_pagos', 'pago_facturas.tipopago_id', '=', 'tipo_pagos.id')
        ->select('pago_facturas.*','tipo_pagos.tipopago')  
        ->get();

        return view('cxcs.show', compact('cxcs','facturas','pagos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cxcs = CxC::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $facturas = Factura::find($cxcs->factura_id);
        $tipo_pagos = TipoPago::where('cia_id', '=', session('cia_id'))
              ->whereNull('deleted_at')
              ->orWhere('id','=',1)
              ->orderBy('tipo_pagos.tipopago', 'asc')
              ->get();

        return view('cxcs.edit', compact('cxcs','facturas','tipo_pagos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CxCRequest $request, $id)
    {

        //contrulle el array de pago a partir de las tres columnas del array
        $pago = $this->crearArrayPago($request->tipo_pago_id, $request->valor_pagado, $request->comprobantepago);

        //calcula el total pagado
        $total_pagado=0;
        foreach ($request->valor_pagado as $valor_pagado) 
        {
            $total_pagado += $valor_pagado;
        }

        //registra el pago o el abono a la factura
        Pago::createPagoFacturaLote($pago, $request['total_factura'], $request['factura_id']);

        //acturaliza la info de la cuenta por pagar
        $cxcs = CxC::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $cxcs->valor_pendiente -= round( $total_pagado,2);
        $cxcs->valor_pagado += round( $total_pagado,2);

        $cxcs->save();

        
        //realiza el registro del flujo de caja createFlujoLote($pagos, $referencia, $movimiento_id, $signo)
        //para los tipos de pagos que se realizaron
        Flujo::createFlujoLote($pago,'Pago de cxc de fact: '.$request->codfactura, 6, 1);


        //determina si ya se realizo el total del pago para darla como pagada
        if($request['total_factura'] == $cxcs->valor_pagado)
        {
            //realiza el guardado del estado de la factura
            Estado::updateEstadoFactura($request['factura_id'], 'Pagado');

            return redirect('/cxcs')->with('message', 'La factura fue pagada en su totalidad');  
        }
        else
        {
            return redirect('/cxcs')->with('message', 'El abono a la factura fue registrado');    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public static function createCxC($factura_id, $fecha_vencimiento, $valor_pendiente)
    {
        //guarda la cuenta por cobrar
        $cxcs = CxC::create([
            'fecha' => new \DateTime(),
            'fecha_vencimiento' => $fecha_vencimiento,
            'valor_pendiente' =>  round($valor_pendiente, 2),
            'valor_pagado' => 0,
            'cia_id' => session('cia_id'),
            'agencia_id' => session('agencia_id'),
            'factura_id' => $factura_id,
            'user_id' => Auth::id(),
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('cxcs','CxC Factura: '.$cxcs->factura_id, $cxcs->id);

        return;

    }

    //Contruye el array para el pago de la tabala pago_facturas  a partir de sus 3 columnas basicas
    public static function crearArrayPago($tipopago_id,$valor_pagado, $comprobantepago)
    {
        //declara un array vacio para el pago de la factura
        //sera utilizado para llenar la tabla paago_factura
        $pago = array();

        //contador de las interacciones dentro del array
        $contador = 0;

        //recorre los arrayas
        foreach ($tipopago_id as $id) {

            //une los arrays segun el formato de la tabla pago_facturas
            $pago = array_add($pago,"$contador",[
                    'tipopago_id'        =>  $tipopago_id[$contador],
                    'valor_pagado'         =>  $valor_pagado[$contador],
                    'comprobantepago'    =>  $comprobantepago[$contador],
                ]);
            
            //amuenta el contador
            $contador ++;
            
        }
        return $pago;
    }
}
