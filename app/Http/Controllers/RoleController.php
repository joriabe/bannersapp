<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(9);
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permission::get();
        return view('roles.create', compact('permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //crea el rol
        $roles = Role::create($request->all());

        //actualizar los permisos
        $roles->permissions()->sync($request->get('permissions'));

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('roles','Creación Rol: '.$roles->name, $roles->id);

        return redirect('/roles')->with('message', 'Rol registrado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::find($id);
        $permisos = Permission::get();

        return view('roles.edit', compact('roles','permisos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //busca el rol a actualizar
        $roles = Role::find($id);

        //actualizar el rol
        $roles->update($request->all());

        //actualizar los permisos
        $roles->permissions()->sync($request->get('permissions'));

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('roles','Actualización Rol: '.$roles->name, $roles->id);

        Session::flash('message','Rol Actualizado correctamente');
        return Redirect::to('/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
