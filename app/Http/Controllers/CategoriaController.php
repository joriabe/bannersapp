<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Categoria;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::where('cia_id','=',session('cia_id'))
        ->orderBy('categorias.updated_at', 'desc')
        ->paginate(9);
        return view('categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categorias = new Categoria([
            'id' => $request['id'],
            'categoria' => $request['categoria'],
            'acronimo' => $request['acronimo'],
            'cia_id' => session('cia_id'),
        ]);
        $categorias->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia    
        Log::register('categorias','Creación Categoría: '.$categorias->acronimo, $categorias->id);


         //Retornamos la Respuesta Segun sea el caso.
        if ($request['form_pos'] == 1) {
            //Si se Llamo desde un modal
            $respuesta = ['msg' => 'Categoria Registrada Correctamente', 'id' => $categorias->id, 'nombre' => $categorias->categoria];
            print json_encode($respuesta);
        } 
        if ($request['form_pos'] == 0) {
            //Si se Llamo desde el dashboard
            return redirect('/categorias')->with('message', 'Categoría registrada correctamente'); 
        }


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('categorias.edit', compact('categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categorias = Categoria::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $categorias->fill($request ->all());
        $categorias->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia    
        Log::register('categorias','Actualización Categoría: '.$categorias->acronimo, $categorias->id);

        Session::flash('message','Categoía '.$request['acronimo'].'   actualizado correctamente');
        return Redirect::to('/categorias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
        $categorias = Categoria::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $categorias->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia    
        Log::register('categorias','Eliminación Categoría: '.$categorias->acronimo , $categorias->id);

        Session::flash('message','Categoría '.$id.' eliminada correctamente');
        return Redirect::to('/categorias');
    }

}
