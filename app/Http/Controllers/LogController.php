<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use \App\Log;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {     
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Log::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "logs.index";
      $etiqueta = "Busqueda por fecha, agencia o usuario.";
      
      return view('logs.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function register($tabla, $descripcion, $referencia)
    {
        $logs = Log::create([
            'tabla' => $tabla,
            'descripcion' => $descripcion,
            'referencia' => $referencia,
            'cia_id' => session('cia_id'),
            'agencia_id' => session('agencia_id'),
            'user_id' => Auth::id(),
        ]);

        return;
    }
}
