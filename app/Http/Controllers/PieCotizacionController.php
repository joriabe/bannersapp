<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\PieCotizacion;

class PieCotizacionController extends Controller
{
    public static function createPieCotizacion($pie, $cotizacion_id)
    {
        //guarda el registro del pie
        $pies = PieCotizacion::create([
            'totalimpuesto' => round($pie['totalimpuesto'],2),
            'calculoimpuesto' => round($pie['calculoimpuesto'],2),
            'tipo_impositivo_id' => $pie['tipo_impositivo_id'],
            'cotizacion_id' => $cotizacion_id,
        ]);

        return;

    }

    
 
    public static function createPieCotizacionLote($pies, $cotizacion_id)
    {

        //recorre el lote de los pies de factura
        foreach ($pies as $pie) 
        {
            PieCotizacionController::createPieCotizacion($pie, $cotizacion_id);
        }

        return;
    }
}
