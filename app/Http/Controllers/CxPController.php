<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\CxCRequest;

use \App\CxP;
use \App\DeclaracionCompra;
use \App\PagoCxP;
use \App\TipoPago;

use App\Http\Controllers\LogController as Log;
use App\Http\Controllers\CxCController as CxC;
use App\Http\Controllers\PagoCompraController as Pago;
use App\Http\Controllers\FlujoController as Flujo;


class CxPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = CxP::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "cxps.index";
      $etiqueta = "Busqueda por fecha, fecha v. o proveedor.";
      
      return view('cxps.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cxps = CxP::where('cxps.id','=',$id)
        ->join('agencias', 'agencias.id', '=', 'cxps.agencia_id')
        ->where('agencias.cia_id','=',session('cia_id'))
        ->firstOrFail();

        $compras = DeclaracionCompra::where('declaracion_compras.id','=',$cxps->compra_id)
        ->join('proveedores', 'declaracion_compras.proveedor_id', '=', 'proveedores.id')
        ->select('declaracion_compras.*','proveedores.*')  
        ->first();


        $pagos = PagoCxP::where('pago_cxps.factura_id', '=', $cxps->compra_id)
        ->join('tipo_pagos', 'pago_cxps.tipopago_id', '=', 'tipo_pagos.id')
        ->select('pago_cxps.*','tipo_pagos.tipopago')  
        ->get();

        return view('cxps.show', compact('cxps','compras','pagos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cxps = CxP::where('cxps.id','=',$id)
        ->join('agencias', 'agencias.id', '=', 'cxps.agencia_id')
        ->where('agencias.cia_id','=',session('cia_id'))
        ->select('cxps.*')
        ->firstOrFail();

        $facturas = DeclaracionCompra::find($cxps->compra_id);

        $tipo_pagos = TipoPago::tipopagos();

        return view('cxps.edit', compact('cxps','facturas','tipo_pagos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CxCRequest $request, $id)
    {
        //contrulle el array de pago a partir de las tres columnas del array
        $pago = CxC::crearArrayPago($request->tipo_pago_id, $request->valor_pagado, $request->comprobantepago);

        //calcula el total pagado
        $total_pagado=0;
        foreach ($request->valor_pagado as $valor_pagado) 
        {
            $total_pagado += $valor_pagado;
        }

        //registra el pago o el abono a la factura
        Pago::createPagoCompraLote($pago, $request['total_factura'], $request['factura_id']);

        //acturaliza la info de la cuenta por pagar
        $cxps = CxP::where('cxps.id','=',$id)
        ->join('agencias', 'agencias.id', '=', 'cxps.agencia_id')
        ->where('agencias.cia_id','=',session('cia_id'))
        ->select('cxps.*')
        ->firstOrFail();

        $cxps->valor_pendiente -= round( $total_pagado,2);
        $cxps->valor_pagado += round( $total_pagado,2);

        $cxps->save();

        
        //realiza el registro del flujo de caja createFlujoLote($pagos, $referencia, $movimiento_id, $signo)
        //para los tipos de pagos que se realizaron
        Flujo::createFlujoLote($pago,'Pago de cxp de fact: '.$request->codfactura, 6, -1);


        //determina si ya se realizo el total del pago para darla como pagada
        if($request['total_factura'] == $cxps->valor_pagado)
        {
            return redirect('/cxps')->with('message', 'La factura fue pagada en su totalidad');  
        }
        else
        {
            return redirect('/cxps')->with('message', 'El abono a la factura fue registrado');    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function createCxP($factura_id, $fecha_vencimiento, $valor_pendiente)
    {
        $cxps = CxP::create([
            'fecha' => new \DateTime(),
            'fecha_vencimiento' => $fecha_vencimiento,
            'valor_pendiente' =>  round($valor_pendiente, 2),
            'valor_pagado' => 0,
            'agencia_id' => session('agencia_id'),
            'compra_id' => $factura_id,
            'user_id' => Auth::id(),
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('cxps','CxP Factura: '.$cxps->factura_id, $cxps->id);

        return;
    }
}
