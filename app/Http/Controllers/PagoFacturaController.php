<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\PagoFactura;
use \App\Factura;

use App\Http\Controllers\LogController as Log;

class PagoFacturaController extends Controller
{
	/**
     * Realiza el recorrido y los calculos correspondiente
     * para la el detalle de la factura
     * @param  array  $detalle_pago
     * @return array  totalpag
     */
    public static function calculoPago($detalle_pago)
    {	
    	//Contendra el valor pagado
    	$totalpag = 0;

    	//reccorrido para obtener el valor pagado total 
        foreach ($detalle_pago as $pago) {
                $totalpag = $totalpag + floatval($pago['valor_pagado']);
        }

        return $totalpag;
    }

    public static function createPagoFactura($pago, $total_factura, $factura_id)
    {
        //guarda el registro del pago
        $pagos = PagoFactura::create([
            'total_factura' => round(($total_factura),2),
            'valor_pago' => round($pago['valor_pagado'],2),
            'comprobantepago' => $pago['comprobantepago'],
            'factura_id' => $factura_id,
            'tipopago_id' => $pago['tipopago_id'],
        ]);

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('pago_facturas','Pago factura: '.$factura_id, $pagos->id);

        return;
    }
 
    public static function createPagoFacturaLote($pagos, $total_factura, $factura_id)
    {
        //recorre el lote de los pagos de factura
        foreach ($pagos as $pago) 
        {
            if ($pago['valor_pagado'] != 0 or $pago['valor_pagado'] != -0) {
                PagoFacturaController::createPagoFactura($pago, $total_factura, $factura_id);
            }
            
        }
        return;
    } 

    public static function findPagoFactura($factura_id)
    {
        $pagos = PagoFactura::where('factura_id','=', $factura_id)->get();
        $cambio = Factura::find($factura_id);


        
        $pago_facturas[]=array();

        foreach ($pagos as $pago) 
        {

            //determina si el pago es en efectivo
            if ($pago->tipopago_id == 1)
            {
                $pago_facturas = array_add($pago_facturas, $pago->tipopago_id, [ 
                    'tipopago_id' => $pago->tipopago_id,
                    'valor_pagado' => ($pago->valor_pago - $cambio->cambio),
                    'comprobantepago' => $pago->comprobantepago,
                ]);

            }
            else
            {
                $pago_facturas = array_add($pago_facturas, $pago->tipopago_id, [ 
                    'tipopago_id' => $pago->tipopago_id,
                    'valor_pagado' => $pago->valor_pago,
                    'comprobantepago' => $pago->comprobantepago,
                ]);


            }
        }

        //limpia el array para quitar la llave 0
        array_forget($pago_facturas, "0");

        return $pago_facturas;
    } 
}
