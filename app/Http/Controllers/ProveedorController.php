<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ProveedorRequest;

use \App\Proveedor;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
              //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Proveedor::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "proveedores.index";
      $etiqueta = "Busqueda por cod, nombre o razon social.";
      
      return view('proveedores.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProveedorRequest $request)
    {
        $proveedores = new Proveedor([
            'codinterno' => $request['codinterno'],
            'name' => $request['name'],
            'razon_social' => $request['razon_social'],
            'rtn' => $request['rtn'],
            'cai' => $request['cai'],
            'name_contacto' => $request['name_contacto'],
            'cargo_contacto' => $request['cargo_contacto'],
            'direccion' => $request['direccion'],
            'ciudad' => $request['ciudad'],
            'telefono' => $request['telefono'],
            'correo' => $request['correo'],
            'dias_credito' => $request['dias_credito'],
            'condiciones_pago' => $request['condiciones_pago'],
            'nota' => $request['nota'],
            'cia_id' => session('cia_id'),
        ]);

        $proveedores->save();

        //guarda un registro de lo realizado 
        Log::register('proveedores','Creación proveedor: '.$request['name'], $proveedores->id);

        //Retornamos la Respuesta Segun sea el caso.
        if ($request['form_pos'] == 1) {
            //Si se Llamo desde un modal
            $respuesta = ['msg' => 'Proveedor Registrado Correctamente', 'id' => $proveedores->id, 'nombre' => $proveedores->name];
            print json_encode($respuesta);
        } 
        if ($request['form_pos'] == 0) {
            //Si se Llamo desde el dashboard
            return redirect('/proveedores')->with('message', 'Proveedor '.$request['name'].' registrado correctamente');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $proveedores = Proveedor::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('proveedores.show', compact('proveedores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedores = Proveedor::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('proveedores.edit', compact('proveedores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProveedorRequest $request, $id)
    {
        $proveedores = Proveedor::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $proveedores->fill($request ->all());
        $proveedores->save();

        //guarda un registro de lo realizado 
        Log::register('proveedores','Actualización Proveedor: '.$proveedores->name, $proveedores->id);

        Session::flash('message','Proveedor '.$request['name'].'   actualizado correctamente');
        return Redirect::to('/proveedores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
        $proveedores = Proveedor::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        $proveedores->delete();

        //guarda un registro de lo realizado 
        Log::register('proveedores','Eliminación Proveedor: '.$proveedores->name, $proveedores->id);

        Session::flash('message','Proveedor '.$proveedores->name.' eliminado correctamente');
        return Redirect::to('/proveedores');
    }
}
