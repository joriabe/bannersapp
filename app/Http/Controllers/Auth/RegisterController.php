<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Http\Controllers\LogController as Log;

use App\Cia;
use \App\RoleUser;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/users';

     /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $agencias = Cia::with('agencias')->get();
        $roles = Role::all();

        return view('auth.register',['cias'=>$agencias, 'roles'=>$roles]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'agencia_id' => 'required',
            'rol_id' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //abre una nueva transaccion
        DB::beginTransaction();
        
        //abre un try por si ocurre algun error en las distintas consultas a la BD
        try {

            //Realiza el retorno y la creacion del registro del usuario
            //Nota: se almacena la consulta en una variable para poder retornarla
            //ya que esta funsion exige que se retorne esta consulta
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'agencia_id' => $data['agencia_id'],
            ]);

            //Asignar Rol al Usuario
            RoleUser::create([
                'role_id' =>  $data['rol_id'],
                'user_id' => $user->id,
            ]);

            //Realiza un registro de la actividad del usuario
            //tabla, descripcion, referencia
            Log::register('users','Registrar User: '.$user->name, $user->id);

            //realiza los cambio en la base de datos
            DB::commit();

            return $user;

        } catch (\Exception $e) {
            
            //si ocurre un error, almacena la info en $error
            $error = $e->getMessage();
            //luego da vuelta a tras los cambios realizados a la BD
            DB::rollback();

            //retorna los errores
            return $error;
        }

    }
}
