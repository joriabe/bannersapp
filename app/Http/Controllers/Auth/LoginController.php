<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\SuscripcionController;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
    */
    protected function authenticated()
    {
        //Obtiene los datos de la Cia y la Agencia
        $datos = DB::table('users')
        ->join('agencias', 'users.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->where('users.id', '=', Auth::id())
        ->select('users.agencia_id','agencias.cia_id', 'agencias.nombrecomercial')
        ->first();


        //Determina si el usuario tiene una caja abierta y se le es asignada.
        $caja_abierta = DB::table('cajas')
        ->where('cajas.user_id','=',Auth::id())
        ->whereNull('cajas.deleted_at')
        ->select('cajas.id')
        ->first();

        //asigna las variables de session
        session(['agencia_id' => $datos->agencia_id, 'cia_id' => $datos->cia_id, 'cia_name' => $datos->nombrecomercial]);

        $suscripcion = SuscripcionController::estadoSuscripcion();

        //asigna las variables de session
        session(['suscripcion' => $suscripcion]); 

        if (is_null($caja_abierta))
        {
          
        }
        else
        {
            //asigna las variables de session
            session(['caja' => $caja_abierta->id]); 
        }


    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        //el rol acces.seperadmin no exite en la base de datos
        //pero cualquier usuario que tenga all-access podra ser redireccionado
        //a esta ruta
        if (Auth::user()->can('access.superadmin')) {
            return '/home';
        }

        //comprueba si tieen permisos para crear factura para redireccionarlo al POS
        if (Auth::user()->can('facturas.create')) {
            return '/pos';
        }
        //en caso contrario lo reenviara a la pagina de inicio del user admin
        else
        {
            return '/home';
        }
    }
}
