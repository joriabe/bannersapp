<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {

        //Asigna los valores a las variables de session
        //Espera como parametros el modelo usuario con el que se llanaran la variable
        ResetPasswordController::llenadoSession($user);

        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    /**
    * Rellena los datos de sesscion
    * 
    * @return 0
        //Asigna los valores a las variables de session
        //Espera como parametros el modelo usuario con el que se llanaran la variable
    */
    public static function llenadoSession($user)
    {
        //Obtiene los datos de la Cia y la Agencia
        $datos = DB::table('users')
        ->join('agencias', 'users.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->where('users.id', '=', $user->id)
        ->select('users.agencia_id','agencias.cia_id', 'agencias.nombrecomercial')
        ->first();


        //Determina si el usuario tiene una caja abierta y se le es asignada.
        $caja_abierta = DB::table('cajas')
        ->where('cajas.user_id','=',$user->id)
        ->whereNull('cajas.deleted_at')
        ->select('cajas.id')
        ->first();

        //dd(is_null($caja_abierta));

        if (is_null($caja_abierta)) {
            //asigna las variables de session
            session(['agencia_id' => $datos->agencia_id, 'cia_id' => $datos->cia_id, 'cia_name' => $datos->nombrecomercial]); 
        } else {
            //asigna las variables de session
            session(['agencia_id' => $datos->agencia_id, 'cia_id' => $datos->cia_id, 'cia_name' => $datos->nombrecomercial, 'caja' => $caja_abierta->id]); 
        }
    }
}
