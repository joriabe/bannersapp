<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;

use \App\Cliente;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
      //obtiene la consulta sobre el index y sus respectivos filtros
      $listados = Cliente::indexConsulta($request);
      
      //variables necesarias para el form de la busqueda
      $ruta = "clientes.index";
      $etiqueta = "Busqueda por codigo, nombre o RTN.";
      
      return view('clientes.index', compact('listados','ruta','etiqueta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {
        $clientes = new Cliente([
            'codinterno' => $request['codinterno'],
            'rtn' => $request['rtn'],
            'nombrecliente' => $request['nombrecliente'],
            'razonsocialcliente' => $request['razonsocialcliente'],
            'direccion' => $request['direccion'],
            'telefono' => $request['telefono'],
            'correo' => $request['correo'],
            'cia_id' => session('cia_id'),
        ]);

        $clientes->save();

        //guarda un registro de lo realizado 
        Log::register('clientes','Creación cliente: '.$request['nombrecliente'], $clientes->id);

        //Retornamos la Respuesta Segun sea el caso.
        if ($request['form_pos'] == 1) {
            //Si se Llamo desde el modulo POS
            $respuesta = ['msg' => 'Cliente Registrado Correctamente', 'id' => $clientes->id, 'nombre' => $clientes->nombrecliente];
            print json_encode($respuesta);
        } 
        if ($request['form_pos'] == 0) {
            //Si se Llamo desde el dashboard
            return redirect('/clientes')->with('message', 'Cliente registrado correctamente');
        }  

         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientes = Cliente::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('clientes.edit', compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $request, $id)
    {
        $clientes = Cliente::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $clientes->fill($request ->all());
        $clientes->save();

        //guarda un registro de lo realizado 
        Log::register('clientes','Actualización Cliente: '.$clientes->nombrecliente, $clientes->id);

        Session::flash('message','Cliente '.$request['nombrecliente'].'   actualizado correctamente');
        return Redirect::to('/clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Realiza un borrado suabe en un recurso especifico almacenado
    */
    public function delete($id)
    {
        $clientes = Cliente::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();
        
        $clientes->delete();

        //guarda un registro de lo realizado 
        Log::register('clientes','Eliminación Cliente: '.$clientes->nombrecliente, $clientes->id);

        Session::flash('message','Cliente '.$id.' eliminado correctamente');
        return Redirect::to('/clientes');
    }
}
