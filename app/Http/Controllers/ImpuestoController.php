<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\TipoImpositivo;

use App\Http\Controllers\LogController as Log;

use Session;
use Redirect;

class ImpuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $impuestos = TipoImpositivo::where('cia_id','=',session('cia_id'))
        ->whereNull('deleted_at')
        ->paginate(9);
        return view('impuestos.index', compact('impuestos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('impuestos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $impuestos = new TipoImpositivo([
            'descripcion' => $request['descripcion'],
            'impuesto' => $request['impuesto'],
            'cia_id' => session('cia_id'),
        ]);

        $impuestos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_impositivos','Creación Impuesto : '.$impuestos->descripcion, $impuestos->id);

        return redirect('/impuestos')->with('message', 'Impuesto registrado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $impuestos = TipoImpositivo::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        return view('impuestos.edit', compact('impuestos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $impuestos = TipoImpositivo::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $impuestos->fill($request ->all());
        $impuestos->save();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_impositivos','Actualización Impuesto : '.$impuestos->descripcion, $impuestos->id);

        Session::flash('message','Impuesto '.$request['descripcion'].'   actualizado correctamente');
        return Redirect::to('/impuestos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $impuestos = TipoImpositivo::where('id','=',$id)
        ->where('cia_id','=',session('cia_id'))
        ->firstOrFail();

        $impuestos->delete();

        //Realiza un registro de la actividad del usuario
        //tabla, descripcion, referencia
        Log::register('tipo_impositivos','Eliminación Impuesto : '.$impuestos->descripcion, $impuestos->id);

        Session::flash('message','Impuesto '.$id.' eliminado correctamente');
        return Redirect::to('/impuestos');
    }
}
