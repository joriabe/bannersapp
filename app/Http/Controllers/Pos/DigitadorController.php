<?php

namespace App\Http\Controllers\Pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Cliente as Cliente;
use App\TipoPago as TipoPago;

class DigitadorController extends Controller
{
    public function getClientes()
    {
    	$clientes = Cliente::where('cia_id','=',session('cia_id'))
        ->whereNull('deleted_at')
        ->orWhere('id', '=', 1)
        ->select('id','nombrecliente')
        ->orderBy('id', 'asc')
        ->get();

    	return $clientes;
    }
    public function getTipoPago()
    {
    	$tipopago = TipoPago::where('cia_id','=',session('cia_id'))
        ->whereNull('deleted_at')
        ->orWhere('id', '=', 1)
        ->select('id','tipopago')
        ->get();

    	return $tipopago;    	
    }
    public function getCotizacion($id)
    {
        $productos = DB::table('cotizacions')
            ->join('agencias', 'cotizacions.agencia_id', '=', 'agencias.id')
            ->join('detalle_cotizacions', 'cotizacions.id', '=', 'detalle_cotizacions.cotizacion_id')
            ->join('producto_servicios', 'detalle_cotizacions.producto_servicio_id', '=', 'producto_servicios.id')
            ->join('unidads', 'producto_servicios.unidad_id', '=', 'unidads.id')
            ->join('tipo_impositivos', 'producto_servicios.tipoimpositivos_id','=','tipo_impositivos.id')
            ->join('descuentos', 'producto_servicios.descuento_id','=', 'descuentos.id')
            ->where('agencias.cia_id', '=', session('cia_id')) 
            ->where('producto_servicios.cia_id','=',session('cia_id'))
            ->where('cotizacions.cod_interno', '=', $id)
            ->select('producto_servicios.id as p_id', 'detalle_cotizacions.descripcion', 'detalle_cotizacions.cantidad', 'detalle_cotizacions.precio', 'detalle_cotizacions.subtotal', 'unidads.acronimo as unidad', 'unidads.id as u_id','tipo_impositivos.id as tp_id','tipo_impositivos.impuesto', 'descuentos.descuento', 'cotizacions.cliente_id')
            ->get();

        return $productos;       
    }

}