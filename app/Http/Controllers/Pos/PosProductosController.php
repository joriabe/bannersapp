<?php

namespace App\Http\Controllers\Pos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categoria as Categoria;
use App\Caja;
use App\Http\Controllers\FlujoController as Flujo;
use App\ProductoServicio as ProductoServicio;

class PosProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::where('cia_id','=',session('cia_id'))
        ->select('id','categoria')
        ->orderBy('categoria', 'asc')
        ->get();

        $cajas = Caja::where('id','=',session('caja'))
        ->where('agencia_id','=',session('agencia_id'))
        ->firstOrFail();

        //obtiene los flujos para la caja del usuario
        $flujos = Flujo::resumenFlujoCaja($cajas->apertura, session('caja'));

        return view('pos.pos_main', ['categorias' => $categorias, 'flujos' => $flujos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //aqui mandamos los producto de una categoria
        $productos = \DB::table('producto_servicios')
            ->join('unidads', 'producto_servicios.unidad_id', '=', 'unidads.id')
            ->where('categoria_id', $id)
            ->select('producto_servicios.id as p_id', 'producto_servicios.descripcion', 'producto_servicios.precio', 'unidads.acronimo as unidad', 'unidads.id as u_id')
            ->orderBy('descripcion', 'asc')
            ->get();

        return $productos;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($producto)
    {
        //aqui buscamos el producto enviado desde el buscador

        $productos = \DB::table('producto_servicios')
            ->join('unidads', 'producto_servicios.unidad_id', '=', 'unidads.id')
            ->where('producto_servicios.cia_id','=',session('cia_id'))
            ->where(function ($query) use ($producto){
                $query->where('descripcion', 'like' , '%'.$producto.'%')
                      ->orWhere('codinterno', 'like' , '%'.$producto.'%');
            })
            ->select('producto_servicios.id as p_id', 'producto_servicios.descripcion', 'producto_servicios.precio', 'unidads.unidad', 'unidads.id as u_id')
            ->orderBy('descripcion', 'asc')
            ->get();

        //return view('pos.prueba',['prueba' => $productos]);
        return $productos;


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
