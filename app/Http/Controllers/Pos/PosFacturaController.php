<?php

namespace App\Http\Controllers\Pos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductoServicio as ProductoServicio;

class PosFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //mandamos el producto cuando se cliclea uuno o se usa el codbarra
        $producto = \DB::table('producto_servicios')
            ->join('unidads', 'producto_servicios.unidad_id', '=', 'unidads.id')
            ->join('tipo_impositivos', 'producto_servicios.tipoimpositivos_id','=','tipo_impositivos.id')
            ->join('descuentos', 'producto_servicios.descuento_id','=', 'descuentos.id') 
            ->where('producto_servicios.cia_id','=',session('cia_id'))
            ->where(function ($query) use ($id) {
                $query->where('producto_servicios.id', '=', $id)
                      ->orWhere('codinterno', '=', $id);
            })
            ->select('producto_servicios.id as p_id', 'producto_servicios.descripcion', 'producto_servicios.precio', 'unidads.acronimo as unidad', 'unidads.id as u_id','tipo_impositivos.id as tp_id','tipo_impositivos.impuesto','descuento')
            ->get();

        return $producto;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
