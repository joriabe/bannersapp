<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Session;

class MovimientoInventario extends Model
{
    protected $fillable = ['cantidad_inicial','cantidad','cantidad_final','nota','destino','tipo_movimiento','user_id','inventario_id'];
}
