<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

class Factura extends Model
{
	use SoftDeletes;
    
    protected $fillable = ['id', 'codfactura', 'fecha','totalantesimpuesto','totaldespuesimpuesto','descuento','pagado','cambio','agencia_id','cliente_id','usuario_id'];
    protected $dates = ['fecha','deleted_at'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Factura::where('agencias.cia_id', '=', session('cia_id'))
        		->join('clientes', 'facturas.cliente_id', '=', 'clientes.id')
        		->join('agencias', 'facturas.agencia_id', '=', 'agencias.id')
        		->orderBy('facturas.created_at', 'desc')
  				->withTrashed()
        		->filtro($request)
		        ->select('facturas.*', 'nombrecliente')  
		        ->paginate(9);
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return $query->where('codfactura', 'like', '%'.$busqueda.'%')
                     ->orwhere('facturas.fecha', 'like', '%'.$busqueda.'%')
                     ->orwhere('nombrecliente', 'like', '%'.$busqueda.'%');
 

    }

  	public function agencias()
	{
		return $this->belongsTo(Agencia::class);
	}
	public function clientes()
	{
		return $this->belongsTo(Cliente::class);
	}
	public function users()
	{
		return $this->belongsTo(User::class);
	}
	public function detalle_facturas()
  	{
      return $this->hasMany(DetalleFactura::class);
  	}
  	public function pie_facturas()
	{
		return $this->hasMany(PieFactura::class);
	}
	public function estado_facturas()
	{
		return $this->belongsTo(PieFactura::class);
	}
}
