<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
//llamamos DB para hacer la query mas personalizable
use Illuminate\Support\Facades\DB;


class DeclaracionVentasExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function forMonth(int $month)
    {
        $this->month = $month;
        
        return $this;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                $tamaño = $this->a_count + 3;

                $cellRange = 'A1:H2'; // All headers
                $cellRange2 = 'A'.$tamaño.':H'.$tamaño;

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
            },
        ];
    }

    public function view(): View
	{	
		$mes = $this->month;

		$array_facturas = [];

	    $facturas = DB::table('facturas')
		->join('agencias', 'facturas.agencia_id', '=', 'agencias.id')
		->join('estado_facturas', 'estado_facturas.factura_id', '=', 'facturas.id')
		->where('agencias.cia_id', '=', session('cia_id'))
		->where('estado_facturas.estado1', '=', 1)
		->whereMonth('fecha', $mes)
		->orderBy('facturas.id', 'asc')
		->select('facturas.id', 'codfactura','fecha')
	    ->get();

	    foreach ($facturas as $factura) {

	    	$array = [
		    "fecha" => "",
		    "codfactura" => "",
		    "importe_exento" => 0,
		    "importe_15" => 0,
		    "importe_18" => 0,
		    "impuesto_15" => 0,
		    "impuesto_18" => 0,
			];

			$array["fecha"] = $factura->fecha;
			$array["codfactura"] = $factura->codfactura;


			$detalles = DB::table('pie_facturas')
			->join('tipo_impositivos', 'pie_facturas.tipo_impositivo_id', '=', 'tipo_impositivos.id')
			->where('pie_facturas.factura_id', '=', $factura->id)
			->select('totalimpuesto', 'calculoimpuesto', 'impuesto')
			->orderBy('impuesto', 'asc')
			->get();

			foreach ($detalles as $detalle) {
				if ($detalle->impuesto == 0) {
					$array["importe_exento"] = $detalle->totalimpuesto;
				}
				if ($detalle->impuesto == 15) {
					$array["importe_15"] = $detalle->totalimpuesto;
					$array["impuesto_15"] = $detalle->calculoimpuesto;
				}
				if ($detalle->impuesto == 18) {
					$array["importe_18"] = $detalle->totalimpuesto;
					$array["impuesto_18"] = $detalle->calculoimpuesto;
				}
			}

			$array_facturas[] = $array;
	    }

	    $this->a_count = count($array_facturas);

		return view('exports.isv_ventas', ['ventas' => $array_facturas]);
	}
}
