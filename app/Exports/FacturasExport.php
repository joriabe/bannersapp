<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
//llamamos DB para hacer la query mas personalizable
use Illuminate\Support\Facades\DB;


class FacturasExport implements FromView, ShouldAutoSize, WithEvents
{
	use Exportable;

	public function forDate($date)
    {
        $this->date = $date;
        
        return $this;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {        

                $cellRange = 'A1:G1'; // All headers               

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
        ];
    }

    public function view(): View
	{	
		$dia1 = $this->date->input_fechai;
        $dia2 = $this->date->input_fechaf;

		$facturas = DB::table('facturas')
		->join('agencias', 'facturas.agencia_id', '=', 'agencias.id')
		->join('clientes', 'facturas.cliente_id', '=', 'clientes.id')
		->join('estado_facturas', 'facturas.id', '=', 'estado_facturas.factura_id')
		->join('users', 'facturas.usuario_id', '=', 'users.id')
		->where('agencias.cia_id', '=', session('cia_id'))
		->whereDate('fecha', '>=' , $dia1)
        ->whereDate('fecha', '<=' , $dia2)
		->orderBy('facturas.id', 'asc')
		->select('facturas.codfactura','facturas.fecha','clientes.nombrecliente','facturas.totalantesimpuesto','facturas.totaldespuesimpuesto', 'users.name','estado_facturas.estado2')
	    ->get();

	    return view('exports.facturas', ['facturas' => $facturas]);

    }
}
