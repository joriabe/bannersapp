<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
//llamamos DB para hacer la query mas personalizable
use Illuminate\Support\Facades\DB;

class FlujosExport implements FromView, ShouldAutoSize, WithEvents
{

	use Exportable;

	public function forDate($date)
    {
        $this->date = $date;
        
        return $this;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {        

                $cellRange = 'A1:G1'; // All headers               

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
        ];
    }
    public function view(): View
	{	
		$dia1 = $this->date->input_fechai;
        $dia2 = $this->date->input_fechaf;

		$flujos = DB::table('flujos')
        ->join('tipo_pagos', 'flujos.tipopago_id', '=', 'tipo_pagos.id')
        ->join('users', 'flujos.usuario_id', '=', 'users.id')
        ->join('tipo_movimientos', 'flujos.movimiento_id', '=', 'tipo_movimientos.id')
        ->join('agencias', 'flujos.agencia_id', '=', 'agencias.id')
        ->join('cias', 'agencias.cia_id', '=', 'cias.id')
        ->where('agencias.cia_id', '=', session('cia_id'))
        ->whereDate('flujos.fecha', '>=', $dia1)
        ->whereDate('flujos.fecha', '<=', $dia2)
        ->whereNull('agencias.deleted_at')
        ->whereNull('cias.deleted_at')
        ->orderBy('flujos.updated_at', 'desc')        
        ->select('flujos.fecha','flujos.valor','flujos.referencia','users.name','tipo_movimientos.descripcion', 'tipo_pagos.tipopago', 'agencias.descripcion as agencia')           
	    ->get();

	    return view('exports.flujos', ['flujos' => $flujos]);

    }
}
