<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
//llamamos DB para hacer la query mas personalizable
use Illuminate\Support\Facades\DB;



class DeclaracionComprasExport implements FromView, ShouldAutoSize, WithEvents
{
	use Exportable;

    public function forMonth(int $month)
    {
        $this->month = $month;
        
        return $this;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                $tamaño = $this->a_count + 3;

                $cellRange = 'A1:N2'; // All headers
                $cellRange2 = 'A'.$tamaño.':N'.$tamaño;

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
            },
        ];
    }

   	public function view(): View
	{	
		$mes = $this->month;

		$datos_declaracion = DB::table('declaracion_compras')
	        ->join('proveedores', 'declaracion_compras.proveedor_id', '=', 'proveedores.id')
	        ->whereMonth('fecha', $mes)
            ->where('declaracion_compras.cia_id', session('cia_id'))
            ->whereNull('declaracion_compras.deleted_at')
	        ->orderBy('fecha', 'asc')
	        ->get();

        $this->a_count = count($datos_declaracion);

	    return view('exports.isv_compras', ['compras' => $datos_declaracion]);
	}
}
