<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CxC extends Model
{
	use SoftDeletes;	
    
    protected $table = 'cxcs';
    
    protected $fillable = ['id','fecha','fecha_vencimiento','valor_pendiente','valor_pagado','cia_id','agencia_id','factura_id','user_id'];

    protected $dates = ['deleted_at'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return CxC::where('cxcs.cia_id', '=', session('cia_id'))
            ->join('cias', 'cxcs.cia_id', '=', 'cias.id')
            ->join('facturas', 'cxcs.factura_id', '=', 'facturas.id')
            ->join('clientes', 'facturas.cliente_id', '=', 'clientes.id')
            ->filtro($request)
            ->orderBy('cxcs.updated_at', 'desc')
            ->select('cxcs.*','clientes.nombrecliente as cliente_name','facturas.codfactura')        
            ->paginate(9); 
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
        $busqueda = trim($request->i_buscar);

        return $query->where('cxcs.fecha', 'like', '%'.$busqueda.'%')
                      ->orwhere('cxcs.fecha_vencimiento', 'like', '%'.$busqueda.'%')
                      ->orwhere('clientes.nombrecliente', 'like', '%'.$busqueda.'%');
    }

    //Debuelve un conjunto de cxc que estan vencidas o por vencer
    public static function cxcVencidasAvencer()
    { 	
    	$hoy = Carbon::now();
    	$hoymas2semanas = Carbon::now()->addWeeks(2);

    	//$vencimiento = new Carbon\Carbon($cotizacion->fechavencimiento);

    	  $cxcs = CxC::where('cxcs.cia_id', '=', session('cia_id'))
            ->join('cias', 'cxcs.cia_id', '=', 'cias.id')
            ->join('facturas', 'cxcs.factura_id', '=', 'facturas.id')
            ->join('clientes', 'facturas.cliente_id', '=', 'clientes.id')
            ->where('valor_pendiente','>',0)
            ->whereBetween('cxcs.fecha_vencimiento', array($hoy, $hoymas2semanas))
            ->orderBy('cxcs.fecha_vencimiento', 'desc')
            ->select('cxcs.*','clientes.nombrecliente as cliente_name','facturas.codfactura')        
            ->paginate(4);


            return $cxcs;
    }
}
