<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

class ProductoInventario extends Model
{
	use SoftDeletes;

	protected $fillable = ['cantidad','cantidad_minima','alerta_existencia','facturar_sin_existencia','producto_id','bodega_id'];
	
	protected $dates = ['deleted_at'];

	//debuelve el inventario de un producto por bodega
    public static function inventarioProducto($producto_id)
    {
    	return ProductoInventario::where('producto_id', '=', $producto_id)
    					->join('bodegas','producto_inventarios.bodega_id','=','bodegas.id')
        				->select('bodega_id','bodega','cantidad','cantidad_minima','alerta_existencia','facturar_sin_existencia')
        				->get();
    }
}
