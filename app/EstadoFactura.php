<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoFactura extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'estado1','estado2','factura_id'];
    protected $dates = ['deleted_at'];

    public function facturas()
	{
		return $this->belongsTo(Factura::class);
	}
}
