<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoImpositivo extends Model
{
    use SoftDeletes;

    protected $fillable = ['id','descripcion','impuesto','cia_id'];
    protected $dates = ['deleted_at'];

    //*********Consultas BD*******************//
    public static function impuestosLista()
    {
    	return TipoImpositivo::where('cia_id', '=', session('cia_id'))
              ->whereNull('deleted_at')
              ->orWhere('id','=',1)
              ->orWhere('id','=',2)
              ->orderBy('tipo_impositivos.id', 'asc')
        			->pluck('descripcion','id');
    }

    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
	public function producto_servicios()
  	{
   		return $this->hasMany(ProductoServicio::class);
	}
	public function pie_facturas()
  	{
   		return $this->hasMany(PieFactura::class);
	}
}
