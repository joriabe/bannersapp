<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supscripcion extends Model
{
	use SoftDeletes;

	protected $table = 'supscripcions';

	protected $fillable = ['id','cia_id','plan_id','cod_agreement','fecha_renovacion','fecha_inicio','fecha_cancelacion','deleted_at'];

    protected $dates = ['deleted_at','fecha_renovacion','fecha_inicio','fecha_cancelacion'];

    //CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Supscripcion::select('supscripcions.id','supscripcions.cod_agreement','supscripcions.fecha_inicio','supscripcions.fecha_renovacion','planes.descripcion_factura','cias.nombrecomercial')  
            ->join('cias', 'supscripcions.cia_id', '=', 'cias.id')
            ->join('planes', 'supscripcions.plan_id', '=', 'planes.id')
            ->filtro($request)
            ->orderBy('cias.nombrecomercial', 'desc')
                 
            ->paginate(9); 
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
        $busqueda = trim($request->i_buscar);

        return $query->where('cias.nombrecomercial', 'like', '%'.$busqueda.'%')
                      ->orwhere('supscripcions.fecha_inicio', 'like', '%'.$busqueda.'%')
                      ->orwhere('supscripcions.fecha_renovacion', 'like', '%'.$busqueda.'%')
                      ->orwhere('planes.descripcion_factura', 'like', '%'.$busqueda.'%');
    }
}
