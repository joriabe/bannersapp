<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Descuento extends Model
{   
    use SoftDeletes;
    
    protected $fillable = ['fecha_inicio', 'fecha_fin','descripcion','descuento','cia_id'];
    protected $dates = ['deleted_at'];

//*********Consultas BD*******************//
    public static function descuentosLista()
    {

        $descuentos = Descuento::select('id',
          DB::raw('CONCAT(descuento, "% - ",descripcion) AS tag'))
        ->where('cia_id','=',session('cia_id'))
        ->whereNull('deleted_at')
        ->orWhere('id','=',1)
        ->orderBy('descuentos.descuento', 'asc')
        ->pluck('tag', 'id');

        //agrega un elemento a la lista
        //array_add($descuentos,'','Sin Descuento');

       return($descuentos);
    }

    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
    public function productos()
    {
        return $this->hasMany(ProductoServicio::class);
    }

}