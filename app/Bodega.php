<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

class Bodega extends Model
{
    use SoftDeletes;

	protected $fillable = ['cod_interno','bodega','direccion','primaria','agencia_id'];
	
	protected $dates = ['deleted_at'];


    //debuelve un listado de los proveedores para los dropdown
    public static function bodegas()
    {
    	return Bodega::where('agencias.cia_id', '=', session('cia_id'))
    					->join('agencias', 'bodegas.agencia_id', '=', 'agencias.id')
    					->join('cias', 'agencias.cia_id', '=', 'cias.id')
    					->whereNull('bodegas.deleted_at')
                        ->orderBy('bodega', 'asc')
        				->get();
    }

    //Obtiene la bodega principal segun el usuario que este logeado
    public static function bodegaPrincipal()
    {
        return Bodega::where('agencia_id', '=', session('agencia_id'))
                        ->where('primaria', '1')
                        ->whereNull('bodegas.deleted_at')
                        ->select('id')
                        ->first();

    }
}
