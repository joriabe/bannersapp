<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Proveedor extends Model
{
    use SoftDeletes;

    protected $table = 'proveedores';

	protected $fillable = ['id','codinterno','name','razon_social','rtn','cai','name_contacto','cargo_contacto','direccion','ciudad','telefono','correo','dias_credito','condiciones_pago','nota','cia_id'];
	
	protected $dates = ['deleted_at'];

//CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Proveedor::where('cia_id','=',session('cia_id'))
            ->filtro($request)
            ->orderBy('proveedores.updated_at', 'desc')
            ->paginate(9);
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
        $busqueda = trim($request->i_buscar);

        return $query->where('codinterno', 'like', '%'.$busqueda.'%')
                    ->orwhere('name', 'like', '%'.$busqueda.'%')
                    ->orwhere('razon_social', 'like', '%'.$busqueda.'%');
    }

    //debuelve un listado de los proveedores para los dropdown
    public static function proveedoresLista()
    {
    	return Proveedor::where('cia_id', '=', session('cia_id'))
    					->whereNull('deleted_at')
                        ->orderBy('proveedores.name', 'asc')
        				->pluck('name','id');
    }

    //debuelve un listado de los proveedores para  Solo para registro de productos
    //incluye el proveedore (ninguno id=5)
    public static function proveedoresListaProducto()
    {
        $d = Proveedor::where('cia_id', '=', session('cia_id'))
                        ->orwhere('proveedores.id', '5')
                        ->whereNull('deleted_at')
                        ->orderBy('proveedores.name', 'asc')
                        ->pluck('name','id');
        
        //agrega un elemento a la lista
        //$d = Arr::prepend($d,"Ninguno");
        //dd($d);

        return $d;
    }

    //*********Relaciones*********************//
	public function cias()
	{
		return $this->belongsTo(Cia::class);
	}
    public function declaracion_compras()
    {
        return $this->hasMany(DeclaracionCompra::class);
    }
}
