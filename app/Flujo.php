<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use \App\Caja;

class Flujo extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['id', 'fecha','valor','referencia','tipopago_id','usuario_id','movimiento_id','agencia_id'];
    protected $dates = ['fecha','deleted_at'];

	//CONSULTAS A LA BASE DE DATOS
    /*****************************/

    //consultas generales

    //consulta para la seleccion del index
    public static function indexConsulta($request)
    {
      return Flujo::where('agencias.cia_id', '=', session('cia_id'))
            ->join('tipo_pagos', 'flujos.tipopago_id', '=', 'tipo_pagos.id')
            ->join('users', 'flujos.usuario_id', '=', 'users.id')
            ->join('tipo_movimientos', 'flujos.movimiento_id', '=', 'tipo_movimientos.id')
            ->join('agencias', 'flujos.agencia_id', '=', 'agencias.id')
            ->join('cias', 'agencias.cia_id', '=', 'cias.id')
            ->whereNull('agencias.deleted_at')
            ->whereNull('cias.deleted_at')
       		->filtro($request)
	        ->orderBy('flujos.updated_at', 'desc')        
            ->select('flujos.*','agencias.nombrecomercial as agencia_name','users.name as user_name','tipo_movimientos.descripcion as movimiento', 'tipo_pagos.tipopago')
            ->paginate(9); 
    }

    //query scope, son tipos de consultas que aumentan el alcance de las consultas
    public function scopeFiltro($query, $request)
    {
    	$busqueda = trim($request->i_buscar);

     	return $query->where('flujos.fecha', 'like', '%'.$busqueda.'%')
                      ->orwhere('agencias.nombrecomercial', 'like', '%'.$busqueda.'%')
                      ->orwhere('flujos.valor', 'like', '%'.floatval($busqueda).'%')
                      ->orwhere('flujos.referencia', 'like',  '%'.$busqueda.'%')
                      ->orwhere('users.name', 'like', '%'.$busqueda.'%');
    }

    //Realiza una suma agrupada por agencias, por tipo de pago
    public static function resumenCajaFlujo($caja_id)
    {
    	if (Auth::user()->can('rep.flujosexcel')) 
    	{
	    	$caja = Caja::where('cajas.id', '=', $caja_id)->first();
	    }
	    else
		{
			$caja = Caja::where('cajas.id', '=', session('caja'))->first();	
		}
    	

    	return DB::table('flujos')
		       	->join('tipo_pagos', 'flujos.tipopago_id', '=', 'tipo_pagos.id')
		        ->where('flujos.usuario_id', '=', $caja->user_id)
		        ->where('flujos.created_at', '>=', $caja->created_at)
		        ->select('tipo_pagos.tipopago','movimiento_id','tipo_pagos.id as tipo_pago_id', (DB::raw('SUM(flujos.valor) as suma_valor')))
		        ->groupBy('tipopago','movimiento_id', 'tipo_pagos.id')
		        ->get();
    }

    public function tipo_pagos()
	{
		return $this->belongsTo(TipoPago::class);
	}
	public function users()
	{
		return $this->belongsTo(User::class);
	}
	public function tipo_movimientos()
	{
		return $this->belongsTo(TipoMovimiento::class);
	}
	public function agencias()
	{
		return $this->belongsTo(Agencia::class);
	}

}
