<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PieCotizacion extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'totalimpuesto','calculoimpuesto','cotizacion_id','tipo_impositivo_id'];
    protected $dates = ['deleted_at'];

    public function cotizaciones()
	{
		return $this->belongsTo(Cotizacion::class);
	}
	public function tipo_impositivos()
    {
   		return $this->hasMany(TipoImpositivo::class);
	}
}
