<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleFactura extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['id', 'descripcion','cantidad','precio','subtotal','factura_id','producto_servicio_id'];
    
    protected $dates = ['deleted_at'];

    public function facturas()
	{
		return $this->belongsTo(Factura::class);
	}
	public function producto_servicios()
	{
		return $this->belongsTo(ProductoServicio::class);
	}
}
