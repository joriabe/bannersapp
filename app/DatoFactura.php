<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatoFactura extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'correlativof','correlativo','contador_cotizacion','kai','ragosuperior','rangoinferior','fechalimite','frase','logo','agencia_id'];
	
	protected $dates = ['fechalimite', 'deleted_at'];
	
	public function agencias()
	{
		return $this->hasOne(Agencias::class);
	}
}
