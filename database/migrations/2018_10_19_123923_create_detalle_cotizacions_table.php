<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_cotizacions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('descripcion');
            $table->double('cantidad');
            $table->double('precio');
            $table->double('subtotal');

            $table->integer('cotizacion_id')->unsigned()->index();
            $table->foreign('cotizacion_id')->references('id')->on('cotizacions');

            $table->integer('producto_servicio_id')->unsigned()->index();
            $table->foreign('producto_servicio_id')->references('id')->on('producto_servicios');

            $table->softDeletes(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_cotizacions');
    }
}
