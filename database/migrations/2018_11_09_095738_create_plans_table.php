<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planes', function (Blueprint $table) {
            $table->increments('id');

            //paypal
            $table->string('id_plan');
            $table->string('plan');
            $table->string('descripcion');
            $table->string('descripcion_factura');
            $table->double('valor');
            $table->string('frecuencia');
            $table->integer('intervalo');
            $table->integer('ciclo');

            //contenido
            $table->integer('cias');
            $table->integer('sucursales');
            $table->integer('usuarios');
            $table->double('instlacion');

            $table->integer('cia_id')->unsigned()->index()->nullable()->default('1');
            $table->foreign('cia_id')->references('id')->on('cias');         

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planes');
    }
}
