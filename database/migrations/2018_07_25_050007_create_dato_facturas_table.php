<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatoFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_facturas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('correlativof')->unsigned();
            $table->integer('correlativo')->unsigned();
            $table->string('kai');
            $table->string('ragosuperior');
            $table->string('rangoinferior');
            $table->date('fechalimite');
            $table->string('frase')->nullable($value = true);

            $table->integer('agencia_id')->unsigned()->index();
            $table->foreign('agencia_id')->references('id')->on('agencias');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_facturas');
    }
}
