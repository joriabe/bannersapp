<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodUnidadToProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('producto_servicios', function (Blueprint $table) {
            
            $table->integer('unidad_id')->unsigned()->index()->after('tipoimpositivos_id');
            $table->foreign('unidad_id')->references('id')->on('unidads');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_servicios', function (Blueprint $table) {
             $table->dropForeign(['unidad_id']);
             $table->dropColumn('unidad_id');
        });
    }
}
