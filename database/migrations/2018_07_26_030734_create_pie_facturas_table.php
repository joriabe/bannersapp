<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePieFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pie_facturas', function (Blueprint $table) {
            $table->increments('id');

            $table->double('totalimpuesto');
            $table->double('calculoimpuesto');

            $table->integer('factura_id')->unsigned()->index();
            $table->foreign('factura_id')->references('id')->on('facturas');

            $table->integer('tipo_impositivo_id')->unsigned()->index();
            $table->foreign('tipo_impositivo_id')->references('id')->on('tipo_impositivos');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pie_facturas');
    }
}
