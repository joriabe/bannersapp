<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientoInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento_inventarios', function (Blueprint $table) {
            $table->increments('id');

            $table->double('cantidad_inicial');
            $table->double('cantidad');
            $table->double('cantidad_final');
            $table->string('nota');
            $table->string('destino');//factura , bodega;
            $table->string('tipo_movimiento'); //Venta, Baja, Transferencia, Ajuste, Devolucion.

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('inventario_id')->unsigned()->index();
            $table->foreign('inventario_id')->references('id')->on('producto_inventarios');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimiento_inventarios');
    }
}
