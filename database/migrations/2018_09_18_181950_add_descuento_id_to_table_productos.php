<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescuentoIdToTableProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_servicios', function (Blueprint $table) {
            
            $table->integer('descuento_id')->unsigned()->index()->after('unidad_id')->nullable();
            $table->foreign('descuento_id')->references('id')->on('descuentos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_servicios', function (Blueprint $table) {
             $table->dropForeign(['descuento_id']);
             $table->dropColumn('descuento_id');
        });
    }
}
