<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclaracionComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declaracion_compras', function (Blueprint $table) {
            $table->increments('id');

            $table->date('fecha');

            $table->string('numfact1'); //3
            $table->string('numfact2'); //3
            $table->string('numfact3'); //2
            $table->string('numfact4'); //8

            $table->double('subtotal_exento')->unsigned()->nullable()->default('0');
            $table->double('subtotal_15')->unsigned()->nullable()->default('0');
            $table->double('subtotal_18')->unsigned()->nullable()->default('0');


            $table->integer('proveedor_id')->unsigned()->index();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');

            $table->integer('cia_id')->unsigned()->index();
            $table->foreign('cia_id')->references('id')->on('cias');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declaracion_compras');
    }
}
