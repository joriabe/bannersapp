<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupscripcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supscripcions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cia_id')->unsigned()->index();
            $table->foreign('cia_id')->references('id')->on('cias');

            $table->integer('plan_id')->unsigned()->index();
            $table->foreign('plan_id')->references('id')->on('planes'); 

            $table->timestamp('fecha_renovacion')->nullable(); //fecha de renovación de plan null si no se seguira renovando
            $table->timestamp('fecha_inicio')->nullable(); //fecha de inicio del plan
            $table->timestamp('fecha_cancelacion')->nullable()->default(null);        

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supscripcions');
    }
}
