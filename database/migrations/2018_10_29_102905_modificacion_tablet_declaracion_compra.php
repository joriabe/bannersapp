<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificacionTabletDeclaracionCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('declaracion_compras', function (Blueprint $table) {
            
            $table->integer('tipo_docto')->unsigned()->nullable()->default('0')->after('id'); 
            //0    Facturas Proveedores Locales (materiales o servicios)
            //1    Recibos de Servicios Públicos
            //2    Documentos del Sistema Financiero y Seguros
            //3    Boletos de Transportes Aéreos de Pasajeros
            //4    Otros Autorizados por el SAR
            $table->string('num_docto')->after('id');

            $table->dropColumn('numfact1'); //3
            $table->dropColumn('numfact2'); //3
            $table->dropColumn('numfact3'); //2
            $table->dropColumn('numfact4'); //8

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    {
        Schema::table('declaracion_compras', function (Blueprint $table) {
            $table->dropColumn('tipo_docto');
            $table->dropColumn('num_docto');

            $table->string('numfact1'); //3
            $table->string('numfact2'); //3
            $table->string('numfact3'); //2
            $table->string('numfact4'); //8
        });
    }
    }
}
