<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagoCxPsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_cxps', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('total_factura');
            $table->decimal('valor_pago');
            $table->string('comprobantepago')->nullable($value = true);

            $table->integer('factura_id')->unsigned()->index();
            $table->foreign('factura_id')->references('id')->on('declaracion_compras');

            $table->integer('tipopago_id')->unsigned()->index();
            $table->foreign('tipopago_id')->references('id')->on('tipo_pagos');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_cxps');
    }
}
