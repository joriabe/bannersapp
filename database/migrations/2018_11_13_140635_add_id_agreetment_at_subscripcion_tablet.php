<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdAgreetmentAtSubscripcionTablet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supscripcions', function (Blueprint $table) {
            
            $table->string('cod_agreement')->after('plan_id')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supscripcions', function (Blueprint $table) {
            $table->dropColumn('cod_agreement');
        });
    }
}
