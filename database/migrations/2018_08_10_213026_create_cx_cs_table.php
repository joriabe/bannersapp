<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCxCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cxcs', function (Blueprint $table) {
            $table->increments('id');

            $table->date('fecha');
            $table->date('fecha_vencimiento');
            $table->decimal('valor_pendiente');
            $table->decimal('valor_pagado');

            $table->integer('cia_id')->unsigned()->index();
            $table->foreign('cia_id')->references('id')->on('cias');

            $table->integer('agencia_id')->unsigned()->index();
            $table->foreign('agencia_id')->references('id')->on('agencias');

            $table->integer('factura_id')->unsigned()->index();
            $table->foreign('factura_id')->references('id')->on('facturas');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cxcs');
    }
}
