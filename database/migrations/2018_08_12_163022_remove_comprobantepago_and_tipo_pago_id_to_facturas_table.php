<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveComprobantepagoAndTipoPagoIdToFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('facturas', function (Blueprint $table) {
            $table->dropColumn('comprobantepago');

            $table->dropForeign(['tipopago_id']);
            $table->dropColumn('tipopago_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturas', function (Blueprint $table) {
            $table->string('comprobantepago')->nullable($value = true)->after('cambio');

            $table->integer('tipopago_id')->unsigned()->index()->after('agencia_id')->nullable($value = true);
            $table->foreign('tipopago_id')->references('id')->on('tipo_pagos');
        });
    }
}
