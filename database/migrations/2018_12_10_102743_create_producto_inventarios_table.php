<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_inventarios', function (Blueprint $table) {
            $table->increments('id');

            $table->double('cantidad')->default(0);
            $table->double('cantidad_minima')->default(0);

            $table->boolean('alerta_existencia')->default(false);//alerta por llegar a la cantidad minima //1 si, 0 no
            $table->boolean('facturar_sin_existencia')->default(true); //facturarar cuando la existencia llegue a 0
            $table->integer('producto_id')->unsigned()->index();
            $table->foreign('producto_id')->references('id')->on('producto_servicios');

            $table->integer('bodega_id')->unsigned()->index();
            $table->foreign('bodega_id')->references('id')->on('bodegas');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_inventarios');
    }
}
