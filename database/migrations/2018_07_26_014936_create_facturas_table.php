<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codfactura')->index();
            
            $table->timestamp('fecha');
            $table->double('totalantesimpuesto');
            $table->double('totaldespuesimpuesto');
            $table->double('pagado');
            $table->double('cambio');
            $table->string('comprobantepago')->nullable($value = true);

            $table->integer('agencia_id')->unsigned()->index();
            $table->foreign('agencia_id')->references('id')->on('agencias');

            $table->integer('tipopago_id')->unsigned()->index();
            $table->foreign('tipopago_id')->references('id')->on('tipo_pagos');

            $table->integer('cliente_id')->unsigned()->index();
            $table->foreign('cliente_id')->references('id')->on('clientes');

            $table->integer('usuario_id')->unsigned()->index();
            $table->foreign('usuario_id')->references('id')->on('users');

            $table->softDeletes(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
