<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlujosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flujos', function (Blueprint $table) {
            $table->increments('id');

            $table->date('fecha');
            $table->double('valor');
            $table->string('referencia');

            $table->integer('tipopago_id')->unsigned()->index();
            $table->foreign('tipopago_id')->references('id')->on('tipo_pagos');

            $table->integer('usuario_id')->unsigned()->index();
            $table->foreign('usuario_id')->references('id')->on('users');

            $table->integer('movimiento_id')->unsigned()->index();
            $table->foreign('movimiento_id')->references('id')->on('tipo_movimientos');

             $table->integer('agencia_id')->unsigned()->index();
            $table->foreign('agencia_id')->references('id')->on('agencias');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flujos');
    }
}
