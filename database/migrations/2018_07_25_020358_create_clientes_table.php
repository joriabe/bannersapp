<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codinterno')->index();
            
            $table->string('rtn')->nullable($value = true);
            $table->string('nombrecliente');
            $table->string('razonsocialcliente')->nullable($value = true);
            $table->string('direccion')->nullable($value = true);
            $table->string('telefono')->nullable($value = true);
            $table->string('correo')->nullable($value = true);

            $table->integer('cia_id')->unsigned()->index();
            $table->foreign('cia_id')->references('id')->on('cias');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
