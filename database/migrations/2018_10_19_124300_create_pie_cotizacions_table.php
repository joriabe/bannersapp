<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePieCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pie_cotizacions', function (Blueprint $table) {
            $table->increments('id');

            $table->double('totalimpuesto');
            $table->double('calculoimpuesto');

            $table->integer('cotizacion_id')->unsigned()->index();
            $table->foreign('cotizacion_id')->references('id')->on('cotizacions');

            $table->integer('tipo_impositivo_id')->unsigned()->index();
            $table->foreign('tipo_impositivo_id')->references('id')->on('tipo_impositivos');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pie_cotizacions');
    }
}
