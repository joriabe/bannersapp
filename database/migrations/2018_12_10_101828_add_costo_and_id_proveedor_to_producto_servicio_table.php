<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostoAndIdProveedorToProductoServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_servicios', function (Blueprint $table) {
            
            $table->double('costo')->after('precio')->default(0);

            $table->integer('proveedor_id')->unsigned()->index()->after('tipoimpositivos_id')->default(1); //proveedor 1 = Ningun proveedor
            $table->foreign('proveedor_id')->references('id')->on('proveedores');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_servicios', function (Blueprint $table) {
            $table->dropColumn('costo');
            $table->dropForeign(['proveedor_id']);
            $table->dropColumn('proveedor_id');
        });
    }
}
