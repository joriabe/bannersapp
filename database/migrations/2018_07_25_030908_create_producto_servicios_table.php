<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codinterno')->index();

            $table->string('descripcion');
            $table->double('precio');
            $table->binary('prodoctooservicio');

            $table->integer('categoria_id')->unsigned()->index();
            $table->foreign('categoria_id')->references('id')->on('categorias');

            $table->integer('tipoimpositivos_id')->unsigned()->index();
            $table->foreign('tipoimpositivos_id')->references('id')->on('tipo_impositivos');

            $table->integer('cia_id')->unsigned()->index();
            $table->foreign('cia_id')->references('id')->on('cias');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_servicios');
    }
}

