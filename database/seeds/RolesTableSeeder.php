<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Roles
        Role::create([
        	'name'			=>	'Admin',
        	'slug'			=>	'admin',
        	'description'	=>	'Privilegios de administrador',
        	'special'		=>	'all-access',
        ]);

        Role::create([
            'name'          =>  'Guest',
            'slug'          =>  'guest',
            'description'   =>  'Ningún Acceso',
            'special'       =>  'all-access',
        ]);
    }
}
