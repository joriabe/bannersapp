<?php

use Illuminate\Database\Seeder;
use \App\TipoMovimiento;

class TipoMovimientoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//TipoMovimiento
        TipoMovimiento::create([
        	'descripcion'	=>	'Venta',
        ]);

        TipoMovimiento::create([
            'descripcion'   =>  'Retiro',
        ]);

        TipoMovimiento::create([
            'descripcion'   =>  'Devolución',
        ]);

        TipoMovimiento::create([
            'descripcion'   =>  'Ajuste',
        ]);
   
        TipoMovimiento::create([
            'descripcion'   =>  'Cambio',
        ]);

        TipoMovimiento::create([
            'descripcion'   =>  'Abono',
        ]);

        TipoMovimiento::create([
            'descripcion'   =>  'Anulación',
        ]);
    
        TipoMovimiento::create([
            'descripcion'   =>  'Compra',
        ]);
    }
}
