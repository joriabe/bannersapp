<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permisos para la tabla users
        Permission::create([
        	'name'	=>	'Navegar Usuarios',
        	'slug'	=>	'users.index',
        	'description'	=>	'Consulta de todos los usuarios del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Usuarios',
        	'slug'	=>	'users.edit',
        	'description'	=>	'Edición de todos los usuarios del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Usuarios',
        	'slug'	=>	'users.create',
        	'description'	=>	'Creación de usuarios para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Activar y Desactivar Usuarios',
        	'slug'	=>	'users.delete',
        	'description'	=>	'Da de baja y reactiva a los usuarios del sistema.',
        ]);

        //Permisos para la tabla roles
        Permission::create([
        	'name'	=>	'Navegar Rol',
        	'slug'	=>	'roles.index',
        	'description'	=>	'Consulta de todos los roles del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Rol',
        	'slug'	=>	'roles.edit',
        	'description'	=>	'Edición de todos los roles del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Rol',
        	'slug'	=>	'roles.create',
        	'description'	=>	'Creación de roles para el sistema.',
        ]);

        //Permisos para la tabla cia
        Permission::create([
        	'name'	=>	'Navegar Compañias',
        	'slug'	=>	'cias.index',
        	'description'	=>	'Consulta de todas las compañias del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Compañias',
        	'slug'	=>	'cias.edit',
        	'description'	=>	'Edición de todas las compañias del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Compañias',
        	'slug'	=>	'cias.create',
        	'description'	=>	'Creación de compañias para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Eliminar Compañias',
        	'slug'	=>	'cias.delete',
        	'description'	=>	'Da de baja a las compañias del sistema.',
        ]);

        //Permisos para la tabla agencias
        Permission::create([
            'name'  =>  'Navegar Agencias',
            'slug'  =>  'agencias.index',
            'description'   =>  'Consulta de todos las agencias del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Agencias',
            'slug'  =>  'agencias.edit',
            'description'   =>  'Edición de todos las agencias del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Agencias',
            'slug'  =>  'agencias.create',
            'description'   =>  'Creación de agencias para el sistema.',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Agencias',
            'slug'  =>  'agencias.delete',
            'description'   =>  'Da de baja a las agencias del sistema.',
        ]);
        //Permisos para la tabla categorias
        Permission::create([
        	'name'	=>	'Navegar Categoría',
        	'slug'	=>	'categorias.index',
        	'description'	=>	'Consulta de todas las categorías del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Categoría',
        	'slug'	=>	'categorias.edit',
        	'description'	=>	'Edición de todas las categorías del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Categoría',
        	'slug'	=>	'categorias.create',
        	'description'	=>	'Creación de categorías para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Eliminar Categoría',
        	'slug'	=>	'categorias.delete',
        	'description'	=>	'Da de baja a las agencias del sistema.',
        ]);

        //Permisos para la tabla clientes
        Permission::create([
        	'name'	=>	'Navegar Cliente',
        	'slug'	=>	'clientes.index',
        	'description'	=>	'Consulta de todos los clientes del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Cliente',
        	'slug'	=>	'clientes.edit',
        	'description'	=>	'Edición de todos los clientes del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Cliente',
        	'slug'	=>	'clientes.create',
        	'description'	=>	'Creación de clientes para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Eliminar Cliente',
        	'slug'	=>	'clientes.delete',
        	'description'	=>	'Da de baja a los clientes del sistema.',
        ]);

        //Permisos para la tabla impuestos
        Permission::create([
        	'name'	=>	'Navegar Impuesto',
        	'slug'	=>	'impuestos.index',
        	'description'	=>	'Consulta de todos los impuestos del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Impuesto',
        	'slug'	=>	'impuestos.edit',
        	'description'	=>	'Edición de todos los impuestos del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Impuesto',
        	'slug'	=>	'impuestos.create',
        	'description'	=>	'Creación de impuestos para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Eliminar Impuesto',
        	'slug'	=>	'impuestos.delete',
        	'description'	=>	'Da de baja a los impuestos del sistema.',
        ]);

        //Permisos para la tabla productos
        Permission::create([
        	'name'	=>	'Navegar Producto',
        	'slug'	=>	'productos.index',
        	'description'	=>	'Consulta de todos los productos del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Editar Producto',
        	'slug'	=>	'productos.edit',
        	'description'	=>	'Edición de todos los productos del sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Crear Producto',
        	'slug'	=>	'productos.create',
        	'description'	=>	'Creación de productos para el sistema.',
        ]);
        Permission::create([
        	'name'	=>	'Eliminar Producto',
        	'slug'	=>	'productos.delete',
        	'description'	=>	'Da de baja a los productos del sistema',
        ]);

        //Permisos para la tabla unidades
        Permission::create([
            'name'  =>  'Navegar Unidad',
            'slug'  =>  'unidades.index',
            'description'   =>  'Consulta de todos las unidades del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Unidad',
            'slug'  =>  'unidades.edit',
            'description'   =>  'Edición de todos las unidades del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Unidad',
            'slug'  =>  'unidades.create',
            'description'   =>  'Creación de pruductos para el sistema.',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Unidad',
            'slug'  =>  'unidades.delete',
            'description'   =>  'Da de baja a las unidades del sistema',
        ]);

        //Permisos para la tabla tipopagos
        Permission::create([
            'name'  =>  'Navegar Tipo de Pago',
            'slug'  =>  'tipopagos.index',
            'description'   =>  'Consulta de todos los tipo de pagos del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Tipo de Pago',
            'slug'  =>  'tipopagos.edit',
            'description'   =>  'Edición de todos los tipo de pagos del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Tipo de Pago',
            'slug'  =>  'tipopagos.create',
            'description'   =>  'Creación de tipos de pagos para el sistema.',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Tipo de Pago',
            'slug'  =>  'tipopagos.delete',
            'description'   =>  'Da de baja a los tipo de pagos del sistema',
        ]);

        //Permisos para la tabla datofactura
        Permission::create([
            'name'  =>  'Navegar Datos de Facturación',
            'slug'  =>  'datofacturas.index',
            'description'   =>  'Consulta de todos los datos de facturación del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Datos de Facturación',
            'slug'  =>  'datofacturas.edit',
            'description'   =>  'Edición de todos los datos de facturación del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Datos de Facturación',
            'slug'  =>  'datofacturas.create',
            'description'   =>  'Creación de datos de facturación para el sistema.',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Datos de Facturación',
            'slug'  =>  'datofacturas.delete',
            'description'   =>  'Da de baja a los datos de facturación del sistema',
        ]);

        //Permisos para la factura
        Permission::create([
            'name'  =>  'Navegar Facturas',
            'slug'  =>  'facturas.index',
            'description'   =>  'Consulta de todos las facturas del sistema.',
        ]);

        Permission::create([
            'name'  =>  'Ver Facturas',
            'slug'  =>  'facturas.show',
            'description'   =>  'Imprime y visualiza de todos las facturas del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Facturas',
            'slug'  =>  'facturas.edit',
            'description'   =>  'Edición de todas las facturas del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Facturas',
            'slug'  =>  'facturas.create',
            'description'   =>  'Creación de facturas.',
        ]);
        Permission::create([
            'name'  =>  'Anular Facturas',
            'slug'  =>  'facturas.delete',
            'description'   =>  'Anulación de facturas',
        ]);
        Permission::create([
            'name'  =>  'Editar detalle de Facturación',
            'slug'  =>  'facturas.edit.detalle',
            'description'   =>  'Permite editar el detalle antes de elaborar la factura',
        ]);
        Permission::create([
            'name'  =>  'Editar precio de Facturación',
            'slug'  =>  'facturas.edit.precio',
            'description'   =>  'Permite editar el precio antes de elaborar la factura',
        ]);

        //Permisos para la tabla logs
        Permission::create([
            'name'  =>  'Navegar Actividad de Usuarios',
            'slug'  =>  'logs.index',
            'description'   =>  'Consulta de la actividad de los usuarios del sistema.',
        ]);

        //Permisos para la tabla cajas
        Permission::create([
            'name'  =>  'Navegar Caja',
            'slug'  =>  'cajas.index',
            'description'   =>  'Consulta las cajas del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Caja',
            'slug'  =>  'cajas.edit',
            'description'   =>  'Edición de las cajas del sistema (Necesario para Facturar)',
        ]);
        Permission::create([
            'name'  =>  'Crear Caja',
            'slug'  =>  'cajas.create',
            'description'   =>  'Creación de cajas para el sistema (Necesario para Facturar).',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Caja',
            'slug'  =>  'cajas.delete',
            'description'   =>  'Da de baja a las cajas del sistema.',
        ]);

        //Permisos para la tabla flujos
        Permission::create([
            'name'  =>  'Navegar Flujo de Caja',
            'slug'  =>  'flujos.index',
            'description'   =>  'Consulta los flujos de caja.',
        ]);
        Permission::create([
            'name'  =>  'Crear Flujo de Caja',
            'slug'  =>  'flujos.create',
            'description'   =>  'Creación de flujos de caja.',
        ]);


        /*SEGUNDA PARTE*/

        //Permisos para Proveedores
        Permission::create([
            'name'  =>  'Navegar Proveedores',
            'slug'  =>  'proveedores.index',
            'description'   =>  'Consulta de todos las proveedores del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Proveedores',
            'slug'  =>  'proveedores.edit',
            'description'   =>  'Edición de todas las proveedores del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Proveedores',
            'slug'  =>  'proveedores.create',
            'description'   =>  'Creación de proveedores.',
        ]);
        Permission::create([
            'name'  =>  'Anular Proveedores',
            'slug'  =>  'proveedores.delete',
            'description'   =>  'Anulación de proveedores',
        ]);
        Permission::create([
            'name'  =>  'Mostrar Proveedores',
            'slug'  =>  'proveedores.show',
            'description'   =>  'Consultar la información de proveedores',
        ]);

        //Permisos para Declaración de Compras
        Permission::create([
            'name'  =>  'Navegar Declaración de Compras',
            'slug'  =>  'declaracioncompras.index',
            'description'   =>  'Consulta de todos las declaraciones de compras del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Declaración de Compras',
            'slug'  =>  'declaracioncompras.edit',
            'description'   =>  'Edición de todas las declaraciones de compras del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Declaración de Compras',
            'slug'  =>  'declaracioncompras.create',
            'description'   =>  'Creación de declaraciones de compras.',
        ]);
        Permission::create([
            'name'  =>  'Anular Declaración de Compras',
            'slug'  =>  'declaracioncompras.delete',
            'description'   =>  'Anulación de declaraciones de compras',
        ]);
        Permission::create([
            'name'  =>  'Mostrar Declaración de Compras',
            'slug'  =>  'declaracioncompras.show',
            'description'   =>  'Consultar la información de declaraciones de compras',
        ]);

        //Permisos para CXC
        Permission::create([
            'name'  =>  'Navegar CxC',
            'slug'  =>  'cxcs.index',
            'description'   =>  'Consulta de todos las CxC del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar CxC',
            'slug'  =>  'cxcs.edit',
            'description'   =>  'Edición de todas las CxC del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear CxC',
            'slug'  =>  'cxcs.create',
            'description'   =>  'Creación de CxC.',
        ]);
        Permission::create([
            'name'  =>  'Anular CxC',
            'slug'  =>  'cxcs.delete',
            'description'   =>  'Anulación de CxC',
        ]);
        Permission::create([
            'name'  =>  'Mostrar CxC',
            'slug'  =>  'cxcs.show',
            'description'   =>  'Consultar la información de CxC',
        ]);

        //Permisos para Descuento
        Permission::create([
            'name'  =>  'Navegar Descuento',
            'slug'  =>  'descuentos.index',
            'description'   =>  'Consulta de todos las CxC del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar Descuento',
            'slug'  =>  'descuentos.edit',
            'description'   =>  'Edición de todas las CxC del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Crear Descuento',
            'slug'  =>  'descuentos.create',
            'description'   =>  'Creación de CxC.',
        ]);
        Permission::create([
            'name'  =>  'Acti y Desact Descuento',
            'slug'  =>  'descuentos.delete',
            'description'   =>  'Activación de Desactivació de descuento.',
        ]);

        //Permisos para Reportes
        Permission::create([
            'name'  =>  'Reporte Declaraciones',
            'slug'  =>  'rep.declaraciones',
            'description'   =>  'Generar reporte declaración de compras y ventas',
        ]);
        Permission::create([
            'name'  =>  'Reporte de Facturas Excel',
            'slug'  =>  'rep.facturasexcel',
            'description'   =>  'Generar reporte de facturas en excel',
        ]);
        Permission::create([
            'name'  =>  'Reporte de Flujos Excel',
            'slug'  =>  'rep.flujosexcel',
            'description'   =>  'Generar reporte de flujos en excel',
        ]);


        /*TERCERA PARTE*/

        //Permisos para CxP
        Permission::create([
            'name'  =>  'Navegar CxP',
            'slug'  =>  'cxps.index',
            'description'   =>  'Consulta de todos las cxp del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Editar CxP',
            'slug'  =>  'cxps.edit',
            'description'   =>  'Edición de todas las cxp del sistema.',
        ]);

        Permission::create([
            'name'  =>  'Mostrar CxP',
            'slug'  =>  'cxps.show',
            'description'   =>  'Consultar la información de cxp',
        ]);

        //Permisos para Suscripcion
        Permission::create([
            'name'  =>  'Ver y Editar Suscripción',
            'slug'  =>  'suscripcion.edit',
            'description'   =>  'Ver, editar y cancelar la suscripción.',
        ]);


        /*CUARTA PARTE*/

        //Permisos para Cotizaciones
        Permission::create([
            'name'  =>  'Navegar las Cotizaciones',
            'slug'  =>  'cotizaciones.index',
            'description'   =>  'Consulta de todos las cotizaciones del sistema.',
        ]);
        Permission::create([
            'name'  =>  'Eliminar Cotizaciones',
            'slug'  =>  'cotizaciones.delete',
            'description'   =>  'Edición de todas las cotizaciones del sistema.',
        ]);

        Permission::create([
            'name'  =>  'Re imprimir Cotizaciones',
            'slug'  =>  'cotizaciones.show',
            'description'   =>  'Re imprimir cotizaciones ya creadas.',
        ]);

    }
}
