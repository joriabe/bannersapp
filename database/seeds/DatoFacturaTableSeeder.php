<?php

use Illuminate\Database\Seeder;
use \App\DatoFactura;

class DatoFacturaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Datos de Facturacion
        DatoFactura::create([
        	'correlativof'		=>	'51',
        	'correlativo'		=>	'1',
        	'kai'				=>	'B11D76-B43F6E-1D4C88-842093-225385-FA',
            'ragosuperior'      =>  '000-001-01-00000150',
            'rangoinferior'		=>	'000-001-01-00000051',
        	'fechalimite'		=>	'2018-04-30',
        	'frase'				=>	'La factura es beneficio de todos, exijala',
            'logo'              =>  'storage/sinlogo.png',
            'agencia_id'        =>  '1',
        ]);
    }
}
