<?php

use Illuminate\Database\Seeder;
use \App\Unidad;
use \App\TipoImpositivo;

class UnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unidad::create([
        	'unidad'	=>	'Pieza',
        	'acronimo'	=>	'Pza',
        	'cia_id'	=>	'1',
        ]);
    }
}
