<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /** 
     * Seed the application's database.
     * php artisan db:seed
     * @return void
     */
    public function run()
    {

        //seeders
        $this->call(Cia_AgenciaSeeder::class);
        $this->call(PermisosTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(User_RolSeeder::class);
        $this->call(TipoMovimientoTableSeeder::class);


        //factories
        $this->call(UnidadSeeder::class);        
        $this->call(CategoriaTableSeeder::class); 
        $this->call(ClientesTableSeeder::class);
        $this->call(DatoFacturaTableSeeder::class);
        $this->call(ProveedorSeeder::class);


    }
}
