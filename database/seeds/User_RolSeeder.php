<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\RoleUser;

class User_RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuario
        User::create([
        	'name'				=>	'admin',
        	'email'				=>	'admin@admin.com',
        	'password'			=>	Hash::make('vDmu5k6J'),
        	'agencia_id'		=>	'1',
        ]);

        //Asignar Rol al Usuario
        RoleUser::create([
            'role_id'           =>  '1',
            'user_id'           =>  '1',
        ]);
    }
}
