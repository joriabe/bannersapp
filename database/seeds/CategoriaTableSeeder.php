<?php

use Illuminate\Database\Seeder;
use \App\Categoria;
use \App\ProductoServicio;


class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Categoria::class, 8)->create()->each(function ($u) {
        	for ($i=0; $i < 10 ; $i++) { 
        		$u->productoservicios()->save(factory(App\ProductoServicio::class)->make());
        	}  	

    	});
    }
}
