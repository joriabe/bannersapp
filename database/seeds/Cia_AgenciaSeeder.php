<?php

use Illuminate\Database\Seeder;
use \App\Cia;
use \App\Agencia;
use \App\TipoPago;
use \App\Descuento;
use \App\Cliente;
use \App\TipoImpositivo;

class Cia_AgenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Cia
        Cia::create([
        	'rtn'				=>	'101011990007169',
        	'nombrecomercial'	=>	'SGF Pyme',
        	'razonsocial'		=>	'Sistema de Gestion de Facturas Pyme S.A.',
            'logo'              =>  'logo',
        ]);

        //Agencia
        Agencia::create([
            'nombrecomercial'   =>  'SGF Pyme',
        	'descripcion'		=>	'Agencia principal',
            'direccion'         =>  'Res. El Toronjal #2, Calle 21, Plaza Kamila, Local N6',
            'telefono'          =>  '98601139',
            'correo'            =>  'eduar.jzr@gmail.com',
        	'codigointerno'		=>	'1',
        	'cia_id'			=>	'1',
        ]);

        //tipo pago
        TipoPago::create([
            'tipopago'          =>  'Efectivo',
            'cia_id'            =>  '1',
        ]);

        //descuento
        Descuento::create([
            'fecha_inicio'      =>  '2000-01-01 00:00:00',
            'fecha_fin'         =>  '3000-01-01 00:00:00',
            'descripcion'       =>  'Sin Descuento',
            'descuento'         =>  '0',
            'cia_id'            =>  '1',
        ]);

        //cliente
        Cliente::create([
            'codinterno'        => '1001001',
            'rtn'               => '---',
            'nombrecliente'     => 'Consumidor Final',
            'razonsocialcliente'=> 'N/A',
            'direccion'         => 'N/A',
            'telefono'          => 'N/A',
            'correo'            => 'N/A',
            'cia_id'            => '1',
        ]);

        //Impuesto
        TipoImpositivo::create([
            'descripcion'       => 'ISV 15%',
            'impuesto'          => '15',
            'cia_id'            => '1',
        ]);

        TipoImpositivo::create([
            'descripcion'       => 'Exento',
            'impuesto'          => '0',
            'cia_id'            => '1',
        ]);
    }
}
