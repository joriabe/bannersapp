<?php

use Faker\Generator as Faker;

$factory->define(App\Categoria::class, function (Faker $faker) {
    return [
        'categoria' => 'Cat-' . $faker->state,
        'acronimo' => 'C-' . $faker->stateAbbr,
        'cia_id' => 1,
    ];
});
