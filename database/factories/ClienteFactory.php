<?php

use Faker\Generator as Faker;

$factory->define(App\Cliente::class, function (Faker $faker) {
    return [
        'codinterno' => $faker->numberBetween($min = 1000, $max = 9000),
        'rtn' => strtoupper($faker->randomLetter) . $faker->ean8,
        'nombrecliente' => $faker->name,
        'razonsocialcliente' => $faker->company,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'correo' => $faker->safeEmail,
        'cia_id' => 1,
    ];
});
