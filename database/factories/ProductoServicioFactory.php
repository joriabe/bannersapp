<?php

use Faker\Generator as Faker;

$factory->define(App\ProductoServicio::class, function (Faker $faker) {
    return [
        'codinterno' => $faker->ean13,
        'descripcion' => 'Pro-'.$faker->city,
        'precio' => $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 1000), // 48.8932,
        'prodoctooservicio' => 1,
        'unidad_id' => 1,
        'tipoimpositivos_id' => 1,
        'cia_id' => 1,

    ];
});
