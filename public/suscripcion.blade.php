@extends('layouts.dashboard')

@section('content')
    <!-- Determina si hay un msj del resultado de las operaciones para ser mostrado en pantalla -->
    @include('alerts.succes')

    @include('complementos.paypal2')

@endsection
