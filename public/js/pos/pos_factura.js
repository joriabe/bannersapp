//variables globales para teclado 
var elem_glob = 0;
var edit_glo = 0;
var cliente_glob = 0;

function cargar_item_factura(id, url){	

	//AJAX
	$.get(url+'_fac/'+id, function (data){

		//comprobamos si el producto ya se encuentra creado
		var pro_exist = document.getElementById("pro-"+data[0].p_id+"-0"); 
		//variable a ser usada como extra id
		var i;
		//al haber varias veces un producto, le crea un nuevo extra id
		for (i = 0; pro_exist !== null ; i++) {
	        var a = i + 1;
			pro_exist = document.getElementById("pro-"+data[0].p_id+"-"+a); 
		}
		//escribimos la descripcion
		var descripcion_pro = "";
		if (data[0].descuento > 0) { descripcion_pro = data[0].descripcion + " - D. "+ data[0].descuento + "%";  } else {descripcion_pro = data[0].descripcion;} 
		//creamos la tabal el contenido
		var html_producto = "";
		var html_producto = '<table style="margin-bottom: 2px;" id="pro-'+data[0].p_id+'-'+i+'" class="w-100 table table-bordered table-hover table-sm"><tr class="table-secondary"><td id="idp" hidden>'+data[0].p_id+'</td><td id="des" contenteditable class="w-75" colspan="5">'+descripcion_pro +'</td><td id="tot" rowspan="2">'+data[0].precio+'</td><td rowspan="2"><button type="button" class="close" aria-label="Close" onclick="eliminarPro(&#39pro-'+data[0].p_id+'-'+i+'&#39);"><span aria-hidden="true">&times;</span></button></td></tr><tr><td id="imp" hidden>'+data[0].impuesto+'</td><td id="dsto" hidden>'+data[0].descuento+'</td><td>Cant.</td><td id="can" contenteditable onkeyup="totalpro(&#39pro-'+data[0].p_id+'-'+i+'&#39)" onkeypress="return isNumberKey(event)" onfocusout="dosdecimales(this)" onclick="seleccionar(this)">1.00</td><td>'+data[0].unidad+'</td><td>Precio:</td><td id="pre" contenteditable onkeyup="totalpro(&#39pro-'+data[0].p_id+'-'+i+'&#39)" onkeypress="return isNumberKey(event)" onfocusout="dosdecimales(this)" onclick="seleccionar(this)">'+data[0].precio+'</td></tr></table>';
		//añadimos el nuevo producto a la pantalla
		document.getElementById("facturas").innerHTML += html_producto;
		//calculamos los totales de la factura
		total_factura();

		//ponemos el focus en la cantidad
		var cant_prod = document.getElementById('pro-'+data[0].p_id+'-'+i).querySelector("#can");
		seleccionar(cant_prod);

		//despues de agregar el producto, mueve la barra hacia abajo.
		$("#f_cont").scrollTop($("#f_cont")[0].scrollHeight);		
	});
}
//funcion para cargar el modal
function cargarmodal(url,tf){
	//vefiricamos el tipo de factura
	document.getElementById("tfactura").value = tf;

	//llenamos el primer farrafo del modal con esta funcion
	var exist_prod = total_factura();
	
	//llenamos los clientes
	cargar_clientes(url);
	//llenamos segun sea al credito o al contado
	credito_contado(url);

	if (exist_prod) {
		$('#envioModal').modal('show');
	}else{
		alert('No Hay Productos Para Facturar');
	}

}
//funcion para cargar los clientes
function cargar_clientes(url){
	//llamamos a la ruta clientes del DigitadorController
	
	$.get(url+'/clientes', function (data){
		//Llenamos el dropdown de clientes
		var html_cliente = "";
		for (i = 0; i < data.length; i++) {

			if (parseInt(data[i].id, 10) == cliente_glob) {
				html_cliente += '<option selected="selected" value="'+data[i].id+'">'+data[i].nombrecliente+'</option>';
			}else {
				
				html_cliente += '<option value="'+data[i].id+'">'+data[i].nombrecliente+'</option>';
			}		
		}
		document.getElementById("clientes_select").innerHTML = html_cliente;
		//añadimos el buscador
		$(".js-example-basic-single").select2({
			tags: true,
	        dropdownParent: $('#envioModal')	        
	    });	

	});

}

//funcion para cargar opciones de credito o contado
function credito_contado(url){

	if (document.getElementById("contado").checked == true )
	{
		//llamamos a la ruta tipopago del DigitadorController
		$.get(url+'/tipopago', function (data){
			//deshabiltamos el boton enviar factura, por si se habia activado
			document.getElementById("btn_enviar").disabled = true;
			document.getElementById("btn_coti").disabled = true;
			//Creamos la tabla de tipo de pagos
			var html_tipopago = '<div id="tipospagos" class="col-md-8">';
			html_tipopago += '<table id="tbl_tp" class="table table-bordered table-sm"><tr><th>Tipo Pago</th><th>Cantidad</th><th>Comprobante</th></tr>';
			for (i = 0; i < data.length; i++) {
			html_tipopago += '<tr id="'+data[i].id+'"><td>'+data[i].tipopago+'</td><td id="cant_tp" contenteditable onkeyup="total_tp()" onkeypress="return isNumberKey(event)" onfocusout="dosdecimales(this)" onclick="seleccionar(this)">0</td><td id="ref_tp" contenteditable></td></tr>';
			}
			html_tipopago += '<tr><th>Total</th><td id="total_tp" colspan="2">0</td></tr></table></div>';

			document.getElementById("c_o_c").innerHTML = html_tipopago;
			document.getElementById("c_o_c").className = "row justify-content-center";

		});	
	} else if (document.getElementById("credito").checked == true ) 
	{
		//habiltamos el boton enviar factura, por si se habia desactivado
		document.getElementById("btn_enviar").disabled = false;
		document.getElementById("btn_coti").disabled = true;

		var html_fecha = '<div class="col-md-6 form-group"><label for="fecha_vencimiento">Fecha de Vencimiento</label><input type="date" class="form-control" id="fecha_vencimiento" ></div><br>';
		document.getElementById("c_o_c").innerHTML = html_fecha;
		document.getElementById("c_o_c").className = "row justify-content-start";
		document.getElementById("fecha_vencimiento").value = fecha_vencimiento();

	}
	else if (document.getElementById("cotizacion").checked == true ) 
	{
		//deshabiltamos el boton enviar factura, por si se habia activado
		document.getElementById("btn_enviar").disabled = true;
		document.getElementById("btn_coti").disabled = false;

		var html_fecha = '<div class="col-md-6 form-group"><label for="fecha_vencimiento">Fecha de Vencimiento</label><input type="date" class="form-control" id="fecha_vencimiento" ></div><div class="col-md-6 form-group"><label for="observaciones_ta">Observaciones</label><textarea class="form-control" id="observaciones_ta" name="observaciones_ta" rows="3">ESTA COTIZACION ES VALIDA HASTA LA FECHA DE VENCIMIENTO</textarea><br>';
		document.getElementById("c_o_c").innerHTML = html_fecha;
		document.getElementById("c_o_c").className = "row justify-content-start";
		document.getElementById("fecha_vencimiento").value = fecha_vencimiento();

	}

	

}
//funcion para añadir 30 dias a la fecha actual,
//para factura sal credito
function fecha_vencimiento(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	//si el dia es un 31 de mes, lo pasa a 30, y le suma un mes
	if (dd==31){
		dd=30;
		mm=mm+1;
	}
	//si el dia es menor o igual a 30, solo le suma un mes
	if (dd<=30){
		mm=mm+1;
	}
	//si el mes es enero, lo pasa a 28 de febrero
	if (mm==1){
		dd=28;
		mm=mm+1;
	}
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	}
	//formato estandar de chrome 
	var today = yyyy+'-'+mm+'-'+dd;

	return today;
}
//funcion para calcular el total d elos tipos de pago
function total_tp() {
	//optenemos las filas que componen la tabla
	var items = document.getElementById("tbl_tp").rows;
	//cada bves que se llama a la funcion se suman todos los tp
	suma_cant = 0;
	//como no queremos tomar en cuenta la ultima fila. -1
	var loop = parseInt(items.length) - 1;

	//como no queremos tomar en cuanta la primera, i=1
	for (i = 1; i < loop; i++) {
		//tomamos el valor de la segunda celda de la fila,  
		var cant = items[i].cells[1].textContent;
		//sumamos el valor
		suma_cant += parseFloat(cant);
	}

	//obtenemos el datos de la factura
    var t_fact = document.getElementById("tfactura").value;
    //obtenemos el total de la factura
    if (t_fact == 1) {
    	totfactura = document.getElementById("v_gtotal").innerHTML;
    } else {
    	totfactura = document.getElementById("total_modal").innerHTML;
    }
	
	
	//actualizamos el total en la celda TOTAL
	if (parseFloat(suma_cant) >= parseFloat(totfactura)) {
		document.getElementById("total_tp").innerHTML = roundToTwo(suma_cant).toFixed(2);
		document.getElementById("total_tp").className = "table-success";

		document.getElementById("btn_enviar").disabled = false;
	}else{
		document.getElementById("total_tp").innerHTML = roundToTwo(suma_cant).toFixed(2);
		document.getElementById("total_tp").className = "table-danger";

		document.getElementById("btn_enviar").disabled = true;
	}
	
}

//funcion para eliminar productos
function eliminarPro(producto){

	var tab = document.getElementById("facturas");
	var tab_child = document.getElementById(producto);
	tab.removeChild(tab_child);

	total_factura();

}

function enviarfactura(purl){

	//creamos los arrar a nesecitar
	var array_factura = [];
	var array_productos = [];
	var array_tipo_pago = []; 

	//obtenemos los productos que forman la factura
	var pro_items = document.getElementById("facturas").children;	
	//los recorremos uno por uno 
	for (i = 0; i < pro_items.length; i++) {      
      var idp = pro_items[i].querySelector("#idp").textContent;
      var des = pro_items[i].querySelector("#des").textContent;
      var can = pro_items[i].querySelector("#can").textContent;
      var pre = pro_items[i].querySelector("#pre").textContent;
      var sub = roundToTwo(parseFloat(can) * parseFloat(pre));
      //aqui llenaremos el array
      var producto = {producto_servicio_id:idp, descripcion:des, cantidad:can, precio:pre, subtotal:sub};
      //añadimos el arreglo producto al de factura
      array_productos.push(producto);      
    }

    //obtenemos el id del cliente
	var x = document.getElementById("clientes_select").selectedIndex;
    var y = document.getElementById("clientes_select").options;
    var cliente_id = y[x].value;

    //obtenemos el datos de la factura
    var t_fact = document.getElementById("tfactura").value;

    //verificamos si es de contado o no
    if (document.getElementById("contado").checked == true )
	{ 	
		//obtenemos los elementos que forman los tipos de pago
		var tp_items = document.getElementById("tbl_tp").rows;
		//como no queremos tomar en cuenta la ultima fila. -1
		var loop = parseInt(tp_items.length) - 1;
		//como no queremos tomar en cuanta la primera, i=1
		for (i = 1; i < loop; i++) {
			var id_tp = tp_items[i].id;
			var can_tp = tp_items[i].cells[1].textContent;
			var ref_tp = tp_items[i].cells[2].textContent;
			//aqui llenaremos el array
		    var tipos_pago = {tipopago_id:id_tp, valor_pagado:can_tp, comprobantepago:ref_tp};
		    //añadimos el arreglo tipos-pago al de factura
		    array_tipo_pago.push(tipos_pago);			
		}
		array_factura[0] = 1;
		array_factura[1] = cliente_id;
		array_factura[2] = t_fact;
		array_factura[3] = array_tipo_pago;
		array_factura[4] = array_productos;

	}else{
		// obtenemos la fecha de vencimiento
		var f_vencimiento = document.getElementById("fecha_vencimiento").value;

		array_factura[0] = 2;
		array_factura[1] = cliente_id;
		array_factura[2] = t_fact;
		array_factura[3] = f_vencimiento;
		array_factura[4] = array_productos;
	}
	//alertObject(array_factura);
    //por laravel, tenemos que agregar el token para el post
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //iniciamos el post de ajax
    $.ajax({
            url: purl,
            type: "post",
            contentType: 'application/json; charset=utf-8',
            processData: false,
            //que hacer si todo sale bien
            success: function (data) {
                muetrafactura(data, purl);
            },
            //convertimos el array JS --> JSON
            data: JSON.stringify(array_factura),
            
     });
    

}
//funcion para actualizar el total del producto luego de un cambio
//en cantidad o precio/unidad
function totalpro(t_id){

	var tabla = document.getElementById(t_id);
	//obtenemos los datos
	var cant = tabla.querySelector("#can").textContent;
	var prec = tabla.querySelector("#pre").textContent;
	
	

	var total = parseFloat(cant) * parseFloat(prec);

	if (isNaN(total)) {
		tabla.querySelector("#tot").textContent = "¡Error!";
		tabla.querySelector("#tot").className =	"table-danger";
	}else{
		tabla.querySelector("#tot").textContent = roundToTwo(total).toFixed(2);
		tabla.querySelector("#tot").className = "";

		total_factura();
	}

	
}
//funcion para saber cuantos productos lleva y el total de la factura
//y llenar el modal con eso datos
function total_factura(){

	//obtenemos los elemnetos que forman la factura
	var items = document.getElementById("facturas").children;
	//si no hay productos seleccionados
	if (items.length == 0) {
		return false;
	}

	//iniciamos las variables que guardaran los totales
	var subtotal_factura = 0;
	var subtotal_impuestos = 0;
	var gtotal_factura = 0;
	var descuento_factura = 0;

	//obtenemos el datos de la factura
    var t_fact = document.getElementById("tfactura").value;

	//los recorremos uno por uno 
	for (i = 0; i < items.length; i++) {
      var can = items[i].querySelector("#can").textContent;
      var pre = items[i].querySelector("#pre").textContent;
      var imp = items[i].querySelector("#imp").textContent;
      var dto = items[i].querySelector("#dsto").textContent;

      var total = parseFloat(can) * parseFloat(pre);
      var descuento = total * (parseFloat(dto)/100);
      var impuesto = (total - descuento ) * (parseFloat(imp) / 100);
      var total_desimp = total - descuento + impuesto;

      subtotal_factura += total;
      descuento_factura += descuento;
      subtotal_impuestos += impuesto;
      gtotal_factura += total_desimp;
      
    }
   	//llenamos la pantalla con los totales de la factura
    //document.getElementById("items").innerHTML = total_item;
    document.getElementById("v_total").innerHTML = roundToTwo(subtotal_factura).toFixed(2);
    document.getElementById("v_impuestos").innerHTML = roundToTwo(subtotal_impuestos).toFixed(2);
    document.getElementById("v_gtotal").innerHTML = roundToTwo(gtotal_factura).toFixed(2);
    document.getElementById("v_desto").innerHTML = roundToTwo(descuento_factura).toFixed(2);

    if (t_fact == 1) {
    	document.getElementById("total_modal").innerHTML = roundToTwo(gtotal_factura).toFixed(2);
    } else {
    	document.getElementById("total_modal").innerHTML = roundToTwo(subtotal_factura - descuento_factura).toFixed(2);
    }    

    return true;
}
//funcion para redondear a dos decimales
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}
//funcion para evitar que se ingresen caracteres que no son numericos 
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	
	//aceptar el punto
	if (charCode == 46){
		return true;
	}
	//no aceptar enter
	if (charCode == 13) {
		//limpiamos el codigo de barras
		$( "#cod_barra" ).val("");
		$( "#cod_barra" ).focus();
		return false;
	}

	//No aceptar caracteres alfabeticos
	if (charCode > 31 && (charCode < 48 || charCode > 57)){
	    return false;
	}
	return true;
}
//funcion para agregar dos unidades decimales a la cantidad y el precio
function dosdecimales(thi) {
	var num = roundToTwo(thi.textContent).toFixed(2);
	thi.textContent = num;
	return true;
}

//funcion para seleccinar todo el contenido, al hacer clic en una celda de precio y cant
function seleccionar(thi) {
	//
	try {
		//redondeamos el elemneto que se acaba de modificar con teclado en pantalla
    	dosdecimales(elem_glob);
	}catch(err) {}	

	var range = document.createRange();
    range.selectNodeContents(thi);  
    var sel = window.getSelection(); 
    sel.removeAllRanges(); 
    sel.addRange(range);

    //asignamos 1 a la variable global para saber que se acaba de seleccionar
    elem_glob = thi;
    edit_glob = 1;
	return true;

}
// Alert javascript object in alert box
function alertObject(obj){      
    for(var key in obj) {
    alert('key: ' + key + '\n' + 'value: ' + obj[key]);
    if( typeof obj[key] === 'object' ) {
        alertObject(obj[key]);
    }
    }
}
// Funcion que muestra la factura
function muetrafactura(data, url){
	//oculta el modal de envio de factura
	$('#envioModal').modal('hide');
	
	//creamos el link para llamar a la factura
	var a = document.createElement("a");
	//funcion para que abra una nueva ventana
	a.onclick = function(){
		window.open(url+'/'+data.msg2,'newwindow','width=850, height=600'); 
		return false;	
	}; 
	a.target = "_blank"
	a.href = url+'/'+data.msg2;
	a.click();

	document.getElementById("cambiofactura").innerText = data.msg;	
	document.getElementById("btn_verfactura").href = url+'/'+data.msg2;
	document.getElementById("btn_verfactura").onclick = function(){
		window.open(url+'/'+data.msg2,'newwindow','width=850, height=600'); 
		return false;	
	}; 
	$('#cambioModal').modal('show');

}
// Funcion que escribe los digitos del teclado
function teclado_num(num){

	var campo = elem_glob;

	if (edit_glob == 0) {
		campo.innerHTML += num;
	}
	if (edit_glob == 1) {
		campo.innerHTML = num;
		edit_glob = 0;		
	}

	try	{
		//calculamos total despues de moficar con teclado en pantalla
		var row = campo.parentNode.parentNode;
    	var table_id = row.parentNode.id;
    	totalpro(table_id);
	}catch(err){}	

}  

// this is the id of the form
$("#clienteform").submit(function(e) {

	e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           dataType: 'json', //convierte la respuesta a javascrip
           success: function(data)
           {
           	   alert(data.msg); // show response from the php script.
               $('#clienteModal').modal('hide');

              
    		   html_cliente = '<option selected value="'+data.id+'">'+data.nombre+'</option>';
    		   document.getElementById("clientes_select").innerHTML += html_cliente;
           }
         });

    
    
});
//funcion para el escaner de codigo de barras
function lector_cbarras(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if(charCode == 13) {
		var p_cod = $( "#cod_barra" ).val();
		var p_url = document.getElementById("prod-url").innerText;


		//cargamos los productos
		cargar_item_factura(p_cod, p_url);

		//limpiamos el codigo de barras
		$( "#cod_barra" ).val("");
		$( "#cod_barra" ).focus();
	}
}
//detectamos las teclas
window.onkeypress = function(e) {	

	var charCode = (e.which) ? e.which : event.keyCode;
	var f_url = document.getElementById("factura-url").innerText;
	//factura pro-forma
  	if (charCode == 17) {
  		
  		e.preventDefault();

    	cargarmodal(f_url,2);
	}
}