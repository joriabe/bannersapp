

	function cargarProductos(id, nombre, url){
	var nombretab = nombre.replace(/ /gi, "_");

	var tab_exist = document.getElementById("tab-"+id); 

	//comprobamos si el tab-categoria ya se encuentra creado
	if (tab_exist !== null) {
		//se llama al que se encuentra creado
		$('#myTab a[href="#'+nombretab+'"]').tab('show');
	} else {
	//se crea uno nuevo de no existir	
	document.getElementById("myTab").innerHTML += '<li class="nav-item" id="tab-'+id+'"><a class="nav-link" id="'+nombretab+'-tab" data-toggle="tab" href="#'+nombretab+'" role="tab" aria-controls="'+nombretab+'" aria-selected="false">'+nombre+'<button type="button" class="close btnx-tab" aria-label="Close" onclick="eliminarTab(&#39'+nombretab+'&#39,'+id+');"><span aria-hidden="true">&times;</span></button></a></li>';

	//AJAX
		$.get(url+'/'+id, function (data){
			var html_productos = "";
			var html_productos = '<div class="tab-pane fade show" id="'+nombretab+'" role="tabpanel" aria-labelledby="'+nombretab+'-tab"><div class="row">';
			for (var i=0; i<data.length; ++i){
				html_productos += '<div class="col-md-6"><div style="margin-top: 5px"><a class="btn btn-outline-info btn-block" onclick="cargar_item_factura('+data[i].p_id+',&#39'+url+'&#39 );" role="button" title="'+data[i].descripcion+'">'+data[i].descripcion.substring(0, 13)+', V: '+data[i].precio+', U: '+data[i].unidad+'</a></div></div>'; 
			}
			html_productos += '</div></div>';

			var tab_content = $('#myTabContent').html() + html_productos;
			$('#myTabContent').html(tab_content);

			$('#myTab a[href="#'+nombretab+'"]').tab('show');
		});

		}

	}//fin de la funcion

	function eliminarTab(nombretab, id){
		
		var tab = document.getElementById("myTab");
		var tab_child = document.getElementById("tab-"+id);
		tab.removeChild(tab_child);


		var tab_content = document.getElementById("myTabContent");
		var tab_content_child = document.getElementById(nombretab);
		tab_content.removeChild(tab_content_child);


		$('#myTab a[href="#home"]').tab('show');
	}

	function buscarProducto(url){

		var producto_buscar = document.getElementById("i_buscar").value;
		
		var tab_exist = document.getElementById("tab-buscar"); 

		//comprobamos si el tab-categoria ya se encuentra creado
		if (tab_exist !== null) {
			//se llama al que se encuentra creado
			eliminarTab('busqueda','buscar');
		}
		
		//AJAX
		$.get(url+'/'+producto_buscar+'/edit', function (data){

			if (data.length == 0){
				alert("No se Encontraron Coincidencias");
			}else{
				//se crea el tab	
				document.getElementById("myTab").innerHTML += '<li class="nav-item" id="tab-buscar"><a class="nav-link" id="busqueda-tab" data-toggle="tab" href="#busqueda" role="tab" aria-controls="busqueda" aria-selected="false">Resultado<button type="button" class="close btnx-tab" aria-label="Close" onclick="eliminarTab(&#39busqueda&#39,&#39buscar&#39);"><span aria-hidden="true">&times;</span></button></a></li>';


				//creamos el contenido
				var html_productos = "";
				var html_productos = '<div class="tab-pane fade show" id="busqueda" role="tabpanel" aria-labelledby="busqueda-tab"><div class="row">';
				for (var i=0; i<data.length; ++i){
					html_productos += '<div class="col-md-6"><div style="margin-top: 5px"><a class="btn btn-outline-info btn-block" href="#" onclick="cargar_item_factura('+data[i].p_id+',&#39'+url+'&#39 ); return false;" role="button" title="'+data[i].descripcion+'">'+data[i].descripcion.substring(0, 13)+', V: '+data[i].precio+', U: '+data[i].unidad+'</a></div></div>'; 
				}
				html_productos += '</div></div>';

				var tab_content = $('#myTabContent').html() + html_productos;
				$('#myTabContent').html(tab_content);

				$('#myTab a[href="#busqueda"]').tab('show');
			}
		});
		
	}

