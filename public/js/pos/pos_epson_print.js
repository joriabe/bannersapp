
var printer = null;
var ePosDev = new epson.ePOSDevice();

function pruebaepson() {
	connect();
	createData();
	send();
}

function connect() {
	
	var ipAddress = '192.168.192.168';
	var port = '8008';
	ePosDev.connect(ipAddress, port, callback_connect);
}

function callback_connect(resultConnect){
	var deviceId = 'local_printer';
	var options = {'crypto' : false, 'buffer' : false};
	if ((resultConnect == 'OK') || (resultConnect == 'SSL_CONNECT_OK')) {
	//Retrieves the Printer object
	ePosDev.createDevice(deviceId, ePosDev.DEVICE_TYPE_PRINTER, options,
	callback_createDevice);
	}
	else {
	//Displays error messages
	}
}

function callback_createDevice(deviceObj, errorCode){
	if (deviceObj === null) {
	//Displays an error message if the system fails to retrieve the Printer object
	return;
	}
	printer = deviceObj;
	//Registers the print complete event
	printer.onreceive = function(response){
		if (response.success) {
		//Displays the successful print message
		}
		else {
		//Displays error messages
		}
	};
}

function createData(){
	
	printer.addTextAlign(ALIGN_CENTER);
	printer.addText('SGF Pyme\n');
	printer.addText('eduar.jzr@gmail.com\n');
	printer.addText('Res. El Toronjal #2, calle 21, Plaza Kamila, local N6\n');
	printer.addText('Tel: 9860-1138\n');
	printer.addTextAlign(ALIGN_LEFT);
	printer.addText('Cliente: Consumidor Final\n');
	printer.addText('RTN: ---\n');
	printer.addText('Direccion: N/A\n');
	printer.addText('Fecha: 23-10-2018 02:58:20 am\n');
	printer.addCut(CUT_FEED);
	
}

function send(){
	if (ePosDev.isConnected) {
		printer.send();
	}
}

//Discards the Printer object
ePosDev.deleteDevice(printer, callback_deleteDevice);
	function callback_deleteDevice(errorCode){
	//Terminates connection with device
	ePosDev.disconnect();
}