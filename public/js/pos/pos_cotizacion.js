function enviarcotizacion(purl){

	//creamos los arrar a nesecitar
	var array_factura = [];
	var array_productos = [];
	var array_tipo_pago = []; 

	//obtenemos los productos que forman la factura
	var pro_items = document.getElementById("facturas").children;	
	//los recorremos uno por uno 
	for (i = 0; i < pro_items.length; i++) {      
      var idp = pro_items[i].querySelector("#idp").textContent;
      var des = pro_items[i].querySelector("#des").textContent;
      var can = pro_items[i].querySelector("#can").textContent;
      var pre = pro_items[i].querySelector("#pre").textContent;
      var sub = roundToTwo(parseFloat(can) * parseFloat(pre));
      //aqui llenaremos el array
      var producto = {producto_servicio_id:idp, descripcion:des, cantidad:can, precio:pre, subtotal:sub};
      //añadimos el arreglo producto al de factura
      array_productos.push(producto);      
    }

    //obtenemos el id del cliente
	var x = document.getElementById("clientes_select").selectedIndex;
    var y = document.getElementById("clientes_select").options;
    var cliente_id = y[x].value;

    //obtenemos el datos de la factura
    var t_fact = document.getElementById("tfactura").value;
     
	// obtenemos la fecha de vencimiento
	var f_vencimiento = document.getElementById("fecha_vencimiento").value;

	//obtenemos las observaciones
	var observaciones = document.getElementById("observaciones_ta").value;



	array_factura[0] = 3;
	array_factura[1] = cliente_id;
	array_factura[2] = observaciones;
	array_factura[3] = f_vencimiento;
	array_factura[4] = array_productos;
	

	//alertObject(array_factura);
    //por laravel, tenemos que agregar el token para el post
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //iniciamos el post de ajax
    $.ajax({
            url: purl,
            type: "post",
            contentType: 'application/json; charset=utf-8',
            processData: false,
            //que hacer si todo sale bien
            success: function (data) {
                muetracotizacion(data, purl);
                //alert(data.msg2);
            },
            //convertimos el array JS --> JSON
            data: JSON.stringify(array_factura),
            
     });
    

}
// Funcion que muestra la cotizacion
function muetracotizacion(data, url){
	//oculta el modal de envio de factura
	$('#envioModal').modal('hide');
	
	//creamos el link para llamar a la factura
	var a = document.createElement("a");
	//funcion para que abra una nueva ventana
	a.onclick = function(){
		window.open(url+'/'+data.msg2,'newwindow','width=850, height=600'); 
		return false;	
	}; 
	a.target = "_blank"
	a.href = url+'/'+data.msg2;
	a.click();

	document.getElementById("cambiofactura").innerText = data.msg;	
	document.getElementById("btn_verfactura").href = url+'/'+data.msg2;
	document.getElementById("btn_verfactura").innerText = "Ver Cotizacion";
	document.getElementById("btn_verfactura").onclick = function(){
		window.open(url+'/'+data.msg2,'newwindow','width=850, height=600'); 
		return false;	
	}; 
	$('#cambioModal').modal('show');

}

//funcion para el escaner de codigo de barras
function lector_coti(evt){

	var charCode = (evt.which) ? evt.which : event.keyCode
	if(charCode == 13) {
		//obtenemos el numero de cotizacion, y la url del controlador
		var c_cod = $( "#cod_coti" ).val();
		var c_url = document.getElementById("coti-url").innerText;

		//borramos los productos que se encuentren
		document.getElementById("facturas").innerHTML = "";
		cliente_glob = 0;	

		//llenamos con los productos
		cargar_item_cotizacion(c_cod, c_url);

		//limpiamos el codigo de barras
		$( "#cod_coti" ).val("");
		$( "#cod_barra" ).focus();
	}
}

function cargar_item_cotizacion(id, url){	

	//AJAX
	$.get(url+'/'+id, function (data){		

		for (i = 0; i < data.length; i++) {

		//asignamos el cliente de la cotizacion
		cliente_glob = data[i].cliente_id; 
		//comprobamos si el producto ya se encuentra creado
		var pro_exist = document.getElementById("pro-"+data[i].p_id+"-0"); 
		//variable a ser usada como extra id
		var a;
		//al haber varias veces un producto, le crea un nuevo extra id
		for (x = 0; pro_exist !== null ; x++) {
	        var a = a + 1;
			pro_exist = document.getElementById("pro-"+data[i].p_id+"-"+a); 
		}
		//escribimos la descripcion
		var descripcion_pro = "";
		if (data[i].descuento > 0) { descripcion_pro = data[i].descripcion + " - D. "+ data[i].descuento + "%";  } else {descripcion_pro = data[i].descripcion;} 
		//creamos la tabal el contenido
		var html_producto = "";
		var html_producto = '<table style="margin-bottom: 2px;" id="pro-'+data[i].p_id+'-'+i+'" class="w-100 table table-bordered table-hover table-sm"><tr class="table-secondary"><td id="idp" hidden>'+data[i].p_id+'</td><td id="des" contenteditable class="w-75" colspan="5">'+descripcion_pro +'</td><td id="tot" rowspan="2">'+data[i].subtotal+'</td><td rowspan="2"><button type="button" class="close" aria-label="Close" onclick="eliminarPro(&#39pro-'+data[i].p_id+'-'+i+'&#39);"><span aria-hidden="true">&times;</span></button></td></tr><tr><td id="imp" hidden>'+data[i].impuesto+'</td><td id="dsto" hidden>'+data[i].descuento+'</td><td>Cant.</td><td id="can" contenteditable onkeyup="totalpro(&#39pro-'+data[i].p_id+'-'+i+'&#39)" onkeypress="return isNumberKey(event)" onfocusout="dosdecimales(this)" onclick="seleccionar(this)">'+data[i].cantidad+'</td><td>'+data[i].unidad+'</td><td>Precio:</td><td id="pre" contenteditable onkeyup="totalpro(&#39pro-'+data[i].p_id+'-'+i+'&#39)" onkeypress="return isNumberKey(event)" onfocusout="dosdecimales(this)" onclick="seleccionar(this)">'+data[i].precio+'</td></tr></table>';
		//añadimos el nuevo producto a la pantalla
		document.getElementById("facturas").innerHTML += html_producto;

		}

		//calculamos los totales de la factura
		total_factura();

		//llamamos el modal
		var f_url = document.getElementById("factura-url").innerText;
		cargarmodal(f_url,1);
		
	});
}
